#!/bin/bash

align="./align"
timidity=`which timidity`

if [ "x$timidity" == "x" ]; then
	echo "TiMidity++ is not installed. Unable to generate audio files."
	echo ""
fi

if [ ! -x $align ]; then
	echo "$align is missing. Exiting..."
	echo ""
	exit
fi

seed=1234

for file in "$@"; do
	output=`basename "$file" ".score.mid"`
	dir=`dirname "$file"`
	$align -g $seed $file -o "$dir/$output.warped.mid" -a "$dir/$output.warped.txt"
	seed=$(( $seed + 2 ))

	$align -g $seed $file -o "$dir/$output.skipped.mid" -a "$dir/$output.skipped.txt" -D 30000

	seed=$(( $seed + 3 ))

	if [ "x$timidity" != "x" ]; then
		$timidity -id -s 44100 -OvM "$dir/$output.warped.mid" -o "$dir/$output.warped.ogg"

		$timidity -id -s 44100 -OvM "$dir/$output.skipped.mid" -o "$dir/$output.skipped.ogg"
	fi
done

