#!/bin/bash
#
# A script that combines two music files (MIDI or audio) to evaluate
# score alignment.
#
# Dependencies:
# - TiMidity++
# - sox

if (( $# < 3 )); then
	echo ""
	echo "Usage: combine_to_vorbis <input1> <input2> <output>"
	echo ""
	echo "Where input files can be audio (WAV, MP3, Vorbis, AAC), or"
	echo "standard MIDI files."
	echo ""
	exit
fi

TIMIDITY=`which timidity`
MPLAYER=`which mplayer`
SOX=`which sox`
SOXMIX=`which soxmix`
OGGENC=`which oggenc`

if [ ! -x "$TIMIDITY" ]; then
	echo "Error: unable to execute TiMidity++ (timidity)"
	exit 1;
fi
if [ ! -x "$MPLAYER" ]; then
	echo "Error: unable to execute MPlayer (mplayer)"
	exit 1;
fi
if [ ! -x "$SOX" ]; then
	echo "Error: unable to execute SoX (sox or soxmix)"
	exit 1;
fi
if [ ! -x "$OGGENC" ]; then
	echo "Error: unable to execute oggenc"
	exit 1;
fi



function convert_input () {
	inputdesc=`file "$1"`
	if [[ "$inputdesc" =~ "MIDI" ]]; then
		$TIMIDITY -id -s 44100 -OwM "$1" -o "$2"
	else
		# $SOX -c 1 -r 44100 -s -2 "$1" -t wav "$2"
		$MPLAYER -quiet -af pan=1:0.5:0.5,resample=44100:0:2,format=s16le -ao pcm:file="$2" "$1"
	fi
}

TMP1=`mktemp tmpXXXXXXXX`
TMP2=`mktemp tmpXXXXXXXX`

convert_input "$1" "$TMP1"
convert_input "$2" "$TMP2"

if [ -x "$SOXMIX" ]; then
	$SOX -t wav "$TMP1" -c 2 -t wav "$TMP1.stereo" pan -1.0
	$SOX -t wav "$TMP2" -c 2 -t wav "$TMP2.stereo" pan 1.0

	$SOXMIX -t wav "$TMP1.stereo" -t wav "$TMP2.stereo" "$3.wav"

	rm -f "$TMP1.stereo" "$TMP2.stereo"
else
	$SOX -M -t wav "$TMP1" -t wav "$TMP2" -o "$3.wav"
fi

$OGGENC -q 1 -o "$3" "$3.wav"

rm -f "$3.wav" "$TMP1" "$TMP2"

