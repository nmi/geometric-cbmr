#!/bin/bash

PROGRAMDIR=`dirname "$0"`

#align="$PROGRAMDIR/align"
align="$PROGRAMDIR/../align"
combine_to_vorbis="$PROGRAMDIR/combine_to_vorbis.sh"

align_params="-x 5 -w 2048 -W 512 -M 128"
align_params_skip="-D 30000"

if [ ! -x $align ]; then
	echo "$align is missing. Exiting..."
	echo ""
	exit
fi

#
# run_align(score_midi, performance_midi, output_base, parameters)
#
function run_align {
	"$align" "$1" -p "$2" -o "$3.mid" -a "$3.aln" -m "$3" $4
}


for file in "$@"; do
	output=`basename "$file" ".score.mid"`
	dir=`dirname "$file"`
	output="$dir/$output"

	# Reference
	run_align "$file" "$output.warped.mid" \
	          "$output.warped.ref" \
	          "$align_params"
	"$combine_to_vorbis" "$output.warped.ogg" "$output.warped.ref.mid" \
	                     "$output.warped.ref.ogg"

	run_align "$file" "$output.skipped.mid" \
	          "$output.skipped.ref" \
	          "$align_params $align_params_skip"
	"$combine_to_vorbis" "$output.skipped.ogg" "$output.skipped.ref.mid" \
	                     "$output.skipped.ref.ogg"

	# RK
	run_align "$file" "$output.warped.ogg.rk.mid" \
	          "$output.warped.rk" \
	          "$align_params"
	"$combine_to_vorbis" "$output.warped.ogg" "$output.warped.rk.mid" \
	                     "$output.warped.rk.ogg"

	run_align "$file" "$output.skipped.ogg.rk.mid" \
	          "$output.skipped.rk" \
	          "$align_params $align_params_skip"
	"$combine_to_vorbis" "$output.skipped.ogg" "$output.skipped.rk.mid" \
	                     "$output.skipped.rk.ogg"

	# PI1
	run_align "$file" "$output.warped.ogg.pi1.mid" \
	          "$output.warped.pi1" \
	          "$align_params"
	"$combine_to_vorbis" "$output.warped.ogg" "$output.warped.pi1.mid" \
	                     "$output.warped.pi1.ogg"

	run_align "$file" "$output.skipped.ogg.pi1.mid" \
	          "$output.skipped.pi1" \
	          "$align_params $align_params_skip"
	"$combine_to_vorbis" "$output.skipped.ogg" "$output.skipped.pi1.mid" \
	                     "$output.skipped.pi1.ogg"

done

