#!/bin/bash

echo ""
echo "Algorithm accuracy test. Prints 1 if the correct match was (probably)"
echo "found and 0 if not."
echo ""
echo "Make sure TESTACCURACY is defined in geometrictest.c"
echo ""
echo "P2 P3"

for (( i=0 ; i<100 ; i+=1 )); do
	./geometrictest 3 1000 5000 3 $i
done

