#!/bin/bash

if [ "x$DATA" == "x" ]; then
	DATA=ref
fi

ACCURACY=100
MIN_SKIP_INTERVAL=50

HISTOGRAM_ACCURACY=25
MAX_HISTOGRAM_OFFSET=1000


PROGRAMDIR=`dirname "$0"`

alignment_offsets="$PROGRAMDIR/alignment_offsets"
alignment_histogram="$PROGRAMDIR/alignment_histogram"

if [ ! -x "$alignment_offsets" ]; then
	echo -e "\nError: unable to execute $alignment_offsets\n"
	exit 1
fi

if [ ! -x "$alignment_histogram" ]; then
	echo -e "\nError: unable to execute $alignment_histogram\n"
	exit 1
fi

GNUPLOT=`which gnuplot`
CONVERT=`which convert`

if [ ! -x "$GNUPLOT" ]; then
	echo -e "\nError: unable to execute gnuplot\n"
	exit 1
fi


for reference in "$@"; do

	dir=`dirname "$reference"`
	base=`basename "$reference" ".aln"`

	datafile="$dir/$base.$DATA.aln"
	output_offsets="$dir/$base.$DATA.offsets.dat"
	output_skips="$dir/$base.$DATA.skips.dat"
	output_histogram="$dir/$base.$DATA.histogram.dat"
	plot="$dir/$base.$DATA.p"
	plot_offsets="$dir/$base.$DATA.offsets"
	plot_histogram="$dir/$base.$DATA.histogram"

	echo "Plotting $datafile"

	"$alignment_offsets" "$reference" "$datafile" "$output_offsets" \
			"$output_skips" $ACCURACY $MIN_SKIP_INTERVAL

	"$alignment_histogram" "$MAX_HISTOGRAM_OFFSET" "$HISTOGRAM_ACCURACY" \
			"$output_offsets" > "$output_histogram"

	# plot offsets

	echo -e "# gnuplot script" > "$plot"
	echo -e "reset" >> "$plot" 
	echo -e "set term postscript eps enhanced solid \"Helvetica\" 18 color" >> "$plot" 
	echo -e "set output \"$plot_offsets.eps\"" >> "$plot"
	echo -e "set size 1.0,0.5" >> "$plot"

	echo -e "set nokey" >> "$plot"
	echo -e "set style line 8 lc rgb '#AAAAAA'" >> "$plot"
	echo -e "set style line 9 lc rgb '#EEEEEE'" >> "$plot"
	echo -e "set grid xtics ytics mytics ls 8 , ls 9 " >> "$plot"
	echo -e "set autoscale xfix" >> "$plot"
	echo -e "set yrange [-1:1]" >> "$plot"
	echo -e "set ytics 0.5" >> "$plot"
	echo -e "set mytics 5" >> "$plot"
	echo -e "set xtics 60.0" >> "$plot"
	echo -e "set mxtics 12" >> "$plot"

	echo -e "set style fill solid 0.33 noborder" >> "$plot"
	echo -e "set boxwidth 4.0" >> "$plot"

#	echo -e "set xlabel \"Time (s)\"" >> "$plot"
#	echo -e "set ylabel \"Offset (s)\"" >> "$plot"

	if [ "x$MARK_SKIPS" == "x" ]; then
		echo -n "plot " >> "$plot"
	else
		echo -e "plot '$output_skips' w boxes lt 1, \\" >> "$plot"
		echo -e "     '$output_skips' u 1:(-\$2) w boxes lt 1, \\" >> "$plot"
	fi

	echo -e "     '$output_offsets' w lines lt 7" >> "$plot"
	"$GNUPLOT" "$plot"

	if [ -x "$CONVERT" ]; then
		"$CONVERT" -density 200x200 -alpha off "$plot_offsets.eps" "$plot_offsets.png"
	fi


	rm -f "$plot"

	# plot histogram

	echo -e "# gnuplot script" > "$plot"
	echo -e "reset" >> "$plot" 
	echo -e "set term postscript eps enhanced solid \"Helvetica\" 18 color" >> "$plot" 
	echo -e "set output \"$plot_histogram.eps\"" >> "$plot"
	echo -e "set size 1.0,0.5" >> "$plot"

	echo -e "set nokey" >> "$plot"
	echo -e "set style line 8 lc rgb '#AAAAAA'" >> "$plot"
	echo -e "set style line 9 lc rgb '#EEEEEE'" >> "$plot"
	echo -e "set grid xtics ytics mxtics mytics ls 8 , ls 9 " >> "$plot"
	echo -e "set autoscale xfix" >> "$plot"
	echo -e "set ytics 0.1" >> "$plot"
	echo -e "set mytics 5" >> "$plot"
	echo -e "set xtics 0.5" >> "$plot"
	echo -e "set mxtics 5" >> "$plot"

	echo -e "set style fill solid 0.25 border -1" >> "$plot"
	echo -e "set boxwidth 0.025" >> "$plot"

#	echo -e "set xlabel \"Offset (s)\"" >> "$plot"
#	echo -e "set ylabel \"Relative frequency\"" >> "$plot"

	echo -e "plot '$output_histogram' w boxes lt 3" >> "$plot"
	"$GNUPLOT" "$plot"

	if [ -x "$CONVERT" ]; then
		"$CONVERT" -density 300x300 -alpha off "$plot_histogram.eps" "$plot_histogram.png"
	fi

	rm -f "$plot"
done


