#!/bin/bash

for file in "$@"; do
	output=`basename "$file" .pgm`
	dir=`dirname "$file"`
	output="$dir/$output.jpg"

	convert -contrast-stretch 0.4%x99.6% $file $output
done

