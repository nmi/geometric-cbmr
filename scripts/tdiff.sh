#!/bin/bash

for ((i=101; i<201; i=i+1)); do
	./test_speed_orig mozart.sc --multiple-matches -vv --seed $i >& tdiff.1
	./test_speed mozart.sc --multiple-matches -vv --seed $i >& tdiff.2

	echo -e "Seed: $i\n"
	diff tdiff.1 tdiff.2 | grep strt
done

