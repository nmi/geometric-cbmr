#!/bin/bash

if [ $# != 2 ]; then
	echo ""
	echo "usage: measure_align_time.sh <song.mid> <out.dat>"
	echo ""
fi

if [ "x$ALG" == "x" ]; then
	ALG="AP3opt"
fi

if [ "x$PARAMETERS" == "x" ]; then
	PARAMETERS="-w 2048 -W 512 -x 5 -R 0 -O 0 -E 0.0"
fi

if [ "x$NOTES" == "x" ]; then
	NOTES="32 64 128 256 512 1024 2048 4096 8192 16384"
	#NOTES="32 64 128 256 512"
fi

PROGRAMDIR=`dirname "$0"`

align="$PROGRAMDIR/../align"
test_speed="$PROGRAMDIR/../test_speed"

song="$1"
out="$2"

if [ "x$ALG" == "xP3" ]; then
	echo -e "# Algorithm: $ALG" > "$out"
else
	echo -e "# Algorithm: $ALG ($PARAMETERS)" > "$out"
fi
echo -e "# notes\t time" >> "$out"

for n in $NOTES; do
	echo "Notes: $n"
	if [ "x$ALG" == "xP3" ]; then
		repeats=1
		if (( $n < 50 )); then
			repeats=10000
		elif (( $n < 200 )); then
			repeats=1000
		elif (( $n < 1000 )); then
			repeats=100
		elif (( $n < 4000 )); then
			repeats=10
		fi
		"$test_speed" -a P3 -n $n -m $n -R $repeats "$song" -p "$song" \
			| sed -e  "s/^\([[:digit:]]\+\)[[:space:]]\+[[:digit:]]\+[[:space:]]\+\([[:digit:].]\+\).*\$/\1\t \2/" >> "$out"
	else

		"$align" $PARAMETERS -a $ALG -n $n "$song" -p "$song" \
			| grep "^[[:space:]]\+0[[:space:]]\+.*\$" \
			| sed -e  "s/^[[:space:]]\+[[:digit:]]\+[[:space:]]\+\([[:digit:]]\+\)[[:space:]]\+[[:digit:]]\+[[:space:]]\+[[:digit:]]\+[[:space:]]\+[[:digit:]]\+[[:space:]]\+[[:digit:]]\+[[:space:]]\+[[:digit:]]\+[[:space:]]\+\([[:digit:].]\+\).*\$/\1\t \2/" >> "$out"
	fi
done

