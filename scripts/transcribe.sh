#!/bin/bash
#
# Runs polyphonic transcription algorithms on the given songs.
#
# This script calls Antonio Pertusa's statically built executables and
# Ryynanen & Klapuri's Matlab/MEX code.
#
# Input preprocessing (stereo to mono downmixing, resampling) is done with
# MPlayer.
#

PROGRAMDIR=`dirname "$0"`

if [ $# == 0 ]; then
	echo ""
	echo "Usage example:"
	echo "transcribe.sh file1.mp3 file2.ogg file3.wma"
	echo ""
	echo "This will output one MIDI file for each transcription algorithm"
	echo "and for each input file. In this example there would be 6 MIDI"
	echo "files."
	echo ""
	exit
fi

for file in "$@"; do
	if [ -e "$file.pi1.mid" ]; then
		echo ""
		echo "Ignoring $file: already transcribed"
		echo ""
	else
        	"$PROGRAMDIR/transcribe-pi1.sh" "$file"
	fi
done

for file in "$@"; do
	if [ -e "$file.rk.mid" ]; then
		echo ""
		echo "Ignoring $file: already transcribed"
		echo ""
	else
        	"$PROGRAMDIR/transcribe-rk.sh" "$file"
	fi
done

