#!/usr/bin/python

"""
Usage: calculate_map.py <query results>
"""

import sys
import getopt



def calculate_map(resultfile):
    querycounts = dict()
    results = []

    f = open(resultfile, "r")
    for line in f:
    	if (len(line.strip()) == 0):
	    continue
        parts = line.split(":")
        queryfile = parts[0].strip()
        matches = parts[1]

        pos = queryfile.rfind("/")
        queryid = queryfile[(pos + 1):]
        pos = queryid.lower().rfind(".mid")
        if (pos > 0):
            queryid = queryid[0:pos]
        if (querycounts.has_key(queryid)):
            querycounts[queryid] += 1
        else:
            querycounts[queryid] = 1
        results.append((queryfile, queryid, matches.split()))

    map = 0.0

    for r in results:
        ap = 0.0
        relevant = 0.0
        pos = 0.0
        for q in r[2]:
            pos += 1.0
            if (q == r[1]):
                relevant += 1.0
                ap += relevant / pos
        all_relevant = querycounts[r[1]]
        if (all_relevant > len(r[2])):
            all_relevant = len(r[2])
        ap = ap / all_relevant
        map += ap
        print r[0] + ": " + str(ap)

    map = map / len(results)
    print "\nMean Average Precision: " + str(map) 
    f.close()


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "h", ["help"])
        except getopt.error, msg:
             raise Usage(msg)
        # Process options
        for o, a in opts:
            if o in ("-h", "--help"):
                print __doc__
                sys.exit(0)
        # Process arguments
        for arg in args:
            calculate_map(arg)
    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 2

if __name__ == "__main__":
    sys.exit(main())

