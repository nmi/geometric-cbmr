#!/bin/bash

PROGRAMDIR=`dirname "$0"`

align="$PROGRAMDIR/align"
combine_to_vorbis="$PROGRAMDIR/combine_to_vorbis.sh"

align_params="-x 5 -w 4096 -W 1024 -M 128 -D 30000"

if [ ! -x $align ]; then
	echo "$align is missing. Exiting..."
	echo ""
	exit
fi

#
# run_align(score_midi, performance_midi, output_base, parameters)
#
function run_align {
	"$align" "$1" -p "$2" -o "$3.mid" -m "$3" $4
}


for file in "$@"; do
	input=`basename "$file" ".ogg"`
	dir=`dirname "$file"`
        output="$dir/aligned-$input"
	input="$dir/$input"

        for num in 1 2 3 4 5 6 7 8 9; do
		if [ -e "$input.$num.mid" ]; then
			# RK
			run_align "$input.$num.mid" "$input.ogg.rk.mid" \
				  "$output.$num.rk" "$align_params"
			"$combine_to_vorbis" "$input.ogg" "$output.$num.rk.mid" \
					     "$output.$num.rk.ogg"

			# PI1
			run_align "$input.$num.mid" "$input.ogg.pi1.mid" \
				  "$output.$num.pi1" "$align_params"
			"$combine_to_vorbis" "$input.ogg" "$output.$num.pi1.mid" \
					     "$output.$num.pi1.ogg"
		fi
	done
done

