#!/usr/bin/python

"""
Usage: calculate_mrr.py <query results>
"""

import sys
import getopt



def calculate_mrr(resultfile):
    querycounts = dict()
    results = []

    f = open(resultfile, "r")
    for line in f:
    	if (len(line.strip()) == 0):
	    continue
        parts = line.split(":")
        queryfile = parts[0].strip()
        matches = parts[1]

        pos = queryfile.rfind("/")
        queryid = queryfile[(pos + 1):]
        pos = queryid.lower().rfind(".mid")
        if (pos > 0):
            queryid = queryid[0:pos]
        if (querycounts.has_key(queryid)):
            querycounts[queryid] += 1
        else:
            querycounts[queryid] = 1
        results.append((queryfile, queryid, matches.split()))

    mrr = 0.0

    for r in results:
        rr = 0.0
        pos = 0.0
        for q in r[2]:
            pos += 1.0
            if (q == r[1]):
                rr = 1.0 / pos
		break
        mrr += rr
        print r[0] + ": " + str(rr)

    mrr = mrr / len(results)
    print "\nMean Reciprocal Rank: " + str(mrr) 
    f.close()


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "h", ["help"])
        except getopt.error, msg:
             raise Usage(msg)
        # Process options
        for o, a in opts:
            if o in ("-h", "--help"):
                print __doc__
                sys.exit(0)
        # Process arguments
        for arg in args:
            calculate_mrr(arg)
    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, "for help use --help"
        return 2

if __name__ == "__main__":
    sys.exit(main())

