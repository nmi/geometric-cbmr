#!/bin/bash

if [ "x$OUT" == "x" ]; then
	OUT=histogram
fi

HISTOGRAM_ACCURACY=25
MAX_HISTOGRAM_OFFSET=2000


PROGRAMDIR=`dirname "$0"`

alignment_histogram="$PROGRAMDIR/alignment_histogram"

if [ ! -x "$alignment_histogram" ]; then
	echo -e "\nError: unable to execute $alignment_histogram\n"
	exit 1
fi

GNUPLOT=`which gnuplot`
CONVERT=`which convert`

if [ ! -x "$GNUPLOT" ]; then
	echo -e "\nError: unable to execute gnuplot\n"
	exit 1
fi

	"$alignment_histogram" "$MAX_HISTOGRAM_OFFSET" "$HISTOGRAM_ACCURACY" \
			"$@" > "$OUT.dat"

	plot="$OUT.p"

	# plot histogram

	echo -e "# gnuplot script" > "$plot"
	echo -e "reset" >> "$plot" 
	echo -e "set term postscript eps enhanced solid \"Helvetica\" 18 color" >> "$plot" 
	echo -e "set output \"$OUT.eps\"" >> "$plot"
	echo -e "set size 2.0,0.5" >> "$plot"

	echo -e "set nokey" >> "$plot"
	echo -e "set style line 8 lc rgb '#AAAAAA'" >> "$plot"
	echo -e "set style line 9 lc rgb '#EEEEEE'" >> "$plot"
	echo -e "set grid xtics ytics mxtics mytics ls 8 , ls 9 " >> "$plot"
	echo -e "set autoscale xfix" >> "$plot"
	echo -e "set ytics 0.1" >> "$plot"
	echo -e "set mytics 5" >> "$plot"
	echo -e "set xtics 0.5" >> "$plot"
	echo -e "set mxtics 5" >> "$plot"

	echo -e "set style fill solid 0.25 border -1" >> "$plot"
	echo -e "set boxwidth 0.025" >> "$plot"

#	echo -e "set xlabel \"Offset (s)\"" >> "$plot"
#	echo -e "set ylabel \"Relative frequency\"" >> "$plot"

	echo -e "plot '$OUT.dat' w boxes lt 3" >> "$plot"
	"$GNUPLOT" "$plot"

	if [ -x "$CONVERT" ]; then
		"$CONVERT" -density 300x300 -alpha off "$OUT.eps" "$OUT.png"
	fi

	rm -f "$plot"

