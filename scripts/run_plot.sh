#!/bin/bash

collections=(classical electronic jazz misc pop rock)

for dir in ${collections[@]}; do
	cd "$dir"
	DATA=ref ../plot_alignments.sh *.warped.aln
	DATA=rk  ../plot_alignments.sh *.warped.aln
	DATA=pi1 ../plot_alignments.sh *.warped.aln
	OUT="../$dir.warped.ref" ../plot_collection_histogram.sh *.warped.ref.offsets.dat
	OUT="../$dir.warped.rk" ../plot_collection_histogram.sh *.warped.rk.offsets.dat
	OUT="../$dir.warped.pi1" ../plot_collection_histogram.sh *.warped.pi1.offsets.dat

	MARK_SKIPS=1 DATA=ref ../plot_alignments.sh *.skipped.aln
	MARK_SKIPS=1 DATA=rk  ../plot_alignments.sh *.skipped.aln
	MARK_SKIPS=1 DATA=pi1 ../plot_alignments.sh *.skipped.aln
	OUT="../$dir.skipped.ref" ../plot_collection_histogram.sh *.skipped.ref.offsets.dat
	OUT="../$dir.skipped.rk" ../plot_collection_histogram.sh *.skipped.rk.offsets.dat
	OUT="../$dir.skipped.pi1" ../plot_collection_histogram.sh *.skipped.pi1.offsets.dat
	cd ..
done

