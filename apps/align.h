/*
 * align.h - Symbolic score alignment structures and definitions.
 *
 * Version 2008-02-29
 *
 *
 * Copyright (C) 2007 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __ALIGN_H__
#define __ALIGN_H__

#include "config.h"
#include "midifile.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * Alignment parameters.
 */
typedef struct {
    int verbose;
    char *program_name;

    int algorithm;

    char *pattern_path;
    char *song_path;
    char *output_midi;
    char *output_alignment;
    char *output_map;
    int max_pattern_size;
    int map_size;

    int skip_cost;
    int map_accuracy;
    int normalize;
    int multiscale;
    int smooth_range;

    int p3_pattern_window_width;
    int p3_pattern_window_skip;
    int p3_scales_num;
    double p3_scales_spread;
    int p3_optimize;
    int p3_onset_delta;
    double p3_onset_slope;
    int p3_window_heuristics;
    int p3_align_rests;
    int p3_max_local_transposition;

    int dtw_constraint;
    int dtw_skip_delta;
    float dtw_slope_weight;

    int generate_alignment;

    int song_margins;
    float song_tempo;
    int delay;
} alignparameters;


/**
 * Alignment line within an alignment map.
 */
typedef struct {
/*    int pattern_position; */
    int pattern_time;
    int target_time;
    unsigned char *values;
    char *transpositions;
} alignmentline;


/**
 * Alignment map: a similarity matrix for all possible alignments of small
 * song segments.
 */
typedef struct {
    int width;
    int height;
    int accuracy;
    int accuracy_shift;
    int slope;
    double *initial_alignment;
    double *initial_alignment_offset;
    alignmentline *lines;
    unsigned char *vbuffer;
    char *tbuffer;
    int target_duration;
    int pattern_duration;
    const song *target;
    const song *pattern;
    const songcollection *sc;
} alignmentmap;


typedef struct {
    int size;
    int *pattern_times;
    int *target_times;
    char *skip;
    unsigned char *quality;
    const song *pattern;
    const song *target;
} alignment;


/*void align(const songcollection *sc, const song *s, const song *p, int alg,
        const alignparameters *parameters, alignment *initial_align,
	alignment *result_align);
*/
size_t init_alignmentmap(alignmentmap *map, const songcollection *sc,
        const song *target, const song *pattern, int accuracy, int width,
        const alignmentmap *initial_map);

void free_alignmentmap(alignmentmap *map);

void map_alignments(alignmentmap *map, int alg,
        const alignparameters *parameters);

void write_alignmentmap_pgm(const char *filename, const alignmentmap *map,
        int invert, int show_alignment_path);

/*void calculate_initial_alignment(const alignparameters *parameters,
        const alignmentmap *map, alignment *a);*/

void dtw_alignment(alignmentmap *map, const alignparameters *parameters);

void normalize_alignmentmap(alignmentmap *map);

void align_midi_song(midisong *midi_s, alignment *a);

void align_init_parameters(alignparameters *p);

void align_free_parameters(alignparameters *p);

void init_alignment(alignment *a, alignmentmap *map);

void clear_alignment(alignment *a);

void free_alignment(alignment *a);

int align_parse_arguments(int argc, char **argv, alignparameters *p);

void generate_alignment(alignment *a, int song_length, int accuracy,
        float max_slope, int skip_interval, int tempo_change_interval,
        float local_variation);

void delay_alignment(alignment *a, int delay);

void print_alignment(alignment *a);

void write_alignment(alignment *a, char *path);

void print_initial_alignment(alignmentmap *map);

#ifdef __cplusplus
}
#endif

#endif

