
/*
 * align_p3.h - External function declarations for P3 alignment algorithm
 *
 * Version 2008-03-05
 *
 *
 * Copyright (C) 2008 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef __ALIGN_P3_H__
#define __ALIGN_P3_H__

#include "config.h"
#include "search.h"
#include "song.h"
#include "align.h"

#include "geometric_p3.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
    /** Length of the alignment in milliseconds*/
    int duration;

    /** Length of the alignment score array in slots */
    int size;

    /** The resulting alignment score for each position of the
      * pattern within the target song will be written to this array.
      * The array should contain negative values by default so that
      * gaps can be filled afterwards. */
    int *values;

    /** Pattern transposition for each result value. */
    char *transpositions;

    /** Accuracy of the score array is calculated as 1 << shift.
      * For example, shift=8 results in each array slot representing 256
      * milliseconds of time. */
    int shift;

    /** Start of the currently relevant part in milliseconds */
    int start;
    /** End of the currently relevant part in milliseconds */
    int end;

    /* Should the gaps be filled. */
    /*int fill_gaps;*/
} ap3results;


typedef struct {
    int *initial_alignment;
    int last_alignment_pos;

    int line_length;
    int *line;
    int *line_factor;
    int last_line_pos;

    int num_lines;
    int last_line;
    int next_line;
    int num_used_lines;

    unsigned char **prev_lines;
    int *prev_line_pos;
    int *prev_line_view_left;
    int *prev_line_view_right;

    unsigned char *linebuf_data;
    unsigned char *linebuf[2];
    int linebuf_pos;
    float *linediff;
    int *linediff_base;
} linecache;

typedef struct {
    unsigned int before;
    unsigned int after;
} ap3localnotes;


/* External function declarations */

void init_ap3results(ap3results *ar, int duration, int shift);

void free_ap3results(ap3results *ar);

void clear_ap3results(ap3results *ar);

void normalize_ap3results(ap3results *ar, int maximum);


void align_turningpoints_p3(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results);

void align_turningpoints_p3onset(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results, int onset_delta, double onset_slope);

void align_turningpoints_p3opt(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results, int onset_delta, double onset_slope,
        int pattern_width, int ltrans);

void align_p3(alignmentmap *map, int alg, const alignparameters *parameters);

void init_linecache(linecache *lc, int num_lines, int line_length,
        alignmentmap *map, int map_pos, double scale);

void free_linecache(linecache *lc);

void update_linecache(linecache *lc, const ap3results *aresult,
        int map_pos, alignmentmap *map);

#ifdef __cplusplus
}
#endif

#endif

