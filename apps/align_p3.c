/*
 * align_p3.c - A modified version of the P3 implementation for symbolic score
 *              alignment
 *
 * Version 2008-02-18
 *
 *
 * Copyright (C) 2004 Mika Turkia
 * Copyright (C) 2008 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "algorithms.h"
#include "search.h"
#include "song.h"
#include "geometric_p3.h"
#include "priority_queue.h"
#include "priority_queue_32.h"
#include "util.h"
#include "align.h"
#include "align_p3.h"
#include "song_window.h"
#include "song_window_p3.h"

/*#define DEBUG_WINDOW_SIZE_HISTOGRAM 1*/

#define ACCURATE_P3_INTERPOLATION 2

#define USE_LINECACHE 1
/*#define LINECACHE_COUNT_LINE_FACTOR 1*/

#define SELECT_OPTIMAL_PQ_FUNCTIONS 1

#ifndef SELECT_OPTIMAL_PQ_FUNCTIONS
  #define pq32_update_key(pq, node) pq32_update_unrolled(pq, node)
#endif

#define SHOW_PROGRESS 1


#define MAX_LOCAL_TRANSPOSITION 12

static double LOCAL_TRANSPOSITION_WEIGHTS[13]
        = {1.0, 0.8, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5};


void init_ap3results(ap3results *ar, int size, int shift) {
    ar->start = 0;
    ar->end = 0;
    ar->size = size;
    ar->values = (int *) calloc(ar->size, sizeof(int));
    ar->transpositions = (char *) calloc(ar->size, sizeof(char));
    if ((ar->values == NULL) || (ar->transpositions == NULL)) {
        fputs("Error in init_ap3results: unable to allocate buffers", stderr);
        free(ar->values);
        free(ar->transpositions);
        ar->values = NULL;
        ar->transpositions = NULL;
        ar->size = 0;
        ar->duration = 0;
        ar->shift = 0;
        return;
    }
    ar->duration = size << shift;
    ar->shift = shift;
}


void free_ap3results(ap3results *ar) {
    free(ar->values);
    free(ar->transpositions);
    ar->values = NULL;
    ar->transpositions = NULL;
    ar->start = 0;
    ar->end = 0;
}

void clear_ap3results(ap3results *ar) {
    memset(ar->values, 0, ar->size * sizeof(int));
    memset(ar->transpositions, 0, ar->size * sizeof(char));
}

void normalize_ap3results(ap3results *ar, int maximum) {
    int i;
    float norm;
    if (maximum > 0) norm = 255.0F / (float) maximum;
    else norm = 0.0F;
    for (i=0; i<ar->size; ++i) {
        ar->values[i] = MIN2(255, lrintf(norm * (float) ar->values[i]));
    }
}


/** 
 * A modified version of the geometric P3 symbolic music retrieval algorithm
 * for score alignment.
 *
 * @param p3s song to scan
 * @param pattern pattern song
 */
void align_turningpoints_p3(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results) {

    int i, j, num_loops;
    VerticalTranslationTableItem *verticaltranslationtable;
    VerticalTranslationTableItem *item = NULL;
    TurningPoint *note_starts = p3s->note_starts;
    TurningPoint *note_ends = p3s->note_ends;
    int num_tpoints = p3s->num_notes;
    int pattern_size = pattern->size;
    vector *pnotes = pattern->notes;

    /* Create a priority queue */
    pqroot *pq;
    pqnode *min;
    TranslationVector *translation_vectors;
    
    if ((pattern_size <= 0) || (num_tpoints <= 0)) return;

    pq = pq_create(pattern_size * 4);
    translation_vectors = (TranslationVector *)
            malloc(pattern_size * 4 * sizeof(TranslationVector));

    /* Initialize a vertical translation array */
    verticaltranslationtable = (VerticalTranslationTableItem *) malloc(
            NOTE_PITCHES * 2 * sizeof(VerticalTranslationTableItem));
    for (i = 0; i < (NOTE_PITCHES * 2); i++) {
        verticaltranslationtable[i].value = 0;
        verticaltranslationtable[i].slope = 0;
        verticaltranslationtable[i].prev_x = 0;
    }

    /*printf("P3 Notes: %d\n", p3s->num_notes);*/

    /* Create an array whose items have two pointers each: one for note_starts
     * and one for note_ends. Each item points to turning point array item Also
     * populate the priority queue with initial items. */

    for (i = 0, j = 0; i < pattern_size; i++) {
        TranslationVector *v;
        /*pattern_duration += (float) pnotes[i].dur;*/


        /* Add translation vectors calculated from the note start points */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x -
                (int) (pnotes[i].strt + pnotes[i].dur);
        v->position = P3_POSITION_PE_TS;
        min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TS;
        min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;


        /* Add translation vectors calculated from the note end points */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) (pnotes[i].strt +
                pnotes[i].dur);
        v->position = P3_POSITION_PE_TE;
        min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TE;
        min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;
    }

    num_loops = (pattern_size * num_tpoints) * 4;

    for (i = 0; i < num_loops; i++) {
        TranslationVector *v;
        int x, y;
        int newval;

        /* Get the minimum element */
        min = pq_getmin(pq);
        v = (TranslationVector *) min->pointer;
        x = v->x;
        y = v->y;

        /* Update value */
        item = &verticaltranslationtable[NOTE_PITCHES + y];
        newval = item->value + item->slope * (x - item->prev_x);

#ifdef ACCURATE_P3_INTERPOLATION
        {
            int old_x, new_x;
            int val, slope, oldpos;

            old_x = item->prev_x + pnotes[0].strt;
            new_x = x + pnotes[0].strt;

    #if (ACCURATE_P3_INTERPOLATION == 2)
            val = 0;
            if (new_x >= 0) {
                if ((new_x >= results->start) && (new_x < results->end)) {
                    j = (new_x - results->start) >> results->shift;
                    if (results->values[j] < newval) val = 1;
                }
            }
            if ((val == 0) && (old_x >= 0)) {
                if ((old_x >= results->start) && (old_x < results->end)) {
                    j = (old_x - results->start) >> results->shift;
                    if (results->values[j] == item->value) val = 1;
                }
            }
            if (val == 1) {
    #endif
                if (item->slope >= 0) {
                    slope = item->slope << results->shift;
                } else {
                    slope = -((-item->slope) << results->shift);
                }

                oldpos = MAX2(old_x - results->start, 0) >> results->shift;
                j = MIN2((MAX2(new_x - results->start, 0) >> results->shift),
                        results->size - 1);
                val = newval - item->slope *
                        MAX2(new_x - results->end, 0);
                if (oldpos == j) oldpos--;
                for (; j>oldpos; --j, val-=slope) {
                    if (results->values[j] < val) {
                        results->values[j] = val;
                        results->transpositions[j] = y;
                    }
                }
                item->value = newval;
    #if (ACCURATE_P3_INTERPOLATION == 2)
            } else item->value = newval;
    #endif
        }
#else
        int new_x = x + pnotes[0].strt;
        item->value = newval;
        if ((new_x >= results->start) && (new_x < results->end)) {
            new_x = (new_x - results->start) >> results->shift;
            if (results->values[new_x] < newval) {
                results->values[new_x] = newval;
                results->transpositions[new_x] = y;
            }
        }

#endif /* ACCURATE_P3_INTERPOLATION */

        item->prev_x = x;

        /* Adjust slope */
        switch (v->position) {
            case P3_POSITION_PS_TE:
            case P3_POSITION_PE_TS:
                item->slope++;
                break;
            case P3_POSITION_PS_TS:
            case P3_POSITION_PE_TE:
                item->slope--;
                break;
        }

        /* Move the pointer and insert a new translation vector according to
         * the turning point type. */
        if (v->target_index < num_tpoints - 1) {
            const vector *patp = &pnotes[v->pattern_index];
            v->target_index++;

            if ((v->position == P3_POSITION_PS_TS) ||
                (v->position == P3_POSITION_PE_TS)) {

                const TurningPoint *startp = &note_starts[v->target_index];
                v->x = (int) startp->x - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
            } else {
                const TurningPoint *endp = &note_ends[v->target_index];
                v->x = (int) endp->x - (int) patp->strt;
                v->y = (int) endp->y - (int) patp->ptch;
            }
            if ((v->position == P3_POSITION_PE_TS) ||
                (v->position == P3_POSITION_PE_TE)) {
                v->x -= (int) patp->dur;
            }

            min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
            pq_update_key1(pq, min);
        } else {
            /* 'Remove' a translation vector by making it very large.
             * It won't be extracted since there will be only as many loops as 
             * there are real vectors. */
            min->key1 = INT_MAX;
            pq_update_key1(pq, min);
        }
    }

    /* Free the reserved memory. */
    pq_free(pq);
    free(translation_vectors);
    free(verticaltranslationtable);
}

/** 
 * A modified version of the geometric P3 symbolic music retrieval algorithm
 * for score alignment.
 *
 * @param p3s song to scan
 * @param pattern pattern song
 */
void align_turningpoints_p3onset(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results, int onset_delta, double onset_slope) {

    int i, j, num_loops;
    FVerticalTranslationTableItem *verticaltranslationtable;
    FVerticalTranslationTableItem *item = NULL;
    TurningPoint *note_starts = p3s->note_starts;
    TurningPoint *note_ends = p3s->note_ends;
    int num_tpoints = p3s->num_notes;
    int pattern_size = pattern->size;
    double accuracy = (double) (1 << results->shift);
    vector *pnotes = pattern->notes;

    /* Create a priority queue */
    pqroot *pq;
    pqnode *min;
    TranslationVector *translation_vectors;
    
    if ((pattern_size <= 0) || (num_tpoints <= 0)) return;

    pq = pq_create(pattern_size * 6);
    translation_vectors = (TranslationVector *)
            malloc(pattern_size * 6 * sizeof(TranslationVector));

    /* Initialize a vertical translation array */
    verticaltranslationtable = (FVerticalTranslationTableItem *) malloc(
            NOTE_PITCHES * 2 * sizeof(FVerticalTranslationTableItem));
    for (i = 0; i < (NOTE_PITCHES * 2); i++) {
        verticaltranslationtable[i].value = 0.0;
        verticaltranslationtable[i].slope = 0.0;
        verticaltranslationtable[i].prev_x = 0;
    }

    /*printf("P3 Notes: %d\n", p3s->size);*/

    /* Create an array whose items have two pointers each: one for note_starts
     * and one for note_ends. Each item points to turning point array item Also
     * populate the priority queue with initial items. */

    for (i = 0, j = 0; i < pattern_size; i++) {
        TranslationVector *v;
        /*pattern_duration += (float) pnotes[i].dur;*/


        /* Add translation vectors calculated from the note start points */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x -
                (int) (pnotes[i].strt + pnotes[i].dur);
        v->position = P3_POSITION_PE_TS;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TS;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;


        /* Add translation vectors calculated from the note end points */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) (pnotes[i].strt +
                pnotes[i].dur);
        v->position = P3_POSITION_PE_TE;
        v->pdur = pnotes[i].dur;
        v->tdur = note_ends[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TE;
        v->pdur = pnotes[i].dur;
        v->tdur = note_ends[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;


        /* Add translation vectors for onset weighting */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - onset_delta - (int) pnotes[i].strt;
        v->position = P3_POSITION_ONSET_START;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x + onset_delta - (int) pnotes[i].strt;
        v->position = P3_POSITION_ONSET_END;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1(pq, min);
        ++j;
    }

    num_loops = (pattern_size * num_tpoints) * 6;

    for (i = 0; i < num_loops; i++) {
        TranslationVector *v;
        int x, y;
        double newval;

        /* Get the minimum element */
        min = pq_getmin(pq);
        v = (TranslationVector *) min->pointer;
        x = v->x;
        y = v->y;

        /* Update value */
        item = &verticaltranslationtable[NOTE_PITCHES + y];
        newval = item->value + item->slope * (double) (x - item->prev_x);

#ifdef ACCURATE_P3_INTERPOLATION
        {
            int old_x, new_x, oldpos;
            double val, slope;

            old_x = item->prev_x + pnotes[0].strt;
            new_x = x + pnotes[0].strt;

    #if (ACCURATE_P3_INTERPOLATION == 2)
            val = 0.0;
            if (new_x >= 0) {
                if ((new_x >= results->start) && (new_x < results->end)) {
                    j = (new_x - results->start) >> results->shift;
                    if (results->values[j] < lrint(newval)) val = 1.0;
                }
            }
            if ((val == 0) && (old_x >= 0)) {
                if ((old_x >= results->start) && (old_x < results->end)) {
                    j = (old_x - results->start) >> results->shift;
                    if (results->values[j] == lrint(item->value)) val = 1.0;
                }
            }
            if (val == 1.0) {
    #endif
                slope = item->slope * accuracy;

                oldpos = MAX2(old_x - results->start, 0) >> results->shift;
                j = MIN2((MAX2(new_x - results->start, 0) >> results->shift),
                        results->size - 1);
                val = newval - item->slope *
                        (double) MAX2(new_x - results->end, 0);
                if (oldpos == j) oldpos--;
                for (; j>oldpos; --j, val-=slope) {
                    int ival = lrint(val);
                    if (results->values[j] < ival) {
                        results->values[j] = ival;
                        results->transpositions[j] = y;
                    }
                }
                item->value = newval;
    #if (ACCURATE_P3_INTERPOLATION == 2)
            } else item->value = newval;
    #endif
        }
#else
        int new_x = x + pnotes[0].strt;
        item->value = newval;
        if ((new_x >= results->start) && (new_x < results->end)) {
            int inewval = lrint(newval);
            new_x = (new_x - results->start) >> results->shift;
            if (results->values[new_x] < inewval) {
                results->values[new_x] = inewval;
                results->transpositions[new_x] = y;
            }
        }

#endif /* ACCURATE_P3_INTERPOLATION */

        item->prev_x = x;

        /* Adjust slope */
        switch (v->position) {
            case P3_POSITION_PS_TE:
            case P3_POSITION_PE_TS:
                item->slope += 1.0;
                break;
            case P3_POSITION_PS_TS:
                item->slope -= onset_slope + onset_slope;
            case P3_POSITION_PE_TE:
                item->slope -= 1.0;
                break;
            case P3_POSITION_ONSET_START:
            case P3_POSITION_ONSET_END:
                item->slope += onset_slope;
                break;
        }

#if 0
        if (v->text_is_start) {
            if (v->pattern_is_start) {
                item->slope -= (((double) MIN2(v->tdur, v->pdur)) /
                        ((double) v->pdur));

            } else {
                item->slope += ((double) MIN2(v->tdur, v->pdur)) /
                        ((double) v->pdur);
            }
        } else {
            if (v->pattern_is_start) {
                item->slope += ((double) MIN2(v->tdur, v->pdur)) / ((double) v->pdur);
            } else {
                item->slope -= ((double) MIN2(v->tdur, v->pdur)) / ((double) v->pdur);
            }
        }
#endif

        /* Move the pointer and insert a new translation vector according to
         * the turning point type. */
        if (v->target_index < num_tpoints - 1) {
            const vector *patp = &pnotes[v->pattern_index];
            const TurningPoint *startp, *endp;
            v->target_index++;
            startp = &note_starts[v->target_index];
            endp = &note_ends[v->target_index];
 
            switch (v->position) {
            case P3_POSITION_PS_TS:
                v->x = (int) startp->x - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PE_TS:
                v->x = (int) startp->x - (int) patp->strt - (int) patp->dur;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PS_TE:
                v->x = (int) endp->x - (int) patp->strt;
                v->y = (int) endp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PE_TE:
                v->x = (int) endp->x - (int) patp->strt - (int) patp->dur;
                v->y = (int) endp->y - (int) patp->ptch;
                break;
            case P3_POSITION_ONSET_START:
                v->x = (int) startp->x - onset_delta - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            case P3_POSITION_ONSET_END:
                v->x = (int) startp->x + onset_delta - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            }

            min->key1 = v->x;
            pq_update_key1(pq, min);
        } else {
            /* 'Remove' a translation vector by making it very large.
             * It won't be extracted since there will be only as many loops as 
             * there are real vectors. */
            min->key1 = UINT_MAX;
            pq_update_key1(pq, min);
        }
    }

    /* Free the reserved memory. */
    pq_free(pq);
    free(translation_vectors);
    free(verticaltranslationtable);
}


/** 
 * A modified version of the geometric P3 symbolic music retrieval algorithm
 * for score alignment.
 *
 * @param p3s song to scan
 * @param pattern pattern song
 */
void align_turningpoints_p3opt(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results, int onset_delta, double onset_slope,
        int pattern_width, int ltrans) {

    int i, j, num_loops;
    FVerticalTranslationTableItem *verticaltranslationtable;
    FVerticalTranslationTableItem *item = NULL;
    TurningPoint *note_starts = p3s->note_starts;
    TurningPoint *note_ends = p3s->note_ends;
    int num_tpoints = p3s->num_notes;
    int pattern_size = pattern->size;
    double accuracy = (double) (1 << results->shift);
    vector *pnotes = pattern->notes;

#ifdef SELECT_OPTIMAL_PQ_FUNCTIONS
    void (* pq32_update_key)(const pq32root *pq, const pq32node *n);
#endif


    /* Calculate a suitable padding to ensure that keys inserted to
       the priority queue are positive */
    int padding = onset_delta + pattern_width * 2;

    /* Create a priority queue */
    pq32root *pq;
    pq32node *min;
    TranslationVector *translation_vectors;
    
    if ((pattern_size <= 0) || (num_tpoints <= 0)) return;

    num_loops = pattern_size * 6;

    pq = pq32_create(num_loops);
#ifdef SELECT_OPTIMAL_PQ_FUNCTIONS
    pq32_update_key = pq32_select_optimal_update(pq);
/*#else
    pq32_update_key = pq32_update_unrolled;*/
#endif

    translation_vectors = (TranslationVector *)
            malloc(num_loops * sizeof(TranslationVector));

    /* Initialize a vertical translation array */
    verticaltranslationtable = (FVerticalTranslationTableItem *) malloc(
            NOTE_PITCHES * 2 * sizeof(FVerticalTranslationTableItem));
    for (i = 0; i < (NOTE_PITCHES * 2); i++) {
        verticaltranslationtable[i].value = 0.0;
        verticaltranslationtable[i].slope = 0.0;
        verticaltranslationtable[i].prev_x = 0;
    }

    /*printf("P3 Notes: %d\n", p3s->size);*/

    /* Create an array whose items have two pointers each: one for note_starts
     * and one for note_ends. Each item points to turning point array item Also
     * populate the priority queue with initial items. */

    for (i = 0, j = 0; i < pattern_size; i++) {
        TranslationVector *v;
        /*pattern_duration += (float) pnotes[i].dur;*/


        /* Add translation vectors calculated from the note start points */

        min = pq32_getnode(pq, j);
        v = &translation_vectors[j];
        v->target = note_starts;
        v->target_index = 0;
        v->pattern = pnotes;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x -
                (int) (pnotes[i].strt + pnotes[i].dur);
        v->position = P3_POSITION_PE_TS;
        min->key = v->x + padding;
        min->pointer = v;
        pq32_update_key(pq, min);
        ++j;

        min = pq32_getnode(pq, j);
        v = &translation_vectors[j];
        v->target = note_starts;
        v->target_index = 0;
        v->pattern = pnotes;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - (int) pnotes[i].strt;
        if ((onset_slope > 0) && (pnotes[i].strt >= 0))
            v->position = P3_POSITION_ONSET_TOP;
        else 
            v->position = P3_POSITION_PS_TS;
        min->key = v->x + padding;
        min->pointer = v;
        pq32_update_key(pq, min);
        ++j;


        /* Add translation vectors calculated from the note end points */

        min = pq32_getnode(pq, j);
        v = &translation_vectors[j];
        v->target = note_ends;
        v->target_index = 0;
        v->pattern = pnotes;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) (pnotes[i].strt +
                pnotes[i].dur);
        v->position = P3_POSITION_PE_TE;
        min->key = v->x + padding;
        min->pointer = v;
        pq32_update_key(pq, min);
        ++j;

        min = pq32_getnode(pq, j);
        v = &translation_vectors[j];
        v->target = note_ends;
        v->target_index = 0;
        v->pattern = pnotes;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TE;
        min->key = v->x + padding;
        min->pointer = v;
        pq32_update_key(pq, min);
        ++j;


        /* Add translation vectors for onset weighting */

        if ((onset_slope > 0) && (pnotes[i].strt >= 0)) {
            min = pq32_getnode(pq, j);
            v = &translation_vectors[j];
            v->target = note_starts;
            v->target_index = 0;
            v->pattern = pnotes;
            v->pattern_index = i;
            v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
            v->x = (int) note_starts[0].x - onset_delta - (int) pnotes[i].strt;
            v->position = P3_POSITION_ONSET_START;
            min->key = v->x + padding;
            min->pointer = v;
            pq32_update_key(pq, min);
            ++j;

            min = pq32_getnode(pq, j);
            v = &translation_vectors[j];
            v->target = note_starts;
            v->target_index = 0;
            v->pattern = pnotes;
            v->pattern_index = i;
            v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
            v->x = (int) note_starts[0].x + onset_delta - (int) pnotes[i].strt;
            v->position = P3_POSITION_ONSET_END;
            min->key = v->x + padding;
            min->pointer = v;
            pq32_update_key(pq, min);
            ++j;
        }
    }

    num_loops = j * num_tpoints;

    for (i = 0; i < num_loops; i++) {
        TranslationVector *v;
        int x, l;
        double newval;

        /* Get the minimum element */
        min = pq32_getmin(pq);
        if (min->pointer == NULL) return;
        v = (TranslationVector *) min->pointer;
        x = v->x;

        /* Update the overlapping values at all local transposition levels */
        for (l=-ltrans; l<=ltrans; ++l) {
            int y;
            int old_x, new_x, oldpos;
            double val, slope, slope_diff;
            double ltw = LOCAL_TRANSPOSITION_WEIGHTS[ABS(l)];

            if (ltw <= 0.0) continue;

            y = v->y + l;
            if ((y < -128) || (y >= 128)) continue;

            item = &verticaltranslationtable[NOTE_PITCHES + y];

            /* Adjust slope */
            slope_diff = 0.0;
            switch (v->position) {
                case P3_POSITION_PS_TE:
                case P3_POSITION_PE_TS:
                    slope_diff += 1.0;
                    break;
                case P3_POSITION_ONSET_TOP:
                    slope_diff -= onset_slope + onset_slope;
                case P3_POSITION_PS_TS:
                case P3_POSITION_PE_TE:
                    slope_diff -= 1.0;
                    break;
                case P3_POSITION_ONSET_START:
                case P3_POSITION_ONSET_END:
                    slope_diff += onset_slope;
                    break;
            }

            if (x == item->prev_x) {
                item->slope += slope_diff * ltw;
                continue;
            }

            newval = item->value + item->slope * (double)
                    (x - item->prev_x);

#ifdef ACCURATE_P3_INTERPOLATION

            old_x = item->prev_x + pnotes[0].strt;
            new_x = x + pnotes[0].strt;

    #if (ACCURATE_P3_INTERPOLATION == 2)
            val = 0.0;
            if (new_x >= 0) {
                if ((new_x >= results->start) && (new_x < results->end)) {
                    j = (new_x - results->start) >> results->shift;
                    if (results->values[j] < lrint(newval)) val = 1.0;
                }
            }
            if ((val == 0) && (old_x >= 0)) {
                if ((old_x >= results->start) && (old_x < results->end)) {
                    j = (old_x - results->start) >> results->shift;
                    if (results->values[j] == lrint(item->value)) val = 1.0;
                }
            }
            if (val == 1.0) {
    #endif
                slope = item->slope * accuracy;

                oldpos = MAX2(old_x - results->start, 0) >> results->shift;
                j = MIN2((MAX2(new_x - results->start, 0) >> results->shift),
                        results->size - 1);
                val = newval - item->slope *
                        (double) MAX2(new_x - results->end, 0);
                if (oldpos == j) oldpos--;
                for (; j>oldpos; --j, val-=slope) {
                    int ival = lrint(val);
                    if (results->values[j] < ival) {
                        results->values[j] = ival;
                        results->transpositions[j] = y;
                    }
                }
                item->value = newval;
    #if (ACCURATE_P3_INTERPOLATION == 2)
            } else item->value = newval;
    #endif
#else
            new_x = x + pnotes[0].strt;
            item->value = newval;
            if ((new_x >= results->start) && (new_x < results->end)) {
                int inewval = lrint(newval);
                new_x = (new_x - results->start) >> results->shift;
                if (results->values[new_x] < inewval) {
                    results->values[new_x] = inewval;
                    results->transpositions[new_x] = y;
                }
            }

#endif /* ACCURATE_P3_INTERPOLATION */

            item->prev_x = x;
            item->slope += slope_diff * ltw;
        }

        /* Move the pointer and insert a new translation vector according to
         * the turning point type. */
        v->target_index++;
        if ((lnotes != NULL) && (pattern_lnotes != NULL)) {
            while (v->target_index < num_tpoints) {
                const TurningPoint *tp = &v->target[v->target_index];
                if ((lnotes[tp->note].before &
                        pattern_lnotes[v->pattern_index].before) ||
                    (lnotes[tp->note].after &
                        pattern_lnotes[v->pattern_index].after)) {
                    break;
                } else {
                    ++v->target_index;
                    ++i;
                }
            }
        }
        if (v->target_index < num_tpoints) {
            const vector *patp = &v->pattern[v->pattern_index];
            const TurningPoint *tp = &v->target[v->target_index];

            switch (v->position) {
            case P3_POSITION_ONSET_TOP:
            case P3_POSITION_PS_TS:
            case P3_POSITION_PS_TE:
                v->x = (int) tp->x - (int) patp->strt;
                v->y = (int) tp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PE_TS:
            case P3_POSITION_PE_TE:
                v->x = (int) tp->x - (int) patp->strt - (int) patp->dur;
                v->y = (int) tp->y - (int) patp->ptch;
                break;
            case P3_POSITION_ONSET_START:
                v->x = (int) tp->x - onset_delta - (int) patp->strt;
                v->y = (int) tp->y - (int) patp->ptch;
                break;
            case P3_POSITION_ONSET_END:
                v->x = (int) tp->x + onset_delta - (int) patp->strt;
                v->y = (int) tp->y - (int) patp->ptch;
                break;
            }

            min->key = v->x + padding;
            /*min->value = i;*/
        } else {
            /* 'Remove' a translation vector by making it very large.
             * It won't be extracted since there will be only as many loops as 
             * there are real vectors. */
            min->key = UINT_MAX;
        }
        pq32_update_key(pq, min);
    }

    /* Free the reserved memory. */
    pq32_free(pq);
    free(translation_vectors);
    free(verticaltranslationtable);
}


static void scale_pattern(song *scaled_p, song *p, alignmentmap *map,
                          int map_pos, double scale) {
    int i, total_duration = 0;
    int end = 0;

    if (map != NULL) {
        double pattern_start, factor;
        pattern_start = map->initial_alignment[map_pos];
        factor = 1.0 / map->accuracy;

        for (i=0; i<p->size; ++i) {
            vector *sn, *n;
            int    j, pos;
            double s1, s2;
            sn = &scaled_p->notes[i];
            n  = &p->notes[i];

            pos = n->strt >> map->accuracy_shift;
            j = map_pos + pos;
            if (j >= map->height) {
                pos -= j - map->height + 1;
                j = map->height - 1;
            }
            s1 = map->initial_alignment[j] + (map->initial_alignment[j + 1] -
                 map->initial_alignment[j]) * factor *
                 (double) (n->strt - (pos << map->accuracy_shift));

            pos = (n->strt + n->dur) >> map->accuracy_shift;
            j = map_pos + pos;
            if (j >= map->height) {
                pos -= j - map->height + 1;
                j = map->height - 1;
            } 
            s2 = map->initial_alignment[j] + (map->initial_alignment[j + 1] -
                 map->initial_alignment[j]) * factor *
                 (double) (n->strt + n->dur - (pos << map->accuracy_shift));

            if (n->strt < 0) sn->strt = -1;
            else sn->strt = lrint(scale * (s1 - pattern_start));
            sn->dur = lrint(scale * (s2 - s1));
            sn->ptch = n->ptch;
            total_duration += sn->dur;
            end = MAX2(end, sn->strt + sn->dur);
        }
    } else {
        for (i=0; i<p->size; ++i) {
            vector *sn, *n;
            sn = &scaled_p->notes[i];
            n  = &p->notes[i];

            if (n->strt < 0) sn->strt = -1;
            else sn->strt = lrint(scale * (double) n->strt);
            sn->dur = lrint(scale * (double) n->dur);            
            sn->ptch = n->ptch;
            total_duration += sn->dur;
            end = MAX2(end, sn->strt + sn->dur);
        }
    }

    scaled_p->size = p->size;
    scaled_p->total_note_duration = total_duration;
    scaled_p->start = 0;
    scaled_p->end = end;
}


void init_linecache(linecache *lc, int num_lines, int line_length,
        alignmentmap *map, int map_pos, double scale) {
    int i;
    double accuracy_factor = 1.0 / (double) map->accuracy;

    memset(lc, 0, sizeof(linecache));

    num_lines = MAX2(num_lines, 1);
    lc->num_lines = num_lines;
    lc->num_used_lines = 0;
    lc->line_length = line_length;
    lc->next_line = 0;
    lc->last_line = -1;
    lc->last_line_pos = 0;
    lc->linebuf_pos = 0;

    lc->last_alignment_pos = map_pos;
    lc->initial_alignment = (int *) malloc(map->height * sizeof(int));
    if (lc->initial_alignment == NULL) {
        fputs("Error: unable to allocate memory in init_linecache\n", stderr);
        memset(lc, 0, sizeof(linecache));
        return;
    }
    for (i=0; i<map->height; ++i) {
        double a = scale * accuracy_factor *
                (map->initial_alignment[i] + map->initial_alignment_offset[i]);
        lc->initial_alignment[i] = (int) lround(a);
    }

    lc->linebuf_data = (unsigned char *) malloc(2 * line_length *
            sizeof(unsigned char));
    lc->linebuf[0] = lc->linebuf_data;
    lc->linebuf[1] = &lc->linebuf_data[line_length];
    lc->linediff = (float *) malloc(2 * line_length * sizeof(float));
    lc->linediff_base = (int *) malloc(2 * line_length * sizeof(int));

    lc->line = (int *) calloc(line_length, sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
    lc->line_factor = (int *) calloc(line_length, sizeof(int));
#endif

    lc->prev_line_view_left = (int *) malloc(num_lines * sizeof(int));
    lc->prev_line_view_right = (int *) malloc(num_lines * sizeof(int));
    lc->prev_line_pos = (int *) malloc(num_lines * sizeof(int));
    lc->prev_lines = (unsigned char **) malloc(num_lines *
            sizeof(unsigned char *));

    if ((lc->line == NULL) || (lc->linediff == NULL) ||
            (lc->linediff_base == NULL) ||
            (lc->linebuf[0] == NULL) ||
#ifdef LINECACHE_COUNT_LINE_FACTOR
            (lc->line_factor == NULL) ||
#endif
            (lc->prev_line_view_left == NULL) ||
            (lc->prev_line_view_right == NULL) ||
            (lc->prev_line_pos == NULL) ||
            (lc->prev_lines == NULL)) {
        fputs("Error: unable to allocate memory in init_linecache\n", stderr);
        free(lc->line);
        free(lc->linebuf_data);
        free(lc->linediff);
        free(lc->linediff_base);
#ifdef LINECACHE_COUNT_LINE_FACTOR
        free(lc->line_factor);
#endif
        free(lc->prev_line_view_left);
        free(lc->prev_line_view_right);
        free(lc->prev_line_pos);
        free(lc->prev_lines);
        memset(lc, 0, sizeof(linecache));
        return;
    }
 
    lc->prev_lines[0] = (unsigned char *) malloc(num_lines * line_length *
                sizeof(unsigned char));
    for (i=1; i<num_lines; ++i) {
        lc->prev_lines[i] = lc->prev_lines[i-1] + line_length *
                sizeof(unsigned char);
    }
}

void free_linecache(linecache *lc) {
    free(lc->initial_alignment);
    free(lc->line);
#ifdef LINECACHE_COUNT_LINE_FACTOR
    free(lc->line_factor);
#endif
    free(lc->linebuf_data);
    free(lc->linediff);
    free(lc->linediff_base);
    free(lc->prev_lines[0]);
    free(lc->prev_lines);
    free(lc->prev_line_view_left);
    free(lc->prev_line_view_right);
    free(lc->prev_line_pos);
    memset(lc, 0, sizeof(linecache));
}


void update_linecache(linecache *lc, const ap3results *aresult,
        int map_pos, alignmentmap *map) {
    int i, j;
    int delta;
    int *avalues = aresult->values;
    int astart = ASHIFTR(aresult->start, aresult->shift);
    unsigned char *linebuf_next = lc->linebuf[(lc->linebuf_pos + 1) & 1];
    unsigned char *linebuf_last = lc->linebuf[lc->linebuf_pos];
    unsigned char *rline = lc->prev_lines[lc->next_line];
    int len = lc->line_length;
    int curlen = MIN2(len, aresult->size);
    unsigned char *mapdata = &map->vbuffer[map_pos * map->width];

#ifndef LINECACHE_COUNT_LINE_FACTOR
    float lf = 1.0F / (float) MIN2(lc->num_used_lines+1, lc->num_lines);
#endif

    delta = (lc->initial_alignment[map_pos] -
             lc->initial_alignment[lc->last_alignment_pos]) -
            (astart - lc->last_line_pos);


    /* Shift values to the correct position */

    if (ABS(delta) < len) {
        if (delta < 0) {
            for (i=0, j=-delta; j<len; ++i, ++j) {
                lc->line[i] = lc->line[j];
#ifdef LINECACHE_COUNT_LINE_FACTOR
                lc->line_factor[i] = lc->line_factor[j];
#endif
            }
            memset(&lc->line[len+delta], 0, (-delta) * sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
            memset(&lc->line_factor[len+delta], 0, (-delta) * sizeof(int));
#endif
        } else if (delta > 0) {
            for (i=len-1, j=len-1-delta; j>=0; --i, --j) {
                lc->line[i] = lc->line[j];
#ifdef LINECACHE_COUNT_LINE_FACTOR
                lc->line_factor[i] = lc->line_factor[j];
#endif
            }
            memset(lc->line, 0, delta * sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
            memset(lc->line_factor, 0, delta * sizeof(int));
#endif
        }
    } else {
        memset(lc->line, 0, len * sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
        memset(lc->line_factor, 0, len * sizeof(int));
#endif
    }

    if (delta < 0) {
        for (i=0; i<lc->num_used_lines; ++i) {
            lc->prev_line_view_left[i] += MAX2(0,
                    -delta - lc->prev_line_pos[i]);
            lc->prev_line_pos[i] = MAX2(0, lc->prev_line_pos[i] + delta);
        }
    } else {
        for (i=0; i<lc->num_used_lines; ++i) {
            lc->prev_line_pos[i] += delta;
            lc->prev_line_view_right[i] -= MAX2(0, lc->prev_line_pos[i] +
                    lc->prev_line_view_right[i] -
                    lc->prev_line_view_left[i] - len);
        }
    }


    /* Add new data */

    for (i=0; i<curlen; ++i) {
        lc->line[i] += avalues[i];
#ifdef LINECACHE_COUNT_LINE_FACTOR
        ++lc->line_factor[i];
#endif
    }
    for (i=curlen; i<len; ++i) {
#ifdef LINECACHE_COUNT_LINE_FACTOR
        ++lc->line_factor[i];
#endif
    }


    /* Remove oldest line */

    if (lc->num_used_lines >= lc->num_lines) {
        int sp, rline_sp, rline_ep;

        rline_sp = lc->prev_line_view_left[lc->next_line];
        rline_ep = lc->prev_line_view_right[lc->next_line];
        sp = lc->prev_line_pos[lc->next_line];
        for(; rline_sp < rline_ep; ++rline_sp, ++sp) {
            lc->line[sp] -= rline[rline_sp];
#ifdef LINECACHE_COUNT_LINE_FACTOR
            lc->line_factor[sp]--;
#endif
        }
    }


    /* Write data to the alignment map */
    
    for (i=0; i<len; ++i) {
#ifdef LINECACHE_COUNT_LINE_FACTOR
        unsigned char val = lc->line[i] / MAX2(1, lc->line_factor[i]);
#else
        unsigned char val = (unsigned char) lrintf(lf * (float) lc->line[i]);
#endif
        linebuf_next[i] = val;
        mapdata[i] = MAX2(mapdata[i], val);
/*        if (lc->line_factor[i] > lc->num_used_lines+1)
            printf("LF: %d/%d, %d\n", lc->line_factor[i], lc->num_used_lines, i); */
    }


    /* Interpolate over skipped lines */

    if (map_pos - lc->last_alignment_pos > 1) {
        float num_skipped_lines = (float) (map_pos - lc->last_alignment_pos);

        if ((delta < -len) || (delta > len)) {
            for (i=lc->last_alignment_pos+1; i<map_pos; ++i) {
                unsigned char *skippedline = &map->vbuffer[i * map->width];
                int line_pos = map->lines[i].target_time >> aresult->shift;
                int d1 = (lc->initial_alignment[i] -
                          lc->initial_alignment[lc->last_alignment_pos]) -
                         (line_pos - lc->last_line_pos);
                int d2 = (lc->initial_alignment[map_pos] -
                          lc->initial_alignment[i]) -
                         (astart - line_pos);
                if ((d1 < 0) && (d1 > -len)) {
                    int p = 0;
                    for (j=-d1; j<len; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_last[j]);
                } else if ((d1 >= 0) && (d1 < len)) {
                    int p = d1;
                    for (j=0; j<len-d1; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_last[j]);
                }
                if ((d2 < 0) && (d2 > -len)) {
                    int p = -d2;
                    for (j=0; j<len+d2; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_next[j]);
                } else if ((d2 >= 0) && (d2 < len)) {
                    int p = 0;
                    for (j=d2; j<len; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_next[j]);
                }
            }
        } else {
            int linediff_lastpos = 0;
            if (delta >= 0) {
                linediff_lastpos = delta;
                for (i=0; i<delta; ++i) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_next[i];
                }
                for (i=delta, j=0; i<len; ++i, ++j) {
                    lc->linediff[i] = (float) linebuf_next[i] - (float)
                            linebuf_last[j];
                    lc->linediff_base[i] = linebuf_last[j];
                }
                for (i=len, j=len-delta; i<len+delta; ++i, ++j) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_last[j];
                }
            } else {
                for (i=0; i<(-delta); ++i) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_last[i];
                }
                for (i=(-delta), j=0; i<len; ++i, ++j) {
                    lc->linediff[i] = (float) linebuf_next[j] -
                            (float) linebuf_last[i];
                    lc->linediff_base[i] = linebuf_last[i];
                }
                for (i=len, j=len+delta; i<len-delta; ++i, ++j) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_next[j];
                }
            }
            for (i=lc->last_alignment_pos+1; i<map_pos; ++i) {
                float ifactor = (float) (i - lc->last_alignment_pos) /
                        num_skipped_lines;
                unsigned char *skippedline = &map->vbuffer[i * map->width];
                int line_pos = map->lines[i].target_time >> aresult->shift;
                int d = (lc->initial_alignment[i] -
                         lc->initial_alignment[lc->last_alignment_pos]) -
                        (line_pos - lc->last_line_pos);
                int p, endp;
                j = linediff_lastpos - d;
                if (j < 0) {
                    p = -j;
                    j = 0;
                    endp = len;
                } else {
                    p = 0;
                    endp = MIN2(len, len + ABS(delta) - j);
                }
                for (; p<endp; ++p, ++j) {
                    skippedline[p] = MAX2(skippedline[p], lc->linediff_base[j] +
                            (int) (ifactor * lc->linediff[j]));
                }
            }
        }
    }


    /* Store current line for future reference */

    for (i=0; i<curlen; ++i) {
        rline[i] = (unsigned char) avalues[i];
    }
    for (i=curlen; i<len; ++i) {
        rline[i] = 0;
    }

    lc->prev_line_pos[lc->next_line] = 0;
    lc->prev_line_view_left[lc->next_line] = 0;
    lc->prev_line_view_right[lc->next_line] = len;


    /* Update state */

    lc->last_alignment_pos = map_pos;
    lc->last_line_pos = astart;

    lc->linebuf_pos = (lc->linebuf_pos + 1) & 1;
    lc->num_used_lines = MIN2(lc->num_lines, lc->num_used_lines + 1);
    lc->last_line++;
    if (lc->last_line >= lc->num_lines) lc->last_line = 0;
    lc->next_line++;
    if (lc->next_line >= lc->num_lines) lc->next_line = 0;
}

static void scan_local_notes_31(const song *s, ap3localnotes *lnotes,
        int window_width, int include_self) {
    int i;
    int pos = 0;
    int offset[128];
    /*unsigned int playing_notes[4];*/
    pq32root *pq;

    pq = pq32_create(128);
    assert(pq->levels == 7);
    for (i=0; i<128; ++i) {
        offset[i] = -1;
    }
    for (i=0; i<s->size; ++i) {
        int j, note;
        vector *ni = &s->notes[i];
        int start = ni->strt;
        int end = ni->strt + ni->dur;
        int left_edge = start - window_width;
        int right_edge = end + window_width;
        int pitch = ni->ptch;
        pq32node *node;
        unsigned int notes31_left = 0, notes31_right = 0;

        while (1) {
            node = pq32_getmin(pq);
            if ((int) node->key <= start) {
                if (offset[node->index] <= (int) node->key) {
/*                    int pitch_h, pitch_l;
*/
                    offset[node->index] = -1;
/*
                    pitch_h = node->index >> 5;
                    pitch_l = node->index - (pitch_h << 5);
                    playing_notes[pitch_h] &= (unsigned int) ~(1 << pitch_l);
*/
                }
                node->key = INT_MAX;
                pq32_update_7(pq, node);
            } else break;
        }
        while (pos < s->size) {
            vector *np = &s->notes[pos];
            if (np->strt < left_edge) {
                if ((np->strt + np->dur) > left_edge) {
/*                    int pitch_h, pitch_l;
*/
                    node = pq32_getnode(pq, (int) np->ptch);
                    node->key = np->strt + np->dur + window_width;
                    pq32_update_7(pq, node);
                    offset[node->index] = node->key;
/*
                    pitch_h = np->ptch >> 5;
                    pitch_l = np->ptch - (pitch_h << 5);
                    playing_notes[pitch_h] |= (unsigned int) 1 << pitch_l;
*/
                }
                pos++;
            } else break;
        }
        note = 0;
        for (j=pitch+1; j<pitch+16; ++j, ++note) {
            if (offset[j] >= 0) {
                notes31_left |= 1 << note;
            }
        }
        notes31_left <<= 16;
        note = 14;
        for (j=pitch-1; j>=pitch-16; --j, --note) {
            if (offset[j] >= 0) {
                notes31_left |= 1 << note;
            }
        }

        for (j=pos; j<s->size; ++j) {
            vector *np = &s->notes[j];
            if (np->strt >= right_edge) break;
            if (np->strt <= start) {
                if ((np->ptch > pitch) && (np->ptch < pitch + 16)) {
                    notes31_left |= 1 << (16 + (np->ptch - pitch - 1));
                } else if ((np->ptch < pitch) && (np->ptch > pitch - 16)) {
                    notes31_left |= 1 << (14 - (pitch - np->ptch - 1));
                } else if ((j != i) || include_self) {
                    notes31_left |= 1 << 15;
                }
            }
            if (np->strt >= start) {
                if ((np->ptch > pitch) && (np->ptch < pitch + 16)) {
                    notes31_right |= 1 << (16 + (np->ptch - pitch - 1));
                } else if ((np->ptch < pitch) && (np->ptch > pitch - 16)) {
                    notes31_right |= 1 << (14 - (pitch - np->ptch - 1));
                } else if ((j != i) || include_self) {
                    notes31_right |= 1 << 15;
                }
            }
        }
        lnotes[i].before = notes31_left;
        lnotes[i].after = notes31_right;

    }
}


static void scan_local_notes_25(const song *s, ap3localnotes *lnotes,
        int window_width, int include_self) {
    int i;
    int pos = 0;
    int offset[128];
    /*unsigned int playing_notes[4];*/
    pq32root *pq;

    pq = pq32_create(128);
    assert(pq->levels == 7);
    for (i=0; i<128; ++i) {
        offset[i] = -1;
    }
    for (i=0; i<s->size; ++i) {
        int j, note;
        vector *ni = &s->notes[i];
        int start = ni->strt;
        int end = ni->strt + ni->dur;
        int left_edge = start - window_width;
        int right_edge = end + window_width;
        int pitch = ni->ptch;
        pq32node *node;
        unsigned int notes25_left = 0, notes25_right = 0;

        while (1) {
            node = pq32_getmin(pq);
            if ((int) node->key <= start) {
                if (offset[node->index] <= (int) node->key) {
/*                    int pitch_h, pitch_l;
*/
                    offset[node->index] = -1;
/*
                    pitch_h = node->index >> 5;
                    pitch_l = node->index - (pitch_h << 5);
                    playing_notes[pitch_h] &= (unsigned int) ~(1 << pitch_l);
*/
                }
                node->key = INT_MAX;
                pq32_update_7(pq, node);
            } else break;
        }
        while (pos < s->size) {
            vector *np = &s->notes[pos];
            if (np->strt < left_edge) {
                if ((np->strt + np->dur) > left_edge) {
/*                    int pitch_h, pitch_l;
*/
                    node = pq32_getnode(pq, (int) np->ptch);
                    node->key = np->strt + np->dur + window_width;
                    pq32_update_7(pq, node);
                    offset[node->index] = node->key;
/*
                    pitch_h = np->ptch >> 5;
                    pitch_l = np->ptch - (pitch_h << 5);
                    playing_notes[pitch_h] |= (unsigned int) 1 << pitch_l;
*/
                }
                pos++;
            } else break;
        }
        note = 0;
        for (j=pitch+1; j<128; ++j, ++note) {
            if (note == 12) note = 0;
            if (offset[j] >= 0) {
                notes25_left |= 1 << note;

            }
        }
        notes25_left <<= 13;
        note = 11;
        for (j=pitch-1; j>=0; --j, --note) {
            if (note == -1) note = 11;
            if (offset[j] >= 0) {
                notes25_left |= 1 << note;
            }
        }

        for (j=pos; j<s->size; ++j) {
            vector *np = &s->notes[j];
            if (np->strt >= right_edge) break;
            if (np->strt <= start) {
                if (np->ptch > pitch) {
                    notes25_left |= 1 << (13 + (np->ptch - pitch - 1) % 12);
                } else if (np->ptch < pitch) {
                    notes25_left |= 1 << (11 - (pitch - np->ptch - 1) % 12);
                } else if ((j != i) || include_self) {
                    notes25_left |= 1 << 12;
                }
            }
            if (np->strt >= start) {
                if (np->ptch > pitch) {
                    notes25_right |= 1 << (13 + (np->ptch - pitch - 1) % 12);
                } else if (np->ptch < pitch) {
                    notes25_right |= 1 << (11 - (pitch - np->ptch - 1) % 12);
                } else if ((j != i) || include_self) {
                    notes25_right |= 1 << 12;
                }
            }
        }
        lnotes[i].before = notes25_left;
        lnotes[i].after = notes25_right;

    }
}


/**
 * Maps local alignments of two songs with the geometric P3 algorithm.
 *
 * @param sc song collection that possibly contains the song in the p3song
 *        format that this function uses. If sc is NULL, necessary conversions
 *        will be made before further processing.
 * @param s song to align the pattern with. If either of the aligned songs
 *        is the "correct" score, it should be given here to improve alignment
 *        results of the P3 algorithm
 * @param p a pattern song that is aligned with the other song. This should
 *        be the piece that may contain more errors.
 * @param parameters alignment parameters
 * @param map the resulting alignment map will be written to this structure 
 */
void align_p3(alignmentmap *map, int alg, const alignparameters *parameters) {

    int i;
    int pattern_skip = 0;
    song target_rests, pattern_rests;
    p3songcollection *p3sc = map->sc->data[DATA_P3];
    p3song *p3s, p3s_rests;
    int map_width_ms = map->width << map->accuracy_shift;
    int pattern_window_width = parameters->p3_pattern_window_width;
    int num_lines = pattern_window_width /
            parameters->p3_pattern_window_skip;
    song scaled_pattern, scaled_pattern_rests;
    song_window pattern_window, pattern_rests_window;
    p3s_window target_window, target_rests_window;
    ap3results aresult, rests_aresult;
    int num_scales = parameters->p3_scales_num * 2 - 1;
    double scales_spread = parameters->p3_scales_spread;
    double scales_step = (scales_spread - 1.0) /
                            ((double) MAX2(1, (parameters->p3_scales_num - 1)));
    double *scales;
    linecache *lc;
    ap3localnotes **lnotes = NULL;
    ap3localnotes *pattern_lnotes = NULL;
    double ltwsum = 1.0;
    int ltrans = 0;
    
#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
    int window_notes[1024];
    memset(window_notes, 0, 1024*sizeof(int));
#endif

    if ((alg != ALG_ALIGN_P3) && (alg != ALG_ALIGN_P3_ONSET) &&
            (alg != ALG_ALIGN_P3_OPT)) {
        fprintf(stderr, "Error in align_p3: invalid algorithm id: %d", alg);
        return;
    }
    if ((p3sc == NULL) || (map->target->id >= p3sc->size)) {
        fputs("Error in align_p3: song collection does not contain P3 data.\nUse update_song_collection_data() to generate it.\n", stderr);
        return;
    }
    p3s = &p3sc->p3_songs[map->target->id];
    if (p3s == NULL) {
        fputs("Error in align_p3: empty P3 song data", stderr);
        return;
    }


    init_ap3results(&aresult, map->width, map->accuracy_shift);
    if (aresult.values == NULL) {
        fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
        return;
    }

    if (parameters->p3_align_rests) {
        memset(&target_rests, 0, sizeof(song));
        memset(&pattern_rests, 0, sizeof(song));

        fputs("Extracting rests from the target song...\n", stderr);
        extract_song_rests(map->target, 0, map->target->end, &target_rests);
        song_to_p3(&target_rests, &p3s_rests);

        fputs("Extracting rests from the pattern...\n", stderr);
        extract_song_rests(map->pattern, 0, map->pattern->end, &pattern_rests);

        init_ap3results(&rests_aresult, map->width, map->accuracy_shift);
        if (rests_aresult.values == NULL) {
            fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
            free_ap3results(&aresult);
            return;
        }
    }

    scales = (double *) malloc(num_scales * sizeof(double));
    if (scales == NULL) {
        fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
        return;
    }
    for (i=parameters->p3_scales_num; i > 1; --i) {
        scales[parameters->p3_scales_num + i - 2] = scales_spread;
        scales[parameters->p3_scales_num - i] = 1.0 / scales_spread;
        scales_spread -= scales_step;
    }
    scales[parameters->p3_scales_num - 1] = 1.0F;

    fprintf(stderr, "\nUsing %d scaled pattern variations:\n  (%f",
            num_scales, scales[0]);
    for (i=1; i<num_scales; ++i) fprintf(stderr, ", %f", scales[i]);
    fprintf(stderr, ")\n");

    if (parameters->p3_window_heuristics) {
        lnotes = (ap3localnotes **) malloc(num_scales * sizeof(ap3localnotes *));
        pattern_lnotes = (ap3localnotes *) malloc(map->pattern->size *
                                                  sizeof(ap3localnotes));
        if ((lnotes == NULL) || (pattern_lnotes == NULL)) {
            fputs("Error in align_p3: failed to allocate a buffer", stderr);
            free(lnotes);
            free(pattern_lnotes);
            free(scales);
            return;
        }
        lnotes[0] = (ap3localnotes *) malloc(num_scales * map->target->size *
                sizeof(ap3localnotes));
        if (lnotes[0] == NULL) {
            fputs("Error in align_p3: failed to allocate a buffer", stderr);
            free(lnotes[0]);
            free(lnotes);
            free(pattern_lnotes);
            free(scales);
            return;
        }
    }
    lc = (linecache *) malloc(num_scales * sizeof(linecache));
    if (lc == NULL) {
        fputs("Error in align_p3: unable to allocate a line cache", stderr);
        free_ap3results(&aresult);
        free_ap3results(&rests_aresult);
        if (parameters->p3_window_heuristics) {
            free(lnotes[0]);
            free(lnotes);
            free(pattern_lnotes);
        }
        free(scales);
        return;
    }

    fputs("Initializing line cache and scanning local notes...", stderr);
    for (i=0; i<num_scales; ++i) {
        init_linecache(&lc[i], num_lines, map->width, map, 0, scales[i]);

        if (parameters->p3_window_heuristics) {
            lnotes[i] = &lnotes[0][i * map->target->size];
            scan_local_notes_31(map->target, lnotes[i],
                    scales[i] * ((double) pattern_window_width), 0);
/*{
                    int k;
                    printf("Song (scale: %d):\n", (int) (parameters->p3_scales[i] * (double) pattern_window_width));
                    for (k=0; k<map->target->size; ++k) {
                        vector *snote = &map->target->notes[k];
                        printf("(%d, %d, %d : %x.%x)\n", snote->strt, snote->strt + snote->dur, snote->ptch, lnotes[i][k].before, lnotes[i][k].after);
                    }
                    printf("\n");
}*/
        }
    }
    fputs(" Done.\n", stderr);

    init_song_window(&pattern_window, map->pattern, 1);
    init_p3s_window(&target_window, p3s);

    memset(&scaled_pattern, 0, sizeof(song));
    scaled_pattern.notes = (vector *) malloc(map->pattern->size *
            sizeof(vector));


    if (parameters->p3_align_rests) {
        init_song_window(&pattern_rests_window, &pattern_rests, 1);
        init_p3s_window(&target_rests_window, &p3s_rests);

        memset(&scaled_pattern_rests, 0, sizeof(song));
        scaled_pattern_rests.notes = (vector *) malloc(pattern_rests.size *
            sizeof(vector));
    }

    if (parameters->p3_max_local_transposition > 0) {
        ltrans = MIN2(MAX_LOCAL_TRANSPOSITION,
                parameters->p3_max_local_transposition);
        for (i=1; i<=ltrans; ++i) {
            ltwsum += 2.0 * LOCAL_TRANSPOSITION_WEIGHTS[i];
        }
    }

    for (i=0; i<map->height; ++i) {
        int j;
        alignmentline *mapline = &map->lines[i];
        int pattern_time = i << map->accuracy_shift;
        int target_time = mapline->target_time;
        int window_onsets = 0;


        aresult.start = target_time;
        aresult.end = target_time + map_width_ms;
        if (parameters->p3_align_rests) {
            rests_aresult.start = target_time;
            rests_aresult.end = target_time + map_width_ms;
        }
#ifdef SHOW_PROGRESS
        fprintf(stderr, "   Line %d/%d\r", i + 1, map->height);
#endif

        if ((pattern_skip <= 0) || (i == map->height - 1)) {
            move_song_window(&pattern_window, pattern_time,
                    pattern_time + pattern_window_width);
            if (parameters->p3_align_rests) {
                move_song_window(&pattern_rests_window, pattern_time,
                        pattern_time + pattern_window_width);
            }
#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
            if (pattern_window.window.size < 1023)
                window_notes[pattern_window.window.size]++;
            else window_notes[1023]++;
#endif

            for (j=0; j<num_scales; ++j) {
                double ps = scales[j];
                double s_pattern_window_width = ps * ((double)
                        pattern_window_width);
                ap3localnotes *ln = NULL, *pln = NULL;
                int norm;

                scale_pattern(&scaled_pattern, &pattern_window.window,
                              map, i, ps);

                if (j == 0) {
                    int k;
                    for (k=0; k<scaled_pattern.size; ++k) {
                        if (scaled_pattern.notes[k].strt >= 0) ++window_onsets;
                    }
                }
                if (parameters->p3_window_heuristics) {
                    ln = lnotes[j];
                    pln = pattern_lnotes;
                    scan_local_notes_31(&scaled_pattern, pln,
                            s_pattern_window_width, (scaled_pattern.size <= 1));

/*                {
                    int k;
                    printf("Pattern:\n");
                    for (k=0; k<scaled_pattern.size; ++k) {
                        vector *snote = &scaled_pattern.notes[k];
                        printf("(%d, %d, %d : %x.%x)\n", snote->strt, snote->strt + snote->dur, snote->ptch, pattern_lnotes[k].before, pattern_lnotes[k].after);
                    }
                    printf("\n");
                }
*/
                }
                move_p3s_window(&target_window, target_time,
                        target_time + map_width_ms +
                        (int) s_pattern_window_width);

                clear_ap3results(&aresult);

                norm = scaled_pattern.total_note_duration;


                switch (alg) {
                case ALG_ALIGN_P3:
                    align_turningpoints_p3(&target_window.window, ln,
                            &scaled_pattern, pln, &aresult);
                    break;

                case ALG_ALIGN_P3_ONSET:
                    align_turningpoints_p3onset(&target_window.window,
                            ln, &scaled_pattern, pln,
                            &aresult, parameters->p3_onset_delta,
                            parameters->p3_onset_slope);
                    norm += window_onsets * lrint(parameters->p3_onset_slope *
                            (double) parameters->p3_onset_delta);
                    break;

                case ALG_ALIGN_P3_OPT:
                    align_turningpoints_p3opt(&target_window.window, ln,
                            &scaled_pattern, pln, &aresult,
                            parameters->p3_onset_delta,
                            parameters->p3_onset_slope,
                            (int) s_pattern_window_width,
                            ltrans);
                    norm += window_onsets * lrint(parameters->p3_onset_slope *
                            (double) parameters->p3_onset_delta);
                    /*norm = lrint(ltwsum * (double) norm);*/
                }

                if (parameters->p3_align_rests) {
                    int k;
                    scale_pattern(&scaled_pattern_rests,
                            &pattern_rests_window.window, map, i, ps);
                    move_p3s_window(&target_rests_window, target_time,
                            target_time + map_width_ms +
                            (int) s_pattern_window_width);
                    clear_ap3results(&rests_aresult);

                    align_turningpoints_p3opt(&target_rests_window.window, NULL,
                            &scaled_pattern_rests, NULL, &rests_aresult, 0, 0,
                            (int) s_pattern_window_width, 0);

                    norm += scaled_pattern_rests.total_note_duration;

                    for (k=0; k<aresult.size; ++k) {
                        aresult.values[k] += rests_aresult.values[k];
                    }
                }
                normalize_ap3results(&aresult, norm);

#ifdef USE_LINECACHE
                update_linecache(&lc[j], &aresult, i, map);
#else
                {
                    int k;
                    for (k=0; k<aresult.size; ++k) {
                        mapline->values[k] = MAX2(mapline->values[k],
                                (unsigned char) aresult.values[k]);
                    }
                }
#endif
            }
            pattern_skip = parameters->p3_pattern_window_skip;
        }
        pattern_skip -= map->accuracy;
    }
#ifdef SHOW_PROGRESS
    fprintf(stderr, "\n");
#endif

#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
    puts("Window sizes:\n");
    for (i=0; i<1024; ++i) {
        printf("   %d:\t %d\n", i, window_notes[i]);
    }
#endif

    for (i=0; i<num_scales; ++i) {
        free_linecache(&lc[i]);
    }
    if (parameters->p3_window_heuristics) {
        free(lnotes[0]);
        free(lnotes);
        free(pattern_lnotes);
    }
    if (parameters->p3_align_rests) {
        free_song(&pattern_rests);
        free_song(&target_rests);
        free_p3_song(&p3s_rests);
        free_ap3results(&rests_aresult);
        free_song(&scaled_pattern_rests);
    }
    free(lc);
    free_song(&scaled_pattern);
    free_ap3results(&aresult);
    free_song_window(&pattern_window);
    free_p3s_window(&target_window);
    free(scales);
}

