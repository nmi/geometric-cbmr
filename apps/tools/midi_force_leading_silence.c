/*
 * midi_force_leading_silence.c: Inserts a silent note event to the beginning of *                               the last track in a MIDI file to force
 *                               TiMidity++ respect the leading silence.
 *
 * Version 2008-03-19
 *
 *
 * Copyright (C) 2008 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../midifile.h"

#if 0
static unsigned char notedata[2];

/**
 * Inserts a silent note to the beginning of the last track of the given MIDI
 * song.
 *
 * @param midi_s a MIDI song
 */
static void insert_silent_starting_note(midisong *midi_s) {
    int i;
    int track = midi_s->num_tracks - 1;
    track_event *te;
    track_event *newtrack;

    notedata[0] = 0;
    notedata[1] = 1;

    if (midi_s->track_size[track] > 0) {
        for (i=0; i<midi_s->track_size[track]; ++i) {
            te = &midi_s->track_data[track][i];
            if (te->tick > 0.0) break;
        }
    } else i = 0;

    newtrack = (track_event *) malloc((2 +
            midi_s->track_size[track]) * sizeof(track_event));
    if (midi_s->track_size[track] > 0) {
        memcpy(newtrack, midi_s->track_data[track], i * sizeof(track_event));
        if (midi_s->track_size[track] > i)
            memcpy(&newtrack[i+2], &midi_s->track_data[track][i],
                    (midi_s->track_size[track] - i) * sizeof(track_event));
    }
    /* Insert a Note On event */
    te = &newtrack[i];
    te->tick = 0.0;
    te->status = 0x90;
    te->length = 2;
    te->data = notedata;
    /* Insert a Note Off event */
    te = &newtrack[i+1];
    te->tick = 0.0;
    te->status = 0x80;
    te->length = 2;
    te->data = notedata;
    free(midi_s->track_data[track]);
    midi_s->track_data[track] = newtrack;;
    midi_s->track_size[track] += 2;
}
#endif

int main(int argc, char **argv) {
    midisong midi_s;

    if (argc < 3) {
        puts("\nUsage: midi_force_leading_silence <input.mid> <output.mid>\n");
        return 1;
    }

    if (!read_midi_file(argv[1], NULL, &midi_s, 0)) {
        return 1;
    }

    if (midi_s.num_tracks < 1) {
        fputs("Error: there are no tracks in the input file.\n", stderr);
        free_midisong(&midi_s);
        return 1;
    }

    /*insert_silent_starting_note(&midi_s);*/

    if (!write_midi_file(argv[2], &midi_s, 1)) {
        fprintf(stderr, "Error: unable to open input file: %s\n", argv[2]);
        free_midisong(&midi_s);
        return 1;
    }

    free_midisong(&midi_s);

    return 0;
}

