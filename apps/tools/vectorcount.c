#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include "../util.h"

const char *USAGE = "\nCounts the number of good index vectors for a pattern with errors.\nDifferent numbers of consecutive notes to use when selecting the vectors\n(from 1 to pattern length) are considered.\n\nUsage: vectorcount <points> <errors>\n\nExample: testvectors.py 50 0.75\n\n";

static int max_skip;
static int *valid_vectors;


static int count_all_vectors(int points, int maxskip) {
    int i, j;
    int vectors = 0;
    for (i=0; i<points-1; ++i) {
        for (j=1; j<=maxskip; ++j) {
            if (i + j < points) {
                ++vectors;
            }
        }
    }
    return vectors;
}

static inline void evaluate_set(int points, char *set) {
    int v, i, j;
    for (v=1; v<=max_skip; ++v) {
        int valid = 0;
        for (i=0; i<points-1; ++i) {
            for (j=1; j<=v; ++j) {
                if ((i + j < points) &&
                        ((set[i] == 1) && set[i+j] == 1))
                    ++valid;
            }
        }
        if (valid < valid_vectors[v-1]) valid_vectors[v-1] = valid;
    }
}

static void build_pointset(int points, char *set, int i,
        int bad_points, int good_points) {
    if (i == points) {
        evaluate_set(points, set);
    } else {
        if (bad_points > 0) {
            set[i] = 0;
            build_pointset(points, set, i + 1, bad_points - 1, good_points);
        }
        if (good_points > 0) {
            set[i] = 1;
            build_pointset(points, set, i + 1, bad_points, good_points - 1);
        }
    }
}

static int count_good_vectors(int points, float errors) {
    int bad_points = round64((double) points * (double) errors);
    int good_points = points - bad_points;

    char *set = (char *) malloc(points * sizeof(char));
    if (set == NULL) return 0;
    build_pointset(points, set, 0, bad_points, good_points);
    free(set);
    return 1;
}


int main(int argc, char **argv) {
    int i;
    int points = 0;
    float errors = 0;

    if (argc != 3) {
        printf("%s", USAGE);
        return 0;
    }

    sscanf(argv[1], "%d", &points);
    sscanf(argv[2], "%f", &errors);

    max_skip = points - 1;
    /* max_skip = (int) roundf(1.0 / (1.0 - errors)); */
    valid_vectors = (int *) malloc(max_skip * sizeof(int));
    for (i=0; i<max_skip; ++i) valid_vectors[i] = INT_MAX;

    count_good_vectors(points, errors);

    printf("\n");
    for (i=1; i<=max_skip; ++i) {
        int all = count_all_vectors(points, i);
        printf("%d: %d/%d = %f\n", i, valid_vectors[i-1],
                all, ((double) valid_vectors[i-1]) / ((double) all));
    }
    printf("\n");

    free(valid_vectors);
    return 1;
}

