/*
 * alignment_offsets.c - Compares two alignment files and outputs offsets at
 *                       all time positions.
 *
 * Version 2009-01-12
 *
 *
 * Copyright (C) 2009 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* For getline */
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "../util.h"


typedef struct {
    int pos;
    int a1;
    int a2;
} aligndiff;


/**
 * Alignment comparison tool.
 *
 * @param argc number of arguments
 * @param argv argument array
 *
 * @return 0
 */
int main(int argc, char **argv) {
    
    int RET = 0;
    FILE *f1, *f2, *f_offsets, *f_skips;
    char *linebuf = NULL;
    size_t linebuf_size;
    aligndiff *adiff;
    int adiffsize = 100000;
    int lastpos = INT_MIN;
    int i, line = 0;
    int accuracy;
    int min_skip_interval;

    if (argc < 7) {
        fputs("\nCompares two alignment files\n", stderr);
        fputs("\nUsage: alignment_offsets <reference.aln> <alignment.aln>\n", stderr);
        fputs("                         <output_offsets.dat> <output_skips.dat>\n", stderr);
        fputs("                         <accuracy> <min_skip_interval>\n\n", stderr);
        return 1;
    }

    f1 = fopen(argv[1], "r");
    f2 = fopen(argv[2], "r");
    f_offsets = fopen(argv[3], "w");
    f_skips = fopen(argv[4], "w");

    accuracy = atoi(argv[5]);
    min_skip_interval = atoi(argv[6]);

    if ((f1 == NULL) || (f2 == NULL) || (f_offsets == NULL) || (f_skips == NULL)) {
        /*fclose(f1);
        fclose(f2);
        fclose(f_offsets);
        fclose(f_skips);*/
        fputs("\nFailed to open input or output files\n\n", stderr);
        return 1;
    }

    adiff = (aligndiff *) calloc(adiffsize, sizeof(aligndiff));
    if (adiff == NULL) {
        fprintf(stderr, "Error: unable to allocate memory (%d bytes)\n",
                (int) (adiffsize * sizeof(aligndiff)));
        RET = 1;
        goto EXIT;
    }

    while(getline(&linebuf, &linebuf_size, f1) >= 0) {
        int pos;
        int a;
        int ret = sscanf(linebuf, " %d %d ", &pos, &a);
        if (ret < 2) {
            fprintf(stderr, "Error in %s, line %d: invalid data\n\n", argv[1], line + 1);
            RET = 1;
            goto EXIT;
        }
        if (pos < lastpos) {
            fprintf(stderr, "Error in %s, line %d: time position shifted backwards\n\n", argv[1], line + 1);
            RET = 1;
            goto EXIT;
        }
        lastpos = pos;

        adiff[line].pos = pos;
        adiff[line].a1 = a;
        adiff[line].a2 = INT_MIN;

        if (line > 0) {
            if (ABS(adiff[line].a1 - adiff[line-1].a1) > min_skip_interval) {
                fprintf(f_skips, "%f\t %d\n", (double) pos * 0.001, 1);
            }
        }

        ++line;
        if (line >= adiffsize) {
            adiffsize <<= 1;
            adiff = (aligndiff *) realloc(adiff, adiffsize * sizeof(aligndiff));
            if (adiff == NULL) {
                fprintf(stderr, "Error: unable to reallocate memory\n");
                RET = 1;
                goto EXIT;
            }
        }
    }
    adiffsize = line;

    line = 0;
    lastpos = INT_MIN;
    i = 0;
    while(getline(&linebuf, &linebuf_size, f2) >= 0) {
        int pos;
        int a;
        int ret = sscanf(linebuf, " %d %d ", &pos, &a);
        if (ret < 2) {
            fprintf(stderr, "Error in %s, line %d: invalid data\n\n", argv[2], line + 1);
            RET = 1;
            goto EXIT;
        }
        if (pos < lastpos) {
            fprintf(stderr, "Error in %s, line %d: time position shifted backwards\n\n", argv[2], line + 1);
            RET = 1;
            goto EXIT;
        }
        lastpos = pos;

        if (pos < 0) continue;

        while ((i < adiffsize) && (adiff[i].pos < pos)) ++i;

        if (i >= adiffsize) continue;

        if (adiff[i].pos != pos) {
            fprintf(stderr, "Warning: no matching time position found: %d != %d\n", adiff[i].pos, pos);
        }
        adiff[i].a2 = a;

        ++line;
    }

    double mean = 0.0;
    int samples = 0;
    if (adiffsize < 2) goto EXIT;
    accuracy = MAX2(0, accuracy - adiff[1].pos - adiff[0].pos);
    for (i=0; i<adiffsize; ++i) {
        double d;
        /* Skip unaligned parts (silence at the end) */
        if (adiff[i].a2 == INT_MIN) continue;
        d = 0.001 * (double) (adiff[i].a2 - adiff[i].a1);
        mean += d;
        ++samples;
        if ((adiff[i].pos - adiff[i-samples+1].pos) >= accuracy) {
            fprintf(f_offsets, "%f\t %f\n", 0.001 * (double) adiff[i-samples+1].pos, mean / (double) samples);
            samples = 0;
            mean = 0.0;
        }
    }

EXIT:
    fclose(f1);
    fclose(f2);
    fclose(f_offsets);
    fclose(f_skips);
    free(adiff);
    free(linebuf);
    return RET;
}


