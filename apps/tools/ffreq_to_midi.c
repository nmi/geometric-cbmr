/*
 * ffreq_to_midi.c - Converts MIREX '07 multiple fundamental frequency output
 *                   files to MIDI
 *
 * Version 2009-01-20
 *
 *
 * Copyright (C) 2009 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../song.h"
#include "../midifile.h"

/**
 * FFreq to MIDI converter tool.
 *
 * @param argc number of arguments
 * @param argv argument array
 *
 * @return 0
 */
int main(int argc, char **argv) {
    song s;
    midisong ms;

    memset(&s, 0, sizeof(song));
    memset(&ms, 0, sizeof(midisong));

    if (argc < 3) {
        fputs("\nConverts MIREX '07 multiple fundamental frequency output files to MIDI\n", stderr);
        fputs("\nUsage: ffreq_to_midi <linear_drift_correction> <input.frq> <output.mid>\n\n", stderr);
        return 1;
    }

    if (!read_mirex07_fundamental_frequency_file(argv[2], &s, atof(argv[1]))) {
        goto EXIT;
    }

    song_to_midisong(&s, &ms);

    write_midi_file(argv[3], &ms, 1);

EXIT:
    free_song(&s);
    free_midisong(&ms);

    return 0;
}


