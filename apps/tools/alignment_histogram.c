/*
 * alignment_histogram.c - Calculates an alignment offset histogram.
 *
 * Version 2009-01-12
 *
 *
 * Copyright (C) 2009 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* For getline */
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "../util.h"


/**
 * Alignment comparison tool.
 *
 * @param argc number of arguments
 * @param argv argument array
 *
 * @return 0
 */
int main(int argc, char **argv) {
    
    int RET = 0;
    int fnum;
    char *linebuf = NULL;
    size_t linebuf_size = 0;
    double *adiff;
    int adiffsize = 100000;
    int i, line = 0;
    double mediandiff, highest95, highest;

    int histogram_origin, histogram_size;
    double max_offset, accuracy;
    double *histogram_limits;
    double *histogram;

    if (argc < 4) {
        fputs("\nCalculates an alignment offset histogram and some statistical values\n", stderr);
        fputs("\nUsage: alignment_histogram <max_histogram_offset> <histogram_accuracy>\n", stderr);
        fputs("                           <input_file_1> ...\n\n", stderr);
        return 1;
    }

    max_offset = atoi(argv[1]);
    accuracy = atoi(argv[2]);
    max_offset *= 0.001;
    accuracy *= 0.001;
    histogram_origin = lrint(max_offset / accuracy);
    histogram_size = 2 * histogram_origin;

    histogram_limits = (double *) malloc(histogram_size * sizeof(double));
    histogram = (double *) calloc(histogram_size, sizeof(double));

    adiff = (double *) calloc(adiffsize, sizeof(double));
    if ((adiff == NULL) || (histogram_limits == NULL) || (histogram == NULL)) {
        fprintf(stderr, "Error: unable to allocate memory (%d bytes)\n",
                (int) (adiffsize * sizeof(double)));
        RET = 1;
        goto EXIT;
    }

    for (i=0; i<histogram_origin; ++i) {
        histogram_limits[histogram_origin+i] = accuracy * (double)(i+1);
        histogram_limits[histogram_origin-i-1] = accuracy * (double) (-(i+1));
    }


    line = 0;
    for (fnum=3; fnum<argc; ++fnum) {
        FILE *f = fopen(argv[fnum], "r");
        double lastpos = -1.0;

        if (f == NULL) {
            fprintf(stderr, "\nFailed to open input file %s\n\n", argv[fnum]);
            continue;
        }

        while(getline(&linebuf, &linebuf_size, f) >= 0) {
            double pos, a;
            int ret = sscanf(linebuf, " %lf %lf ", &pos, &a);
            if (ret < 2) {
                fprintf(stderr, "Error in %s, line %d: invalid data\n\n",
                        argv[fnum], line + 1);
                break;
            }
            if (pos < lastpos) {
                fprintf(stderr, "Error in %s, line %d: time position shifted backwards\n\n",
                        argv[fnum], line + 1);
                RET = 1;
                break;
            }
            lastpos = pos;

            adiff[line] = a;

            ++line;
            if (line >= adiffsize) {
                adiffsize <<= 1;
                adiff = (double *) realloc(adiff, adiffsize * sizeof(double));
                if (adiff == NULL) {
                    fprintf(stderr, "Error: unable to reallocate memory\n");
                    RET = 1;
                    fclose(f);
                    goto EXIT;
                }
            }
        }
        fclose(f);
    }

    highest = 0.0;
    for (i=0; i<line; ++i) {
        double d = adiff[i];
        int bin;
        if (highest < ABS(d)) highest = ABS(d);
        if (d < 0) {
            adiff[i] = -d;
            bin = histogram_origin + ceil(d / accuracy) - 1;
        } else {
            bin = histogram_origin + floor(d / accuracy);
        }
        if ((bin >= 0) && (bin < histogram_size)) {
            histogram[bin] += 1.0;
        }
    }

    mediandiff = kth_smallest_double(adiff, line, line / 2);
    highest95 = kth_smallest_double(adiff, line, lrint((double) line * 0.95));


    printf("# Median absolute offset:   %f\n", mediandiff);
    printf("# 95 %% absolute offset:     %f\n", highest95);
    printf("# Highest absolute offset:  %f\n", highest);

    printf("#\n# offset_bin\t relative_frequency\n");
    for (i=0; i<histogram_size; ++i) {
        if (histogram_limits[i] > 0.0) histogram_limits[i] -= 0.5 * accuracy;
        else histogram_limits[i] += 0.5 * accuracy;
        printf("%f\t %f\n", histogram_limits[i], histogram[i]/(double)line);
    }


EXIT:
    free(adiff);
    free(linebuf);
    free(histogram_limits);
    free(histogram);
    return RET;
}


