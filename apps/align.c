/*
 * align.c - Symbolic song synchronization functions.
 *
 * Version 2008-02-29
 *
 *
 * Copyright (C) 2008 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include "config.h"
#include "midifile.h"
#include "priority_queue.h"
#include "test.h"
#include "util.h"

#include "align.h"
#include "align_p3.h"
#include "song_window.h"
#include "song_window_p3.h"

/** Skip percussion tracks when converting MIDI files to the internal formats */
#define SKIP_PERCUSSION 1

#define MULTISCALE_MAP_WIDTH 512

/* #define MAP_TRANSPOSITIONS 1 */

enum dtwconstraint {
    DTW_SLOPE_UNCONSTRAINED = 0,
    DTW_SLOPE_2 = 2,
    DTW_SLOPE_3 = 3,
    DTW_SLOPE_4 = 4,
    DTW_SLOPE_5 = 5
};

enum dtwtrack {
    DTW_TRACK_END = 0,
    DTW_TRACK_L,
    DTW_TRACK_LLLLS,
    DTW_TRACK_LLLS,
    DTW_TRACK_LLS,
    DTW_TRACK_LS,
    DTW_TRACK_S,
    DTW_TRACK_DS,
    DTW_TRACK_DDS,
    DTW_TRACK_DDDS,
    DTW_TRACK_DDDDS,
    DTW_TRACK_D,
    DTW_TRACK_SKIP
};


static void (* const ALIGNMENT_MAP_FUNCTIONS[])(alignmentmap *map, int alg,
        const alignparameters *parameters) = {
          NULL,
/*  1 */  NULL, 
/*  2 */  NULL, 
/*  3 */  NULL, 
/*  4 */  NULL, 
/*  5 */  NULL, 
/*  6 */  NULL, 
/*  7 */  NULL, 
/*  8 */  NULL, 
/*  9 */  NULL, 
/* 10 */  NULL, 
/* 11 */  NULL, 
/* 12 */  NULL, 
/* 13 */  NULL, 
/* 14 */  NULL, 
/* 15 */  NULL, 
/* 16 */  NULL, 
/* 17 */  NULL, 
/* 18 */  NULL, 
/* 19 */  NULL, 
/* 20 */  NULL, 
/* 21 */  NULL, 
/* 22 */  NULL, 
/* 23 */  NULL, 
/* 24 */  NULL, 
/* 25 */  NULL, 
/* 26 */  NULL, 
/* 27 */  align_p3, 
/* 28 */  align_p3, 
/* 29 */  align_p3, 
};



/** Command-line argument IDs (also used as short arguments) */

#define ALIGN_ARG_HELP                 'h'
#define ALIGN_ARG_ALGORITHM            'A'
#define ALIGN_ARG_OUTPUT_ALIGNMENT     'a'
#define ALIGN_ARG_OUTPUT_MIDI          'o'
#define ALIGN_ARG_OUTPUT_PGM_MAP       'm'
#define ALIGN_ARG_MULTISCALE           'u'
#define ALIGN_ARG_SMOOTH_RANGE         'S'
#define ALIGN_ARG_MAP_ACCURACY         'M'
#define ALIGN_ARG_NORMALIZE            'N'
#define ALIGN_ARG_PATTERN              'p'
#define ALIGN_ARG_MAX_PATTERN_SIZE     'n'
#define ALIGN_ARG_MAP_SIZE             's'
#define ALIGN_ARG_P3_WINDOW_SKIP       'W'
#define ALIGN_ARG_P3_WINDOW_SIZE       'w'
#define ALIGN_ARG_P3_WINDOW_HEURISTICS 'r'
#define ALIGN_ARG_P3_SCALES            'x'
#define ALIGN_ARG_P3_SCALES_SPREAD     'X'
#define ALIGN_ARG_P3_OPTIMIZE          'O'
#define ALIGN_ARG_P3_ONSET_DELTA       'e'
#define ALIGN_ARG_P3_ONSET_SLOPE       'E'
#define ALIGN_ARG_P3_ALIGN_RESTS       'R'
#define ALIGN_ARG_P3_MAX_LTRANS        'L'
#define ALIGN_ARG_SONG_MARGINS         'T'
#define ALIGN_ARG_SONG_TEMPO           't'
#define ALIGN_ARG_ALIGNMENT_DELAY      'd'
#define ALIGN_ARG_DTW_SKIP_DELTA       'D'
#define ALIGN_ARG_DTW_CONSTRAINT       'C'
#define ALIGN_ARG_DTW_SLOPE_WEIGHT     'l'
#define ALIGN_ARG_GENERATE_ALIGNMENT   'g'


static const struct option LONG_OPTIONS[] = {
    {"help",                no_argument,        0, ALIGN_ARG_HELP},
    {"algorithm",           required_argument,  0, ALIGN_ARG_ALGORITHM},
    {"output-alignment",    required_argument,  0, ALIGN_ARG_OUTPUT_ALIGNMENT},
    {"output-midi",         required_argument,  0, ALIGN_ARG_OUTPUT_MIDI},
    {"output-pgm-map",      required_argument,  0, ALIGN_ARG_OUTPUT_PGM_MAP},
    {"multiscale",          required_argument,  0, ALIGN_ARG_MULTISCALE},
    {"smooth-range",        required_argument,  0, ALIGN_ARG_SMOOTH_RANGE},
    {"map-accuracy",        required_argument,  0, ALIGN_ARG_MAP_ACCURACY},
    {"normalize",           required_argument,  0, ALIGN_ARG_NORMALIZE},
    {"pattern",             required_argument,  0, ALIGN_ARG_PATTERN},
    {"max-pattern-size",    required_argument,  0, ALIGN_ARG_MAX_PATTERN_SIZE},
    {"map-size",            required_argument,  0, ALIGN_ARG_MAP_SIZE},
    {"p3-window-skip",      required_argument,  0, ALIGN_ARG_P3_WINDOW_SKIP},
    {"p3-window-size",      required_argument,  0, ALIGN_ARG_P3_WINDOW_SIZE},
    {"p3-window-heuristics",required_argument,  0, ALIGN_ARG_P3_WINDOW_HEURISTICS},
    {"p3-scales",           required_argument,  0, ALIGN_ARG_P3_SCALES},
    {"p3-scales-spread",    required_argument,  0, ALIGN_ARG_P3_SCALES_SPREAD},
    {"p3-optimize",         required_argument,  0, ALIGN_ARG_P3_OPTIMIZE},
    {"p3-onset-delta",      required_argument,  0, ALIGN_ARG_P3_ONSET_DELTA},
    {"p3-onset-slope",      required_argument,  0, ALIGN_ARG_P3_ONSET_SLOPE},
    {"p3-align-rests",      required_argument,  0, ALIGN_ARG_P3_ALIGN_RESTS},
    {"p3-max-ltrans",       required_argument,  0, ALIGN_ARG_P3_MAX_LTRANS},
    {"song-margins",        required_argument,  0, ALIGN_ARG_SONG_MARGINS},
    {"song-tempo",          required_argument,  0, ALIGN_ARG_SONG_TEMPO},
    {"delay",               required_argument,  0, ALIGN_ARG_ALIGNMENT_DELAY},
    {"dtw-skip-delta",      required_argument,  0, ALIGN_ARG_DTW_SKIP_DELTA},
    {"dtw-constraint",      required_argument,  0, ALIGN_ARG_DTW_CONSTRAINT},
    {"dtw-slope-weight",    required_argument,  0, ALIGN_ARG_DTW_SLOPE_WEIGHT},
    {"generate-sample",     required_argument,  0, ALIGN_ARG_GENERATE_ALIGNMENT},
    {0, 0, 0, 0}
};


/**
 * Prints program usage information.
 *
 * @param p default parameter values
 */
static void align_print_usage(const alignparameters *p) {
    puts("\nTest program for symbolic score alignment algorithms.\n"
         "Aligns a MIDI pattern (for example a captured performance on\n"
         "a MIDI keyboard) against a MIDI score of the same piece.\n");

    printf("Syntax: %s [options] -p <performance pattern> <score collection> \n\n", p->program_name);

    puts(  "Where score collection is a single MIDI file or a directory of MIDI files\n");

    puts(  "Options:\n");

    fputs( "  -p, --pattern [filename]          Pattern to align. A single MIDI file.\n", stdout);

    fputs( "  -n, --max-pattern-size [int]      Maximum pattern size (notes)\n", stdout);
    fputs( "  -s, --map-size [int]              Alignment map size (seconds)\n", stdout);

    fputs( "  -o, --output-midi [path]          Write aligned score to this file [no]\n", stdout);

    fputs( "  -a, --output-alignment [path]     Write alignment positions to this file [no]\n", stdout);

    fputs( "  -m, --output-pgm-map [path]       Output alignment map as a PGM image [no]\n", stdout);

    fputs( "  -A, --algorithm [string]          Alignment algorithm [AP3opt]\n", stdout);

    fputs( "  -u, --multiscale [int]            Number of multiscale alignment levels [1]\n", stdout);

    fputs( "  -S, --smooth-range [int]          Alignment smoothing range [512]\n", stdout);

    fputs( "  -M, --map-accuracy [int]          Map accuracy in milliseconds [128]\n", stdout);

    fputs( "  -N, --normalize [0/1]             Normalize alignment map [1]\n", stdout);

    fputs( "  -W, --p3-window-skip [int]        AP3 window skip [2048]\n", stdout);
    fputs( "  -w, --p3-window-size [int]        AP3 window size [4096]\n", stdout);
    fputs( "  -r, --p3-window-heuristics [int]  Enable window heuristics [0]\n", stdout);

    fputs( "  -x, --p3-scales [int]             Number of scaled pattern variations [3]\n", stdout);
    fputs( "                                    (the total number is p3-scales * 2 - 1)\n", stdout);

    fputs( "  -X, --p3-scales-spread [float]    Spread of the scale variations [2.0]\n", stdout);

    fputs( "  -O, --p3-optimize [int]           Combine notes that are too close [300]\n", stdout);

    fputs( "  -e, --p3-onset-delta [int]        AP3o onset alignment minimum distance [300]\n", stdout);

    fputs( "  -E, --p3-onset-slope [double]     AP3o onset alignment slope [3.0]\n", stdout);

    fputs( "  -R, --p3-align-rests [int]        Align rests [1]\n", stdout);
    fputs( "  -L, --p3-max-ltrans [int]         Maximum local transposition (semitones) [0]\n", stdout);

    fputs( "  -T, --song-margins [int]          Trim silence at the beginning and the end\n", stdout);
    fputs( "                                    of the song [4096]\n", stdout);

    fputs( "  -t, --song-tempo [float]          Adjust song tempo [1.0]\n", stdout);

    fputs( "  -d, --delay [int]                 Alignment delay in milliseconds [0]\n", stdout);

    fputs( "  -D, --dtw-skip-delta [int]        Minimum time between alignment skips\n", stdout);
    fputs("                                     in seconds [INT_MAX]\n", stdout);
    fputs( "  -C, --dtw-constraint [int]        Local DTW slope constraint: 0..5 [3]\n", stdout);

    fputs( "  -l, --dtw-slope-weight [float]    DTW slope transition weight [1.3]\n\n", stdout);

    fputs( "  -g, --generate-sample [int]       Generate a random alignment for\n"
           "                                    measurements [no].\n\n", stdout);


}


/**
 * Initializes the given parameter struct with default values.
 *
 * @param p parameter struct
 */
void align_init_parameters(alignparameters *p) {
    p->verbose = 1;
    p->program_name = NULL;
    p->algorithm = ALG_ALIGN_P3_OPT;
    p->pattern_path = NULL;
    p->max_pattern_size = -1;
    p->map_size = -1;
    p->output_midi = NULL;
    p->output_alignment = NULL;
    p->output_map = NULL;
    p->song_path = NULL;

    p->map_accuracy = 128;
    p->smooth_range = 512;
    p->multiscale = 1;
    p->normalize = 1;

    p->p3_pattern_window_width = 4096;
    p->p3_pattern_window_skip = 2048;
    p->p3_window_heuristics = 0;
    p->p3_optimize = 300;

    p->p3_scales_num = 3;
    p->p3_scales_spread = 2.0;

    p->p3_onset_delta = 300;
    p->p3_onset_slope = 3.0;

    /*p->p3_align_rests = 0;*/
    /*p->song_margins = -1;*/
    p->p3_align_rests = 1;
    p->p3_max_local_transposition = 0;
    p->song_margins = 4096;

    p->song_tempo = 1.0F;
    p->delay = 0;

    p->dtw_skip_delta = INT_MAX;
    p->dtw_constraint = DTW_SLOPE_3;
    p->dtw_slope_weight = 1.3F;

    p->generate_alignment = 0;
}

void align_free_parameters(alignparameters *p) {
    /*free(p->p3_scales);*/
}

/**
 * Parses command-line arguments into the given struct, overriding the
 * previous values.
 *
 * @param argc argument count
 * @param argv argument list
 * @param p pointer to a parameter struct
 *
 * @return 1 if successful, 0 otherwise
 */
int align_parse_arguments(int argc, char **argv, alignparameters *p) {
    int i, j, option_index = 0, ret = 1;
    char *short_options = make_short_options(LONG_OPTIONS);

    if (!short_options) return 0;

    p->program_name = argv[0];

    while ((i = getopt_long(argc, argv, short_options,
            LONG_OPTIONS, &option_index)) != -1) {
        switch (i) {
            case ALIGN_ARG_HELP:
                align_print_usage(p);
                ret = 0;
                goto EXIT;
            case ALIGN_ARG_ALGORITHM:
                j = get_algorithm_id(optarg);
                if (j == 0) {
                    print_algorithms();
                    ret = 0;
                    goto EXIT;
                } else p->algorithm = j;
                break;
            case ALIGN_ARG_PATTERN:
                p->pattern_path = optarg;
                break;
            case ALIGN_ARG_OUTPUT_MIDI:
                p->output_midi = optarg;
                break;
            case ALIGN_ARG_OUTPUT_ALIGNMENT:
                p->output_alignment = optarg;
                break;
            case ALIGN_ARG_OUTPUT_PGM_MAP:
                p->output_map = optarg;
                break;
            case ALIGN_ARG_MULTISCALE:
                p->multiscale = atoi(optarg);
                break;
            case ALIGN_ARG_SMOOTH_RANGE:
                p->smooth_range = MAX2(0, atoi(optarg));
                break;
            case ALIGN_ARG_MAP_SIZE:
                p->map_size = MAX2(0, 1000 * atoi(optarg));
                break;
            case ALIGN_ARG_MAP_ACCURACY:
                p->map_accuracy = MAX2(1, atoi(optarg));
                break;
            case ALIGN_ARG_NORMALIZE:
                p->normalize = MAX2(0, MIN2(1, atoi(optarg)));
                break;
            case ALIGN_ARG_P3_WINDOW_SKIP:
                p->p3_pattern_window_skip = MAX2(1, atoi(optarg));
                break;
            case ALIGN_ARG_P3_WINDOW_SIZE:
                p->p3_pattern_window_width = MAX2(2, atoi(optarg));
                break;
            case ALIGN_ARG_P3_WINDOW_HEURISTICS:
                p->p3_window_heuristics = MAX2(0, MIN2(1, atoi(optarg)));
                break;
            case ALIGN_ARG_P3_SCALES:
                p->p3_scales_num = MAX2(1, atoi(optarg));
                break;
            case ALIGN_ARG_P3_SCALES_SPREAD:
                p->p3_scales_spread = MAX2(1.0, atof(optarg));
                break;
            case ALIGN_ARG_P3_OPTIMIZE:
                p->p3_optimize = MAX2(0, atoi(optarg));
                break;
            case ALIGN_ARG_P3_ONSET_DELTA:
                p->p3_onset_delta = MAX2(1, atoi(optarg));
                break;
            case ALIGN_ARG_P3_ONSET_SLOPE:
                p->p3_onset_slope = MAX2(0.0, atof(optarg));
                break;
            case ALIGN_ARG_P3_ALIGN_RESTS:
                p->p3_align_rests = MAX2(0, MIN2(1, atoi(optarg)));
                break;
            case ALIGN_ARG_P3_MAX_LTRANS:
                p->p3_max_local_transposition = MAX2(0, atoi(optarg));
                break;
            case ALIGN_ARG_SONG_MARGINS:
                p->song_margins = atoi(optarg);
                break;
            case ALIGN_ARG_SONG_TEMPO:
                p->song_tempo = atof(optarg);
                break;
            case ALIGN_ARG_ALIGNMENT_DELAY:
                p->delay = atoi(optarg);
                break;
            case ALIGN_ARG_MAX_PATTERN_SIZE:
                p->max_pattern_size = atoi(optarg);
                break;
            case ALIGN_ARG_DTW_SKIP_DELTA:
                p->dtw_skip_delta = MAX2(0, 1000 * atoi(optarg));
                break;
            case ALIGN_ARG_DTW_CONSTRAINT:
                j = atoi(optarg);
                if ((j >= 0) && (j <= DTW_SLOPE_5))
                    p->dtw_constraint = j;
                else {
                    fprintf(stderr, "Warning: unknown DTW constraint '%d'. Using default slope [3]\n", j);
                    p->dtw_constraint = DTW_SLOPE_3;
                }
                break;
            case ALIGN_ARG_DTW_SLOPE_WEIGHT:
                p->dtw_slope_weight = atof(optarg);
                if (p->dtw_slope_weight < 1.0F) {
                    fprintf(stderr, "Warning: invalid DTW slope weight. Using minimum weight [1.0]\n");
                    p->dtw_slope_weight = 1.0F;
                }
                break;
            case ALIGN_ARG_GENERATE_ALIGNMENT:
                p->generate_alignment = MAX2(1, atoi(optarg));
                break;
            default:
                ret = 0;
                goto EXIT;
        }
    }

    /* Calculate a power of two floor for the alignment map accuracy */
    i = 0;
    while (p->map_accuracy > 1) {
        p->map_accuracy >>= 1;
        ++i;
    }
    p->map_accuracy = 1 << i;
    fprintf(stderr, "Setting alignment accuracy to %d ms\n", p->map_accuracy);

    /* Calculate a power of two floor for the P3 window skip */
    i = 0;
    while (p->p3_pattern_window_skip > 1) {
        p->p3_pattern_window_skip >>= 1;
        ++i;
    }
    p->p3_pattern_window_skip = 1 << i;
    if (p->p3_pattern_window_skip < p->map_accuracy) {
        p->p3_pattern_window_skip = p->map_accuracy;
    }
    fprintf(stderr, "Setting P3 pattern window skip to %d ms\n",
            p->p3_pattern_window_skip);

    /* Parse remaining non-option arguments */
    if (optind < argc) {
        p->song_path = argv[optind];
    }

    fprintf(stderr, "\nAlgorithm: %s\n\n",
            get_algorithm_full_name(p->algorithm));
    if (p->algorithm == ALG_ALIGN_P3) {
        fprintf(stderr, "Disabling onset weighting (it is not supported by AP3)...\n");
        p->p3_onset_slope = 0.0;
        p->p3_onset_delta = 0;
    }

EXIT:
    free(short_options);
    return ret;
}


/**
 * Aligns a pattern with a song.
 *
 * @param sc the song collection
 * @param s song to align the pattern with. If either of the aligned songs
 *        is the "correct" score, it should be given here to improve alignment
 *        results of the P3 algorithm
 * @param p a pattern song that is aligned with the other song. This should
 *        be the piece that may contain more errors.
 * @param alg the algorithm ID as defined in algorithms.h
 * @param parameters alignment parameters for different algorithms
 * @param initial_align initial alignment. See calculate_initial_alignment().
 * @param result_align resulting alignment
 */
/*void align(const songcollection *sc, const song *s, const song *p, int alg,
        const alignparameters *parameters, alignment *initial_align,
        alignment *result_align) {

    if ((alg <= 0) || (alg > NUM_ALGORITHMS)) {
        fprintf(stderr, "Error in align: Undefined algorithm %d\n", alg);
        return;
    }
    if (ALIGNMENT_FUNCTIONS[alg] != NULL) {
        ALIGNMENT_FUNCTIONS[alg](sc, s, p, alg, parameters, initial_align,
                result_align);
    } else {
        fprintf(stderr, "Error in align: No alignment function defined for algorithm %d\n", alg);
    }
}
*/

/**
 * Maps all local alignments of two songs.
 *
 * @param sc the song collection
 * @param s song to align the pattern with. If either of the aligned songs
 *        is the "correct" score, it should be given here to improve alignment
 *        results of the P3 algorithm
 * @param p a pattern song that is aligned with the other song. This should
 *        be the piece that may contain more errors.
 * @param alg the algorithm ID as defined in algorithms.h
 * @param parameters alignment parameters for different algorithms
 * @param map resulting alignment map will be stored here
 */
void map_alignments(alignmentmap *map, int alg,
        const alignparameters *parameters) {

    if ((alg <= 0) || (alg > NUM_ALGORITHMS)) {
        fprintf(stderr, "Error in map_alignments: Undefined algorithm %d\n",
                alg);
        return;
    }
    if (ALIGNMENT_MAP_FUNCTIONS[alg] != NULL) {
        ALIGNMENT_MAP_FUNCTIONS[alg](map, alg, parameters);
    } else {
        fprintf(stderr, "Error in map_alignments: No alignment function defined for algorithm %d\n", alg);
    }
}



size_t init_alignmentmap(alignmentmap *map, const songcollection *sc,
        const song *target, const song *pattern, int accuracy, int width,
        const alignmentmap *initial_map) {
    int i, pos, ptime;
    int accuracy_shift = 0;
    size_t allocated_memory = 0;
    while (accuracy > 1) {
        ++accuracy_shift;
        accuracy >>= 1;
    }
    if (width <= 0) width = target->end;
    map->accuracy = 1 << accuracy_shift;
    map->accuracy_shift = accuracy_shift;
    map->sc = sc;
    map->target = target;
    map->pattern = pattern;
    map->target_duration = target->end;
    map->pattern_duration = pattern->end;
    map->width = width >> accuracy_shift;
    map->height = map->pattern_duration >> accuracy_shift;

    if (width < target->end) map->slope = 1;
    else map->slope = 0;

    map->lines = (alignmentline *) malloc(map->height * sizeof(alignmentline));
    allocated_memory += map->height * sizeof(alignmentline);

    map->vbuffer = calloc(map->width * map->height, sizeof(unsigned char));
    allocated_memory += map->height * map->width * sizeof(unsigned char);

    map->initial_alignment = (double *) malloc((map->height + 1) *
            sizeof(double));
    map->initial_alignment_offset = (double *) malloc((map->height + 1) *
            sizeof(double));
    allocated_memory += 2 * (map->height+1) * sizeof(double);

#ifdef MAP_TRANSPOSITIONS
    map->tbuffer = calloc(map->width * map->height, sizeof(char));
    allocated_memory += map->height * map->width * sizeof(char);
    if ((map->lines == NULL) || (map->vbuffer == NULL) ||
            (map->tbuffer == NULL) ||
            (map->initial_alignment == NULL) ||
            (map->initial_alignment_offset == NULL)) {
#else
    map->tbuffer = NULL;
    if ((map->lines == NULL) || (map->vbuffer == NULL) ||
            (map->initial_alignment == NULL) ||
            (map->initial_alignment_offset == NULL)) {
#endif
        free(map->lines);
        free(map->vbuffer);
        free(map->tbuffer);
        free(map->initial_alignment);
        free(map->initial_alignment_offset);
        map->height = 0;
        map->width = 0;
        return 0;
    }

    pos = 0;
    ptime = 0;
    if (initial_map == NULL) {
        for (i=0; i<map->height; ++i, pos+=map->width, ptime+=map->accuracy) {
            alignmentline *l = &map->lines[i];
            l->pattern_time = ptime;
            l->target_time = 0;
            l->values = &map->vbuffer[pos];
#ifdef MAP_TRANSPOSITIONS
            l->transpositions = &map->tbuffer[pos];
#else
            l->transpositions = NULL;
#endif
            map->initial_alignment[i] = ptime; 
            map->initial_alignment_offset[i] = 0; 
        }
        map->initial_alignment[i] = ptime;
        map->initial_alignment_offset[i] = 0; 
    } else {
        int pshift = MAX2(0, initial_map->accuracy_shift - map->accuracy_shift);
        double scalefactor = 1.0 / ((double) (1 << pshift));
        for (i=0; i<map->height; ++i, pos+=map->width) {
            alignmentline *l = &map->lines[i];
            int ipos = i >> pshift;
            int j = MIN2(ipos, initial_map->height - 2);
            double ia = initial_map->initial_alignment[j+1] -
                    initial_map->initial_alignment[j];
            j = MIN2(ipos, initial_map->height - 1);
            ia = initial_map->initial_alignment[j] + ia * scalefactor *
                    (double) (i - (ipos << pshift));

            map->initial_alignment[i] = ia;
            map->initial_alignment_offset[i] =
                    initial_map->initial_alignment_offset[j];
            l->pattern_time = i * map->accuracy;
            l->target_time = map->initial_alignment[i] + map->initial_alignment_offset[i] - (width >> 1);
            l->values = &map->vbuffer[pos];
#ifdef MAP_TRANSPOSITIONS
            l->transpositions = &map->tbuffer[pos];
#else
            l->transpositions = NULL;
#endif
        }
        map->initial_alignment[i] = map->initial_alignment[i-1] +
                (map->initial_alignment[i-1] - map->initial_alignment[i-2]);
        map->initial_alignment_offset[i] = map->initial_alignment_offset[i-1]; 
    }
    return allocated_memory;
}


void free_alignmentmap(alignmentmap *map) {
    free(map->lines);
    free(map->vbuffer);
    free(map->tbuffer);
    free(map->initial_alignment);
    free(map->initial_alignment_offset);
    memset(map, 0, sizeof(alignmentmap)); 
}

static void free_alignmentmap_buffers(alignmentmap *map) {
    free(map->vbuffer);
    free(map->tbuffer);
    map->vbuffer = NULL;
    map->tbuffer = NULL;
}


void init_alignment(alignment *a, alignmentmap *map) {
    if (map == NULL) {
        a->size = 0;
        a->pattern_times = NULL;
        a->target_times = NULL;
        a->quality = NULL;
        a->pattern = NULL;
        a->target = NULL;
    } else {
        int i;
        a->size = map->height;
        a->pattern_times = (int *) malloc(a->size * sizeof(int));
        a->target_times = (int *) malloc(a->size * sizeof(int));
        a->skip = (char *) malloc(a->size * sizeof(char));
        a->quality = (unsigned char *) malloc(a->size * sizeof(unsigned char));
        if ((a->pattern_times == NULL) || (a->target_times == NULL) ||
                (a->skip == NULL) || (a->quality == NULL)) {
            fputs("Error in init_alignment: failed to allocate memory\n",
                    stderr);
            free_alignment(a);
            return;
        }
        for (i=0; i<a->size; ++i) {
            a->pattern_times[i] = map->lines[i].pattern_time -
                    map->pattern->start;
            a->target_times[i] = lrint(map->initial_alignment[i] +
                    map->initial_alignment_offset[i]);
            if ((i > 0) && (map->initial_alignment_offset[i] !=
                            map->initial_alignment_offset[i-1])) {
                a->skip[i] = 1;
            } else {
                a->skip[i] = 0;
            }
            a->quality[i] = map->lines[i].values[(a->target_times[i] -
                    map->lines[i].target_time) >> map->accuracy_shift];
            a->target_times[i] -= map->target->start;
        }
    }
}

void free_alignment(alignment *a) {
    free(a->pattern_times);
    free(a->target_times);
    free(a->skip);
    free(a->quality);
    memset(a, 0, sizeof(alignment));
}



/**
 * Writes the given alignment map to disk as a PGM image file.
 *
 * @param filename output file path
 * @param map alignment map to write
 * @param a alignment to overlay on the image. Use NULL to only output the map.
 * @param invert set to 1 to invert the image colors
 */
void write_alignmentmap_pgm(const char *filename, const alignmentmap *map,
        int invert, int show_alignment_path) {
    unsigned char *buffer;
    int width = map->width;
    int height = map->height;
    int i, pos = 0;


    buffer = (unsigned char *) malloc(width * height * sizeof(unsigned char));
    if (buffer == NULL) {
        fputs("Error in write_alignmentmap_pgm: failed to allocate memory for image buffer\n", stderr);
        return;
    }

    for (i=map->height-1; i>=0; --i) {
        unsigned char *mapline = map->lines[i].values;
        if (invert) {
            int j;
            for (j=0; j<width; ++j, ++pos) {
                buffer[pos] = 255 - mapline[j];
            }
        } else {
            memcpy(&buffer[pos], mapline, width);
            pos += width;
        }
        if (show_alignment_path) {
            int pathpos = ASHIFTR(lrint(map->initial_alignment[i] +
                    map->initial_alignment_offset[i]) -
                    map->lines[i].target_time, map->accuracy_shift);
            if ((pathpos >= 0) && (pathpos < width)) {
                pathpos = pos - width + pathpos;
                if (invert) buffer[pathpos] = 255;
                else buffer[pathpos] = 0;
            }
        }
    }
    write_pgm(filename, buffer, width, height);
    free(buffer);
}


void dtw_alignment(alignmentmap *map, const alignparameters *parameters) {
    int i;
    int lastmax = 0;
    int ashift = map->accuracy_shift;
    int height = map->height;
    int width = map->width;
    int local_constraint = parameters->dtw_constraint;
    int delta[5];
    int *sum[6];
    int *sbuffer;
    unsigned char *trackbuffer;
    int *maxlinepos;
    int max_right_pos = -1, max_right = 0;
    int map_slope = map->slope;
    int skips_allowed = (parameters->dtw_skip_delta < INT_MAX);
    float slope_weight = parameters->dtw_slope_weight;

    if (skips_allowed) {
        local_constraint = MAX2(local_constraint, DTW_SLOPE_2);
        slope_weight -= 1.0F;
    }

    maxlinepos = (int *) calloc(height, sizeof(int));
    trackbuffer = (unsigned char *) calloc(width * height,
            sizeof(unsigned char));
    memset(delta, 0, 5 * sizeof(int));

    sbuffer = (int *) calloc(6 * width, sizeof(int));
    sum[0] = sbuffer;
    sum[1] = &sum[0][width];
    sum[2] = &sum[1][width];
    sum[3] = &sum[2][width];
    sum[4] = &sum[3][width];
    sum[5] = &sum[4][width];

    if ((maxlinepos == NULL) || (trackbuffer == NULL) || (sbuffer == NULL)) {
        fputs("Error in dtw_alignmentmap: failed to allocate memory\n", stderr);
        free(maxlinepos);
        free(trackbuffer);
        free(sbuffer);
        return;
    }

    for (i=0; i<height; ++i) {
        int j;
        int *tmp;
        int max = 0, maxpos = -1;
        unsigned char *track = &trackbuffer[i * width];
        unsigned char *mline = map->lines[i].values;
        unsigned char *mline1 = NULL, *mline2 = NULL, *mline3 = NULL,
                      *mline4 = NULL;
        if (i >= 1) mline1 = map->lines[i-1].values;
        if (i >= 2) mline2 = map->lines[i-2].values;
        if (i >= 3) mline3 = map->lines[i-3].values;
        if (i >= 4) mline4 = map->lines[i-4].values;

/*        if ((i > 0) && map_slope) {
            if (map->initial_alignment_offset[i] == map->initial_alignment_offset[i-1]) {
                delta[0] = (map->lines[i].target_time - map->lines[i-1].target_time) >> map->accuracy_shift;
            } else {
                delta[0] = 1;
            }
        } else  delta[0] = 0;
*/
        delta[0] = map_slope;
        for (j=1; j<5; ++j) delta[j] += delta[0];

        if (i > 0) {
            /* / */

            int k;
            int p = delta[0] - 1;
            int end = width;
            if (p < 0) {
                j = -p;
                p = 0;
            } else {
                j = 0;
                end -= p;
            }
            for (k = 0; k<j; ++k) sum[0][k] = 0;
            for (k = end; k<width; ++k) sum[0][k] = 0;
            for (; j<end; ++j, ++p) {
                sum[0][j] = sum[1][p] + (int)
                        (slope_weight * (float) mline[j]);
                track[j] = DTW_TRACK_S;
            }

            /* | left edge */
            sum[0][0] = 128 * i;
            track[0] = DTW_TRACK_D;

            /* | right edge */
            if (sum[0][width-1] < sum[1][width-1]) {
                sum[0][width-1] = sum[1][width-1];
                track[width-1] = DTW_TRACK_D;
            }

        } else memset(sum[0], 0, width * sizeof(int));

        switch (local_constraint) {

        case DTW_SLOPE_5:
            /*  .__.__.__.__ 
               /             */
            if (i > 0) {
                int p = delta[0] - 5;
                int end = width;
                if (p < 0) {
                    j = MAX2(4, -p);
                    p = 0;
                } else {
                    j = 4;
                    end -= p;
                }
                if (skips_allowed) {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + (int)
                                (slope_weight * 0.2F * (float) mline[j]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LLLLS;
                        }
                    }
                } else {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + mline[j-1] + mline[j-2] +
                                mline[j-3] + (int) ((slope_weight + 1.0F) *
                                    (float) mline[j-4]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LLLLS;
                        }
                    }
                }
            }

            /*  !
                ! 
                ! 
                ! 
               /  */
            if (i > 4) {
                int p1 = delta[0];
                int p2 = delta[1];
                int p3 = delta[2];
                int p4 = delta[3];
                int p5 = delta[4] - 1;
                int pmin = MIN2(MIN2(MIN2(MIN2(p1, p2), p3), p4), p5);
                int pmax = MAX2(MAX2(MAX2(MAX2(p1, p2), p3), p4), p5);
                int end = width;
                j = 0;
                if (pmin < 0) {
                    j -= pmin;
                    p1 -= pmin;
                    p2 -= pmin;
                    p3 -= pmin;
                    p4 -= pmin;
                    p5 -= pmin;
                }
                if (pmax > 0) {
                    end -= pmax;
                }
                for (; j<end; ++j, ++p1, ++p2, ++p3, ++p4, ++p5) {
                    int newsum = sum[5][p5] + mline1[p1] + mline2[p2] +
                            mline3[p3] + (int) ((slope_weight + 1.0F) *
                                (float) mline4[p4]);
                    if (sum[0][j] < newsum) {
                        sum[0][j] = newsum;
                        track[j] = DTW_TRACK_DDDDS;
                    }
                }
            }

        case DTW_SLOPE_4:
            /*  .__.__.__ 
               /          */
            if (i > 0) {
                int p = delta[0] - 4;
                int end = width;
                if (p < 0) {
                    j = MAX2(3, -p);
                    p = 0;
                } else {
                    j = 3;
                    end -= p;
                }
                if (skips_allowed) {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + (int)
                                (slope_weight * 0.25F * (float)mline[j]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LLLS;
                        }
                    }
                } else {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + mline[j-1] + mline[j-2] + (int)
                                ((slope_weight + 1.0F) * (float) mline[j-3]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LLLS;
                        }
                    }
                }
            }

            /*  !
                ! 
                ! 
               /  */
            if (i > 3) {
                int p1 = delta[0];
                int p2 = delta[1];
                int p3 = delta[2];
                int p4 = delta[3] - 1;
                int pmin = MIN2(MIN2(MIN2(p1, p2), p3), p4);
                int pmax = MAX2(MAX2(MAX2(p1, p2), p3), p4);
                int end = width;
                j = 0;
                if (pmin < 0) {
                    j -= pmin;
                    p1 -= pmin;
                    p2 -= pmin;
                    p3 -= pmin;
                    p4 -= pmin;
                }
                if (pmax > 0) {
                    end -= pmax;
                }
                for (; j<end; ++j, ++p1, ++p2, ++p3, ++p4) {
                    int newsum = sum[4][p4] + mline1[p1] + mline2[p2] + (int)
                            ((slope_weight + 1.0F) * (float) mline3[p3]);
                    if (sum[0][j] < newsum) {
                        sum[0][j] = newsum;
                        track[j] = DTW_TRACK_DDDS;
                    }
                }
            }

        case DTW_SLOPE_3:
            /*  .__.__ 
               /       */
            if (i > 0) {
                int p = delta[0] - 3;
                int end = width;
                if (p < 0) {
                    j = MAX2(2, -p);
                    p = 0;
                } else {
                    j = 2;
                    end -= p;
                }
                if (skips_allowed) {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + (int)
                                (slope_weight * 0.333333F * (float)mline[j]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LLS;
                        }
                    }
                } else {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + mline[j-1] + (int)
                                ((slope_weight + 1.0F) * (float) mline[j-2]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LLS;
                        }
                    }
                }
            }

            /*  !
                ! 
               /  */
            if (i > 2) {
                int p1 = delta[0];
                int p2 = delta[1];
                int p3 = delta[2] - 1;
                int pmin = MIN2(MIN2(p1, p2), p3);
                int pmax = MAX2(MAX2(p1, p2), p3);
                int end = width;
                j = 0;
                if (pmin < 0) {
                    j -= pmin;
                    p1 -= pmin;
                    p2 -= pmin;
                    p3 -= pmin;
                }
                if (pmax > 0) {
                    end -= pmax;
                }
                for (; j<end; ++j, ++p1, ++p2, ++p3) {
                    int newsum = sum[3][p3] + mline1[p1] + (int)
                            ((slope_weight + 1.0F) * (float) mline2[p2]);
                    if (sum[0][j] < newsum) {
                        sum[0][j] = newsum;
                        track[j] = DTW_TRACK_DDS;
                    }
                }
            }

        case DTW_SLOPE_2:
            /*  .__ 
               /    */
            if (i > 0) {
                int p = delta[0] - 2;
                int end = width;
                if (p < 0) {
                    j = -p;
                    p = 0;
                } else {
                    j = 1;
                    end -= p;
                }
                if (skips_allowed) {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + (int)
                                (slope_weight * 0.5F * (float) mline[j]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LS;
                        }
                    }
                } else {
                    for (; j<end; ++j, ++p) {
                        int newsum = sum[1][p] + (int)
                                ((slope_weight + 1.0F) * (float) mline[j-1]);
                        if (sum[0][j] < newsum) {
                            sum[0][j] = newsum;
                            track[j] = DTW_TRACK_LS;
                        }
                    }
                }
            }

            /*  ! 
               /  */
            if (i > 1) {
                int p1 = delta[0];
                int p2 = delta[1] - 1;
                int end = width;
                int pmin = MIN2(p1, p2);
                int pmax = MAX2(p1, p2);
                j = 0;
                if (pmin < 0) {
                    j -= pmin;
                    p1 -= pmin;
                    p2 -= pmin;
                }
                if (pmax > 0) {
                    end -= pmax;
                }

                for (; j<end; ++j, ++p1, ++p2) {
                    int newsum = sum[2][p2] + (int)
                                ((slope_weight + 1.0F) * (float) mline1[p1]);
                    if (sum[0][j] < newsum) {
                        sum[0][j] = newsum;
                        track[j] = DTW_TRACK_DS;
                    }
                }
            }

            /* Add the current value to the sum */
            for (j=0; j<width; ++j) {
                sum[0][j] += mline[j];
                if (sum[0][j] > max) {
                    max = sum[0][j];
                    maxpos = j;
                }
            }
            break;
        case DTW_SLOPE_UNCONSTRAINED:
            /* | */
            if (i > 0) {
                int p = delta[0];
                int end = width;
                if (p < 0) {
                    j = -p;
                    p = 0;
                } else {
                    j = 0;
                    end -= p;
                }
                for (; j<end; ++j, ++p) {
                    if (sum[0][j] < sum[1][p]) {
                        sum[0][j] = sum[1][p];
                        track[j] = DTW_TRACK_D;
                    }
                }
            }

            /* -- */
            sum[0][0] += mline[0];
            max = sum[0][0];
            maxpos = 0;
            for (j=1; j<width; ++j) {
                if (sum[0][j] < sum[0][j-1]) {
                    sum[0][j] = sum[0][j-1];
                    track[j] = DTW_TRACK_L;
                }
                sum[0][j] += mline[j];
                if (sum[0][j] > max) {
                    max = sum[0][j];
                    maxpos = j;
                }
            }
            break;
        default:
            fprintf(stderr, "Error in dtw_alignmentmap: unknown local DTW constraint: %d\n", local_constraint);
            goto exit;
        }


        /* Skip within the target song */
        if (i > 0) {
            if (map->initial_alignment_offset[i] !=
                map->initial_alignment_offset[i-1]) {

                for (j=0; j<width; ++j) {
                    sum[0][j] = lastmax + mline[j];
                    track[j] = DTW_TRACK_SKIP;
                    if (max < sum[0][j]) {
                        max = sum[0][j];
                        maxpos = j;
                    }
                }

            } else if (skips_allowed) {
                int average_score = MAX2(16, (lastmax / i - 128));
                double skip_penalty = average_score *
                        (parameters->dtw_skip_delta >> ashift) /
                        (local_constraint + 1);
                for (j=0; j<width; ++j) {
                    int skipsum = lastmax - (int) (skip_penalty *
                            (0.1 + 0.9 * ((double) ABS(maxlinepos[i-1] - j)) /
                            ((double) width)));
                    if (sum[0][j] < (skipsum + mline[j])) {
                        sum[0][j] = skipsum + mline[j];
                        track[j] = DTW_TRACK_SKIP;
                        if (max < sum[0][j]) {
                            max = sum[0][j];
                            maxpos = j;
                        }
                    }
                }
            }
        }

        lastmax = max;
        maxlinepos[i] = maxpos;
        if (sum[0][width-1] > max_right) {
            max_right_pos = i;
            max_right = sum[0][width-1];
        }

        /* Shift sum lines */
        tmp = sum[5];
        sum[5] = sum[4];
        for (j=4; j>0; --j) {
            sum[j] = sum[j-1];
            delta[j] = delta[j-1];
        }
        sum[0] = tmp;
    }

    /* Track alignment path backwards */
    {
        int pos;
        double offset = 0.0;
        if (max_right > lastmax) {
            int j;
            i = max_right_pos;
            pos = width-1;
            for (j=i; j<height-1; ++j) {
                map->initial_alignment_offset[j] = offset;
                map->initial_alignment[j] = map->target_duration;
            }
        } else {
            i = height-1;
            pos = maxlinepos[height-1];
        }
        for (; i>=0; --i) {
            int j, loop, d;
            unsigned char *track = &trackbuffer[i * width];

            if (pos >= 0) map->initial_alignment[i] = (double)
                    MAX2(0, map->lines[i].target_time + (pos << ashift));
            else map->initial_alignment[i] = 0.0;
            map->initial_alignment_offset[i] = offset;

            if (track[pos] == DTW_TRACK_L) {
                d = pos;
                do {
                    --pos;
                } while ((pos > 0) && (track[pos] == DTW_TRACK_L));
                map->initial_alignment[i] -= 0.5 * (double) ((d - pos) <<
                        ashift);
            }
            switch (track[pos]) {
            case DTW_TRACK_LLLLS:
                pos += map_slope - 5;
                break;

            case DTW_TRACK_LLLS:
                pos += map_slope - 4;
                break;

            case DTW_TRACK_LLS:
                pos += map_slope - 3;
                break;

            case DTW_TRACK_LS:
                pos += map_slope - 2;
                break;

            case DTW_TRACK_S:
                pos += map_slope - 1;
                break;

            case DTW_TRACK_D:
                pos += map_slope;
                break;

            case DTW_TRACK_DS:
                loop=1;
                goto loop_down;

            case DTW_TRACK_DDS:
                loop=2;
                goto loop_down;

            case DTW_TRACK_DDDS:
                loop=3;
                goto loop_down;

            case DTW_TRACK_DDDDS:
                loop=4;
loop_down:
                pos += map_slope;
                for (j=0; j<loop; ++j) {
                    --i;
                    map->initial_alignment[i] = (double)
                            map->lines[i].target_time + (pos << ashift);
                    map->initial_alignment_offset[i] = offset;
                    pos += map_slope;
                }
                --pos;
                break;

            case DTW_TRACK_SKIP:
                if (i > 0) {
                    offset += (map->lines[i-1].target_time +
                                 (maxlinepos[i-1] << ashift)) -
                             (map->lines[i].target_time + ((pos-1) << ashift));
                    pos = maxlinepos[i-1];
                } else pos = -1;
                break;

            case DTW_TRACK_END:
            default:
                pos = -1;
            }
        }
    }

    /* Postprocess alignment positions */
    {
        double newoffset = map->initial_alignment[0];
        map->initial_alignment[0] = 0.0;
        for (i=1; i<height; ++i) {
            double d = map->initial_alignment_offset[i] -
                    map->initial_alignment_offset[i-1];
            map->initial_alignment_offset[i-1] = newoffset;
            newoffset += d;
            map->initial_alignment[i] -= newoffset;
        }
        map->initial_alignment_offset[i-1] = newoffset;
    }

exit:
    free(trackbuffer);
    free(maxlinepos);
    free(sbuffer);
}


/**
 * Normalizes first each column and then each row of an alignment map to have
 * a mean value of 128 (there will be variation if values are clipped).
 *
 * @param map the alignment map to normalize
 * @param avoid_clipping set to 1 to avoid clipping the highest and lowest
 *        values
 */
void normalize_alignmentmap(alignmentmap *map) {
    int i, j;
    int width = map->width;
    int height = map->height;

    int *sum;
    int *num_values;
    int num_columns = width;
    int num_rows = height;

    for (i=0; i<height; ++i) {
        int end = ASHIFTR(map->lines[i].target_time, map->accuracy_shift) +
                width;
        num_columns = MAX2(num_columns, end);
    }

    sum = (int *) calloc(MAX2(num_columns, num_rows),
            sizeof(int));
    num_values = (int *) calloc(MAX2(num_columns, num_rows),
            sizeof(int));

    if ((sum == NULL) || (num_values == NULL)) {
        fputs("Error in normalize_alignmentmap: failed to allocate memory\n", stderr);
        free(sum);
        free(num_values);
        return;
    }

    /* Calculate column-wise sums */
    for (i=0; i<height; ++i) {
        unsigned char *buf = map->lines[i].values;
        int start = ASHIFTR(map->lines[i].target_time, map->accuracy_shift);
        int end = MIN2(width, ASHIFTR(map->target_duration -
                map->lines[i].target_time, map->accuracy_shift));
        j = MAX2(0, -start);
        for (; j<end; ++j) {
            sum[start + j] += buf[j];
            num_values[start + j]++;
        }
    }
    
    /* Calculate mean for each column */
    for (i=0; i<num_columns; ++i) {
        if (num_values[i] > 2) {
            sum[i] = lrintf((float) sum[i] / (float) num_values[i]);
        } else sum[i] = 128;
    }

    /* Column-wise normalization: shift the values in each column so that
     * the mean is ~ 127 */
    for (i=0; i<height; ++i) {
        unsigned char *buf = map->lines[i].values;
        int start = ASHIFTR(map->lines[i].target_time, map->accuracy_shift);
        int end = MIN2(width, ASHIFTR(map->target_duration -
                map->lines[i].target_time, map->accuracy_shift));
        j = MAX2(0, -start);
        for (; j<end; ++j) {
            int adjust = 128 - sum[start + j];
            buf[j] = (unsigned char) MIN2(255, MAX2(0, ((int) buf[j]) + adjust));
        }
    }

    memset(sum, 0, num_rows * sizeof(int));
    memset(num_values, 0, num_rows * sizeof(int));

    /* Calculate row-wise sums */
    for (i=0; i<height; ++i) {
        unsigned char *buf = map->lines[i].values;
        int start = ASHIFTR(map->lines[i].target_time, map->accuracy_shift);
        int end = MIN2(width, ASHIFTR(map->target_duration -
                map->lines[i].target_time, map->accuracy_shift));
        j = MAX2(0, -start);
        num_values[i] += end - j;
        for (; j<end; ++j) {
            sum[i] += buf[j];
        }
    }

    /* Calculate mean for each row */
    for (i=0; i<height; ++i) {
        if (num_values[i] > 5) {
            sum[i] = lrintf((float) sum[i] / (float) num_values[i]);
        } else sum[i] = 128;
    }

    /* Row-wise normalization: shift the values in each row so that
     * the mean is ~ 127 */
    for (i=0; i<height; ++i) {
        unsigned char *buf = map->lines[i].values;
        int adjust = 128 - sum[i];
        int start = ASHIFTR(map->lines[i].target_time, map->accuracy_shift);
        int end = MIN2(width, ASHIFTR(map->target_duration -
                map->lines[i].target_time, map->accuracy_shift));
        j = MAX2(0, -start);
        for (; j<end; ++j) {
            buf[j] = (unsigned char) MIN2(255, MAX2(0, ((int) buf[j]) + adjust));
        }
    }

    free(sum);
    free(num_values);
}

/**
 * Smooths the alignment curve by averaging it locally.
 */
static void smooth_initial_alignment(alignmentmap *map, int smooth_range) {
    double *a = (double *) malloc(map->height * sizeof(double));
    int i;
    double sum, num_values;
    if (a == NULL) {
        fputs("Error: failed to allocate memory in smooth_initial_alignment().\n", stderr);
        return;
    }
    smooth_range >>= map->accuracy_shift;
    sum = 0.0;
    num_values = 0.0;
    for (i=0; i<map->height; ++i) {
        /*printf("a: %f o: %f\n", map->initial_alignment[i], map->initial_alignment_offset[i]); */
        if ((i <= smooth_range) || (i >= map->height - 1 - smooth_range)) {
            int j, sr;
            sum = 0.0;
            num_values = 0.0;
            sr = MIN2(i, map->height - 1 - i);
            for (j=i-sr; j<=i+sr; ++j) {
                sum += map->initial_alignment[j];
                num_values += 1.0;
            }
        } else {
            sum += map->initial_alignment[i+smooth_range];
            sum -= map->initial_alignment[i-smooth_range-1];
        }
        a[i] = sum / num_values;
    }
    memcpy(map->initial_alignment, a, map->height * sizeof(double));
    free(a);
}


/**
 * Adjusts song tempo by a given scale ratio.
 *
 * @param scale tempo ratio. For example 3.0 triples the tempo, 0.5 halves it.
 */
static void adjust_song_tempo(song *s, float scale) {
    vector *notes = s->notes;
    int i;
    for (i=0; i<s->size; ++i) {
        notes[i].strt = (int) ((float) notes[i].strt * scale);
        notes[i].dur = (int) ((float) notes[i].dur * scale);
    }
}


static inline void midi_song_init_event_queue(midisong *midi_s,
        pqroot *eventqueue, int *track_position) {
    int i;
    int num_tracks = midi_s->num_tracks;
    for (i=0; i<num_tracks; ++i) {
        pqnode *node = pq_getnode(eventqueue, i);
        if (midi_s->track_size[i] > 0) {
            track_event *te = &midi_s->track_data[i][0];
            node->key1 = (int) te->tick;
            node->pointer = te;
        } else {
            node->key1 = INT_MAX;
            node->pointer = NULL;
        }
        node->key2 = i;
        pq_update_key1(eventqueue, node);
        track_position[i] = 0;
    }
}

static inline void midi_song_get_next_event(midisong *midi_s,
        pqroot *eventqueue, int *track_position, track_event **te, int *track) {
    pqnode *node = pq_getmin(eventqueue);
    int i = node->key2;
    *track = i;
    *te = (track_event *) node->pointer;
    if ((i<0) || (i >= midi_s->num_tracks)) return;
    ++track_position[i];
    if (track_position[i] < midi_s->track_size[i]) {
        track_event *e = &midi_s->track_data[i][track_position[i]];
        node->key1 = (int) e->tick;
        node->pointer = e;
    } else {
        node->key1 = INT_MAX;
        node->pointer = NULL;
    }
    pq_update_key1(eventqueue, node);
}


/**
 * Aligns a MIDI song by the given alignment data.
 *
 * @param midi_s MIDI song to align
 * @param a the alignment to use
 */
void align_midi_song(midisong *midi_s, alignment *a) {
    int i, apos;
    int skips = 0;
    int num_tracks = midi_s->num_tracks;
    int *track_position;
    int *ebuf_position;
    int *pattern_times = a->pattern_times;
    int *target_times = a->target_times;
    track_event **ebuf;
    pqroot *eventqueue;
    double slope = 1.0;

    /* Count skips */
    for (i=0; i<a->size; ++i) {
        if (a->skip[i]) {
            ++skips;
        }
    }
    ebuf = (track_event **) malloc(num_tracks * sizeof(track_event *));
    ebuf_position = (int *) calloc(num_tracks, sizeof(int));
    track_position = (int *) malloc(num_tracks * sizeof(int));
    if ((ebuf == NULL) || (ebuf_position == NULL) || (track_position == NULL)) {
        fputs("Error in align_midi_song(): Failed to allocate memory for track array\n", stderr);
        free(ebuf);
        free(ebuf_position);
        free(track_position);
        return;
    }
    for (i=0; i<num_tracks; ++i) {
        int extra;
        if (i == 0) extra = 20;
        else extra = 2;
        ebuf[i] = (track_event *) malloc((midi_s->track_size[i]+extra) *
                sizeof(track_event) * (skips + 1));
        if (ebuf[i] == NULL) {
            for (--i; i>=0; --i) free(ebuf[i]);
            free(ebuf);
            free(ebuf_position);
            free(track_position);
            fputs("Error in align_midi_song(): Failed to allocate memory for track event buffer\n", stderr);
            return;
        }
    }

    /* Create an event queue, sorted by event time */
    eventqueue = pq_create(num_tracks);

    
    /* Loop through all continuous alignment sequences that are separated
     * by skips in the target song. */
    apos = 0;
    while((apos < a->size) && ((pattern_times[apos] < 0) ||
                (target_times[apos] < 0))) ++apos;
    while(apos < a->size) {
        int curtime = INT_MIN;
        int track;
        track_event *te = NULL;
        int target_time = target_times[apos];

        midi_song_init_event_queue(midi_s, eventqueue, track_position);

        while(curtime < target_time) {
            unsigned char status;
            midi_song_get_next_event(midi_s, eventqueue, track_position,
                    &te, &track);
            if (te == NULL) break;
            else curtime = te->tick;

            /* Keep record of state variations, such as program changes */
            status = te->status & STATUS_MASK;
            if ((status == EVENT_PROGRAM_CHANGE) ||
                    (status == EVENT_CONTROLLER) ||
                    ((status == EVENT_SYSEX) &&
                        ((te->status == EVENT_SYSEX) ||
                        (te->status == EVENT_ESCAPE)))) {
                track_event *e = &ebuf[track][ebuf_position[track]++];
                memcpy(e, te, sizeof(track_event));
                if (apos == 0) e->tick = 0;
                else e->tick = pattern_times[apos];
                
            /* Copy META events that are at the beginning of the track */
            } else if ((apos == 0) && (te->status == EVENT_META)) {
                track_event *e = &ebuf[track][ebuf_position[track]++];
                memcpy(e, te, sizeof(track_event));
                e->tick = 0.0;
            }
        }
/*
        fprintf(stderr, "APOS: %d/%d time: %d, mtime: %d\n", apos, a->size, target_time, curtime);
*/
        /* Find the first event that is potentially within the window */
        /* End of the target song was encountered. Find the next skip,
         * if there is one */
        if (te == NULL) {
            while ((++apos < a->size) && (!a->skip[apos]));
            continue;
        }
        /* Align target to the current alignment window sequence */
        do {
            int pattern_time = pattern_times[apos];
            int next_target_time = INT_MAX;
            /* Calculate time slope for the alignment window */
            while(++apos < a->size) {
                if (a->skip[apos]) {
                    next_target_time = target_time + (int) ((double)
                            (pattern_times[apos] - pattern_time) / slope);
                    break;
                } else if (target_time < target_times[apos]) {
                    next_target_time = target_times[apos];
                    slope = (double) (pattern_times[apos] - pattern_time) /
                            (double) (target_times[apos] - target_time);
                    break;
                }
            }
            if (apos >= a->size) {
                /* End. Insert end-of-track events. */
                for (track=0; track<num_tracks; ++track) {
                    track_event *e = &ebuf[track][ebuf_position[track]++];
                    e->tick = pattern_times[a->size-1];
                    e->status = EVENT_META;
                    e->metatype = EVENT_END_OF_TRACK;
                    e->data = NULL;
                    e->length = 0;
                }
                break;
            }
            /* Align events within the window and put them to the new buffer */
            while(curtime < next_target_time) {
                track_event *e = &ebuf[track][ebuf_position[track]++];
                memcpy(e, te, sizeof(track_event));
                e->tick = pattern_time + (int) (slope *
                        (double) (e->tick - target_time));
/*printf("  tick: %f (%f) curtime: %d, slope: %f, t1: %d, p1: %d\n", e->tick, te->tick, curtime, slope, target_time, pattern_time);*/
                midi_song_get_next_event(midi_s, eventqueue, track_position,
                        &te, &track);
                if (te == NULL) break;
                else curtime = te->tick;
            }
            /* End of the target song was encountered. Find the next skip,
             * if there is one */
            if (te == NULL) {
                while ((!a->skip[apos]) && (++apos < a->size));
                break;
            }
            target_time = next_target_time;
        } while((apos < a->size) && (!a->skip[apos]));

        /* Turn all notes off before skipping */
        if (apos < a->size) {
            static unsigned char midi_notes_off[2] = {123, 0};
            char channel;
            /*printf("SKIP: %d\n", pattern_times[apos]);*/
            for (channel=0; channel<16; ++channel) {
                te = &ebuf[0][ebuf_position[0]++];
                te->tick = pattern_times[apos + 1];
                te->status = 0xB0 + channel;
                te->metatype = 0;
                te->data = midi_notes_off;
                te->length = 2;
            }
        }
    }

    /* Replace old track event arrays with new ones */
    for (i=0; i<num_tracks; ++i) {
        free(midi_s->track_data[i]);
        midi_s->track_data[i] = (track_event *) malloc(ebuf_position[i] *
                sizeof(track_event));
        memcpy(midi_s->track_data[i], ebuf[i], ebuf_position[i] *
                sizeof(track_event));
        midi_s->track_size[i] = ebuf_position[i];
        free(ebuf[i]);
    }
    free(ebuf);
    free(ebuf_position);
    free(track_position);
    pq_free(eventqueue);
}


void generate_alignment(alignment *a, int song_length, int accuracy,
        float max_slope, int skip_interval, int tempo_change_interval,
        float local_variation) {

    int i, ptime, next_skip, next_tempo_change;
    double ttime;
    double delta;
    double max_delta = max_slope * (double) accuracy;
    double min_delta = ((double) accuracy) / max_slope;

    a->size = max_slope * song_length / accuracy;
    a->pattern_times = (int *) malloc(a->size * sizeof(int));
    a->target_times = (int *) malloc(a->size * sizeof(int));
    a->skip = (char *) calloc(a->size, sizeof(char));
    a->quality = (unsigned char *) malloc(a->size * sizeof(unsigned char));
    if ((a->pattern_times == NULL) || (a->target_times == NULL) ||
            (a->skip == NULL) || (a->quality == NULL)) {
        fputs("Error in generate_alignment: failed to allocate memory\n",
                stderr);
        free_alignment(a);
        return;
    }

    ptime = 0;
    ttime = 0.0;
    delta = min_delta + randd() * (max_delta - min_delta);

    next_skip = lrint((1.0 + (4.0 * randd())) * (double) skip_interval);
    next_tempo_change = lrint((1.0 + (4.0 * randd())) *
            (double) tempo_change_interval);
    if ((skip_interval < INT_MAX) && (randd() < 1.0 / 3.0)) {
        ttime = randd() * (double) (song_length / 2);
    }

    fprintf(stderr, "\nsong_length: %d\n", song_length);
    fprintf(stderr, "\ndelta: %f, tempo_change: %d, skip: %d\n\n", delta,
            next_tempo_change, next_skip);
    for (i=0; i<a->size; ++i, ptime+=accuracy, ttime+=delta) {
        if (ttime >= song_length) {
            if (skip_interval == INT_MAX) {
                a->size = i;
                return;
            } else {
                next_skip = ptime;
            }
        }
        if ((skip_interval < INT_MAX) && (ptime >= next_skip)) {
            a->skip[i] = 1;
            ttime = randd() * (double) MAX2(0, (song_length -
                    skip_interval));
            next_skip = ptime + lrint((1.0 + (4.0 * randd())) * (double) skip_interval);
            fprintf(stderr, "%d: skip to %d\n", ptime, (int) lrint(ttime));
        }
        if (ptime >= next_tempo_change) {
            delta = min_delta + randd() * (max_delta - min_delta);

            next_tempo_change = ptime + lrint((1.0 + (4.0 * randd())) *
                    ((double) tempo_change_interval));

            fprintf(stderr, "%d: tempo change\n", ptime);
        }
        a->pattern_times[i] = ptime;
        a->target_times[i] = MAX2(0, MIN2(lrint(ttime), song_length));
        a->quality[i] = 128;

        delta += (0.5 - randd()) * local_variation * (double) accuracy;
        delta = MAX2(min_delta, MIN2(delta, max_delta));
    }
}

void delay_alignment(alignment *a, int delay) {
    int i;
    for (i=0; i<a->size; ++i) {
        a->target_times[i] = a->target_times[i] + delay;
    }
}


void print_alignment(alignment *a) {
    int i;
    for (i=0; i<a->size; ++i) {
        printf("%d\t%d\t%d\n", a->pattern_times[i], a->target_times[i], a->quality[i]);
    }
}


void write_alignment(alignment *a, char *path) {
    int i;
    FILE *f = fopen(path, "w");
    if (f == NULL) {
        fprintf(stderr, "Error in write_alignment: unable to write to %s\n", path);
        return;
    }
    for (i=0; i<a->size; ++i) {
        fprintf(f, "%d\t%d\t%d\n", a->pattern_times[i], a->target_times[i], a->quality[i]);
    }
    fclose(f);
}


void print_initial_alignment(alignmentmap *map) {
    int i;
    for (i=0; i<map->height; ++i) {
        if ((i > 0) && (map->initial_alignment_offset[i] !=
                        map->initial_alignment_offset[i-1])) {
            printf("----skip-----\n");
        }
        printf("%d\t%f\t%f\n", map->lines[i].pattern_time, map->initial_alignment[i], map->initial_alignment_offset[i]);
    }
}


int main(int argc, char **argv) {
    alignparameters p;
    songcollection sc, pc;
    int i;
    int algorithms[2];
    dataparameters data_parameters;
    struct timeval start, end, tstart, tend;
    alignmentmap map1, map2;
    alignment a;
    char *pgm_filename = NULL;

    align_init_parameters(&p);
    if (!align_parse_arguments(argc, argv, &p)) return 1;
    if (p.song_path == NULL) {
        fputs("\nError: no score specified\n\n", stderr);
        return 1;
    }
    if ((p.generate_alignment == 0) && (p.pattern_path == NULL)) {
        fputs("\nError: no song specified\n\n", stderr);
        return 1;
    }

    algorithms[0] = p.algorithm;
    algorithms[1] = -1;

    data_parameters.c_window = 30;
    data_parameters.avindex_vector_max_width = 10000;
    data_parameters.avindex_vector_max_height = 128;

    init_song_collection(&sc, 0);
    init_song_collection(&pc, 0);

    /* Read songs */

    if (p.verbose >= LOG_IMPORTANT) fputs("Reading song files...\n", stderr);
    gettimeofday(&start, NULL);
    i = read_midi_collection(p.song_path, &sc, SKIP_PERCUSSION);
    if (i <= 0) {
        fprintf(stderr, "\nError: Unable to read songs from: %s\n\n",
                p.song_path);
        goto EXIT;
    }
    if (p.pattern_path != NULL) {
        i = read_midi_collection(p.pattern_path, &pc, SKIP_PERCUSSION);
        if (i <= 0) {
            fprintf(stderr, "\nError: Unable to read songs from: %s\n\n",
                    p.pattern_path);
            goto EXIT;
        }
    }
    gettimeofday(&end, NULL);
    if (p.verbose >= LOG_IMPORTANT)
        fprintf(stderr, "Time: %f\n", timediff(&end, &start));


    /* Preprocess songs (convert, remove overlaps) */

    if (p.verbose >= LOG_IMPORTANT) fputs("\nPreprocessing...\n", stderr);
    gettimeofday(&start, NULL);

    preprocess_songs(&sc, 0, p.song_margins, p.map_accuracy, 0, -1);
    if (p.p3_optimize > 0) {
        p3_optimize_song_collection(&sc, p.p3_optimize);
    }
    if (p.song_tempo != 1.0F) {
        for (i=0; i<sc.size; ++i) {
            adjust_song_tempo(&sc.songs[i], p.song_tempo);
        }
    }

    if (p.max_pattern_size > 0) {
        sc.songs[0].size = MIN2(sc.songs[0].size, p.max_pattern_size);
        sc.songs[0].end = sc.songs[0].notes[sc.songs[0].size-1].strt;
    }

    update_song_collection_statistics(&sc);
    update_song_collection_data(&sc, algorithms, &data_parameters);

    if (p.pattern_path != NULL) {
        preprocess_songs(&pc, 0, p.song_margins, p.map_accuracy, 0, -1);
        if (p.p3_optimize > 0) {
            p3_optimize_song_collection(&pc, p.p3_optimize);
        }
        if (p.max_pattern_size > 0) {
            pc.songs[0].size = MIN2(pc.songs[0].size, p.max_pattern_size);
            pc.songs[0].end = pc.songs[0].notes[pc.songs[0].size-1].strt;
        }
        update_song_collection_statistics(&pc);
        update_song_collection_data(&pc, algorithms, &data_parameters);
    }

    gettimeofday(&end, NULL);

    if (p.verbose >= LOG_IMPORTANT)
        fprintf(stderr,"Time: %f\n", timediff(&end, &start));

    if (p.output_map != NULL) {
        pgm_filename = (char *) malloc((strlen(p.output_map) + 256) *
                sizeof(char));
    }

#if 0
    srand(1234);
    for (i=0; i<sc.size; ++i) {
        test_song_window(&sc.songs[i], 30, 30, 1, 30000);
        test_p3s_window(&sc.songs[i], 100, 100, 2000);
    }
#endif

    memset(&map1, 0, sizeof(alignmentmap));
    memset(&map2, 0, sizeof(alignmentmap));


    if (p.generate_alignment > 0) {
        srand(p.generate_alignment);
        generate_alignment(&a, sc.songs[0].end,
                p.map_accuracy >> (4 * p.multiscale),
                p.p3_scales_spread,
                p.dtw_skip_delta, 30000, 0.03F);
                
        if (p.output_alignment != NULL) {
            write_alignment(&a, p.output_alignment);
        }

        if (p.output_midi != NULL) {
            midisong midi_s;
            read_midi_file(sc.songs[0].title, NULL, &midi_s, 0);
            align_midi_song(&midi_s, &a);
            write_midi_file(p.output_midi, &midi_s, 1);
            free_midisong(&midi_s);
        }
        free_alignment(&a);
        goto EXIT;
    }

    for (i=0; i<pc.size; ++i) {
        song *pattern = &pc.songs[i];
        song *target = &sc.songs[0];
        alignmentmap *curmap = &map1, *prevmap = NULL;
        int map_width = -1;
        int j;
        double total_search_time = 0.0;
        double total_dtw_time = 0.0;
        size_t total_dtw_space = 0;

        fputs("\n\n================================================\n", stderr);
        fprintf(stderr, "Aligning song %s (%d notes)\n", pattern->title,
                pattern->size);
        fprintf(stderr, "  With score: %s (%d notes)\n", target->title,
                target->size);
 
        gettimeofday(&tstart, NULL);

        if (p.map_size > 0) {
            pattern->end = p.map_size;
            target->end = p.map_size;
        }
        for (j=0; j<=p.multiscale; ++j) {
            double search_time;
            double dtw_time;
            size_t dtw_space;
            if (p.multiscale) {
                fputs("\n------------------------------\n", stderr);
                fprintf(stderr, "Multiscale alignment level: %d\n", j+1);
                fputs("------------------------------\n", stderr);
            }
            dtw_space = init_alignmentmap(curmap, &sc, target, pattern,
                    p.map_accuracy, map_width, prevmap);

            fputs("\nCreating correlation matrix...\n", stderr);
            gettimeofday(&start, NULL);
            map_alignments(curmap, p.algorithm, &p);
            if (p.normalize) {
                normalize_alignmentmap(curmap);
            }
            gettimeofday(&end, NULL);
            search_time = timediff(&end, &start);
            fprintf(stderr, "Time: %f\n", search_time);
            search_time = search_time * 1000.0;

            fputs("\nCalculating alignment...\n", stderr);
            gettimeofday(&start, NULL);
            dtw_alignment(curmap, &p);
            gettimeofday(&end, NULL);
            dtw_time = timediff(&end, &start);
            fprintf(stderr, "Time: %f\n", dtw_time);
            dtw_time = dtw_time * 1000.0;

            fputs("\nSmoothing alignment...\n", stderr);
            gettimeofday(&start, NULL);
            smooth_initial_alignment(curmap, p.smooth_range);
            gettimeofday(&end, NULL);
            fprintf(stderr, "Time: %f\n", timediff(&end, &start));

            /*fputs("\nAlignment:\n", stderr);
            print_initial_alignment(curmap);*/

            if (p.output_map != NULL) {
                fprintf(stderr, "\nWriting alignment map to file: %s\n\n",
                        p.output_map);
                if (pc.size > 1) {
                    snprintf(pgm_filename, strlen(p.output_map) + 256,
                            "%s.pat%d.scale%d.pgm", p.output_map, i+1, j+1);
                } else {
                    snprintf(pgm_filename, strlen(p.output_map) + 256,
                            "%s.scale%d.pgm", p.output_map, j+1);
                }
                write_alignmentmap_pgm(pgm_filename, curmap, 1, 1);
            }

            p.map_accuracy = MAX2(1, p.map_accuracy >> 4);
            p.p3_scales_num = 5;
            p.p3_scales_spread = 1.5;
            p.p3_pattern_window_skip = p.p3_pattern_window_width >> 1;
            p.dtw_skip_delta = 100000;
            p.smooth_range = p.smooth_range >> 4;
            map_width = p.map_accuracy * MULTISCALE_MAP_WIDTH;
            if (curmap == &map1) {
                curmap = &map2;
                prevmap = &map1;
            } else {
                curmap = &map1;
                prevmap = &map2;
            }
            if (j == p.multiscale) {
                init_alignment(&a, prevmap);
            }
            free_alignmentmap_buffers(prevmap);
            free_alignmentmap(curmap);

            /* Add dtw_alignment() memory consumption */
            dtw_space += prevmap->height * prevmap->width +
                    prevmap->height * sizeof(int) +
                    6 * prevmap->width * sizeof(int);

            total_search_time += search_time;
            total_dtw_time += dtw_time;
            total_dtw_space = MAX2(total_dtw_space, dtw_space);

            /* Write search time and memory usage information to stdout */
            if (j == 0) fprintf(stderr, "# multiscale_level\t song_notes\t score_notes\t map_seconds\t map_accuracy\t map_height\t map_width\t search_time\t dtw_time\t dtw_space\n");
            printf("  %d\t\t\t %d\t\t %d\t\t %d\t\t %d\t\t %d\t\t %d\t\t %f\t %f\t %ld\n",
                    j+1, pattern->size, target->size,
                    p.map_size/1000, prevmap->accuracy,
                    prevmap->height, prevmap->width,
                    search_time, dtw_time, dtw_space);
        }
        printf("  %d\t\t\t %d\t\t %d\t\t %d\t\t %d\t\t %d\t\t %d\t\t %f\t %f\t %ld\n",
                0, pattern->size, target->size,
                p.map_size/1000, 0, 0, 0,
                total_search_time, total_dtw_time, total_dtw_space);

        gettimeofday(&tend, NULL);
        fprintf(stderr, "Total alignment time: %f\n\n",
                timediff(&tend, &tstart));
 
    
        delay_alignment(&a, p.delay);

        /*fputs("\nAlignment:\n", stderr);*/
        /*print_initial_alignment(prevmap);*/
        /*print_alignment(&a);*/
        if (p.output_alignment != NULL) {
            write_alignment(&a, p.output_alignment);
        }

        if (p.output_midi != NULL) {
            midisong midi_s;
            read_midi_file(target->title, NULL, &midi_s, 0);
            align_midi_song(&midi_s, &a);
            write_midi_file(p.output_midi, &midi_s, 1);
            free_midisong(&midi_s);
        }
        free_alignmentmap(&map2);
        free_alignmentmap(&map1);
        free_alignment(&a);
    }


EXIT:
    free(pgm_filename);
    align_free_parameters(&p);
    free_song_collection(&sc);
    free_song_collection(&pc);

    return 0;
}

