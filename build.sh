#!/bin/sh

SRC_ROOT=`dirname "$0"`

echo -e "\nBuilding in $SRC_ROOT/build"
cd "$SRC_ROOT"

mkdir -p build
cd build

cmake ..
make VERBOSE=1

