# CMake rules to build libclt

project (clt)

set (CLT_VERSION_MAJOR 0)
set (CLT_VERSION_MINOR 4)


add_library(clt clt.c)


include (CheckIncludeFiles)
include (CheckSymbolExists)

check_include_files (malloc.h HAVE_MALLOC_H)
check_symbol_exists (posix_memalign stdlib.h HAVE_POSIX_MEMALIGN)

configure_file (
  "${PROJECT_SOURCE_DIR}/config.h.in"
  "${PROJECT_BINARY_DIR}/config.h"
)

include_directories ("${PROJECT_BINARY_DIR}")

