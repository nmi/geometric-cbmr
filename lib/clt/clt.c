/* clt.c: Command line interface tools
 *
 * Author: Niko Mikkilä
 *
 * Public Domain
 */

#include "config.h"

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#if HAVE_MALLOC_H
#include <malloc.h>
#endif

#include "clt.h"


static int log_target_set = 0;
static FILE *log_target = NULL;
static enum clt_log_level log_level = CLT_LOG_LEVEL_INFO;
static const char * const log_level_strings[] = {"Error", "Warning", "Info"};
static int log_source_functions = 0;


/**
 * Returns current target stream for CLT_ERROR, CLT_WARNING and CLT_INFO
 * logging macros. Default target is stderr and it can be changed with
 * clt_set_log_target.
 *
 * @return log target stream
 */
FILE *clt_get_log_target(void) {
    if (log_target_set) return log_target;
    else return CLT_DEFAULT_LOG_TARGET;
}


/**
 * Sets target stream for CLT_ERROR, CLT_WARNING and CLT_INFO logging macros.
 *
 * @param target FILE stream for logging
 */
void clt_set_log_target(FILE *target) {
    log_target = target;
    log_target_set = 1;
}

/**
 * Returns logging level: CLT_LOG_LEVEL_ERROR, CLT_LOG_LEVEL_WARNING or
 * CLT_LOG_LEVEL_INFO.
 *
 * @return current logging level.
 */
enum clt_log_level clt_get_log_level(void) {
    return log_level;
}


/**
 * Sets logging level. To only log error messages, use
 * CLT_LOG_LEVEL_ERROR. For errors and warnings, use CLT_LOG_LEVEL_WARNING.
 * For everything, use CLT_LOG_LEVEL_INFO.
 *
 * @param level        logging level
 * @param print_source set to 1 to print source code files and line numbers
 *                     in error and warning messages
 */
void clt_set_log_level(enum clt_log_level level, int print_source) {
    log_level = level;
    log_source_functions = print_source;
}


/**
 * Writes a message to the log. This function should be called through
 * the CLT_INFO(), CLT_ERROR() or CLT_WARNING() preprocessor macros.
 *
 * @param level    message logging level: CLT_LOG_LEVEL_ERROR or
 *                 CLT_LOG_LEVEL_WARNING
 * @param file     source file name
 * @param function source function name
 * @param line     source line number
 * @param format   output formatting string to pass to fprintf()
 * @param ...      variable arguments for fprintf()
 */
void _attribute_printf_(5, 6)
clt_log(enum clt_log_level level, const char *file,
             const char *function, int line, const char *format, ...) {
    va_list ap;
    FILE *out;
    if (level > log_level) return;
    if (log_target_set) out = log_target;
    else out = CLT_DEFAULT_LOG_TARGET;
    if (out == NULL) return;
    if (log_source_functions && (file != NULL) && (function != NULL)) {
        fprintf(out, "[%s] at %s (%s, line %d):\n    ",
                log_level_strings[level], function, file, line);
    } else {
        fprintf(out, "[%s] ", log_level_strings[level]);
    }
    va_start(ap, format);
    vfprintf(out, format, ap);
    fputs("\n", out);
}


/**
 * Returns a pointer to the option struct that matches given id or short option.
 *
 * @param options pointer to an array of option structs
 * @param optid short option character or option id
 *
 * @return pointer to matching option struct or NULL if no match is found
 */
const clt_option *clt_get_short_option(const clt_option *options, int optid) {
    int i = 0;
    if (options == NULL)
    do {
        if (options[i].id == optid) return &options[i];
        ++i;
    } while (options[i].id >= 0);
    if (optid < 0) return &options[i];
    else return NULL;
}


/**
 * Returns a pointer to the option struct that matches given long option string.
 *
 * @param options   pointer to an array of option structs
 * @param optstring long option string
 *
 * @return pointer to matching option struct or NULL if no match is found
 */
const clt_option *clt_get_long_option(const clt_option *options,
                                      const char *optstring) {
    int i = 0;
    do {
        if (((optstring == NULL) && (options[i].id < 0)) ||
                (!strcmp(optstring, options[i].long_option)))
            return &options[i];
        ++i;
    } while (options[i].id >= 0);
    if (optstring == NULL) return &options[i];
    else return NULL;
}


/**
 * Parses an option and its argument from the argv array.
 *
 * @param opt               option struct
 * @param argument          option argument string
 * @param target_parameters parameter structure passed through to the
 *                          argument parser callback function
 *
 * @return number of parsed arguments for the option. 0 if the option itself
 *         is an argument, -1 if a required argument is missing.
 */
int clt_parse_option(const clt_option *opt, const char *argument,
                 void *target_parameters) {
    const char *arg;

    if (opt == NULL) {
        return -1;
    }
    if (opt->has_argument == CLT_NO_ARG) {
        arg = NULL;
    } else if (opt->has_argument == CLT_OPTIONAL_ARG) {
        if ((argument != NULL) && (argument[0] != '\0')) {
            if (argument[0] == '-') arg = NULL;
            else arg = argument;
        } else arg = NULL;
    } else {
        if ((argument == NULL) || (argument[0] == '\0')) {
            CLT_ERROR("Required argument missing");
            /*clt_show_error_position(stderr, pos, argc, argv);*/
            return -1;
        } else arg = argument;
    }
    if (opt->parser == NULL) {
        if (isalnum(opt->id) && (opt->long_option != NULL))
            CLT_ERROR("Missing argument parsing function for option -%c, --%s", 
                      opt->id, opt->long_option);
        else if (opt->long_option != NULL)
            CLT_ERROR("Missing argument parsing function for option --%s", 
                      opt->long_option);
        else CLT_ERROR("Missing parsing function for non-option arguments");
        return -1;
    } else {
        if (!(opt->parser)(opt->id, arg, target_parameters)) return -1;
    }

    if (opt->id < 0) return 0;
    else if (arg != NULL) return 1;
    else return -1;
}


/**
 * Parses described options and other arguments from the command line.
 *
 * @param argc              number of command line arguments
 * @param argv              array of pointers to argument strings
 * @param options           pointer to an array of option descriptions
 * @param target_parameters pointer to a parameter structure that will be
 *                          passed to the callback parser function.
 * @return 1 if successful, 0 otherwise
 */
int clt_parse_argv(int argc, const char * const argv[],
                   const clt_option *options, void *target_parameters) {
    int i;
    int pos = 0;
    for (i=1; i<argc; ++i) {
        int ret = 0;
        const char *arg = argv[i];
        const char *nextarg;
        const clt_option *opt;

        if (i < argc-1) nextarg = argv[i+1];
        else nextarg = NULL;

        if (arg[0] == '\0') continue;
        if (arg[0] == '-') {
            if (arg[1] == '\0') {
                /* Plain "-" is passed through as a separate argument */
                opt = clt_get_short_option(options, -1);
                nextarg = arg;
            } else if (arg[1] == '-') {
                if (arg[2] == '\0') {
                    /* Short option "--" is also supported */
                    opt = clt_get_short_option(options, '-');
                    nextarg = arg;
                } else {
                    /* Long option */
                    opt = clt_get_long_option(options, &arg[2]);
                }
            } else {
                int j = 1;
                /* Parse multiple concatenated short options */
                while (arg[j+1] != '\0') {
                    opt = clt_get_short_option(options, arg[j]);
                    ret = clt_parse_option(opt, NULL, target_parameters);
                    if (ret < 0) {
                        if (opt == NULL) {
                            CLT_ERROR("Unknown option \"-%c\"", arg[j]);
                            clt_show_error_position(clt_get_log_target(),
                                                    pos, argc, argv);
                            return 0;
                        }
                        break;
                    }
                    ++j;
                }
                /* Only the last option can have an argument */
                opt = clt_get_short_option(options, arg[j]);
            }
        } else {
            /* This is a non-option argument */
            opt = clt_get_short_option(options, -1);
            nextarg = arg;
        }
        if (ret >= 0) ret = clt_parse_option(opt, nextarg, target_parameters);
        if (ret < 0) {
            if (opt == NULL) CLT_ERROR("Unknown option \"%s\"", argv[i]);
            clt_show_error_position(clt_get_log_target(), pos, argc, argv);
            return 0;
        }
        else {
            pos += (int) strlen(argv[i]);
            if (ret) {
                pos += (int) strlen(argv[i+1]);
            }
            i += ret;
        }
    }
    return 1;
}


/**
 * Prints command line arguments until the given position is reached and
 * marks it with a '^' on the next line.
 *
 * @param out   output file or stream, for example stderr
 * @param pos   position of an incorrect argument
 * @param argc  number of arguments
 * @param argv  array of pointers to argument strings
 */
void clt_show_error_position(FILE *out, int pos, int argc, const char * const argv[]) {
    int i;
    int argp = 0;
    int p = 0;
    if (out == NULL) return;
    for (i=1; i<argc; ++i) {
        int j;
        int len = (int) strlen(argv[i]);
        for (j=0; j<len; ++j, ++p, ++argp) {
            if (p >= 78) {
                fputc('\n', out);
                if (pos < 0) {
                    for (; pos < 0; ++pos) fputc(' ', out);
                    fputc('^', out);
                    fputc('\n', out);
                    break;
                }
                p = 0;
            }
            fputc(argv[i][j], out);
            if (pos == argp) pos = -p;
        }
        fputc(' ', out);
    }
    if (pos < 0) {
        for (; pos < 0; ++pos) fputc(' ', out);
        fputc('^', out);
        fputc('\n', out);
    }
}


/**
 * Prints option descriptions neatly.
 *
 * @param out         output file or stream, for example stderr
 * @param options     pointer to an array of option structs
 * @param col1_width  width of the first column (option names), for example 32
 * @param col2_width  width of the second column (descriptions), for example 48
 */
void clt_print_options(FILE *out, const clt_option *options,
                       int col1_width, int col2_width) {
    int i = 0;
    const char *arg_brackets[] = {"  ", "[]", "<>"};
    int indent = 2;

    while (options[i].id >= 0) {
        int pos = 0;
        int cut = 0;
        int lastcut = 0;
        int col2w = col2_width;
        int has_arg = options[i].has_argument;
        const char *description = options[i].description;
        int dlen = (int) strlen(description);

        /* Print short option */
        if (isalnum(options[i].id)) {
            pos += fprintf(out, "  -%c", options[i].id);
        } else {
        }

        /* Print long option */
        if (options[i].long_option != NULL) {
            if (pos == 0) pos += fputs("     ", out);
            else pos += fputs(", ", out);
            pos += fprintf(out, "--%s", options[i].long_option);
        }
        if ((options[i].argument_type != NULL) && has_arg) {
            pos += fprintf(out, " %c%s%c", arg_brackets[has_arg][0],
                    options[i].argument_type, arg_brackets[has_arg][1]);
        }
        if (pos >= col1_width) {
            fputc('\n', out);
            pos = 0;
        }
        for (; pos < col1_width-1; ++pos) fputc(' ', out);

        /* Print description */
        while (lastcut < dlen) {
            int hardbreak = lastcut + col2w;
            cut = lastcut;
            if (hardbreak < dlen) {
                /* Try to cut at a space to fit the description to its column */
                for (pos = hardbreak; pos > cut; --pos) {
                    if (description[pos] == ' ') cut = pos;
                }
                if (cut == lastcut) cut = hardbreak;
            } else {
                cut = dlen;
            }

            for (pos = lastcut; pos < cut; ++pos) {
                if (description[pos] == '\n') {
                    cut = pos;
                    break;
                }
                fputc(description[pos], out);
            }
            fputc('\n', out);

            lastcut = cut + 1;
            if (lastcut < dlen) {
                for (pos = 0; pos < col1_width + indent; ++pos) fputc(' ', out);
            }
            col2w = col2_width - indent;
        }
        ++i;
    }
}


/**
 * Returns the closest power of two that is larger than (or equal to) the given 
 * value or CLT_MIN_ALIGNMENT.
 *
 * @param alignment an unsigned integer value
 *
 * @return closest larger or equal power of two
 */
static size_t clt_get_power_of_two_alignment(size_t alignment) {
    if (alignment < CLT_MIN_ALIGNMENT) alignment = CLT_MIN_ALIGNMENT;
    else if ((alignment & (alignment - 1)) != 0) {
        size_t a = CLT_MIN_ALIGNMENT;
        while (a < alignment) a = a << 1;
        alignment = a;
    }
    return alignment;
}


/**
 * Aligned malloc. Allocated area must be freed with clt_free.
 *
 * @param alignment Returned area will be located at an address multiple of
 *        this value in bytes. Must be a power of two.
 * @param size amount of memory to allocate
 *
 * @return pointer to the allocated area, or NULL if allocation fails.
 */
void *clt_alloc_aligned(size_t alignment, size_t size) {
    void *ptr = NULL;

    /* Make sure that alignment is a power of 2 */
    alignment = clt_get_power_of_two_alignment(alignment);

#if defined(HAVE_POSIX_MEMALIGN)
    {
        int ret;
        ret = posix_memalign(&ptr, alignment, size);
        if (ret == EINVAL) {
            CLT_ERROR("Invalid alignment. This should not happen. Problem in function clt_get_power_of_two_alignment.");
        }
    }
#elif defined(HAVE_MALLOC_H)
    ptr = memalign(alignment, size);
#else
    {
        void *p = malloc(size + alignment + sizeof(void **));
        if (p) {
            ptr = p + (alignment - 1) + sizeof(void **);
            ptr -= (intptr_t) ptr & (alignment - 1);
            *((void **) (ptr - sizeof(void **))) = p;
        }
    }
#endif
    if (!ptr) CLT_ERROR("Memory allocation of %zd bytes failed", size);
    return ptr;
}

/**
 * Aligned realloc.
 *
 * NOTE: Unlike the library realloc, this function frees the original memory
 *       area if allocation fails. Use clt_realloc_safe if you need to keep
 *       the data block untouched in case of failure.
 *
 * @param ptr pointer to previously allocated memory. If NULL, this function
 *        works like clt_alloc_aligned.
 * @param alignment
 * @param size new size for the area
 *
 * @return pointer to a reallocated area, or NULL if allocation fails.
 */
void *clt_realloc_aligned(void *ptr, size_t alignment, size_t size) {
    if (clt_realloc_safe(&ptr, alignment, size)) {
        clt_free(ptr);
        return NULL;
    } else return ptr;
}


/**
 * Aligned realloc with a posix_memalign-style interface. Calls the C library
 * realloc and moves the memory area back to an aligned position if necessary.
 *
 * If realigning the memory block fails, it will remain unaligned. In this
 * case the function returns 1. If realloc fails, function will return ENOMEM.
 *
 * @param ptr pointer to a pointer to previously allocated memory. If *ptr is
 *            NULL, this function works like clt_alloc_aligned.
 * @param alignment
 * @param size new size for the area
 *
 * @return 0 if successful, 1 if the area was reallocated but remains
 *         unaligned, or ENOMEM if reallocation failed completely.
 */
int clt_realloc_safe(void **pptr, size_t alignment, size_t size) {
    void *ptr = *pptr;
    void *newptr;

    if (ptr == NULL) {
        *pptr = clt_alloc_aligned(alignment, size);
        if (*pptr == NULL) return ENOMEM;
        else return 0;
    }

    /* Make sure that alignment is a power of 2 */
    alignment = clt_get_power_of_two_alignment(alignment);

#if defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MALLOC_H)

    newptr = realloc(ptr, size);
    if (newptr == NULL) {
        /* Free the area on failure, to keep things consistent with the
           other case below. */
        return ENOMEM;
    } else if ((uintptr_t) newptr & (alignment - 1)) {
        /* realloc moved the data to an unaligned position. Realign. */
  #if defined(HAVE_POSIX_MEMALIGN)
        {
            int ret;
            ret = posix_memalign(&ptr, alignment, size);
            if (ret == EINVAL) {
                CLT_ERROR("Invalid alignment. This should not happen. Problem is in function clt_get_power_of_two_alignment.");
            }
        }
  #else
        ptr = memalign(alignment, size);
  #endif
        if (ptr == NULL) {
            /* Failed to allocate new area. Return pointer to the unaligned
               data. */
            *pptr = newptr;
            return 1;
        }
        memcpy(ptr, newptr, size);
        free(newptr);
        *pptr = ptr;
    } else {
        *pptr = newptr;
    }
    return 0;

#else

    {
        void *p = *(((void **) ptr) - 1);
        size_t pdiff = ptr - p;
        void *newptr = realloc(p, size + CLT_MAX(pdiff, alignment +
                                                 sizeof(void **)));
        if (newptr == NULL) {
            return ENOMEM;
        } else if (((intptr_t) newptr + pdiff) & (alignment - 1)) {
            /* Realign the data block */
            p = newptr + (alignment - 1) + sizeof(void **);
            p -= (intptr_t) p & (alignment - 1);
            memmove(p, newptr + pdiff, size);
            *((void **) (p - sizeof(void **))) = newptr;
            *pptr = p;
            return 0;
        }
    }
#endif
}

/**
 * Frees memory allocated by clt_malloc.
 *
 * @param ptr pointer to a memory area allocated by clt_malloc
 */
void clt_free(void *ptr) {
    if (ptr) {
#if defined(HAVE_POSIX_MEMALIGN) || defined(HAVE_MALLOC_H)
        free(ptr);
#else
        free(*(((void **) ptr) - 1));
#endif
    }
}


