/* clt.h: Command line interface tools
 *
 * Author: Niko Mikkilä
 *
 * Public Domain
 */

#ifndef __CLT_H__
#define __CLT_H__

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif


/** Log levels */
enum clt_log_level {CLT_LOG_LEVEL_ERROR=0, CLT_LOG_LEVEL_WARNING=1,
                    CLT_LOG_LEVEL_INFO=2};

#define CLT_DEFAULT_LOG_TARGET stderr

#define CLT_ERROR(...) clt_log(CLT_LOG_LEVEL_ERROR, __FILE__, \
                               __func__, __LINE__, __VA_ARGS__)

#define CLT_WARNING(...) clt_log(CLT_LOG_LEVEL_WARNING, __FILE__, \
                                 __func__, __LINE__, __VA_ARGS__)

#define CLT_INFO(...) clt_log(CLT_LOG_LEVEL_INFO, NULL, NULL, 0, __VA_ARGS__)


/** Returns the larger of two numerical values. */
#define CLT_MAX(a,b) ((a)>(b)?(a):(b))

/** Returns the smaller of two numerical values. */
#define CLT_MIN(a,b) ((a)<(b)?(a):(b))


/** Minimum memory alignment for clt_malloc */
#define CLT_MIN_ALIGNMENT 16

/** Allocate memory aligned to CLT_MIN_ALIGNMENT. Returned area must be freed
    with clt_free. */
#define clt_malloc(s) clt_malloc_aligned(CLT_MIN_ALIGNMENT, s)


/** Argument presence values */
enum clt_arg_presence {CLT_NO_ARG=0, CLT_OPTIONAL_ARG=1, CLT_REQUIRED_ARG=2};


/**
 * CLT command-line option data structure
 */
typedef struct {
    /* Long option string */
    const char *long_option;
    /* Short argument type description: "int", "float", "string", ... */
    const char *argument_type;
    /* Option description */
    const char *description;
    /* Parser callback function */
    int (*parser)(int option_id, const char *arg, void *parameters);
    /* Argument presence */
    enum clt_arg_presence has_argument;
    /* Option ID and short option if alphanumeric. Use -1 for the last
     * option in an array of options to pass a parser function for non-option
     * arguments and signal the end of the list. */
    int id;
} clt_option;



/* Function declarations */

FILE *clt_get_log_target(void);

void clt_set_log_target(FILE *target);

enum clt_log_level clt_get_log_level(void);

void clt_set_log_level(enum clt_log_level level, int print_source);

void clt_log(enum clt_log_level level, const char *file,
             const char *function, int line, const char *format, ...);


const clt_option *clt_get_short_option(const clt_option *options, int optid);

const clt_option *clt_get_long_option(const clt_option *options,
                                      const char *optstring);

int clt_parse_option(const clt_option *opt, const char *argument,
                 void *target_parameters);

int clt_parse_argv(int argc, const char * const argv[],
                   const clt_option *options, void *target_parameters);

void clt_show_error_position(FILE *out, int pos, int argc,
                             const char * const argv[]);

void clt_print_options(FILE *out, const clt_option *options,
                       int col1_width, int col2_width);


void *clt_alloc_aligned(size_t alignment, size_t size);

void *clt_realloc_aligned(void *ptr, size_t alignment, size_t size);

int clt_realloc_safe(void **pptr, size_t alignment, size_t size);

void clt_free(void *ptr);


#ifdef __cplusplus
}
#endif

#endif

