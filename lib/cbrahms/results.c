/*
 * results.c - Functions for manipulating search result data structures
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*#include <limits.h>*/

#include "config.h"
#include "results.h"
/*#include "util.h"*/


/**
 * Frees memory used by a match result.
 *
 * @param ms the match to free
 */
void free_match(match *m) {
    if((m != NULL) && (m->notes != NULL)) free(m->notes);
}

/**
 * Frees memory used by a set of match results.
 *
 * @param ms the set of matches to free
 */
void free_match_set(matchset *ms) {
    if(ms->matches != NULL) {
        uint32_t i;
        for (i = 0; i < ms->capacity; i++) {
            free_match(&ms->matches[i]);
        }
        free(ms->matches);
    }
    memset(ms, 0, sizeof(matchset));
}


/**
 * Initializes a set of match results.
 *
 * @param ms           matchset to initialize
 * @param size         capacity of the set
 * @param pattern_size space required for storing note positions of
 *                     the matches. If zero, no space is reserved for
 *                     storing the positions
 * @param measure_search_time
 *                     time the search
 * @param multiple_matches_per_song
 *                     set to 1 to store multiple matches per song,
 *                     0 to only keep the best match
 *
 * @return 1 if successful, 0 otherwise;
 */
int init_match_set(matchset *ms, uint32_t size, uint32_t pattern_size,
        int measure_search_time, int multiple_matches_per_song) {
    ms->measure_search_time       = measure_search_time;
    ms->multiple_matches_per_song = multiple_matches_per_song;
    ms->time.indexing  = 0.0;
    ms->time.verifying = 0.0;
    ms->time.other     = 0.0;
    ms->capacity       = size;
    ms->num_matches    = 0;
    ms->matches        = (match *) calloc(size, sizeof(match));
    if (ms->matches == NULL) {
        fputs("Error in init_match_set(): failed to allocate memory", stderr);
        return 0;
    }
    if (pattern_size > 0) {
        uint32_t i;
        for (i = 0; i < size; i++) {
            match *m = &ms->matches[i];
            m->num_notes = pattern_size;
            m->notes = (int *) calloc(pattern_size, sizeof(int));
            if (m->notes == NULL) {
                fputs("Warning in init_match_set(): failed to allocate "
                      "memory for match pattern", stderr);
                m->num_notes = 0;
            }
        }
    }
    return 1;
}


/**
 * Clears a set of match results.
 *
 * @param ms the set of matches to free
 */
void clear_match_set(matchset *ms) {
    ms->time.indexing  = 0.0;
    ms->time.verifying = 0.0;
    ms->time.other     = 0.0;
    ms->num_matches    = 0;
    if(ms->matches != NULL) {
        uint32_t i, j;
        match *m;
        for (i = 0; i < ms->capacity; i++) {
            m = &ms->matches[i];
            m->song          = 0;
            m->start         = 0;
            m->end           = 0;
            m->transposition = 0;
            m->similarity    = 0.0F;
            if (m->notes != NULL) {
                for (j = 0; j < m->num_notes; j++) m->notes[j] = -1;
            }
        }
    }
}


/**
 * Checks if the given match overlaps with a specific interval.
 *
 * @param m a match item
 * @param start interval start
 * @param end interval end
 *
 * @return 1 if the match overlaps with the interval, 0 otherwise
 */
static inline int match_overlap(match *m, int start, int end) {
    if ((m->end <= m->start) && ((m->start >= end) || (m->start < start)))
        return 0;
    else if ((m->start >= end) || (m->end <= start)) return 0;
    else return 1;
}


/**
 * Adds a match to a set of matches if it is good enough to fit.
 *
 * Note: caller should take care of filling the note position array within
 * the match item.
 *
 * @param ms a set of matches where the new match is added
 * @param songid song number
 * @param start match start time in the song
 * @param end match end time in the song
 * @param transposition match transposition in the song compared to the
 *        original pattern
 * @param similarity match score given by the caller
 *
 * @return the match item if it was inserted, NULL otherwise.
 */
match *insert_match(matchset *ms, uint32_t songid, int start, int end,
        int8_t transposition, float similarity) {
    int32_t  *mnotes = NULL;
    match    *m = NULL;
    int       i;

    /* Check if there is already a match for this song */
    for (i = 0; i < (int) ms->num_matches; i++) {
        match *mi = &ms->matches[i];
        if (songid == mi->song) {
            int j;
            /* If multiple matches per song are allowed and the previous
               match doesn't overlap with this one, continue */
            if (ms->multiple_matches_per_song &&
                    (!match_overlap(mi, start, end))) continue;

            if (similarity <= mi->similarity) return NULL;

            /* Move up in the array */
            for (j = i - 1; j >= 0; j--) {
                if (ms->matches[j].similarity < similarity)
                    memcpy(&ms->matches[j+1], &ms->matches[j],
                            sizeof(match));
                else break;
            }
            m = &ms->matches[j+1];
            break;
        }
    }
    if (m == NULL) {
        /* Multiple matches per song are allowed or there was not a previous
           result for this song */
        for (i = 0; i < (int) ms->capacity; i++) {
            match *mi = &ms->matches[i];
            if (similarity > mi->similarity) {
                int j;
                mnotes = ms->matches[ms->capacity-1].notes;

                /* Move other matches down in the array */
                if (ms->num_matches < ms->capacity) ms->num_matches++;
                for (j = (int) ms->num_matches - 1; j > i; j--) {
                    memcpy(&ms->matches[j], &ms->matches[j-1], sizeof(match));
                }
                m = mi;
                break;
            }
        }
    }
    if (m != NULL) {
        m->notes = mnotes;
        m->song = songid;
        m->start = start;
        m->end = end;
        m->transposition = transposition;
        m->similarity = similarity;
    }
    return m;
}


