/*
 * compress_song.c - Compress a 2-dimensional pointset by finding maximal
 *                   repeated patterns.
 *
 * Version 2010-01-15
 *
 *
 * Copyright (C) 2009 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#include <stdio.h>
#include <stdlib.h>
/*#include <string.h>
#include <limits.h>*/

#include "config.h"
#include "song.h"
#include "util.h"
#include "priority_queue.h"
#include "compress_song.h"

#if 0
struct patternvector {
    unsigned short source;
    unsigned short target;
};

struct pattern {
/*    int next_pattern;*/
    unsigned short num_notes;
/*    unsigned short num_map_pieces;*/
    int ptr;
/*    char *map_pieces;
    unsigned int *map;*/
} __attribute__((__packed__));

struct patternset {
    int num_patterns;
    int num_vectors;
    struct pattern *patterns;
    struct patternvector *vectors;
};
#endif

struct repeatmatrix {
    int aw, w;
    unsigned short *data;
}

void compress_song_p2(const song *s, const song *p) {
}


static int init_repeatmatrix(struct repeatmatrix *rm, int w) {
    int size;
    rm->w = w;
    rm->aw = CEIL_MOD_CACHE(w);
    size = rm->aw * rm->w * sizeof(unsigned char);
    rm->data = (unsigned short *) alloc_aligned(CACHE_ALIGNMENT, size);
    if (rm->data == NULL) {
        rm->w = 0;
        rm->aw = 0;
        return 0;
    }
    memset(rm->data, 0, size);
    return 1;
}

static void free_repeatmatrix(struct repeatmatrix *rm) {
    free(rm->data);
    rm->w = 0;
    rm->aw = 0;
    rm->data = NULL;
}

/**
 * Runs geometric algorithm P2 to calculate the sizes of all maximal repeated
 * patterns within a song, anchored to note onsets.
 *
 * @param s song to scan.
 * @param rm matrix that holds the pattern sizes. The matrix will be initialized
 *        by this function and needs to be freed by the caller.
 */
static int create_repeatmatrix(const song *s, struct repeatmatrix *rm) {

    int num_loops, i;
    pqroot *tree = NULL;
    int c, repeatsize, ;
    int previous_key;
    int num_notes = s->size;
    vector *notes = s->notes;
    int *match;

    rm->w = 0;
    rm->aw = 0;

    if (num_notes < 4) return 0;

    match = (int) calloc(num_notes * sizeof(int));

    if (match == NULL) return 0;
    if (!init_repeatmatrix(rm, num_notes)) return 0;

    /* Initialize the priority queue */
    tree = pq_create(num_notes);
    for (i = 0; i < num_notes; i++) {
        /* Add translation vectors to the priority queue */
        pqnode *node = pq_getnode(tree, num_notes-i-1);
        node->key1 = (((int) s[0].strt - (int) s[i].strt) << 8)
                + (int) s[0].ptch - (int) s[i].ptch;
        node->key2 = 0;
        pq_update_key1_nodeorder(tree, node);
    }

    c = 0;
    rsize = 1;
    mpos = 0;
    previous_key = INT_MIN;
    
    num_loops = num_notes * num_notes / 2 + 1;
    for (i = 0; i < num_loops; i++) {
        pqnode *min = pq_getmin(tree);
        int patternpos = min->index;
        int textpos = min->key2;
        vector *patternnote = &notes[patternpos];
        vector *textnote = &notes[textpos];
        if (min->key1 >= 0) {
            printf("create_repeatmatrix: finished scanning at %d/%d\n", i+1,
                    num_loops);
            break;
        }

        match[c] = textpos;
        if (previous_key == min->key1) {
            for (; mpos<c; ++mpos) {
                if (mpos > patternpos) break;
                else if (mpos == patternpos) {
                    --rsize;
                    break;
                }
            }
            rm->data[rm->aw * patternpos + textpos] = (unsigned short) rsize; 
            ++c;
            ++rsize;
        } else {
            /* end of a matching section */
            previous_key = min->key1;
            c = 0;
            rsize = 1;
        }


        if (textpos < num_notes - 1) {
            /* The current pointer is not at the end of source:
             * move to the next note */
            min->key2++;
            textnote = &notes[min->key2];

            /* Update the vector in the priority queue. */
            min->key1 = (((int) textnote->strt - (int) patternnote->strt +
                    pattern_end) << 8) + (int) textnote->ptch -
                    (int) patternnote->ptch + NOTE_PITCHES;
            pq_update_key1_p2(tree, min);
        } else {
            /* Current pointer is at the end of the text;
             * remove the difference vector from the priority queue. */
            min->key1 = INT_MAX;
            pq_update_key1_p2(tree, min);
        }
    }

    free(match);
    pq_free(tree);
    return 1;
}


