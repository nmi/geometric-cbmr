/*
 * align_P3_bitmap.c - Optimized versions of the P3 alignment algorithm
 *
 * Version 2009-04-01
 *
 *
 * Copyright (C) 2009 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "config.h"

#include "util.h"

/*
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "algorithms.h"
#include "search.h"
#include "song.h"
#include "song_window.h"
*/

/*#define DEBUG_WINDOW_SIZE_HISTOGRAM 1*/

#define USE_LINECACHE 1
/*#define LINECACHE_COUNT_LINE_FACTOR 1*/

#define SHOW_PROGRESS 1

#define NUM_LCT_LINES 256

/* #define FORCE_UNROLL */

#define BUFFER_ALIGNMENT 16

#define VECTOR_BYTES 16
#define VECTOR_SHIFT 4

#define CEIL_16(x) (((x) <= 0) ? 0 : (((x) - 1) & 0xFFFFFFF0) + 0x10)
#define VECTOR_CEIL(x) CEIL_16(x)

/*
#define SAT_ADD_U16(a, b) ((a > 0xFFFF - b) ? 0xFFFF : (a + b))
#define SAT_ADD_U8(a, b) ((a > 0xFF - b) ? 0xFF : (a + b))
*/

#define SAT_ADD_U16(a, b) (a + b)
#define SAT_ADD_U8(a, b) (a + b)


typedef struct {
    int size;
    int origin;
    unsigned char *data;
    /* There is another copy of the stripe without onset weight at data[size] */
    /* unsigned char *data_no_onset; */
} stripe;

typedef struct {
    int sample_bytes;
    int max_note_duration;
    int num_notes;
    int data_accuracy;
    int data_accuracy_shift;
    int duration_accuracy;
    int duration_accuracy_shift;
    int onset_step_size;
    int num_onset_steps;
    int row_shift;
    stripe *stripes;
    unsigned char *data;
} stripeset;

typedef struct {
    int num_stripes;
    unsigned char *data;
} lctmapline;

typedef struct {
    int line_size;
    unsigned char *data;
    unsigned char *maxline;
    lctmapline lines[NUM_LCT_LINES];
} lctmap;


static void init_lctmap(lctmap *lmap, int map_duration, int data_accuracy,
        int sample_type, int origin) {
    lmap->line_size = VECTOR_CEIL((map_duration >> fastlog2(data_accuracy))
            << sample_bytes);
    lmap->data = (unsigned char *) allocate_aligned(BUFFER_ALIGNMENT,
            (NUM_LCT_LINES + 1) * lmap->line_size);
    if (lmap->data == NULL) {
        fputs("Error in init_lctmap: failed to allocate memory", stderr);
        memset(lmap, 0, sizeof(lctmap));
        return;
    }
    lmap->maxline = &lmap->data[NUM_LCT_LINES * lmap->line_size];
    memset(lmap->data, 0, (NUM_LCT_LINES + 1) * lmap->line_size);
    for (i=0; i<NUM_LCT_LINES; ++i) {
        lmap->lines[i].num_stripes = 0;
        lmap->lines[i].data = &lmap->data[i * lmap->line_size];
    }
}

static void free_lctmap(lctmap *lmap) {
    free(lmap->data);
    memset(lmap, 0, sizeof(lctmap));
}

static inline void clear_lctmap(lctmap *lmap) {
    int i;
    for (i=0; i<NUM_LCT_LINES; ++i) lmap->lines[i].num_stripes = 0;
    memset(lmap->data, 0, (NUM_LCT_LINES + 1) * lmap->line_size);
}

static void lct(lctmap *lmap, stripeset *sset, song *pattern, song *target,
        int pad) {
    int i;
    int sample_bytes = sset->sample_bytes;
    int duration_accuracy_shift = sset->duration_accuracy_shift;
    int data_accuracy_shift = sset->data_accuracy_shift << sample_bytes;
    for (i=0; i<pattern.size; ++i) {
        int j;
        int px = pattern.notes[i].strt;
        int py = pattern.notes[i].ptch;
        int pdur = pattern.notes[i].dur;
        stripes *pstripes = &sset->stripes[
                (pdur >> duration_accuracy_shift) << sset->row_shift];
        for (j=0; j<target.size; ++j) {
            int k;
            int tx = target.notes[j].strt;
            int ty = target.notes[j].ptch;
            int tdur = target.notes[j].dur;
            stripe *str = &pstripes[tdur >> duration_accuracy_shift];
            int ly = ty-py+128;
            int lx = ((tx-px+pad) >> data_accuracy_shift) - str->origin;
            lctmapline *target_line = &lmap->lines[ly];
            ++target_line->num_stripes;
            if (sample_bytes == 1) {
                int strsize = str->size >> 1;
                short *strdata = (short *) str->data;
                short *target = (short *) target_line->data[lx];
#ifdef FORCE_UNROLL
                // Unroll manually
                for (k=0; k<(strsize & 7); ++k) {
                    target[k] = SAT_ADD_U16(strdata[k], strdata[k]);
                }
                for (; k<strsize; k += 8) {
                    target[k] = SAT_ADD_U16(strdata[k], strdata[k]);
                    target[k+1] = SAT_ADD_U16(strdata[k+1], strdata[k+1]);
                    target[k+2] = SAT_ADD_U16(strdata[k+2], strdata[k+2]);
                    target[k+3] = SAT_ADD_U16(strdata[k+3], strdata[k+3]);
                    target[k+4] = SAT_ADD_U16(strdata[k+4], strdata[k+4]);
                    target[k+5] = SAT_ADD_U16(strdata[k+5], strdata[k+5]);
                    target[k+6] = SAT_ADD_U16(strdata[k+6], strdata[k+6]);
                    target[k+7] = SAT_ADD_U16(strdata[k+7], strdata[k+7]);
                }
#else
                for (k=0; k<strsize; ++k) {
                    target[k] = SAT_ADD_U16(strdata[k], strdata[k]);
                }
#endif
            } else if (sample_bytes == 0) {
                int strsize = str->size;
                unsigned char *strdata = str->data;
                unsigned char *target = target_line->data[lx];
#ifdef FORCE_UNROLL
                // Unroll manually
                for (k=0; k<(strsize & 0xF); ++k) {
                    target[k] = SAT_ADD_U8(target[k], strdata[k]);
                }
                for (; k<strsize; k += 16) {
                    target[k] = SAT_ADD_U8(target[k], strdata[k]);
                    target[k+1] = SAT_ADD_U8(target[k+1], strdata[k+1]);
                    target[k+2] = SAT_ADD_U8(target[k+2], strdata[k+2]);
                    target[k+3] = SAT_ADD_U8(target[k+3], strdata[k+3]);
                    target[k+4] = SAT_ADD_U8(target[k+4], strdata[k+4]);
                    target[k+5] = SAT_ADD_U8(target[k+5], strdata[k+5]);
                    target[k+6] = SAT_ADD_U8(target[k+6], strdata[k+6]);
                    target[k+7] = SAT_ADD_U8(target[k+7], strdata[k+7]);

                    target[k+8] = SAT_ADD_U8(target[k+8], strdata[k+8]);
                    target[k+9] = SAT_ADD_U8(target[k+9], strdata[k+9]);
                    target[k+10] = SAT_ADD_U8(target[k+10], strdata[k+10]);
                    target[k+11] = SAT_ADD_U8(target[k+11], strdata[k+11]);
                    target[k+12] = SAT_ADD_U8(target[k+12], strdata[k+12]);
                    target[k+13] = SAT_ADD_U8(target[k+13], strdata[k+13]);
                    target[k+14] = SAT_ADD_U8(target[k+14], strdata[k+14]);
                    target[k+15] = SAT_ADD_U8(target[k+15], strdata[k+15]);
                }
#else
                for (k=0; k<strsize; ++k) {
                    target[k] = SAT_ADD_U8(target[k], strdata[k]);
                }
#endif
            }
        } 
    }
}

static void normalize_lct(lctmap *lmap, ap3results *ar, int sample_bytes,
        int norm) {
    int i, j;
    int size = MIN2((lmap->line_size >> sample_bytes), ar->size);
    unsigned int factor = 0xFFFFFFFF / norm;

    for (j=0; j<NUM_LCT_LINES; ++j) {
        if (lmap->lines[j].num_stripes < MIN_STRIPES_PER_LINE) continue;
        if (sample_bytes == 1) {
            short *maxline = (short *) lmap->maxline;
            short *line = (short *) lmap->lines[j].data;
            for (i=0; i<size; ++i) {
               lmap->maxline[i] = MAX2(lmap->maxline[i], line[i]);
            }
        } else if (sample_bytes == 0) {
            unsigned char *maxline = lmap->maxline;
            unsigned char *line = lmap->lines[j].data;
            for (i=0; i<size; ++i) {
               lmap->maxline[i] = MAX2(lmap->maxline[i], line[i]);
            }
        }
    }
    if (sample_bytes == 1) {
        short *maxline = (short *) lmap->maxline;
        for (i=0; i<size; ++i) {
            ar->values[i] = (unsigned short)
                    ((factor * (unsigned int) maxline[i]) >> 22);
        }
    } else if (sample_bytes == 0) {
        unsigned char *maxline = lmap->maxline;
        for (i=0; i<size; ++i) {
            ar->values[i] = (unsigned short)
                    ((factor * (unsigned int) maxline[i]) >> 22);
        }
    }
}

static void init_stripeset(stripeset *sset, int sample_bytes,
        int max_note_duration, int duration_accuracy, int data_accuracy,
        int onset_step_size, int num_onset_steps, int use_sse2) {
    int i, j;
    int data_size = 0;
    int pos = 0;
    int rshift;

    duration_accuracy = MAX2(duration_accuracy, data_accuracy);
    sset->sample_bytes = sample_bytes;
    sset->max_note_duration = 1 << fastlog2(max_note_duration);
    sset->data_accuracy_shift = fastlog2(data_accuracy);
    sset->data_accuracy = 1 << sset->data_accuracy_shift;
    sset->duration_accuracy_shift = fastlog2(duration_accuracy);
    sset->duration_accuracy = 1 << sset->duration_accuracy_shift;
    sset->num_notes = sset->max_note_duration >> sset->duration_accuracy_shift;
    sset->row_shift = fastlog2(sset->num_notes);
    sset->max_note_duration = MAX2(sset->max_note_duration,
            (num_onset_steps << sset->data_accuracy_shift));
    sset->stripes = NULL;
    sset->data = NULL;

    sset->stripes = (stripe *) alloc_aligned(BUFFER_ALIGNMENT,
            sset->num_notes * sset->num_notes * sizeof(stripe));

    /* Approximate data buffer size based on the maximum vector size */
    data_size = VECTOR_CEIL(sset->max_note_duration
            >> sset->data_accuracy_shift) * sset->num_notes * sset->num_notes
            * 2 << sample_bytes;

    sset->data = (unsigned char *) alloc_aligned(BUFFER_ALIGNMENT,
            data_size);
    if ((stripeset->stripes == NULL) || (stripeset->data == NULL)) {
        free(sset->stripes);
        free(sset->data);
        fputs("Error in init_stripeset: failed to allocate memory", stderr);
        memset(sset, 0, sizeof(stripeset));
        return;
    }
    memset(sset->data, 0, data_size);
    shift = sset->duration_accuracy_shift >> sset->data_accuracy_shift;
    for (i=0; i<sset->num_notes; ++i) {
        int idur = (i+1) << shift;

        for (j=0; j<sset->num_notes; ++j) {
            int jdur = (j+1) << shift;
            int k, origin, start, end, tp1, tp2;
            int stripe, dsize, next_pos;
            if (idur < num_onset_steps) {
                origin = num_onset_steps - 1;
                start = num_onset_steps - idur;
            } else {
                origin = idur - 1;
                start = 0;
            }
            if (idur > jdur) {
                tp1 = start + jdur;
                tp2 = start + idur;
            } else {
                tp1 = start + idur;
                tp2 = start + jdur;
            }
            end = origin + jdur;

            if (use_sse2) dsize = VECTOR_CEIL(
                    MAX2(end, origin + num_onset_steps - 1) << sample_bytes);
            else dsize = MAX2(end, origin + num_onset_steps - 1) << sample_bytes;

            next_pos = pos + dsize;
            if (sample_bytes == 1) {
                short val = 0;
                for (k=start; k<tp1; ++k) {
                    (short *) sset->data[pos + (k << 1)] = (++val);
                    (short *) sset->data[next_pos + (k << 1)] = val;
                }
                for (k=tp1; k<tp2; ++k) {
                    (short *) sset->data[pos + (k << 1)] = val;
                    (short *) sset->data[next_pos + (k << 1)] = val;
                }
                for (k=tp2; k<end; ++k) {
                    (short *) sset->data[pos + (k << 1)] = (--val);
                    (short *) sset->data[next_pos + (k << 1)] = val;
                }
                /* Add onset weight */
                val = 0;
                for (k=origin-num_onset_steps+1; k<=origin; ++k) {
                    val += onset_step_size;
                    (short *) sset->data[pos + (k << 1)] += val;
                }
                for (; k<origin+num_onset_steps; ++k) {
                    val -= onset_step_size;
                    (short *) sset->data[pos + (k << 1)] += val;
                }
            } else if (sample_bytes == 0) {
                unsigned char val = 0;
                for (k=start; k<tp1; ++k) {
                    sset->data[pos + k] = (++val);
                    sset->data[next_pos + k] = val;
                }
                for (k=tp1; k<tp2; ++k) {
                    sset->data[pos + k] = val;
                    sset->data[next_pos + k] = val;
                }
                for (k=tp2; k<end; ++k) {
                    sset->data[pos + k] = (--val);
                    sset->data[next_pos + k] = val;
                }
                /* Add onset weight */
                val = 0;
                for (k=origin-num_onset_steps+1; k<=origin; ++k) {
                    val += onset_step_size;
                    (short *) sset->data[pos + k] += val;
                }
                for (; k<origin+num_onset_steps; ++k) {
                    val -= onset_step_size;
                    (short *) sset->data[pos + k] += val;
                }
            }

            stripe = i << sset->row_shift + j;
            sset->stripes[stripe].data = sset->data[pos];
            sset->stripes[stripe].size = dsize;
            sset->stripes[stripe].origin = origin;
            pos = nextpos + dsize;

            printf("i: %d (%d),\t j: %d (%d),\t dsize: %d,\t origin: %d,\t start: %d,\t end: %d,\t tp1: %d,\t tp2: %d\n", i, idur, j, jdur, dsize, origin, start, end, tp1, tp2);
        }
    }

    puts("\nStripe stats:");
    printf("  buffer size: %d, Used: %d\n", data_size, pos);
    printf("  row_shift:\t%d\n", sset->row_shift);
    printf("  sample_bytes:\t\t%d\n", sset->sample_bytes + 1);
    printf("  max_note_duration:\t%d (request: %d)\n", sset->max_note_duration,
            max_note_duration);
    printf("  num_notes:\t%d \n", sset->num_notes);
    printf("  duration_accuracy:\t%d (request: %d), shift: %d\n",
            sset->duration_accuracy, duration_accuracy,
            sset->duration_accuracy_shift);
    printf("  data_accuracy:\t%d (request: %d), shift: %d\n",
            sset->data_accuracy, data_accuracy, sset->data_accuracy_shift);
    printf("  onset_step_size:\t%d\n", sset->onset_step_size);
    printf("  num_onset_steps:\t%d\n", sset->num_onset_steps);
}


void free_stripeset(stripeset *sset) {
    free(sset->stripes);
    free(sset->data);
    memset(sset, 0, sizeof(sripeset));
}


static void init_ap3results(ap3results *ar, int size, int shift) {
    ar->start = 0;
    ar->end = 0;
    ar->size = size;
    ar->values = (unsigned short *) alloc_aligned(BUFFER_ALIGNMENT,
            ar->size * sizeof(unsigned short));
    if (ar->values == NULL) {
        fputs("Error in init_ap3results: unable to allocate buffers", stderr);
        free(ar->values);
        ar->values = NULL;
        ar->size = 0;
        ar->duration = 0;
        ar->shift = 0;
        return;
    }
    ar->transpositions = NULL;
    ar->duration = size << shift;
    ar->shift = shift;
}


static void free_ap3results(ap3results *ar) {
    free(ar->values);
    free(ar->transpositions);
    ar->values = NULL;
    ar->transpositions = NULL;
    ar->start = 0;
    ar->end = 0;
}

static void clear_ap3results(ap3results *ar) {
    if (ar->values != NULL)
        memset(ar->values, 0, ar->size * sizeof(unsigned short));
    if (ar->transpositions != NULL)
        memset(ar->transpositions, 0, ar->size * sizeof(char));
}

static void normalize_ap3results(ap3results *ar, float maximum) {
    int i;
    float norm;
    if (maximum > 0.0F) norm = 1023.0F / maximum;
    else norm = 0.0F;
    for (i=0; i<ar->size; ++i) {
        ar->values[i] = MIN2(1023, lrintf(norm * (float) ar->values[i]));
    }
}

static void scale_pattern(song *scaled_p, song *p, alignmentmap *map,
                          int map_pos, double scale) {
    int i, total_duration = 0;
    int end = 0;

    if (map != NULL) {
        double pattern_start, factor;
        pattern_start = map->initial_alignment[map_pos];
        factor = 1.0 / map->accuracy;

        for (i=0; i<p->size; ++i) {
            vector *sn, *n;
            int    j, pos;
            double s1, s2;
            sn = &scaled_p->notes[i];
            n  = &p->notes[i];

            pos = n->strt >> map->accuracy_shift;
            j = map_pos + pos;
            if (j >= map->height) {
                pos -= j - map->height + 1;
                j = map->height - 1;
            }
            s1 = map->initial_alignment[j] + (map->initial_alignment[j + 1] -
                 map->initial_alignment[j]) * factor *
                 (double) (n->strt - (pos << map->accuracy_shift));

            pos = (n->strt + n->dur) >> map->accuracy_shift;
            j = map_pos + pos;
            if (j >= map->height) {
                pos -= j - map->height + 1;
                j = map->height - 1;
            } 
            s2 = map->initial_alignment[j] + (map->initial_alignment[j + 1] -
                 map->initial_alignment[j]) * factor *
                 (double) (n->strt + n->dur - (pos << map->accuracy_shift));

            if (n->strt < 0) sn->strt = -1;
            else sn->strt = lrint(scale * (s1 - pattern_start));
            sn->dur = lrint(scale * (s2 - s1));
            sn->ptch = n->ptch;
            total_duration += sn->dur;
            end = MAX2(end, sn->strt + sn->dur);
        }
    } else {
        for (i=0; i<p->size; ++i) {
            vector *sn, *n;
            sn = &scaled_p->notes[i];
            n  = &p->notes[i];

            if (n->strt < 0) sn->strt = -1;
            else sn->strt = lrint(scale * (double) n->strt);
            sn->dur = lrint(scale * (double) n->dur);            
            sn->ptch = n->ptch;
            total_duration += sn->dur;
            end = MAX2(end, sn->strt + sn->dur);
        }
    }

    scaled_p->size = p->size;
    scaled_p->total_note_duration = total_duration;
    scaled_p->start = 0;
    scaled_p->end = end;
}


static void init_linecache(linecache *lc, int num_lines, int line_length,
        alignmentmap *map, int map_pos, double scale) {
    int i;
    double accuracy_factor = 1.0 / (double) map->accuracy;

    memset(lc, 0, sizeof(linecache));

    num_lines = MAX2(num_lines, 1);
    lc->num_lines = num_lines;
    lc->num_used_lines = 0;
    lc->line_length = line_length;
    lc->next_line = 0;
    lc->last_line = -1;
    lc->last_line_pos = 0;
    lc->linebuf_pos = 0;

    lc->last_alignment_pos = map_pos;
    lc->initial_alignment = (int *) malloc(map->height * sizeof(int));
    if (lc->initial_alignment == NULL) {
        fputs("Error: unable to allocate memory in init_linecache\n", stderr);
        memset(lc, 0, sizeof(linecache));
        return;
    }
    for (i=0; i<map->height; ++i) {
        double a = scale * accuracy_factor *
                (map->initial_alignment[i] + map->initial_alignment_offset[i]);
        lc->initial_alignment[i] = (int) lround(a);
    }

    lc->linebuf_data = (unsigned char *) malloc(2 * line_length *
            sizeof(unsigned char));
    lc->linebuf[0] = lc->linebuf_data;
    lc->linebuf[1] = &lc->linebuf_data[line_length];
    lc->linediff = (float *) malloc(2 * line_length * sizeof(float));
    lc->linediff_base = (int *) malloc(2 * line_length * sizeof(int));

    lc->line = (int *) calloc(line_length, sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
    lc->line_factor = (int *) calloc(line_length, sizeof(int));
#endif

    lc->prev_line_view_left = (int *) malloc(num_lines * sizeof(int));
    lc->prev_line_view_right = (int *) malloc(num_lines * sizeof(int));
    lc->prev_line_pos = (int *) malloc(num_lines * sizeof(int));
    lc->prev_lines = (unsigned char **) malloc(num_lines *
            sizeof(unsigned char *));

    if ((lc->line == NULL) || (lc->linediff == NULL) ||
            (lc->linediff_base == NULL) ||
            (lc->linebuf[0] == NULL) ||
#ifdef LINECACHE_COUNT_LINE_FACTOR
            (lc->line_factor == NULL) ||
#endif
            (lc->prev_line_view_left == NULL) ||
            (lc->prev_line_view_right == NULL) ||
            (lc->prev_line_pos == NULL) ||
            (lc->prev_lines == NULL)) {
        fputs("Error: unable to allocate memory in init_linecache\n", stderr);
        free(lc->line);
        free(lc->linebuf_data);
        free(lc->linediff);
        free(lc->linediff_base);
#ifdef LINECACHE_COUNT_LINE_FACTOR
        free(lc->line_factor);
#endif
        free(lc->prev_line_view_left);
        free(lc->prev_line_view_right);
        free(lc->prev_line_pos);
        free(lc->prev_lines);
        memset(lc, 0, sizeof(linecache));
        return;
    }
 
    lc->prev_lines[0] = (unsigned char *) malloc(num_lines * line_length *
                sizeof(unsigned char));
    for (i=1; i<num_lines; ++i) {
        lc->prev_lines[i] = lc->prev_lines[i-1] + line_length *
                sizeof(unsigned char);
    }
}

void free_linecache(linecache *lc) {
    free(lc->initial_alignment);
    free(lc->line);
#ifdef LINECACHE_COUNT_LINE_FACTOR
    free(lc->line_factor);
#endif
    free(lc->linebuf_data);
    free(lc->linediff);
    free(lc->linediff_base);
    free(lc->prev_lines[0]);
    free(lc->prev_lines);
    free(lc->prev_line_view_left);
    free(lc->prev_line_view_right);
    free(lc->prev_line_pos);
    memset(lc, 0, sizeof(linecache));
}


void update_linecache(linecache *lc, const ap3results *aresult,
        int map_pos, alignmentmap *map) {
    int i, j;
    int delta;
    int *avalues = aresult->values;
    int astart = ASHIFTR(aresult->start, aresult->shift);
    unsigned char *linebuf_next = lc->linebuf[(lc->linebuf_pos + 1) & 1];
    unsigned char *linebuf_last = lc->linebuf[lc->linebuf_pos];
    unsigned char *rline = lc->prev_lines[lc->next_line];
    int len = lc->line_length;
    int curlen = MIN2(len, aresult->size);
    unsigned char *mapdata = &map->vbuffer[map_pos * map->width];

#ifndef LINECACHE_COUNT_LINE_FACTOR
    float lf = 1.0F / (float) MIN2(lc->num_used_lines+1, lc->num_lines);
#endif

    delta = (lc->initial_alignment[map_pos] -
             lc->initial_alignment[lc->last_alignment_pos]) -
            (astart - lc->last_line_pos);


    /* Shift values to the correct position */

    if (ABS(delta) < len) {
        if (delta < 0) {
            for (i=0, j=-delta; j<len; ++i, ++j) {
                lc->line[i] = lc->line[j];
#ifdef LINECACHE_COUNT_LINE_FACTOR
                lc->line_factor[i] = lc->line_factor[j];
#endif
            }
            memset(&lc->line[len+delta], 0, (-delta) * sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
            memset(&lc->line_factor[len+delta], 0, (-delta) * sizeof(int));
#endif
        } else if (delta > 0) {
            for (i=len-1, j=len-1-delta; j>=0; --i, --j) {
                lc->line[i] = lc->line[j];
#ifdef LINECACHE_COUNT_LINE_FACTOR
                lc->line_factor[i] = lc->line_factor[j];
#endif
            }
            memset(lc->line, 0, delta * sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
            memset(lc->line_factor, 0, delta * sizeof(int));
#endif
        }
    } else {
        memset(lc->line, 0, len * sizeof(int));
#ifdef LINECACHE_COUNT_LINE_FACTOR
        memset(lc->line_factor, 0, len * sizeof(int));
#endif
    }

    if (delta < 0) {
        for (i=0; i<lc->num_used_lines; ++i) {
            lc->prev_line_view_left[i] += MAX2(0,
                    -delta - lc->prev_line_pos[i]);
            lc->prev_line_pos[i] = MAX2(0, lc->prev_line_pos[i] + delta);
        }
    } else {
        for (i=0; i<lc->num_used_lines; ++i) {
            lc->prev_line_pos[i] += delta;
            lc->prev_line_view_right[i] -= MAX2(0, lc->prev_line_pos[i] +
                    lc->prev_line_view_right[i] -
                    lc->prev_line_view_left[i] - len);
        }
    }


    /* Add new data */

    for (i=0; i<curlen; ++i) {
        lc->line[i] += avalues[i];
#ifdef LINECACHE_COUNT_LINE_FACTOR
        ++lc->line_factor[i];
#endif
    }
    for (i=curlen; i<len; ++i) {
#ifdef LINECACHE_COUNT_LINE_FACTOR
        ++lc->line_factor[i];
#endif
    }


    /* Remove oldest line */

    if (lc->num_used_lines >= lc->num_lines) {
        int sp, rline_sp, rline_ep;

        rline_sp = lc->prev_line_view_left[lc->next_line];
        rline_ep = lc->prev_line_view_right[lc->next_line];
        sp = lc->prev_line_pos[lc->next_line];
        for(; rline_sp < rline_ep; ++rline_sp, ++sp) {
            lc->line[sp] -= rline[rline_sp];
#ifdef LINECACHE_COUNT_LINE_FACTOR
            lc->line_factor[sp]--;
#endif
        }
    }


    /* Write data to the alignment map */
    
    for (i=0; i<len; ++i) {
#ifdef LINECACHE_COUNT_LINE_FACTOR
        unsigned char val = lc->line[i] / MAX2(1, lc->line_factor[i]);
#else
        unsigned char val = (unsigned char) lrintf(lf * (float) lc->line[i]);
#endif
        linebuf_next[i] = val;
        mapdata[i] = MAX2(mapdata[i], val);
/*        if (lc->line_factor[i] > lc->num_used_lines+1)
            printf("LF: %d/%d, %d\n", lc->line_factor[i], lc->num_used_lines, i); */
    }


    /* Interpolate over skipped lines */

    if (map_pos - lc->last_alignment_pos > 1) {
        float num_skipped_lines = (float) (map_pos - lc->last_alignment_pos);

        if ((delta < -len) || (delta > len)) {
            for (i=lc->last_alignment_pos+1; i<map_pos; ++i) {
                unsigned char *skippedline = &map->vbuffer[i * map->width];
                int line_pos = map->lines[i].target_time >> aresult->shift;
                int d1 = (lc->initial_alignment[i] -
                          lc->initial_alignment[lc->last_alignment_pos]) -
                         (line_pos - lc->last_line_pos);
                int d2 = (lc->initial_alignment[map_pos] -
                          lc->initial_alignment[i]) -
                         (astart - line_pos);
                if ((d1 < 0) && (d1 > -len)) {
                    int p = 0;
                    for (j=-d1; j<len; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_last[j]);
                } else if ((d1 >= 0) && (d1 < len)) {
                    int p = d1;
                    for (j=0; j<len-d1; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_last[j]);
                }
                if ((d2 < 0) && (d2 > -len)) {
                    int p = -d2;
                    for (j=0; j<len+d2; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_next[j]);
                } else if ((d2 >= 0) && (d2 < len)) {
                    int p = 0;
                    for (j=d2; j<len; ++j, ++p)
                        skippedline[p] = MAX2(skippedline[p], linebuf_next[j]);
                }
            }
        } else {
            int linediff_lastpos = 0;
            if (delta >= 0) {
                linediff_lastpos = delta;
                for (i=0; i<delta; ++i) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_next[i];
                }
                for (i=delta, j=0; i<len; ++i, ++j) {
                    lc->linediff[i] = (float) linebuf_next[i] - (float)
                            linebuf_last[j];
                    lc->linediff_base[i] = linebuf_last[j];
                }
                for (i=len, j=len-delta; i<len+delta; ++i, ++j) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_last[j];
                }
            } else {
                for (i=0; i<(-delta); ++i) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_last[i];
                }
                for (i=(-delta), j=0; i<len; ++i, ++j) {
                    lc->linediff[i] = (float) linebuf_next[j] -
                            (float) linebuf_last[i];
                    lc->linediff_base[i] = linebuf_last[i];
                }
                for (i=len, j=len+delta; i<len-delta; ++i, ++j) {
                    lc->linediff[i] = 0.0F;
                    lc->linediff_base[i] = linebuf_next[j];
                }
            }
            for (i=lc->last_alignment_pos+1; i<map_pos; ++i) {
                float ifactor = (float) (i - lc->last_alignment_pos) /
                        num_skipped_lines;
                unsigned char *skippedline = &map->vbuffer[i * map->width];
                int line_pos = map->lines[i].target_time >> aresult->shift;
                int d = (lc->initial_alignment[i] -
                         lc->initial_alignment[lc->last_alignment_pos]) -
                        (line_pos - lc->last_line_pos);
                int p, endp;
                j = linediff_lastpos - d;
                if (j < 0) {
                    p = -j;
                    j = 0;
                    endp = len;
                } else {
                    p = 0;
                    endp = MIN2(len, len + ABS(delta) - j);
                }
                for (; p<endp; ++p, ++j) {
                    skippedline[p] = MAX2(skippedline[p], lc->linediff_base[j] +
                            (int) (ifactor * lc->linediff[j]));
                }
            }
        }
    }


    /* Store current line for future reference */

    for (i=0; i<curlen; ++i) {
        rline[i] = (unsigned char) avalues[i];
    }
    for (i=curlen; i<len; ++i) {
        rline[i] = 0;
    }

    lc->prev_line_pos[lc->next_line] = 0;
    lc->prev_line_view_left[lc->next_line] = 0;
    lc->prev_line_view_right[lc->next_line] = len;


    /* Update state */

    lc->last_alignment_pos = map_pos;
    lc->last_line_pos = astart;

    lc->linebuf_pos = (lc->linebuf_pos + 1) & 1;
    lc->num_used_lines = MIN2(lc->num_lines, lc->num_used_lines + 1);
    lc->last_line++;
    if (lc->last_line >= lc->num_lines) lc->last_line = 0;
    lc->next_line++;
    if (lc->next_line >= lc->num_lines) lc->next_line = 0;
}

/**
 * Maps local alignments of two songs with the geometric P3 algorithm.
 *
 * @param sc song collection that possibly contains the song in the p3song
 *        format that this function uses. If sc is NULL, necessary conversions
 *        will be made before further processing.
 * @param s song to align the pattern with. If either of the aligned songs
 *        is the "correct" score, it should be given here to improve alignment
 *        results of the P3 algorithm
 * @param p a pattern song that is aligned with the other song. This should
 *        be the piece that may contain more errors.
 * @param parameters alignment parameters
 * @param map the resulting alignment map will be written to this structure 
 */
void align_p3_sse2(alignmentmap *map, int alg,
        const alignparameters *parameters) {

    int i;
    int pattern_skip = 0;
    song target_rests, pattern_rests;
    p3songcollection *p3sc = map->sc->data[DATA_P3];
    p3song *p3s, p3s_rests;
    int map_width_ms = map->width << map->accuracy_shift;
    int pattern_window_width = parameters->p3_pattern_window_width;
    int num_lines = pattern_window_width /
            parameters->p3_pattern_window_skip;
    song scaled_pattern, scaled_pattern_rests;
    song_window pattern_window, pattern_rests_window;
    p3s_window target_window, target_rests_window;
    ap3results aresult, rests_aresult;
    int num_scales = parameters->p3_scales_num * 2 - 1;
    double scales_spread = parameters->p3_scales_spread;
    double scales_step = (scales_spread - 1.0) /
                            ((double) MAX2(1, (parameters->p3_scales_num - 1)));
    double *scales;
    linecache *lc;
    ap3localnotes **lnotes = NULL;
    ap3localnotes *pattern_lnotes = NULL;
    double ltwsum = 1.0;
    int ltrans = 0;
    
#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
    int window_notes[1024];
    memset(window_notes, 0, 1024*sizeof(int));
#endif

    if ((alg != ALG_ALIGN_P3) && (alg != ALG_ALIGN_P3_ONSET) &&
            (alg != ALG_ALIGN_P3_OPT)) {
        fprintf(stderr, "Error in align_p3: invalid algorithm id: %d", alg);
        return;
    }
    if ((p3sc == NULL) || (map->target->id >= p3sc->size)) {
        fputs("Error in align_p3: song collection does not contain P3 data.\nUse update_song_collection_data() to generate it.\n", stderr);
        return;
    }
    p3s = &p3sc->p3_songs[map->target->id];
    if (p3s == NULL) {
        fputs("Error in align_p3: empty P3 song data", stderr);
        return;
    }


    init_ap3results(&aresult, map->width, map->accuracy_shift);
    if (aresult.values == NULL) {
        fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
        return;
    }

    if (parameters->p3_align_rests) {
        memset(&target_rests, 0, sizeof(song));
        memset(&pattern_rests, 0, sizeof(song));

        fputs("Extracting rests from the target song...\n", stderr);
        extract_song_rests(map->target, 0, map->target->end, &target_rests);
        song_to_p3(&target_rests, &p3s_rests);

        fputs("Extracting rests from the pattern...\n", stderr);
        extract_song_rests(map->pattern, 0, map->pattern->end, &pattern_rests);

        init_ap3results(&rests_aresult, map->width, map->accuracy_shift);
        if (rests_aresult.values == NULL) {
            fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
            free_ap3results(&aresult);
            return;
        }
    }

    scales = (double *) malloc(num_scales * sizeof(double));
    if (scales == NULL) {
        fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
        return;
    }
    for (i=parameters->p3_scales_num; i > 1; --i) {
        scales[parameters->p3_scales_num + i - 2] = scales_spread;
        scales[parameters->p3_scales_num - i] = 1.0 / scales_spread;
        scales_spread -= scales_step;
    }
    scales[parameters->p3_scales_num - 1] = 1.0F;

    fprintf(stderr, "\nUsing %d scaled pattern variations:\n  (%f",
            num_scales, scales[0]);
    for (i=1; i<num_scales; ++i) fprintf(stderr, ", %f", scales[i]);
    fprintf(stderr, ")\n");

    lc = (linecache *) malloc(num_scales * sizeof(linecache));
    if (lc == NULL) {
        fputs("Error in align_p3: unable to allocate a line cache", stderr);
        free_ap3results(&aresult);
        free_ap3results(&rests_aresult);
        if (parameters->p3_window_heuristics) {
            free(lnotes[0]);
            free(lnotes);
            free(pattern_lnotes);
        }
        free(scales);
        return;
    }

    fputs("Initializing line cache and scanning local notes...", stderr);
    for (i=0; i<num_scales; ++i) {
        init_linecache(&lc[i], num_lines, map->width, map, 0, scales[i]);

        if (parameters->p3_window_heuristics) {
            lnotes[i] = &lnotes[0][i * map->target->size];
            scan_local_notes_31(map->target, lnotes[i],
                    scales[i] * ((double) pattern_window_width), 0);
/*{
                    int k;
                    printf("Song (scale: %d):\n", (int) (parameters->p3_scales[i] * (double) pattern_window_width));
                    for (k=0; k<map->target->size; ++k) {
                        vector *snote = &map->target->notes[k];
                        printf("(%d, %d, %d : %x.%x)\n", snote->strt, snote->strt + snote->dur, snote->ptch, lnotes[i][k].before, lnotes[i][k].after);
                    }
                    printf("\n");
}*/
        }
    }
    fputs(" Done.\n", stderr);

    init_song_window(&pattern_window, map->pattern, 1);
    init_p3s_window(&target_window, p3s);

    memset(&scaled_pattern, 0, sizeof(song));
    scaled_pattern.notes = (vector *) malloc(map->pattern->size *
            sizeof(vector));


    if (parameters->p3_align_rests) {
        init_song_window(&pattern_rests_window, &pattern_rests, 1);
        init_p3s_window(&target_rests_window, &p3s_rests);

        memset(&scaled_pattern_rests, 0, sizeof(song));
        scaled_pattern_rests.notes = (vector *) malloc(pattern_rests.size *
            sizeof(vector));
    }

    if (parameters->p3_max_local_transposition > 0) {
        ltrans = MIN2(MAX_LOCAL_TRANSPOSITION,
                parameters->p3_max_local_transposition);
        for (i=1; i<=ltrans; ++i) {
            ltwsum += 2.0 * LOCAL_TRANSPOSITION_WEIGHTS[i];
        }
    }

    for (i=0; i<map->height; ++i) {
        int j;
        alignmentline *mapline = &map->lines[i];
        int pattern_time = i << map->accuracy_shift;
        int target_time = mapline->target_time;
        int window_onsets = 0;


        aresult.start = target_time;
        aresult.end = target_time + map_width_ms;
        if (parameters->p3_align_rests) {
            rests_aresult.start = target_time;
            rests_aresult.end = target_time + map_width_ms;
        }
#ifdef SHOW_PROGRESS
        fprintf(stderr, "   Line %d/%d\r", i + 1, map->height);
#endif

        if ((pattern_skip <= 0) || (i == map->height - 1)) {
            move_song_window(&pattern_window, pattern_time,
                    pattern_time + pattern_window_width);
            if (parameters->p3_align_rests) {
                move_song_window(&pattern_rests_window, pattern_time,
                        pattern_time + pattern_window_width);
            }
#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
            if (pattern_window.window.size < 1023)
                window_notes[pattern_window.window.size]++;
            else window_notes[1023]++;
#endif

            for (j=0; j<num_scales; ++j) {
                double ps = scales[j];
                double s_pattern_window_width = ps * ((double)
                        pattern_window_width);
                int norm;

                scale_pattern(&scaled_pattern, &pattern_window.window,
                              map, i, ps);

                if (j == 0) {
                    int k;
                    for (k=0; k<scaled_pattern.size; ++k) {
                        if (scaled_pattern.notes[k].strt >= 0) ++window_onsets;
                    }
                }
                if (parameters->p3_window_heuristics) {
                    ln = lnotes[j];
                    pln = pattern_lnotes;
                    scan_local_notes_31(&scaled_pattern, pln,
                            s_pattern_window_width, (scaled_pattern.size <= 1));

/*                {
                    int k;
                    printf("Pattern:\n");
                    for (k=0; k<scaled_pattern.size; ++k) {
                        vector *snote = &scaled_pattern.notes[k];
                        printf("(%d, %d, %d : %x.%x)\n", snote->strt, snote->strt + snote->dur, snote->ptch, pattern_lnotes[k].before, pattern_lnotes[k].after);
                    }
                    printf("\n");
                }
*/
                }
                move_p3s_window(&target_window, target_time,
                        target_time + map_width_ms +
                        (int) s_pattern_window_width);

                clear_ap3results(&aresult);

                norm = scaled_pattern.total_note_duration;


                align_turningpoints_p3_sse2(&target_window.window, ln,
                            &scaled_pattern, pln, &aresult,
                            parameters->p3_onset_delta,
                            parameters->p3_onset_slope,
                            (int) s_pattern_window_width,
                            ltrans);
                norm += window_onsets * lrint(parameters->p3_onset_slope *
                        (double) parameters->p3_onset_delta);

                if (parameters->p3_align_rests) {
                    int k;
                    scale_pattern(&scaled_pattern_rests,
                            &pattern_rests_window.window, map, i, ps);
                    move_p3s_window(&target_rests_window, target_time,
                            target_time + map_width_ms +
                            (int) s_pattern_window_width);
                    clear_ap3results(&rests_aresult);

                    align_turningpoints_p3_sse2(&target_rests_window.window,
                            NULL,
                            &scaled_pattern_rests, NULL, &rests_aresult, 0, 0,
                            (int) s_pattern_window_width, 0);

                    norm += scaled_pattern_rests.total_note_duration;

                    for (k=0; k<aresult.size; ++k) {
                        aresult.values[k] += rests_aresult.values[k];
                    }
                }
                normalize_ap3results(&aresult, norm);

#ifdef USE_LINECACHE
                update_linecache(&lc[j], &aresult, i, map);
#else
                {
                    int k;
                    for (k=0; k<aresult.size; ++k) {
                        mapline->values[k] = MAX2(mapline->values[k],
                                (unsigned char) aresult.values[k]);
                    }
                }
#endif
            }
            pattern_skip = parameters->p3_pattern_window_skip;
        }
        pattern_skip -= map->accuracy;
    }
#ifdef SHOW_PROGRESS
    fprintf(stderr, "\n");
#endif

#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
    puts("Window sizes:\n");
    for (i=0; i<1024; ++i) {
        printf("   %d:\t %d\n", i, window_notes[i]);
    }
#endif

    for (i=0; i<num_scales; ++i) {
        free_linecache(&lc[i]);
    }
    if (parameters->p3_window_heuristics) {
        free(lnotes[0]);
        free(lnotes);
        free(pattern_lnotes);
    }
    if (parameters->p3_align_rests) {
        free_song(&pattern_rests);
        free_song(&target_rests);
        free_p3_song(&p3s_rests);
        free_ap3results(&rests_aresult);
        free_song(&scaled_pattern_rests);
    }
    free(lc);
    free_song(&scaled_pattern);
    free_ap3results(&aresult);
    free_song_window(&pattern_window);
    free_p3s_window(&target_window);
    free(scales);
}

