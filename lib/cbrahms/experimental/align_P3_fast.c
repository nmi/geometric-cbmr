/*
 * align_P3_fast.c - SSE2-optimized P3 for symbolic score alignment
 *
 * Version 2009-07-30
 *
 *
 * Copyright (C) 2009 Niko Mikkila
 *
 * University of Helsinki, Department of Computer Science, C-BRAHMS project
 *
 * Contact: niko.mikkila@gmail.com
 *
 *
 * This file is part of geometric-cbmr,
 * C-BRAHMS Geometric algorithms for Content-Based Music Retrieval.
 *
 * Geometric-cbmr is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option)
 * any later version.
 *
 * Geometric-cbmr is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * geometric-cbmr; if not, write to the Free Software Foundation, Inc., 59
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/*#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <assert.h>
*/
/*#include "algorithms.h"
#include "search.h"
#include "song.h"
#include "geometric_P3.h"
#include "priority_queue.h"
#include "priority_queue_32.h"
#include "util.h"
#include "align.h"
#include "align_P3.h"
#include "song_window.h"
#include "song_window_P3.h"
*/
#include "align_P3.h"
#include "align_P3_fast.h"

#define VECTOR_ALIGNMENT 16

#define CEIL_16(x) (((x) <= 0) ? 0 : (((x) - 1) & 0xFFFFFFF0) + 0x10)
#define VECTOR_CEIL(x) CEIL_16(x)

#define NUM_SEMITONES 128

typedef struct {
    int accuracy;
    int accuracy_shift;
    int sample_size;
    int width;
    int position;
    unsigned char *bitmap;
} ap3state;

typedef struct {
    int origin;
    int slopepos;
    int width;
    float *data;
} dataline;

typedef struct {
    int patternpos;
    int num_scalings;
    dataline *current;
    dataline *previous;

    /* Allocated memory */
    float *b_data;
    dataline *b_lines;
} linebuffer;


typedef struct {
    int start;
    int dur;
} pvector;

typedef struct {
    int pitch;
    int num_notes;
    pvector *notes;
} pline;

typedef struct {
    int num_plines;
    pline *plines;
} psong;

static void free_psong(psong *ps) {
    for (i=0; i<ps->num_plines; ++i) {
        free(ps->plines[i].notes);
    }
    free(ps->plines);
}

static void song_to_psong(song *s, psong *ps) {
    int num_plines;
    int num_notes[NUM_SEMITONES*2];
    int pline_pos[NUM_SEMITONES*2];
    memset(num_notes, 0, NUM_SEMITONES*2*sizeof(int));
    for (i=0; i<s->size; ++i) {
        int p = s->notes[i].ptch;
        if (num_notes[p] == 0) {
            pline_pos[p] = num_plines;
            ++num_plines;
        }
        ++num_notes[p];
    }
    ps->num_plines = num_plines;
    ps->plines = (pline *) calloc(num_plines, sizeof(pline));
    for (i=0; i<s->size; ++i) {
        int p = s->notes[i].ptch;
        
    }
}

static void init_ap3state(ap3state *ap3s, int width, int accuracy_shift,
                          int sample_size) {
    int size = VECTOR_CEIL(width * sample_size * NUM_SEMITONES * 2);
    ap3s->accuracy_shift = accuracy_shift;
    ap3s->accuracy = 1 << accuracy_shift;
    ap3s->width = width;
    ap3s->sample_size = sample_size;
    ap3s->bitmap = (unsigned char *) alloc_aligned(VECTOR_ALIGNMENT, size);
}

static void free_ap3state(ap3state *ap3s) {
    free(ap3s->bitmap);
    memset(ap3s, 0, sizeof(ap3state));
}

static void init_linebuffer(linebuffer *lb, int num_scalings, int width) {
    int i;
    int size;

    width = VECTOR_CEIL(width);
    size = 2 * num_scalings * VECTOR_CEIL(width * sizeof(float));

    lb->b_data = (float *) alloc_aligned(VECTOR_ALIGNMENT, size);
    lb->b_lines = (dataline *) malloc(2 * num_scalings * sizeof(dataline));
    if ((lb->b_data == NULL) || (lb->b_lines == NULL)) {
        lb->num_scalings = 0;
        free(lb->b_data);
        free(lb->b_lines);
        lb->b_data = NULL;
        lb->b_lines = NULL;
        return;
    }

    lb->patternpos = 0;
    lb->num_scalings = num_scalings;
    lb->current = lb->b_lines;
    lb->previous = &lb->current[num_scalings]

    size = width * sizeof(float);
    for (i=0; i<num_scalings; ++i) {
        dataline *l;
        l = &lb->current[i];
        l->origin = 0;
        l->slopepos = 0;
        l->width = width;
        l->data = &lb->b_data[2 * i * size];

        l = &lb->previous[i];
        l->origin = 0;
        l->slopepos = 0;
        l->width = width;
        l->data = &lb->b_data[2 * i * size];
    }
}

static void free_linebuffer(linebuffer *lb) {
    free(lb->b_data);
    free(lb->b_lines);
    memset(lb, 0, sizeof(linebuffer));
}



void ap3(ap3song *t, ap3song *p, ap3state *b, dataline *o) {
    unsigned char *slope[16]
}

/**
 * Maps local alignments of two songs with the geometric P3 algorithm.
 *
 * @param sc song collection that possibly contains the song in the p3song
 *        format that this function uses. If sc is NULL, necessary conversions
 *        will be made before further processing.
 * @param s song to align the pattern with. If either of the aligned songs
 *        is the "correct" score, it should be given here to improve alignment
 *        results of the P3 algorithm
 * @param p a pattern song that is aligned with the other song. This should
 *        be the piece that may contain more errors.
 * @param parameters alignment parameters
 * @param map the resulting alignment map will be written to this structure 
 */
void align_p3_fast(alignmentmap *map, int alg,
                   const alignparameters *parameters) {

    

}

#if 0

/** 
 * A modified version of the geometric P3 symbolic music retrieval algorithm
 * for score alignment.
 *
 * @param p3s song to scan
 * @param pattern pattern song
 */
void align_turningpoints_p3onset(const p3song *p3s, const ap3localnotes *lnotes,
        const song *pattern, const ap3localnotes *pattern_lnotes,
        ap3results *results, int onset_delta, double onset_slope) {

    int i, j, num_loops;
    FVerticalTranslationTableItem *verticaltranslationtable;
    FVerticalTranslationTableItem *item = NULL;
    TurningPoint *note_starts = p3s->note_starts;
    TurningPoint *note_ends = p3s->note_ends;
    int num_tpoints = p3s->num_notes;
    int pattern_size = pattern->size;
    double accuracy = (double) (1 << results->shift);
    vector *pnotes = pattern->notes;

    /* Create a priority queue */
    pqroot *pq;
    pqnode *min;
    TranslationVector *translation_vectors;
    
    if ((pattern_size <= 0) || (num_tpoints <= 0)) return;

    pq = pq_create(pattern_size * 6);
    translation_vectors = (TranslationVector *)
            malloc(pattern_size * 6 * sizeof(TranslationVector));

    /* Initialize a vertical translation array */
    verticaltranslationtable = (FVerticalTranslationTableItem *) malloc(
            NOTE_PITCHES * 2 * sizeof(FVerticalTranslationTableItem));
    for (i = 0; i < (NOTE_PITCHES * 2); i++) {
        verticaltranslationtable[i].value = 0.0;
        verticaltranslationtable[i].slope = 0.0;
        verticaltranslationtable[i].prev_x = 0;
    }

    /*printf("P3 Notes: %d\n", p3s->size);*/

    /* Create an array whose items have two pointers each: one for note_starts
     * and one for note_ends. Each item points to turning point array item Also
     * populate the priority queue with initial items. */

    for (i = 0, j = 0; i < pattern_size; i++) {
        TranslationVector *v;
        /*pattern_duration += (float) pnotes[i].dur;*/


        /* Add translation vectors calculated from the note start points */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x -
                (int) (pnotes[i].strt + pnotes[i].dur);
        v->position = P3_POSITION_PE_TS;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1_p3(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TS;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1_p3(pq, min);
        ++j;


        /* Add translation vectors calculated from the note end points */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) (pnotes[i].strt +
                pnotes[i].dur);
        v->position = P3_POSITION_PE_TE;
        v->pdur = pnotes[i].dur;
        v->tdur = note_ends[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1_p3(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TE;
        v->pdur = pnotes[i].dur;
        v->tdur = note_ends[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1_p3(pq, min);
        ++j;


        /* Add translation vectors for onset weighting */

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - onset_delta - (int) pnotes[i].strt;
        v->position = P3_POSITION_ONSET_START;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1_p3(pq, min);
        ++j;

        min = pq_getnode(pq, j);
        v = &translation_vectors[j];
        v->target_index = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x + onset_delta - (int) pnotes[i].strt;
        v->position = P3_POSITION_ONSET_END;
        v->pdur = pnotes[i].dur;
        v->tdur = note_starts[0].dur;
        min->key1 = v->x;
        min->pointer = v;
        pq_update_key1_p3(pq, min);
        ++j;
    }

    num_loops = (pattern_size * num_tpoints) * 6;

    for (i = 0; i < num_loops; i++) {
        TranslationVector *v;
        int x, y;
        double newval;

        /* Get the minimum element */
        min = pq_getmin(pq);
        v = (TranslationVector *) min->pointer;
        x = v->x;
        y = v->y;

        /* Update value */
        item = &verticaltranslationtable[NOTE_PITCHES + y];
        newval = item->value + item->slope * (double) (x - item->prev_x);

#ifdef ACCURATE_P3_INTERPOLATION
        {
            int old_x, new_x, oldpos;
            double val, slope;

            old_x = item->prev_x + pnotes[0].strt;
            new_x = x + pnotes[0].strt;

    #if (ACCURATE_P3_INTERPOLATION == 2)
            val = 0.0;
            if (new_x >= 0) {
                if ((new_x >= results->start) && (new_x < results->end)) {
                    j = (new_x - results->start) >> results->shift;
                    if (results->values[j] < lrint(newval)) val = 1.0;
                }
            }
            if ((val == 0) && (old_x >= 0)) {
                if ((old_x >= results->start) && (old_x < results->end)) {
                    j = (old_x - results->start) >> results->shift;
                    if (results->values[j] == lrint(item->value)) val = 1.0;
                }
            }
            if (val == 1.0) {
    #endif
                slope = item->slope * accuracy;

                oldpos = MAX2(old_x - results->start, 0) >> results->shift;
                j = MIN2((MAX2(new_x - results->start, 0) >> results->shift),
                        results->size - 1);
                val = newval - item->slope *
                        (double) MAX2(new_x - results->end, 0);
                if (oldpos == j) oldpos--;
                for (; j>oldpos; --j, val-=slope) {
                    int ival = lrint(val);
                    if (results->values[j] < ival) {
                        results->values[j] = ival;
                        results->transpositions[j] = y;
                    }
                }
                item->value = newval;
    #if (ACCURATE_P3_INTERPOLATION == 2)
            } else item->value = newval;
    #endif
        }
#else
        int new_x = x + pnotes[0].strt;
        item->value = newval;
        if ((new_x >= results->start) && (new_x < results->end)) {
            int inewval = lrint(newval);
            new_x = (new_x - results->start) >> results->shift;
            if (results->values[new_x] < inewval) {
                results->values[new_x] = inewval;
                results->transpositions[new_x] = y;
            }
        }

#endif /* ACCURATE_P3_INTERPOLATION */

        item->prev_x = x;

        /* Adjust slope */
        switch (v->position) {
            case P3_POSITION_PS_TE:
            case P3_POSITION_PE_TS:
                item->slope += 1.0;
                break;
            case P3_POSITION_PS_TS:
                item->slope -= onset_slope + onset_slope;
            case P3_POSITION_PE_TE:
                item->slope -= 1.0;
                break;
            case P3_POSITION_ONSET_START:
            case P3_POSITION_ONSET_END:
                item->slope += onset_slope;
                break;
        }

#if 0
        if (v->text_is_start) {
            if (v->pattern_is_start) {
                item->slope -= (((double) MIN2(v->tdur, v->pdur)) /
                        ((double) v->pdur));

            } else {
                item->slope += ((double) MIN2(v->tdur, v->pdur)) /
                        ((double) v->pdur);
            }
        } else {
            if (v->pattern_is_start) {
                item->slope += ((double) MIN2(v->tdur, v->pdur)) / ((double) v->pdur);
            } else {
                item->slope -= ((double) MIN2(v->tdur, v->pdur)) / ((double) v->pdur);
            }
        }
#endif

        /* Move the pointer and insert a new translation vector according to
         * the turning point type. */
        if (v->target_index < num_tpoints - 1) {
            const vector *patp = &pnotes[v->pattern_index];
            const TurningPoint *startp, *endp;
            v->target_index++;
            startp = &note_starts[v->target_index];
            endp = &note_ends[v->target_index];
 
            switch (v->position) {
            case P3_POSITION_PS_TS:
                v->x = (int) startp->x - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PE_TS:
                v->x = (int) startp->x - (int) patp->strt - (int) patp->dur;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PS_TE:
                v->x = (int) endp->x - (int) patp->strt;
                v->y = (int) endp->y - (int) patp->ptch;
                break;
            case P3_POSITION_PE_TE:
                v->x = (int) endp->x - (int) patp->strt - (int) patp->dur;
                v->y = (int) endp->y - (int) patp->ptch;
                break;
            case P3_POSITION_ONSET_START:
                v->x = (int) startp->x - onset_delta - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            case P3_POSITION_ONSET_END:
                v->x = (int) startp->x + onset_delta - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
                break;
            }

            min->key1 = v->x;
            pq_update_key1_p3(pq, min);
        } else {
            /* 'Remove' a translation vector by making it very large.
             * It won't be extracted since there will be only as many loops as 
             * there are real vectors. */
            min->key1 = UINT_MAX;
            pq_update_key1_p3(pq, min);
        }
    }

    /* Free the reserved memory. */
    pq_free(pq);
    free(translation_vectors);
    free(verticaltranslationtable);
}


static void scale_pattern(song *scaled_p, song *p, alignmentmap *map,
                          int map_pos, double scale) {
    int i, total_duration = 0;
    int end = 0;

    if (map != NULL) {
        double pattern_start, factor;
        pattern_start = map->initial_alignment[map_pos];
        factor = 1.0 / map->accuracy;

        for (i=0; i<p->size; ++i) {
            vector *sn, *n;
            int    j, pos;
            double s1, s2;
            sn = &scaled_p->notes[i];
            n  = &p->notes[i];

            pos = n->strt >> map->accuracy_shift;
            j = map_pos + pos;
            if (j >= map->height) {
                pos -= j - map->height + 1;
                j = map->height - 1;
            }
            s1 = map->initial_alignment[j] + (map->initial_alignment[j + 1] -
                 map->initial_alignment[j]) * factor *
                 (double) (n->strt - (pos << map->accuracy_shift));

            pos = (n->strt + n->dur) >> map->accuracy_shift;
            j = map_pos + pos;
            if (j >= map->height) {
                pos -= j - map->height + 1;
                j = map->height - 1;
            } 
            s2 = map->initial_alignment[j] + (map->initial_alignment[j + 1] -
                 map->initial_alignment[j]) * factor *
                 (double) (n->strt + n->dur - (pos << map->accuracy_shift));

            if (n->strt < 0) sn->strt = -1;
            else sn->strt = lrint(scale * (s1 - pattern_start));
            sn->dur = lrint(scale * (s2 - s1));
            sn->ptch = n->ptch;
            total_duration += sn->dur;
            end = MAX2(end, sn->strt + sn->dur);
        }
    } else {
        for (i=0; i<p->size; ++i) {
            vector *sn, *n;
            sn = &scaled_p->notes[i];
            n  = &p->notes[i];

            if (n->strt < 0) sn->strt = -1;
            else sn->strt = lrint(scale * (double) n->strt);
            sn->dur = lrint(scale * (double) n->dur);            
            sn->ptch = n->ptch;
            total_duration += sn->dur;
            end = MAX2(end, sn->strt + sn->dur);
        }
    }

    scaled_p->size = p->size;
    scaled_p->total_note_duration = total_duration;
    scaled_p->start = 0;
    scaled_p->end = end;
}

/**
 * Maps local alignments of two songs with the geometric P3 algorithm.
 *
 * @param sc song collection that possibly contains the song in the p3song
 *        format that this function uses. If sc is NULL, necessary conversions
 *        will be made before further processing.
 * @param s song to align the pattern with. If either of the aligned songs
 *        is the "correct" score, it should be given here to improve alignment
 *        results of the P3 algorithm
 * @param p a pattern song that is aligned with the other song. This should
 *        be the piece that may contain more errors.
 * @param parameters alignment parameters
 * @param map the resulting alignment map will be written to this structure 
 */
void align_p3(alignmentmap *map, int alg, const alignparameters *parameters) {

    int i;
    int pattern_skip = 0;
    song target_rests, pattern_rests;
    p3songcollection *p3sc = map->sc->data[DATA_P3];
    p3song *p3s, p3s_rests;
    int map_width_ms = map->width << map->accuracy_shift;
    int pattern_window_width = parameters->p3_pattern_window_width;
    int num_lines = pattern_window_width /
            parameters->p3_pattern_window_skip;
    song scaled_pattern, scaled_pattern_rests;
    song_window pattern_window, pattern_rests_window;
    p3s_window target_window, target_rests_window;
    ap3results aresult, rests_aresult;
    int num_scales = parameters->p3_scales_num * 2 - 1;
    double scales_spread = parameters->p3_scales_spread;
    double scales_step = (scales_spread - 1.0) /
                            ((double) MAX2(1, (parameters->p3_scales_num - 1)));
    double *scales;
    linecache *lc;
    ap3localnotes **lnotes = NULL;
    ap3localnotes *pattern_lnotes = NULL;
    double ltwsum = 1.0;
    int ltrans = 0;
    
#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
    int window_notes[1024];
    memset(window_notes, 0, 1024*sizeof(int));
#endif

    if ((alg != ALG_ALIGN_P3) && (alg != ALG_ALIGN_P3_ONSET) &&
            (alg != ALG_ALIGN_P3_OPT)) {
        fprintf(stderr, "Error in align_p3: invalid algorithm id: %d", alg);
        return;
    }
    if ((p3sc == NULL) || (map->target->id >= p3sc->size)) {
        fputs("Error in align_p3: song collection does not contain P3 data.\nUse update_song_collection_data() to generate it.\n", stderr);
        return;
    }
    p3s = &p3sc->p3_songs[map->target->id];
    if (p3s == NULL) {
        fputs("Error in align_p3: empty P3 song data", stderr);
        return;
    }


    init_ap3results(&aresult, map->width, map->accuracy_shift);
    if (aresult.values == NULL) {
        fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
        return;
    }

    if (parameters->p3_align_rests) {
        memset(&target_rests, 0, sizeof(song));
        memset(&pattern_rests, 0, sizeof(song));

        fputs("Extracting rests from the target song...\n", stderr);
        extract_song_rests(map->target, 0, map->target->end, &target_rests);
        song_to_p3(&target_rests, &p3s_rests);

        fputs("Extracting rests from the pattern...\n", stderr);
        extract_song_rests(map->pattern, 0, map->pattern->end, &pattern_rests);

        init_ap3results(&rests_aresult, map->width, map->accuracy_shift);
        if (rests_aresult.values == NULL) {
            fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
            free_ap3results(&aresult);
            return;
        }
    }

    scales = (double *) malloc(num_scales * sizeof(double));
    if (scales == NULL) {
        fputs("Error in align_p3: failed to allocate a buffer\n", stderr);
        return;
    }
    for (i=parameters->p3_scales_num; i > 1; --i) {
        scales[parameters->p3_scales_num + i - 2] = scales_spread;
        scales[parameters->p3_scales_num - i] = 1.0 / scales_spread;
        scales_spread -= scales_step;
    }
    scales[parameters->p3_scales_num - 1] = 1.0F;

    fprintf(stderr, "\nUsing %d scaled pattern variations:\n  (%f",
            num_scales, scales[0]);
    for (i=1; i<num_scales; ++i) fprintf(stderr, ", %f", scales[i]);
    fprintf(stderr, ")\n");

    if (parameters->p3_window_heuristics) {
        lnotes = (ap3localnotes **) malloc(num_scales * sizeof(ap3localnotes *));
        pattern_lnotes = (ap3localnotes *) malloc(map->pattern->size *
                                                  sizeof(ap3localnotes));
        if ((lnotes == NULL) || (pattern_lnotes == NULL)) {
            fputs("Error in align_p3: failed to allocate a buffer", stderr);
            free(lnotes);
            free(pattern_lnotes);
            free(scales);
            return;
        }
        lnotes[0] = (ap3localnotes *) malloc(num_scales * map->target->size *
                sizeof(ap3localnotes));
        if (lnotes[0] == NULL) {
            fputs("Error in align_p3: failed to allocate a buffer", stderr);
            free(lnotes[0]);
            free(lnotes);
            free(pattern_lnotes);
            free(scales);
            return;
        }
    }
    lc = (linecache *) malloc(num_scales * sizeof(linecache));
    if (lc == NULL) {
        fputs("Error in align_p3: unable to allocate a line cache", stderr);
        free_ap3results(&aresult);
        free_ap3results(&rests_aresult);
        if (parameters->p3_window_heuristics) {
            free(lnotes[0]);
            free(lnotes);
            free(pattern_lnotes);
        }
        free(scales);
        return;
    }

    fputs("Initializing line cache and scanning local notes...", stderr);
    for (i=0; i<num_scales; ++i) {
        init_linecache(&lc[i], num_lines, map->width, map, 0, scales[i]);

        if (parameters->p3_window_heuristics) {
            lnotes[i] = &lnotes[0][i * map->target->size];
            scan_local_notes_31(map->target, lnotes[i],
                    scales[i] * ((double) pattern_window_width), 0);
/*{
                    int k;
                    printf("Song (scale: %d):\n", (int) (parameters->p3_scales[i] * (double) pattern_window_width));
                    for (k=0; k<map->target->size; ++k) {
                        vector *snote = &map->target->notes[k];
                        printf("(%d, %d, %d : %x.%x)\n", snote->strt, snote->strt + snote->dur, snote->ptch, lnotes[i][k].before, lnotes[i][k].after);
                    }
                    printf("\n");
}*/
        }
    }
    fputs(" Done.\n", stderr);

    init_song_window(&pattern_window, map->pattern, 1);
    init_p3s_window(&target_window, p3s);

    memset(&scaled_pattern, 0, sizeof(song));
    scaled_pattern.notes = (vector *) malloc(map->pattern->size *
            sizeof(vector));


    if (parameters->p3_align_rests) {
        init_song_window(&pattern_rests_window, &pattern_rests, 1);
        init_p3s_window(&target_rests_window, &p3s_rests);

        memset(&scaled_pattern_rests, 0, sizeof(song));
        scaled_pattern_rests.notes = (vector *) malloc(pattern_rests.size *
            sizeof(vector));
    }

    if (parameters->p3_max_local_transposition > 0) {
        ltrans = MIN2(MAX_LOCAL_TRANSPOSITION,
                parameters->p3_max_local_transposition);
        for (i=1; i<=ltrans; ++i) {
            ltwsum += 2.0 * LOCAL_TRANSPOSITION_WEIGHTS[i];
        }
    }

    for (i=0; i<map->height; ++i) {
        int j;
        alignmentline *mapline = &map->lines[i];
        int pattern_time = i << map->accuracy_shift;
        int target_time = mapline->target_time;
        int window_onsets = 0;


        aresult.start = target_time;
        aresult.end = target_time + map_width_ms;
        if (parameters->p3_align_rests) {
            rests_aresult.start = target_time;
            rests_aresult.end = target_time + map_width_ms;
        }
#ifdef SHOW_PROGRESS
        fprintf(stderr, "   Line %d/%d\r", i + 1, map->height);
#endif

        if ((pattern_skip <= 0) || (i == map->height - 1)) {
            move_song_window(&pattern_window, pattern_time,
                    pattern_time + pattern_window_width);
            if (parameters->p3_align_rests) {
                move_song_window(&pattern_rests_window, pattern_time,
                        pattern_time + pattern_window_width);
            }
#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
            if (pattern_window.window.size < 1023)
                window_notes[pattern_window.window.size]++;
            else window_notes[1023]++;
#endif

            for (j=0; j<num_scales; ++j) {
                double ps = scales[j];
                double s_pattern_window_width = ps * ((double)
                        pattern_window_width);
                ap3localnotes *ln = NULL, *pln = NULL;
                int norm;

                scale_pattern(&scaled_pattern, &pattern_window.window,
                              map, i, ps);

                if (j == 0) {
                    int k;
                    for (k=0; k<scaled_pattern.size; ++k) {
                        if (scaled_pattern.notes[k].strt >= 0) ++window_onsets;
                    }
                }
                if (parameters->p3_window_heuristics) {
                    ln = lnotes[j];
                    pln = pattern_lnotes;
                    scan_local_notes_31(&scaled_pattern, pln,
                            s_pattern_window_width, (scaled_pattern.size <= 1));

/*                {
                    int k;
                    printf("Pattern:\n");
                    for (k=0; k<scaled_pattern.size; ++k) {
                        vector *snote = &scaled_pattern.notes[k];
                        printf("(%d, %d, %d : %x.%x)\n", snote->strt, snote->strt + snote->dur, snote->ptch, pattern_lnotes[k].before, pattern_lnotes[k].after);
                    }
                    printf("\n");
                }
*/
                }
                move_p3s_window(&target_window, target_time,
                        target_time + map_width_ms +
                        (int) s_pattern_window_width);

                clear_ap3results(&aresult);

                norm = scaled_pattern.total_note_duration;


                switch (alg) {
                case ALG_ALIGN_P3:
                    align_turningpoints_p3(&target_window.window, ln,
                            &scaled_pattern, pln, &aresult);
                    break;

                case ALG_ALIGN_P3_ONSET:
                    align_turningpoints_p3onset(&target_window.window,
                            ln, &scaled_pattern, pln,
                            &aresult, parameters->p3_onset_delta,
                            parameters->p3_onset_slope);
                    norm += window_onsets * lrint(parameters->p3_onset_slope *
                            (double) parameters->p3_onset_delta);
                    break;

                case ALG_ALIGN_P3_OPT:
                    align_turningpoints_p3opt(&target_window.window, ln,
                            &scaled_pattern, pln, &aresult,
                            parameters->p3_onset_delta,
                            parameters->p3_onset_slope,
                            (int) s_pattern_window_width,
                            ltrans);
                    norm += window_onsets * lrint(parameters->p3_onset_slope *
                            (double) parameters->p3_onset_delta);
                    /*norm = lrint(ltwsum * (double) norm);*/
                }

                if (parameters->p3_align_rests) {
                    int k;
                    scale_pattern(&scaled_pattern_rests,
                            &pattern_rests_window.window, map, i, ps);
                    move_p3s_window(&target_rests_window, target_time,
                            target_time + map_width_ms +
                            (int) s_pattern_window_width);
                    clear_ap3results(&rests_aresult);

                    align_turningpoints_p3opt(&target_rests_window.window, NULL,
                            &scaled_pattern_rests, NULL, &rests_aresult, 0, 0,
                            (int) s_pattern_window_width, 0);

                    norm += scaled_pattern_rests.total_note_duration;

                    for (k=0; k<aresult.size; ++k) {
                        aresult.values[k] += rests_aresult.values[k];
                    }
                }
                normalize_ap3results(&aresult, norm);

#ifdef USE_LINECACHE
                update_linecache(&lc[j], &aresult, i, map);
#else
                {
                    int k;
                    for (k=0; k<aresult.size; ++k) {
                        mapline->values[k] = MAX2(mapline->values[k],
                                (unsigned char) aresult.values[k]);
                    }
                }
#endif
            }
            pattern_skip = parameters->p3_pattern_window_skip;
        }
        pattern_skip -= map->accuracy;
    }
#ifdef SHOW_PROGRESS
    fprintf(stderr, "\n");
#endif

#ifdef DEBUG_WINDOW_SIZE_HISTOGRAM
    puts("Window sizes:\n");
    for (i=0; i<1024; ++i) {
        printf("   %d:\t %d\n", i, window_notes[i]);
    }
#endif

    for (i=0; i<num_scales; ++i) {
        free_linecache(&lc[i]);
    }
    if (parameters->p3_window_heuristics) {
        free(lnotes[0]);
        free(lnotes);
        free(pattern_lnotes);
    }
    if (parameters->p3_align_rests) {
        free_song(&pattern_rests);
        free_song(&target_rests);
        free_p3_song(&p3s_rests);
        free_ap3results(&rests_aresult);
        free_song(&scaled_pattern_rests);
    }
    free(lc);
    free_song(&scaled_pattern);
    free_ap3results(&aresult);
    free_song_window(&pattern_window);
    free_p3s_window(&target_window);
    free(scales);
}
#endif
