/*
 * pq32.h - A priority queue implementation for the geometric P2 and
 *          P3 algorithms.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __PQ_32_H__
#define __PQ_32_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "config.h"
#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * A priority queue node.
 */
typedef struct {
    uint32_t index;
    int32_t  key;
    int32_t  value;
    void    *pointer;
} pq32_node;


/**
 * Priority queue root.
 */
typedef struct {
    pq32_node **tree;
    pq32_node  *nodes;
    uint32_t    size;
    uint32_t    nodecount;
    uint32_t    levels;
    /*unsigned int nodemask;
    unsigned int *tree;
    */
    PAD_STRUCT_64(4)
} pq32;


/*
 * Function definitions.
 * These are are here beceuse most of the functions should be inlined for
 * maximum performance and currently using the "static inline" directive seems
 * to be the most portable way to do it.
 */

/**
 * Creates a priority queue. The queue is stored in an array and each
 * node in the queue can be accessed directly by its index.
 *
 * NOTE! In this implementation the maximum key value depends on the queue size:
 * 0 <= key < 2**(32 - ceil(log2(size)))
 *
 * @param size queue size
 *
 * @return the created queue
 */
static pq32 *pq32_create(unsigned int size) {
    unsigned int i, j, leaves;
    int level, n;
    pq32 *pq;


    pq = (pq32 *) malloc(sizeof(pq32));
    pq->levels = fastlog2(size-1) + 1;
    leaves = 1 << (pq->levels);
    pq->nodes = (pq32node *) malloc(leaves * sizeof(pq32node));
    pq->tree = (unsigned int *) malloc(2 * leaves * sizeof(unsigned int));
    pq->size = size;
    pq->nodemask = leaves - 1;
    pq->nodecount = leaves;


/*    fprintf(stderr, "Creating priority queue (size: %d, levels: %d, leaves: %d, mask: %x)\n",
            size, pq->levels, leaves, pq->nodemask);
*/
    /* generate an empty tree */
    j = UINT_MAX << pq->levels;
    for (i = 0; i < leaves; ++i) {
        unsigned int key = j + i;
        pq->tree[leaves + i] = key;
        pq->nodes[i].key = UINT_MAX;
        pq->nodes[i].index = i;
        pq->nodes[i].value = 0;
        pq->nodes[i].pointer = NULL;
    }
    j = leaves >> 1;
    level = 2;
    n = leaves - level;
    for (i=leaves-1; i>0; --i) {
        pq->tree[i] = (UINT_MAX << pq->levels) + n;
        n -= level;
        if (i == j) {
            level <<= 1;
            j >>= 1;
            n = leaves - level;
        }
    }

/*    j = 2;
    fprintf(stderr, "\n1: ");
    for(i=1; i<2*leaves; ++i) {
        if (i == j) {
            fprintf(stderr, "\n%d: ", j);
            j <<= 1;
        }
        fprintf(stderr, "(%d:%x) ", i, pq->tree[i]);
    }
    fprintf(stderr, "\n");
*/
    return pq;
}

/**
 * Releases the memory buffers allocated for a priority eueue.
 *
 * @param pq the queue to free
 */
static inline void pq32_free(pq32 *pq) {
    free(pq->nodes);
    free(pq->tree);
    free(pq);
}


/**
 * Updates the priority queue after a change in node's key values.
 * Nodes with equal keys are not returned in any particular order. See
 * pq32_update_ascending and pq32_update_descending for
 * versions that use the node index as secondary ordering criteria.
 * 
 * @param pq the priority queue
 * @param n changed node
 */
static inline void pq32_update(pq32 *pq, const pq32_node *n) {
    pq32_node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        pq32_node *n2;
        /* If index is odd then its pair is the previous even entry,
         * otherwise the pair is the next odd entry. */
        n2 = tree[i ^ 1];
        if (n->key >= n2->key) n = n2;
        i >>= 1;
        tree[i] = n;
    }
}

/**
 * Updates the priority queue after a change in node's key.
 *
 * The only difference to pq32_update is that nodes with lesser
 * index are returned first if the keys are equal.
 
 * 
 * @param pq the priority queue
 * @param n changed node
 */
static inline void pq32_update_ascending(pq32 *pq, const pq32_node *n) {
    pq32_node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        pq32_node *n2;
        /* If index is odd then its pair is the previous even entry,
         * otherwise the pair is the next odd entry. */
        n2 = tree[i ^ 1];
        if (n->key > n2->key - (int32_t) (i & 1)) n = n2;
        i >>= 1;
        tree[i] = n;
    }
}


/**
 * Updates the priority queue after a change in node's key values.
 *
 * @param pq the priority queue
 * @param n changed node
 */
static inline void pq32_update_unrolled(const pq32 *pq, const pq32node *n) {
    unsigned int *tree = pq->tree;
    unsigned int i = pq->nodecount + n->index;
    unsigned int key = (n->key << pq->levels) + n->index;
    unsigned int pair;

    tree[i] = key;

    while (i > 7) {
        pair = tree[i ^ 1];
        if (key > pair) key = pair;
        i >>= 1;
        tree[i] = key;

        pair = tree[i ^ 1];
        if (key > pair) key = pair;
        i >>= 1;
        tree[i] = key;

        pair = tree[i ^ 1];
        if (key > pair) key = pair;
        i >>= 1;
        tree[i] = key;
    }
    while (i > 1) {
        pair = tree[i ^ 1];
        if (key > pair) key = pair;
        i >>= 1;
        tree[i] = key;
    }
}

#define PQ32_UPDATE_DECLARE_VARIABLES(levels)          \
    unsigned int *tree = pq->tree;                     \
    unsigned int i = (1 << levels) + n->index;         \
    unsigned int key = (n->key << levels) + n->index;  \
    unsigned int pair;                                 \
    tree[i] = key

#define PQ32_UPDATE_PROCESS_LEVEL    \
        pair = tree[i ^ 1];          \
        if (key > pair) key = pair;  \
        i >>= 1;                     \
        tree[i] = key

static inline void pq32_update_1(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(1);
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_2(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(2);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_3(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(3);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_4(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(4);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_5(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(5);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_6(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(6);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_7(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(7);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static inline void pq32_update_8(const pq32 *pq, const pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(8);
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
    PQ32_UPDATE_PROCESS_LEVEL;
}

static void (* const PQ32_UPDATE_FUNCTIONS[])(
        const pq32 *pq, const pq32node *n) = {
    NULL,
    pq32_update_1,
    pq32_update_2,
    pq32_update_3,
    pq32_update_4,
    pq32_update_5,
    pq32_update_6,
    pq32_update_7,
    pq32_update_8,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled,
    pq32_update_unrolled
};

#define pq32_select_optimal_update(pq) PQ32_UPDATE_FUNCTIONS[pq->levels];


/**
 * A macro that returns the requested node from a priority queue.
 * The caller should make sure that the index does not exceed queue size.
 *
 * @param pq the priority queue
 * @param i index of the node to retrieve
 *
 * @raturns a node in the queue
 */
#define pq32_getnode(pq, i) &pq->nodes[i]


/**
 * A macro that returns the first (minimum) node of the given priority
 * queue.
 *
 * @param pq the priority queue
 *
 * @raturns the first node
 */
#define pq32_getmin(pq) &pq->nodes[pq->tree[1] & pq->nodemask]


#if 0

/* Test functions. */

/**
 * A small test program for the priority queue data structure.
 */
static void pq32_check(int size) {
    pq32 *pq;
    int i;
    unsigned int lastmin;
    unsigned int seed = time(NULL);
    srand(seed);

    pq = pq32_create(size);
/*
    printf("\n\n");
    pq_printtree(pq);
    printf("\n");
*/
    printf("PQ: adding keys...\n");
    for (i=0; i<size; ++i) {
        pq32node *n;
        unsigned int key = rand();
        key = (unsigned int) (((float) key / (float) RAND_MAX - 0.5F)
	        * ((float) size * 0.5F + 1.0F));
        /* int key = i % 3333; */
        n = pq32_getnode(pq, i);
        n->key = key;
        pq32_update(pq, n);
/*
        printf("%d ", key);
*/
    }
/*
    printf("\n\n");
    pq_printtree(pq);
    printf("\n");
*/
    printf("\nPQ: getting keys...\n");
    lastmin = 0;
    for (i=0; i<size; ++i) {
        pq32node *n = pq32_getmin(pq);
/*
        printf("min: %d:%d\n", n->index, n->key1);
*/
	    if (n->key < lastmin)
            fprintf(stderr, "Error: retrieved value is smaller than the previous (%d < %d)\n", n->key, lastmin);

	    lastmin = n->key;
        n->key = UINT_MAX;
        pq32_update(pq, n);
    }
    pq32_free(pq);
}

#endif

#ifdef __cplusplus
}
#endif


#endif

