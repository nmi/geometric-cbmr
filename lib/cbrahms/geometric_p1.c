/*
 * geometric_p1.c - Geometric algorithm P1
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */


/*
   Bugs: dummy 'infinity' value should be added to the end of source, so that
   the implementation could be simplified to follow pseudocode more closely.
   The decision to not add it made the algorithm overly complex. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "config.h"

#include "search.h"
#include "song.h"
#include "geometric_p1.h"


/**
 * Search a song collection with scan_song_p1().
 *
 * @param sc         song collection to scan
 * @param pattern    search pattern
 * @param alg        search algorithm ID (see algorithms.h).
 *                   Not used in this function.
 * @param parameters search parameters
 *                   Not used in this function.
 * @param ms         match set for returning search results
 */
void alg_p1(const songcollection *sc, const song *pattern, int32_t alg,
            const searchparameters *parameters, matchset *ms) {
    uint32_t i;

    /* Unused parameters */
    (void) alg;
    (void) parameters;

    for (i = 0; i < sc->size; i++) {
        scan_song_p1(&sc->songs[i], pattern, ms);
    }
}


/**
 * Scanning phase of geometric algorithm P1. This algorithm is described in
 * Esko Ukkonen, Kjell Lemstrom and Veli Makinen: Sweepline the Music! In
 * Computer Science in Perspective (LNCS 2598), R. Klein, H.-W. Six, L. Wegner
 * (Eds.), pp. 330-342, 2003.
 *
 * Unlike in the article, end of source is not detected by putting
 * (infinity,infinity) to the end of source, but with indexes.
 *
 * @param s the song to scan
 * @param p pattern to search for
 * @param ms pointer to a structure where the results will be stored
 *
 * @return 1 when successful, 0 otherwise
 */
int32_t scan_song_p1(const song *s, const song *p, matchset *ms) {
    vector   *pattern, *text;
    uint32_t *q;

    uint32_t  i, j, pattern_size, song_size;
    int32_t   v0, p0;

    if ((p->size < 2) || (s->size < 2)) return 0;
    if (s->size < p->size) {
        /* Swap song and pattern if pattern is larger */
        pattern_size = s->size;
        song_size    = p->size;
        pattern      = s->notes;
        text         = p->notes;
    } else {
        pattern_size = p->size;
        song_size    = s->size;
        pattern      = p->notes;
        text         = s->notes;
    }

    /* q is an array that points to the last checked note for each
     * pattern position */
    q = (uint32_t *) calloc(pattern_size, sizeof(uint32_t));
    if (q == NULL) return 0;

    p0 = ((int32_t) pattern[0].strt << 8) +
          (int32_t) pattern[0].ptch - NOTE_PITCHES;

    /* Scan all notes */
    for (i = 0; i < song_size - pattern_size + 1; i++) {
        v0 = ((int32_t) text[i].strt << 8) +
              (int32_t) text[i].ptch - p0;
        q[0] = i + 1;

        /* Start finding points of the transposed pattern starting from
         * this source position and do it until a match cannot be found. */
        for (j = 1; j < pattern_size; j++) {
            int32_t v;
            int32_t pj = ((int32_t) pattern[j].strt << 8) +
                          (int32_t) pattern[j].ptch - NOTE_PITCHES;

            if (q[j] < q[j-1]) q[j] = q[j-1];
            if (q[j] == song_size) {
                free(q);
                return 0;
            }

            /* Move q over the notes that can't be part of a match for any
             * remaining pattern position. */
            do {
                v = ((int32_t) text[q[j]].strt << 8) +
                     (int32_t) text[q[j]].ptch - pj;
                q[j]++;
            } while ((v < v0) && (q[j] < song_size));

            /* After the loop we are at either a matching note or bigger,
             * i.e. there is no matching note. */
            if (v != v0) {
                q[j]--;
                break;
            }
        }

        /* Check if a match was found */
        if (j == pattern_size) {
            /* Return the match */
            int32_t start = text[i].strt;
            int32_t end = start + pattern[pattern_size-1].strt +
                          pattern[pattern_size-1].dur - pattern[0].strt;
            int8_t  transposition = (int8_t) (text[i].ptch - pattern[0].ptch);

            insert_match(ms, s->id, start, end, transposition, 1.0F);
        }
    }
    free(q);
    return 1;
}


/**
 * Checks if there is an exact match to a pattern in the given position.
 *
 * @param s a song
 * @param songpos position in the song
 * @param p pattern to match
 * @param patternpos position in the pattern that aligns with songpos
 * @param ms structure where the match information is stored to
 *
 * @return match information item
 */
match *alignment_check_p1(const song *s, uint32_t songpos,
                          const song *p, uint32_t patternpos, matchset *ms) {

    vector *pnotes = p->notes;
    vector *snotes = s->notes;
    match  *m;

    int32_t i, j, end;
    int32_t p0, v0;

    if ((songpos >= s->size) || (patternpos >= p->size)) return NULL;

    p0 = ((int32_t) pnotes[patternpos].strt << 8) +
          (int32_t) pnotes[patternpos].ptch;
    v0 = ((int32_t) snotes[songpos].strt << 8) +
          (int32_t) snotes[songpos].ptch;

    /* Scan the end */
    i = (int) patternpos + 1;
    j = (int) songpos;
    for (; i < (int) p->size; i++) {
        int32_t vi;
        int32_t pi = ((int32_t) pnotes[i].strt << 8) +
                      (int32_t) pnotes[i].ptch - p0;

        /* Skip over notes that are not in the pattern. */
        do {
            j++;
            if (j >= (int) s->size) return NULL;
            vi = ((int32_t) snotes[j].strt << 8) +
                  (int32_t) snotes[j].ptch - v0;
        } while (vi < pi);

        /* Is there a matching note? If not, exit. */
        if (vi != pi) return NULL;
    }
    end = j;

    /* Scan the beginning */
    i = (int) patternpos - 1;
    j = (int) songpos;
    for (; i >= 0; i--) {
        int32_t pi = p0 - ((int32_t) pnotes[i].strt << 8) -
                     (int32_t) pnotes[i].ptch;
        int32_t vi;

        /* Skip over notes that are not in the pattern. */
        do {
            j--;
            if (j < 0) return NULL;
            vi = v0 - ((int32_t) snotes[j].strt << 8) -
                 (int32_t) snotes[j].ptch;
        } while (vi < pi);

        /* Is there a matching note? If not, exit. */
        if (vi != pi) return NULL;
    }

    if (ms == NULL) return (match *) 1;

    m = insert_match(ms, s->id, snotes[j].strt,
            snotes[end].strt + snotes[end].dur,
            (int8_t) (snotes[j].ptch - pnotes[0].ptch), 1.0F);
    if (m == NULL) return NULL;

    /* Match found */
/*    if (ms->num_matches >= ms->size) i = ms->size - 1;
    else {
        i = ms->num_matches;
        ++ms->num_matches;
    }

    m = &ms->matches[i];
    m->song = s->id;
    m->start = snotes[j].strt;
    m->end = snotes[end].strt + snotes[end].dur;
    m->transposition = snotes[j].ptch - pnotes[0].ptch;
    m->similarity = 1.0F;
*/
    /* Find out matching note positions if they are requested */
    if ((m->num_notes > 0) && (m->notes != NULL)) {
        m->notes[0] = j;
        p0 = ((int32_t) pnotes[0].strt << 8) + (int32_t) pnotes[0].ptch;
        v0 = ((int32_t) snotes[j].strt << 8) + (int32_t) snotes[j].ptch;

        /*if (m->num_notes > p->size) m->num_notes = p->size;*/
        for (i = 1; i < (int32_t) m->num_notes; i++) {
            int32_t vi;
            int32_t pi = ((int32_t) pnotes[i].strt << 8) +
                          (int32_t) pnotes[i].ptch - p0;

            /* Skip over notes that are not in the pattern. */
            do {
                j++;
                if (j >= (int) s->size) return m;
                vi = ((int32_t) snotes[j].strt << 8) +
                      (int32_t) snotes[j].ptch - v0;
            } while (vi < pi);

            /* Is there a matching note? If not, exit. */
            if (vi != pi) return m;
            m->notes[i] = j;
        }
    }
    return m;
}


