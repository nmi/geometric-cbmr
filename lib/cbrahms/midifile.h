/*
 * midifile.h - Function declarations for the MIDI file I/O
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __MIDIFILE_H__
#define __MIDIFILE_H__

#include <stdint.h>
#include "song.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Minimum size of a MIDI file */
#define SMF_MIN_SIZE 14
/** Gap length to use between concatenated tracks of Type 3 files. */
#define MIDI_TYPE3_GAP 1000

#define MIDI_DEFAULT_FORMAT 1
#define MIDI_DEFAULT_PPQN_DIVISION 96
#define MIDI_DEFAULT_TEMPO 500.0

/** Should the note buffers be reallocated after the exact number of notes is
  * known. Otherwise the amount of memory allocated for them will usually be
  * 2-3 times higher than necessary. */
#define MIDI_READ_TRIM_BUFFERS 1


/* Constants */

#define MIDI_CHANNELS 16
#define MIDI_PERCUSSION_CHANNEL 9

#define PPQN 0
#define SMPTE 1

#define STATUS_BIT 0x80
#define STATUS_MASK 0xF0
#define CHANNEL_MASK 0x0F

/* Voice events */
#define EVENT_NOTE_OFF 0x80
#define EVENT_NOTE_ON 0x90
#define EVENT_AFTERTOUCH 0xA0
#define EVENT_CONTROLLER 0xB0
#define EVENT_PROGRAM_CHANGE 0xC0
#define EVENT_CHANNEL_PRESSURE 0xD0
#define EVENT_PITCH_WHEEL 0xE0

/* System events */
#define EVENT_SYSEX 0xF0
#define EVENT_ESCAPE 0xF7
#define EVENT_META 0xFF

/* Meta events */
#define EVENT_END_OF_TRACK 0x2F
#define EVENT_TEMPO 0x51

/* Controllers */
#define CONTROLLER_ALL_SOUND_OFF 120
#define CONTROLLER_ALL_NOTES_OFF 123

#define MIDI_HEADER_SIZE 14
#define MIDI_TRACK_HEADER_SIZE 8


/**
 * A struct for MIDI events.
 */
typedef struct {
    const uint8_t *data;
    double   tick;
    uint32_t length;
    uint8_t  status;
    uint8_t  metatype;
} track_event;


/**
 * MIDI song container type.
 */
typedef struct {
    char         *name;
    uint8_t      *buffer;
    uint32_t     *track_size;
    track_event **track_data;
    uint32_t      num_tracks;
    uint32_t      buffer_size;
} midisong;



/* External function declarations */

size_t read_midi_collection(const char *path, songcollection *sc,
        int skip_percussion);

int read_midi_file(const char *file, song *s, midisong *midi_s,
        int skip_percussion);

int write_midi_file(const char *path, const midisong *midi_s,
        int force_leading_silence);

void free_midisong(midisong *midi_s);

int song_to_midisong(const song *s, midisong *midi_s);

#ifdef __cplusplus
}
#endif

#endif

