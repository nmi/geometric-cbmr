/*
 * filter_p2.h - External function declarations for P2-based vector index
 *               filters.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __FILTER_P2_H__
#define __FILTER_P2_H__

#include "results.h"
#include "search.h"
#include "song.h"
#include "vindex.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * A helper structure used by some index-accessing methods.
 */
typedef struct fp2vector {
    vindextable *vit;
    uint32_t     patternpos;
    uint32_t     t1;
    uint32_t     t2;
    int32_t      shift;
} fp2vector;


void filter_p2_greedy_pigeonhole(const songcollection *sc, const song *pattern,
                                 int alg, const searchparameters *parameters,
                                 matchset *ms);

#ifdef ENABLE_BLOSSOM4
void filter_p2_pigeonhole(const songcollection *sc, const song *pattern,
        int alg, const searchparameters *parameters, matchset *ms);
#endif

void filter_p2_window(const songcollection *sc, const song *pattern,
        int alg, const searchparameters *parameters, matchset *ms);

void filter_p2_select_local(const songcollection *sc, const song *pattern,
        int alg, const searchparameters *parameters, matchset *ms);

void filter_p2_select_global(const songcollection *sc, const song *pattern,
        int alg, const searchparameters *parameters, matchset *ms);

void filter_p2_points(const songcollection *sc, const song *pattern,
        int alg, const searchparameters *parameters, matchset *ms);

#ifdef __cplusplus
}
#endif


#endif

