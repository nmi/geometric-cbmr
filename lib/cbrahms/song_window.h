/*
 * song_window.h - External function declarations for song window
 *
 * Copyright (c) 2008-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __SONG_WINDOW_H__
#define __SONG_WINDOW_H__

#include "config.h"
#include "song.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Song window data structure.
 */
typedef struct {
    const song *s;
    vector  *notes_on_p[128];
    song     window;
    int32_t  x1, x2;
    uint32_t n1, n2;
    uint32_t notes_on[4];
    int      relative;
} song_window;


void init_song_window(song_window *w, const song *s, int relative_time);

void free_song_window(song_window *w);

void move_song_window(song_window *w, int x1, int x2);


#ifdef ENABLE_UNIT_TESTS

void test_song_window(const song *s, int moves, int resets, int relative_time,
        int window_length);

#endif


#ifdef __cplusplus
}
#endif

#endif

