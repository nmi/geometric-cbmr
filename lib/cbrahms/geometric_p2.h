/*
 * geometric_p2.h - External function declarations for Geometric algorithm P2
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __GEOMETRIC_P2_H__
#define __GEOMETRIC_P2_H__

#include "config.h"

#include "search.h"
#include "song.h"


#ifdef __cplusplus
extern "C" {
#endif

/* Song duration limit due to the priority queue key coding used in this
 * implementation */
#define P2_TIME_LIMIT (1 << 23)

/**
 * Structure for storing a song in P2 format.
 */
typedef struct {
    const song *s;
    int32_t    *onsets;
    uint32_t    num_notes;
    /*PAD_STRUCT_64(4)*/
} p2song;


/**
 * Structure for storing a song collection in P2 format.
 */
typedef struct {
    p2song               *p2_songs;
    const songcollection *song_collection;
    uint32_t              size;
    /*PAD_STRUCT_64(4)*/
} p2songcollection;


/* External function declarations */

void *alloc_p2_song_collection(void);

int   build_p2_song_collection(p2songcollection *p2_songcollection,
        const songcollection *sc, const dataparameters *dp);
int   build_p2_song_collection_untyped(void *p2_songcollection,
        const songcollection *sc, const dataparameters *dp);

void  clear_p2_song_collection(p2songcollection *p2_songcollection);
void  clear_p2_song_collection_untyped(void *p2_songcollection);

void  unalloc_p2_song_collection(void *p2_songcollection);

void init_p2_song(p2song *p2s);
void free_p2_song(p2song *p2s);
void song_to_p2(const song *s, p2song *p2s);


void alg_p2(const songcollection *sc, const song *pattern, int32_t alg,
            const searchparameters *parameters, matchset *ms);

void alg_p2_points(const songcollection *sc, const song *pattern, int32_t alg,
                   const searchparameters *parameters, matchset *ms);

int scan_song_p2(const song *s, const song *p, const uint32_t errors,
                 matchset *ms);

song *p2_compensate_quantization(const song *p, const int32_t q);

match *alignment_check_p2(const song *s, uint32_t songpos,
                          const song *p, uint32_t patternpos,
                          matchset *ms);

int scan_song_p2_points(const song *s, const song *p, uint32_t num_points,
                        const uint32_t *points, matchset *ms);

#ifdef __cplusplus
}
#endif


#endif

