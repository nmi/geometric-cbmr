/*
 * search_msm.c - Functions for searching patterns in a song collection with
 *            MSM algorithms.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "results.h"
#include "song.h"
#include "algorithms.h"
#include "search_msm.h"
#include "util.h"

#include "music.h"
#include "point2_t.h"

/**
 * Structure for storing songs in MSM format.
 */
typedef struct {
    const songcollection *song_collection;
    music_t *m;
    uint32_t size;
} msm_songcollection;


/**
 * Converts a song to the MSM format.
 *
 * @param s the song to convert
 * @param m reference to an MSM music object
 */
static void song_to_msm(const song *s, music_t &m) {
    vector  *notes = s->notes;
    uint32_t i;

    for (i = 0; i < s->size; i++) {
        vector  *n = &notes[i];
        point2_t p = point2_t(n->strt, n->strt + n->dur);
        m.insert(m.end(), p);
    }
}


/**
 * Initializes an MSM song collection struct.
 *
 * @return pointer to the data structure.
 */
void *init_msm_song_collection(void) {
    msm_songcollection *msc = (msm_songcollection *) calloc(1,
            sizeof(msm_songcollection));
    return msc;
}


/**
 * Converts a song collection to the MSM format.
 *
 * @param sc  song collection to convert
 * @param msc target MSM song collection
 */
int build_msm_song_collection(void *msm_sc, const songcollection *sc,
        const dataparameters *dp) {
    msm_songcollection *msc = (msm_songcollection *) msm_sc;
    uint32_t i;

    /* Unused parameter dp */
    (void) dp;

    msc->song_collection = sc;
    msc->size = sc->size;
    msc->m = new music_t[sc->size];
    for (i = 0; i < sc->size; i++) {
        song_to_msm(&sc->songs[i], msc->m[i]);
    }
    return 1;
}

/**
 * Clears and re-initializes the given MSM song collection.
 *
 * @param msm_sc MSM song data
 */
void clear_msm_song_collection(void *msm_sc) {
    msm_songcollection *msc = (msm_songcollection *) msm_sc;
    delete[] msc->m;
    msc->song_collection = NULL;
    msc->size = 0;
}


/**
 * Frees the given MSM song collection.
 *
 * @param msm_sc MSM song data
 */
void free_msm_song_collection(void *msm_sc) {
    clear_msm_song_collection(msm_sc);
    free(msm_sc);
}


/**
 * Scans the given song for matches to a pattern.
 *
 * @param sc         song collection
 * @param pattern    the input pattern to search for
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         matches
 */
void alg_msm(const songcollection *sc, const song *pattern, int alg,
        const searchparameters *parameters, matchset *ms) {

    msm_songcollection *msc = (msm_songcollection *) sc->data[DATA_MSM];
    music_t  msm_p;
    uint32_t i;

    if (msc == NULL) {
        fputs("Error in alg_msm(): song collection does not contain MSM data.\nUse updata_song_collection_data() to generate it.\n", stderr);
        return;
    }

    song_to_msm(pattern, msm_p);

    for (i = 0; i < msc->size; i++) {
        int max;
        switch (alg) {
            case ALG_MSM_MFD_2D:
                max = most_frequent_distance_2D(msc->m[i], msm_p);
                break;
            case ALG_MSM_MFD_2D_QUICK:
                max = most_frequent_distance_2D_quick(msc->m[i], msm_p);
                break;
            case ALG_MSM_MFD_1D:
                max = most_frequent_distance_1D(msc->m[i], msm_p);
                break;
            case ALG_MSM_MFD_1D_QUICK:
                max = most_frequent_distance_1D_quick(msc->m[i], msm_p);
                break;
            case ALG_MSM_MFD_1D_QUICK_SPACEEFF:
                max = most_frequent_distance_1D_quick_spaceeff(msc->m[i], msm_p);
                break;
            case ALG_MSM_FFT:
                max = largest_fft(msc->m[i], msm_p, parameters->msm_r);
                break;
            case ALG_MSM_FFT_LOOKUP_1:
                max = largest_fft_lookup(msc->m[i], msm_p, parameters->msm_r);
                break;
            case ALG_MSM_FFT_LOOKUP_2:
                max = largest_fft_lookup2(msc->m[i], msm_p, parameters->msm_r);
                break;
            default:
                fprintf(stderr, "Error in alg_msm(): Unknown MSM algorithm %d\n", alg);
                return;
        }

        if (max > 1) {
#ifdef P2_NORMALIZE_SIMILARITY
            float similarity = ((float) max) / ((float)
                    MIN2(pattern->size, sc->songs[i]->size));
#else
            float similarity = ((float) max) / ((float) pattern->size);
#endif
            insert_match(ms, sc->songs[i].id, 0, sc->songs[i].size - 1, 0,
                    similarity);
        }
    }
}

