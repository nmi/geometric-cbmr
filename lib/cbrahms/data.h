/*
 * data.h - Constants and external declarations for data formats
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __DATA_H__
#define __DATA_H__

#include "data_formats.h"
#include "song.h"

#ifdef __cplusplus
extern "C" {
#endif


void *init_data_format(int32_t format);

int32_t convert_song_collection(int32_t format, void *data,
                                const songcollection *sc,
                                const dataparameters *dp);

void clear_data_format(int32_t format, void *data);

void free_data_format(int32_t format, void *data);


#ifdef __cplusplus
}
#endif

#endif

