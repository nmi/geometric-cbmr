/*
 * filter_p1.c - Vector index search filters based on the P1 algorithm
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include "config.h"
#include "filter_p1.h"
#include "geometric_p1.h"
#include "results.h"
#include "vindex.h"
#include "util.h"


#define MAX_TRIES 100

/**
 * Index filter P1(v1). Picks the first valid vector from the pattern and
 * retrieves positions where it occurs in the song collection.
 *
 * @param sc a song collection
 * @param pattern the input pattern to search for
 * @param alg algorithm ID as defined in search.h; not used in this function
 * @param parameters search parameters
 * @param ms information about the found matches will be stored here
 */
void filter_p1_random(const songcollection *sc, const song *pattern,
                      int32_t alg, const searchparameters *parameters,
                      matchset *ms) {
    match       *m;
    vindexrec   *records;
    vindextable *vit;
    song        *songs     = sc->songs;
    vindex      *vi        = (vindex *) sc->data[DATA_VINDEX];

    int32_t  tries         = 0;
    uint32_t d             = parameters->d_window;
    /*int32_t lastsong     = -1;*/
    uint32_t pattern_index = 0;
    uint32_t ir, numrecords;

#ifdef ENABLE_TIME_MEASUREMENTS
    struct timeval t1, t2;
#endif

    /* Parameter alg is unused here. This is to avoid a compiler warning. */
    (void) alg;

    if (vi == NULL) {
        fputs("Error in filter_p1_random(): song collection does not contain "
              "vectorindex data.\nUse update_song_collection_data() before "
              "calling this function.\n", stderr);
        return;
    }

    /* Pick a vector randomly. */
    while (1) {
        int32_t x, y;
        uint32_t p1, p2;
        if (pattern->size <= d) {
            p1 = 0;
        } else {
            p1 = (uint32_t) (randf() * (float) (pattern->size - d - 1));
        }
        p2 = (uint32_t) (randf() * (float) d) + p1 + 1;
        if (p2 >= pattern->size) p2 = pattern->size - 1;

        tries++;
        if (tries > MAX_TRIES) {
            fputs("Error in filter_p1_random(): failed to pick an index "
                  "vector\n", stderr);
            return;
        }
        if (p2 - p1 > d) continue;
        x = pattern->notes[p2].strt - pattern->notes[p1].strt;
        y = pattern->notes[p2].ptch - pattern->notes[p1].ptch;

#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) gettimeofday(&t1, NULL);
#endif
        vit = vindex_table_by_vector(vi, x, y);
#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t2, NULL);
            ms->time.indexing += timediff(&t2, &t1);
        }
#endif
        if (vit != NULL) {
            pattern_index = p1;
            break;
        }
    }

    records    = vit->records;
    numrecords = vit->size;
    for (ir = 0; ir < numrecords; ir++) {
        vindexrec *r = &records[ir];
        /*if (r->song == lastsong) continue;*/

#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t1, NULL);
            ms->time.indexing += timediff(&t1, &t2);
        }
#endif
        m = alignment_check_p1(&songs[r->song], r->note, pattern,
                               pattern_index, ms);
        if (m != NULL) {
            /* Match found, skip to the next song. */
            /*lastsong = r->song;*/
            /*m->song = r->song;*/
            if (ms->num_matches == ms->capacity) return;
        }
#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t2, NULL);
            ms->time.verifying += timediff(&t2, &t1);
        }
#endif
    }
}


/**
 * Index filter P1(v2). Picks the least common valid vector from the
 * pattern to limit the number of locations to check.
 *
 * @param sc a song collection
 * @param pattern the input pattern to search for
 * @param alg algorithm ID as defined in search.h; not used in this function
 * @param parameters search parameters
 * @param ms information about the found matches will be stored here
 */
void filter_p1_select_1(const songcollection *sc, const song *pattern,
                        int32_t alg, const searchparameters *parameters,
                        matchset *ms) {
    match       *m;
    vindexrec   *records;
    vindextable *minvit = NULL;
    song        *songs  = sc->songs;
    vindex      *vi     = (vindex *) sc->data[DATA_VINDEX];

    uint32_t mincount   = INT_MAX;
    uint32_t i, j, ir, patternpos = 0;

    /* Parameter alg is unused here. This is to avoid a compiler warning. */
    (void) alg;

    if (vi == NULL) {
        fputs("Error in filter_p1_select_1: song collection does not contain "
              "vectorindex data.\nUse update_song_collection_data() before "
              "calling this function.\n", stderr);
        return;
    }

    for (i = 0; i < pattern->size - 1; i++) {
        for (j = i + 1; j < pattern->size; j++) {
            vindextable *vit;
            int32_t x, y;

            if (j >= i + parameters->d_window) break;

            x = pattern->notes[j].strt - pattern->notes[i].strt;
            y = pattern->notes[j].ptch - pattern->notes[i].ptch;
            vit = vindex_table_by_vector(vi, x, y);

            if ((vit != NULL) && (vit->size < mincount)) {
                mincount   = vit->size;
                minvit     = vit;
                patternpos = i;
            }
        }
    }
    if (minvit == NULL) {
        fputs("Error in filter_p1_select_1(): failed to pick an index "
              "vector\n", stderr);
        return;
    }

    records = minvit->records;

    /*j = -1;*/
    for (ir = 0; ir < mincount; ir++) {
        vindexrec *r = &records[ir];
        /*if (r->song == j) continue;*/
        m = alignment_check_p1(&songs[r->song], r->note, pattern,
                               patternpos, ms);
        if (m != NULL) {
            /* Match found, skip to the next song. */
            /*j       = r->song;*/
            /*m->song = r->song;*/
            if (ms->num_matches == ms->capacity) return;
        }
    }   
}


/**
 * Index filter P1(v3). Picks the two least common valid vector from the pattern
 * and only checks locations where both vectors appear at the same relative
 * positions as in the pattern.
 *
 * @param sc a song collection
 * @param pattern the input pattern to search for
 * @param alg algorithm ID as defined in search.h; not used in this function
 * @param parameters search parameters
 * @param ms information about the found matches will be stored here
 */
void filter_p1_select_2(const songcollection *sc, const song *pattern,
                        int32_t alg, const searchparameters *parameters,
                        matchset *ms) {
    match       *m;
    vector      *note;
    vindexrec   *records;
    song        *songs       = sc->songs;
    vindex      *vi          = (vindex *) sc->data[DATA_VINDEX];
    vindextable *vit1        = NULL;
    vindextable *vit2        = NULL;

    uint32_t i, j, k, limit  = INT_MAX;
    uint32_t vit1_patternpos = 0;
    uint32_t vit2_patternpos = 0;
    int32_t  patternstrt, patternptch;
    /*int32_t lastsong     = -1;*/

#ifdef ENABLE_TIME_MEASUREMENTS
    struct timeval t1, t2;
    if (ms->measure_time_consumption) gettimeofday(&t1, NULL);
#endif

    /* Parameter alg is unused here. This is to avoid a compiler warning. */
    (void) alg;

    if (vi == NULL) {
        fputs("Error in filter_p1_select_1: song collection does not contain "
              "vectorindex data.\nUse update_song_collection_data() before "
              "calling this function.\n", stderr);
        return;
    }

    /* Pick the two least common vectors from the pattern. */
    for (i = 0; i < pattern->size - 1; i++) {
        for (j = i + 1; j < pattern->size; j++) {
            vindextable *vit;
            int32_t      x, y;

            if (j >= i + parameters->d_window) break;

            x = pattern->notes[j].strt - pattern->notes[i].strt;
            y = pattern->notes[j].ptch - pattern->notes[i].ptch;
            vit = vindex_table_by_vector(vi, x, y);

            if (vit != NULL) {
                if (vit1 == NULL) {
                    vit1 = vit;
                    vit1_patternpos = i;
                    limit = vit->size;
                } else if (vit2 == NULL) {
                    vit2 = vit;
                    vit2_patternpos = i;
                    if (vit->size < limit) limit = vit->size;
                } else if (vit->size < limit) {
                    if (vit2->size < vit1->size) {
                        vit1 = vit;
                        vit1_patternpos = i;
                    } else {
                        vit2 = vit;
                        vit2_patternpos = i;
                    }
                    limit = vit->size;
                }
            }
        }
    }
    if ((vit1 == NULL) || (vit2 == NULL)) {
        fputs("Error in filter_p1_select_2(): failed to pick index "
              "vectors\n", stderr);
        return;
    }

    /* Switch the vectors so that v1 is the less frequent one */
    if (vit1->size > vit2->size) {
        vindextable *tmp = vit1;
        vit1 = vit2;
        vit2 = tmp;

        i = vit2_patternpos;
        vit2_patternpos = vit1_patternpos;
        vit1_patternpos = i;
    }

    records     = vit1->records;
    note        = &pattern->notes[vit1_patternpos];
    patternstrt = note->strt;
    patternptch = note->ptch;
    k = 0;

#ifdef ENABLE_TIME_MEASUREMENTS
    if (ms->measure_time_consumption) {
        gettimeofday(&t2, NULL);
        ms->time.indexing += timediff(&t2, &t1);
    }
#endif

    /* Merge the location lists */
    for (i = 0; i < vit1->size; i++) {
        int32_t    songstrt, songptch, patterndelta;
        int32_t    songnumber = records[i].song;
        int32_t    songpos    = records[i].note;
        int32_t    failed     = 0;
        song      *s          = &songs[songnumber];
        vindexrec *orecords;

        note     = &s->notes[songpos];
        songstrt = note->strt;
        songptch = note->ptch;

        /* Check other vectors in the ascending order of their occurrence
         * counts. */
        note         = &pattern->notes[vit2_patternpos];
        patterndelta = ((patternstrt - note->strt) << 8) +
                        (patternptch - note->ptch);
        orecords = vit2->records;

        for (j = k; j < vit2->size; j++) {
            int32_t osongnumber = orecords[j].song;

            if (osongnumber < songnumber) {
                k = j + 1;
            } else if (osongnumber == songnumber) {
                song   *os       = &songs[osongnumber];
                int32_t osongpos = orecords[j].note;
                int32_t songdelta;

                note = &os->notes[osongpos];
                songdelta = ((songstrt - note->strt) << 8) +
                             (songptch - note->ptch);
                /* Break if found a pair. */
                if (patterndelta < songdelta) {
                    k = j + 1;
                } else if (patterndelta == songdelta) {
                    k = j + 1;
                    break;
                } else {
                    failed = 1;
                    break;
                }
            } else {
                failed = 1;
                break;
            }
        }

#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t1, NULL);
            ms->time.other += timediff(&t1, &t2);
        }
#endif
        if (!failed) {
            vindexrec *r = &records[i];
            /*if (r->song == lastsong) continue;*/
            m = alignment_check_p1(&songs[r->song], r->note, pattern,
                                   vit1_patternpos, ms);
            if (m != NULL) {
                /* Match found, skip to the next song. */
                /*lastsong = r->song;*/
                /*m->song = r->song;*/
                if (ms->num_matches == ms->capacity) return;
            }
        }
#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t2, NULL);
            ms->time.verifying += timediff(&t2, &t1);
        }
#endif
    }
}


/**
 * Index filter P1(v4). Selects the least common valid vector from a group of
 * random vectors picked from the pattern. This makes the search time complexity
 * linear in regard to pattern size.
 *
 * @param sc a song collection
 * @param pattern the input pattern to search for
 * @param alg algorithm ID as defined in search.h; not used in this function
 * @param parameters search parameters
 * @param ms information about the found matches will be stored here
 */
void filter_p1_sample(const songcollection *sc, const song *pattern,
                      int32_t alg, const searchparameters *parameters,
                      matchset *ms) {
    vindex      *vi    = (vindex *) sc->data[DATA_VINDEX];
    song        *songs = sc->songs;
    vindexrec   *records;
    vindextable *best_vit;
    match       *m;

    uint32_t     best_pattern_pos = 0;
    uint32_t     i;
    /*int32_t      lastsong;*/
    vindextable  best_vit_;

    /* Parameter alg is unused here. This is to avoid a compiler warning. */
    (void) alg;

    best_vit_.size = INT_MAX;
    best_vit       = &best_vit_;

    if (vi == NULL) {
        fputs("Error in filter_p1_sample: song collection does not contain "
              "vectorindex data.\nUse update_song_collection_data() before "
              "calling this function.\n", stderr);
        return;
    }

    if (pattern->size < 2) return;

    /* Pick vcount random vectors from the pattern */
    for (i = 0; i < parameters->p1_sample_size; i++) {
        vindextable *vit;
        uint32_t j, k;
        int32_t  x, y;

        j = (uint32_t) lrint(MAX2(randd() * ((double) pattern->size - 2),
                                  0.0));
        k = (uint32_t) (randd() * ((double) parameters->d_window)) + j;
        if (k <= j) k = j + 1;
        else if (k >= pattern->size) k = pattern->size - 1;

        x  = pattern->notes[k].strt - pattern->notes[j].strt;
        y  = pattern->notes[k].ptch - pattern->notes[j].ptch;
        vit = vindex_table_by_vector(vi, x, y);
        if ((vit != NULL) && (vit->size < best_vit->size)) {
            best_vit         = vit;
            best_pattern_pos = j;
        }
    }

    /* If there were no valid vectors in the random group, try P1/F2 */
    if (best_vit->size == INT_MAX) {
        fputs("Warning in filter_p1_sample(): no valid vectors found. "
              "Trying again with P1/F2...", stderr);
        filter_p1_select_1(sc, pattern, alg, parameters, ms);
        return;
    }

    records = best_vit->records;
    /*lastsong = -1;*/

    for (i = 0; i < best_vit->size; i++) {
        vindexrec *r = &records[i];
        /*if (r->song == lastsong) continue;*/
        m = alignment_check_p1(&songs[r->song], r->note, pattern,
                               best_pattern_pos, ms);
        if (m != NULL) {
            /* Match found, skip to the next song. */
            /*lastsong = r->song;*/
            /*m->song = r->song;*/
            if (ms->num_matches == ms->capacity) return;
        }
    }   
}

