/*
 * search.h - Constants and external declarations for the search functions
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __SEARCH_H__
#define __SEARCH_H__

/*#include "config.h"*/
#include "song.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Structure for search parameters
 */
typedef struct {
    uint32_t *p2_points;
    float     p2_select_threshold;
    uint32_t  p2_window;
    uint32_t  p2_num_points;
    uint32_t  quantization;
    uint32_t  c_window;
    uint32_t  d_window;

    int32_t   msm_r;

    uint32_t  p1_sample_size;

    int32_t   sync_accuracy;
    int32_t   syncmap_accuracy;
    int32_t   sync_window_size;

    /* Flags */

    uint8_t measure_time_consumption;
    /* Calculate the overall length of song notes that do _not_ overlap with
     * the pattern at the position of maximal overlapping. This is used as an
     * additional similarity factor and can be useful when comparing whole
     * songs or polyphonic transcriptions. */
    uint8_t p3_calculate_difference;
    uint8_t p3_remove_gaps;

    /*PAD_STRUCT(1)*/
} searchparameters;


void search(const songcollection *sc, const song *pattern, int32_t alg,
        const searchparameters *parameters, matchset *ms);

void print_results(const matchset *ms, const songcollection *sc);


#ifdef __cplusplus
}
#endif

#endif

