/*
 * priority_queue.h - A priority queue implementation for the geometric P2 and
 *                    P3 algorithms.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __PRIORITY_QUEUE_SSE41_H__
#define __PRIORITY_QUEUE_SSE41_H__

/* For posix_memalign */
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE 1
#endif
#include <stdlib.h>

#include <stdio.h>
#include <time.h>
#include <limits.h>

#include "config.h"
#include "priority_queue.h"
#include "priority_queue_32.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * A priority queue node.
 */
typedef struct {
    short index;
    unsigned int key;
    int value;
    void *pointer;
} pqsse41node;


/**
 * A tree node.
 */
typedef struct {
    unsigned short index[8];
    unsigned short key[8];
/*    int smallestpos;
    int unused[7];*/
} pqsse41treenode;


/**
 * Priority queue root.
 */
typedef struct {
    short size;
    short leaves;
    short leaves_start;
    short levels;
    int minpos;
    pqsse41node *nodes;
    pqsse41treenode *tree;
    int treesize[6];
} pqsse41root;



/*
 * Function definitions.
 * These are are here beceuse most of the functions should be inlined for
 * maximum performance and currently using the "static inline" directive seems
 * to be the most portable way to do it. This can be changed after GCC works
 * in C99 mode by default.
 */

/**
 * Creates a priority queue. The queue is stored in an array and each
 * node in the queue can be accessed directly by its index.
 *
 * @param size queue size
 *
 * @return the created queue
 */
static pqsse41root *pqsse41_create(short size) {
    int i;
    short leaves, leaves_start, levels;
    unsigned int index_val, index_increase;
    pqsse41root *pq;

    if (size <= 8) size = 8;

    pq = (pqsse41root *) calloc(1, sizeof(pqsse41root));
    if (pq == NULL) {
        fputs("Error: failed to allocate memory in pqsse41_create()", stderr);
        return pq;
    }

    pq->treesize[0] = 0;
    pq->treesize[1] = 1;
    pq->treesize[2] = 9;
    pq->treesize[3] = 73;
    pq->treesize[4] = 585;
    pq->treesize[5] = 4681;

    levels = (pq_log_2(size-1) / 3) + 1;
    leaves = 1 << (3 * (levels - 1));
    leaves_start = pq->treesize[levels-1];
    pq->nodes = (pqsse41node *) malloc(size * sizeof(pqsse41node));
    i = posix_memalign((void **) &pq->tree, CACHE_ALIGNMENT,
                pq->treesize[levels] * sizeof(pqsse41treenode));
    if (i || (pq->nodes == NULL)) {
        fputs("Error: failed to allocate memory in pqsse41_create()", stderr);
        free(pq->nodes);
        free(pq->tree);
        return pq;
    }
    pq->size = size;
    pq->levels = levels;
    pq->leaves = leaves;
    pq->leaves_start = leaves_start;
    pq->minpos = 0;

/*    printf("pqsse41 - leaves: %d, levels: %d, size: %d, leaves_start: %d\n", leaves, levels, size, leaves_start);
*/
    /* generate an empty tree */

    for (i=0; i < size; ++i) {
        pqsse41node *node = &pq->nodes[i];
        node->index = i;
        node->key = 0xFFFF;
        node->value = 0;
        node->pointer = NULL;
    }

    index_val = 0x00010000;
    index_increase = 0x00020002;
    for (i=levels-1; i>=0; --i) {
        int j;
        int level_start = pq->treesize[i];
        unsigned int current_index = index_val;
        for (j=0; j<leaves; ++j) {
            pqsse41treenode *node = &pq->tree[level_start+j];
            unsigned int *node_key = (unsigned int *) node->key;
            unsigned int *node_index = (unsigned int *) node->index;
            node_index[3] = current_index; current_index += index_increase;
            node_index[2] = current_index; current_index += index_increase;
            node_index[1] = current_index; current_index += index_increase;
            node_index[0] = current_index; current_index += index_increase;
            node_key[0] = UINT_MAX;
            node_key[1] = UINT_MAX;
            node_key[2] = UINT_MAX;
            node_key[3] = UINT_MAX;
        }
        index_val <<= 3;
        index_increase <<= 3;
        leaves >>= 3;
    }
    return pq;
}

/**
 * Releases the memory buffers allocated for a priority eueue.
 *
 * @param pq the queue to free
 */
static inline void pqsse41_free(pqsse41root *pq) {
    free(pq->nodes);
    free(pq->tree);
    free(pq);
}


/**
 * Updates the priority queue after a change in node's key values.
 *
 * @param pq the priority queue
 * @param n changed node
 */
#if 0
static inline void pq32_update_key(pq32root *pq, pq32node *n) {
    unsigned int *tree = pq->tree;
    unsigned int i = pq->nodecount + n->index;
    unsigned int key = (n->key << pq->levels) + n->index;
    tree[i] = key;

    while (i > 1) {
        /* If index is odd then its pair is the previous even entry, otherwise
           the pair is the next odd entry. */
        /* FIXME: This does not seem to work on big-endian processors */
        unsigned int pair = tree[i ^ 1];
        if (key > pair) key = pair;
        i >>= 1;
        tree[i] = key;
    }
}
#endif

#define PHMINPOSUW(vector, result)             \
    __asm__ __volatile__(                      \
        "movaps       (%1), %%xmm0    \n\t"    \
        "phminposuw %%xmm0, %%xmm1    \n\t"    \
        "movd       %%xmm1, %0        \n\t"    \
        : "=r" (result)                        \
        : "r" (vector)                         \
        : "%xmm0", "%xmm1"                     \
    );

#if 0
#define PHMINPOSUW(vector, result) { unsigned short __i, pos=0, smallest=USHRT_MAX; for (__i=0; __i<8; ++__i) if (vector[__i] < smallest) {smallest = vector[__i]; pos = __i;} result = smallest + (pos << 16); }
#endif
 
static inline void pqsse41_update_key_opt(pqsse41root *pq, pqsse41node *n) {
    pqsse41treenode *tree = pq->tree;
    pqsse41treenode *node, *lastnode;
    int fullindex = n->index;
    int i, j;
    unsigned short key = n->key >> 4;
    unsigned int result;

    j = fullindex & 7;
    fullindex >>= 3;
    i = pq->leaves_start + fullindex;

    node = &tree[i];
    node->key[j] = key;
    node->index[j] = n->index;
    lastnode = node;

    switch (pq->levels) {
    case 5:
        PHMINPOSUW(node->key, result);
        j = fullindex & 7;
        fullindex >>= 3;
        i = 73 + fullindex;
        node = &tree[i];
        node->key[j] = (unsigned short) result;
        node->index[j] = lastnode->index[result >> 16];
        lastnode = node;
    case 4:
        PHMINPOSUW(node->key, result);
        j = fullindex & 7;
        fullindex >>= 3;
        i = 9 + fullindex;
        node = &tree[i];
        node->key[j] = (unsigned short) result;
        node->index[j] = lastnode->index[result >> 16];
        lastnode = node;
    case 3:
        PHMINPOSUW(node->key, result);
        j = fullindex & 7;
        fullindex >>= 3;
        i = 1 + fullindex;
        node = &tree[i];
        node->key[j] = (unsigned short) result;
        node->index[j] = lastnode->index[result >> 16];
        lastnode = node;
    case 2:
        PHMINPOSUW(node->key, result);
        j = fullindex & 7;
        fullindex >>= 3;
        i = fullindex;
        node = &tree[i];
        node->key[j] = (unsigned short) result;
        node->index[j] = lastnode->index[result >> 16];
    }
    PHMINPOSUW(node->key, result);
    pq->minpos = node->index[result >> 16];
}


/**
 * A macro that returns the requested node from a priority queue.
 * The caller should make sure that the index does not exceed queue size.
 *
 * @param pq the priority queue
 * @param i index of the node to retrieve
 *
 * @raturns a node in the queue
 */
#define pqsse41_getnode(pq, i) &pq->nodes[i]


/**
 * A macro that returns the first (minimum) node of the given priority
 * queue.
 *
 * @param pq the priority queue
 *
 * @raturns the first node
 */
#define pqsse41_getmin(pq) &pq->nodes[pq->minpos]



/* Test functions. */

/**
 * A small test program for the priority queue data structure.
 */
static void pqsse41_check(short size) {
    pqsse41root *pq;
    int i;
    short lastmin;
    unsigned int seed = time(NULL);
    srand(seed);

    pq = pqsse41_create(size);
/*
    printf("\n\n");
    pq_printtree(pq);
    printf("\n");
*/
    printf("PQ: adding keys...\n");
    for (i=0; i<size; ++i) {
        pqsse41node *n;
        unsigned int key = rand();
        key = (unsigned int) (((float) key / (float) RAND_MAX - 0.5F)
	        * ((float) size * 0.5F + 1.0F));
        /* int key = i % 3333; */
        n = pqsse41_getnode(pq, i);
        n->key = key;
        pqsse41_update_key_opt(pq, n);
/*
        printf("%d ", key);
*/
    }
/*
    printf("\n\n");
    pq_printtree(pq);
    printf("\n");
*/
    printf("\nPQ: getting keys...\n");
    lastmin = 0;
    for (i=0; i<size; ++i) {
        pqsse41node *n = pqsse41_getmin(pq);
/*
        printf("min: %d:%d\n", n->index, n->key1);
*/
	    if (n->key < lastmin)
            fprintf(stderr, "Error: retrieved value is smaller than the previous (%d < %d)\n", n->key, lastmin);

	    lastmin = n->key;
        n->key = USHRT_MAX;
        pqsse41_update_key_opt(pq, n);
    }
    pqsse41_free(pq);
}

#ifdef __cplusplus
}
#endif


#endif

