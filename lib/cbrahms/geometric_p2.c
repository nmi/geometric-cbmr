/*
 * geometric_p2.c - Geometric algorithm P2
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <clt.h>

#include "config.h"

#include "search.h"
#include "song.h"
#include "util.h"
#include "pq32.h"
#include "geometric_p2.h"


#define COMPENSATION_FACTOR 2


/**
 * Allocates and initializes a P2 song.
 *
 * @return an empty P2 song
 */
void init_p2_song(p2song *p2s) {
    p2s->s         = NULL;
    p2s->onsets    = NULL;
    p2s->num_notes = 0;
}


/**
 * Frees a P2 song.
 *
 * @param p2s pointer to the song to be freed
 */
void free_p2_song(p2song *p2s) {
    free(p2s->onsets);
    p2s->s         = NULL;
    p2s->onsets    = NULL;
    p2s->num_notes = 0;
}

/**
 * Converts a song to the P2 format.
 *
 * @param s the song to convert
 * @param p2s pointer to target data structure
 */
void song_to_p2(const song *s, p2song *p2s) {
    uint32_t i;
    vector  *notes = s->notes;

    p2s->s         = s;
    p2s->num_notes = s->size;
    p2s->onsets    = (int32_t *) malloc(s->size * sizeof(int32_t));

    /* Copy notes */
    for (i = 0; i < s->size; i++) {
        vector *n = &notes[i];

        if (n->strt >= P2_TIME_LIMIT) {
            CLT_WARNING("Song duration exceeds the limit of %d units",
                        P2_TIME_LIMIT); 
            p2s->num_notes = i;
            break;
        }
        p2s->onsets[i] = (n->strt << 8) + (int32_t) n->ptch;
    }
}


/**
 * Initializes a p2 song collection struct.
 *
 * @return pointer to the data structure.
 */
void *alloc_p2_song_collection(void) {
    p2songcollection *p2sc =
            (p2songcollection *) calloc(1, sizeof(p2songcollection));
    return p2sc;
}


/**
 * Converts a song collection to the p2 format.
 *
 * @param p2sc target p2 song collection
 * @param sc   song collection to convert
 * @param dp   parameters. Not used in this function.
 */
int build_p2_song_collection(p2songcollection *p2sc,
        const songcollection *sc, const dataparameters *dp) {
    uint32_t i;

    /* Parameter dp is unused */
    (void) dp;

    p2sc->song_collection  = sc;
    p2sc->size             = sc->size;

    p2sc->p2_songs         = (p2song *) calloc(sc->size, sizeof(p2song));
    if (p2sc->p2_songs == NULL) {
        fputs("Error in build_p2_song_collection(): "
              "failed to allocate memory for 'p2_songs'", stderr);
        return 0;
    }
    for (i = 0; i < sc->size; i++) {
        song_to_p2(&sc->songs[i], &p2sc->p2_songs[i]);
    }
    return 1;
}

int build_p2_song_collection_untyped(void *p2sc,
        const songcollection *sc, const dataparameters *dp) {
    return build_p2_song_collection((p2songcollection *) p2sc, sc, dp);
}


/**
 * Clears and re-initializes the given p2 song collection.
 *
 * @param p2sc p2 song data
 */
void clear_p2_song_collection(p2songcollection *p2sc) {
    uint32_t i;

    for (i = 0; i < p2sc->size; i++) {
        free_p2_song(&p2sc->p2_songs[i]);
    }
    free(p2sc->p2_songs);
    p2sc->p2_songs        = NULL;
    p2sc->song_collection = NULL;
    p2sc->size            = 0;
}

void clear_p2_song_collection_untyped(void *p2sc) {
    clear_p2_song_collection((p2songcollection *) p2sc);
}


/**
 * Frees the given p2 song collection.
 *
 * @param p2sc p2 song data
 */
void unalloc_p2_song_collection(void *p2sc) {
    clear_p2_song_collection((p2songcollection *) p2sc);
    free(p2sc);
}


/**
 * Search a song collection with scan_song_p2().
 *
 * @param sc         song collection to scan
 * @param pattern    search pattern
 * @param alg        search algorithm ID (see algorithms.h).
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         match set for returning search results
 */
void alg_p2(const songcollection *sc, const song *pattern, int32_t alg,
            const searchparameters *parameters, matchset *ms) {
    const song *pat;
    song       *q_pattern = NULL;
    uint32_t    i;

    /* Parameter alg is unused */
    (void) alg;

    if (parameters->quantization > 0) {
        q_pattern = p2_compensate_quantization(pattern,
                (int) parameters->quantization);
        pat = q_pattern;
    } else {
        pat = pattern;
    }

    for (i = 0; i < sc->size; i++) {
#ifdef DEBUG
        fprintf(stderr, "Scanning song %s\n", sc->songs[i].title);
#endif
        scan_song_p2(&sc->songs[i], pat, pat->size, ms);
    }

    if (q_pattern != NULL) {
        free_song(q_pattern);
        free     (q_pattern);
    }
}


/**
 * Search a song collection with scan_song_p2_points().
 *
 * @param sc         song collection to scan
 * @param pattern    search pattern
 * @param alg        search algorithm ID (see algorithms.h).
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         match set for returning search results
 
 */
void alg_p2_points(const songcollection *sc, const song *pattern, int32_t alg,
                   const searchparameters *parameters, matchset *ms) {
    const song *pat;
    song       *q_pattern = NULL;
    uint32_t    i;

    /* Parameter alg is unused */
    (void) alg;

    if (parameters->quantization > 0) {
        q_pattern = p2_compensate_quantization(pattern,
                (int) parameters->quantization);
        pat = q_pattern;
    } else {
        pat = pattern;
    }

    for (i = 0; i < sc->size; i++) {
#ifdef DEBUG
        fprintf(stderr, "Scanning song %s\n", sc->songs[i].title);
#endif
        scan_song_p2_points(&sc->songs[i], pat, parameters->p2_num_points,
                            parameters->p2_points, ms);
    }

    if (q_pattern != NULL) {
        free_song(q_pattern);
        free     (q_pattern);
    }
}


/**
 * Quantizes the pattern with two or more alternative notes instead of
 * just one. This may be useful with the P2 scanning algorithms (but not with
 * index filters).
 *
 * @param p the pattern to quantize
 * @param q amount of quantization in milliseconds
 *
 * @return a quantized pattern
 */
song *p2_compensate_quantization(const song *p, const int q) {
    vector  *qnotes, *pnotes;
    song    *newp;
    uint32_t i;

    newp = (song *) malloc(sizeof(song));
    if (newp == NULL) {
        fputs("Error in p2_compensate_quantization(): "
              "failed to allocate memory for 'newp'", stderr);
        return NULL;
    }

    memcpy(newp, p, sizeof(song));
    newp->size  = p->size * COMPENSATION_FACTOR;
    newp->notes = (vector *) malloc(newp->size * sizeof(vector));
    if (newp->notes == NULL) {
        fputs("Error in p2_compensate_quantization(): "
              "failed to allocate memory for 'newp->notes'", stderr);
        free(newp);
        return NULL;
    }   

    pnotes = p->notes;
    qnotes = newp->notes;
    for (i = 0; i < p->size; i++) {
        vector *pn = &pnotes[i];
        vector *qn = &qnotes[i*COMPENSATION_FACTOR];

        qn->strt       = pn->strt;
        qn->ptch       = pn->ptch;
        qn->dur        = pn->dur;
        qn->velocity   = pn->velocity;
        qn->instrument = pn->instrument;

        qn             = &qnotes[i*COMPENSATION_FACTOR+1];
        qn->strt       = pn->strt + q;
        qn->ptch       = pn->ptch;
        qn->dur        = pn->dur;
        qn->velocity   = pn->velocity;
        qn->instrument = pn->instrument;

        if (COMPENSATION_FACTOR == 3) {
            qn             = &qnotes[i*COMPENSATION_FACTOR+2];
            qn->strt       = pn->strt + q + q;
            qn->ptch       = pn->ptch;
            qn->dur        = pn->dur;
            qn->velocity   = pn->velocity;
            qn->instrument = pn->instrument;
        }
    }
    lexicographic_sort(newp);
    return newp;
}


/**
 * Scanning phase of geometric algorithm P2. This algorithm is described in
 * Esko Ukkonen, Kjell Lemstrom and Veli Makinen: Sweepline the Music! In
 * Computer Science in Perspective (LNCS 2598), R. Klein, H.-W. Six, L. Wegner
 * (Eds.), pp. 330-342, 2003.
 *
 * Unlike in the article, end of source is not detected by putting
 * (infinity,infinity) to the end of source, but with indexes.
 * Also because the source format consists of variable length chords
 * the implementation is fairly complex and differs somewhat from
 * the pseudocode. Consult the article for details.
 *
 * @param s      song to scan
 * @param p      search pattern
 * @param errors allowed number of errors (missing notes) in a match
 * @param ms     pointer to a structure where the results will be stored
 *
 * @return 1 when successful, 0 otherwise
 */
int scan_song_p2(const song *s, const song *p, const uint32_t errors,
                 matchset *ms) {
    vector   *pattern = p->notes;
    vector   *text    = s->notes;
    pq32      pq;

    uint32_t  i, num_loops;
    uint32_t  c, maxcount;
    uint32_t  min_pattern_size;
    int32_t   previous_key;

#if 0
    /* Keep all matched notes for the best match here. This is not
     * actually used, but serves as an example of what information
     * can be extracted. */
    uint32_t matchednotes[pattern_size];
#endif
#ifdef P2_CALCULATE_COMMON_DURATION
    float    common_duration = 0;
    uint32_t pattern_duration = 0;
#endif

    if ((p->size == 0) || (s->size == 0)) return 0;

    if (errors >= p->size) min_pattern_size = 0;
    else                   min_pattern_size = p->size - errors;

    /* Initialize the priority queue */
    if (pq32_init(&pq, p->size)) {
        fputs("Error in scan_song_p2(): "
              "failed to initialize priority queue", stderr);
        return 0;
    }
    for (i = 0; i < p->size; i++) {
        pq32node *node;

        /* Add translation vectors to the priority queue */
        node       = pq32_getnode(&pq, i);
        node->key1 = (((int32_t) text[0].strt - (int32_t) pattern[i].strt) << 8)
                     + (int32_t) text[0].ptch - (int32_t) pattern[i].ptch;
        node->key2 = 0;
        /* printf("key: %d\n", node->key1); */
        pq32_update(&pq, node);
#ifdef P2_CALCULATE_COMMON_DURATION
        pattern_duration += pattern[i].dur;
#endif
    }

    c            = 0;
    maxcount     = 1;
    previous_key = INT_MIN;
    
    /* Loop as long as we can take items away from the priority queue.
     * p->size items are added before,
     * p->size * s->size - p->size items are added in the loop. */
    num_loops = s->size * p->size;

    for (i = 0; i < num_loops; i++) {
        pq32node *min = pq32_getmin(&pq);
        uint32_t  patternpos = min->index;
        uint32_t  textpos = (uint32_t) min->key2;
        vector   *textnote = &text[textpos];
        vector   *patternnote = &pattern[patternpos];

        if (previous_key == min->key1) {
            /* Another matching note */
#if 0
            /* Store matched note index */
            matchednotes[c] = textpos;
#endif
            c++;
            if (c > maxcount) maxcount = c;
        } else {
            /* end of a matching section */
            if ((c == maxcount) && (c >= min_pattern_size)) {
                int8_t transposition = (int8_t)
                        ((previous_key & 0xFF) - NOTE_PITCHES);
                int32_t start = (previous_key >> 8) + pattern[0].strt;
                        /* + (((unsigned char) transposition) >> 7); */
                int32_t end   = (previous_key >> 8) +
                        pattern[p->size - 1].strt +
                        pattern[p->size - 1].dur;
                        /* + (((unsigned char) transposition) >> 7); */
#ifdef P2_CALCULATE_COMMON_DURATION                   
                float similarity = common_duration / pattern_duration;
#else
#  ifdef P2_NORMALIZE_SIMILARITY
                float similarity = ((float) c + 1.0F) /
                                   ((float) MIN2(p->size, s->size));
#  else
                float similarity = ((float) c + 1.0F) /
                                   ((float) p->size);
#  endif
#endif
                insert_match(ms, s->id, start, end, transposition, similarity);
            }
            previous_key    = min->key1;
            c               = 0;
#ifdef P2_CALCULATE_COMMON_DURATION                   
            common_duration = 0.0F;
#endif
#if 0
            matchednotes[0] = textpos;
#endif
        }

#ifdef P2_CALCULATE_COMMON_DURATION
        /* Count common duration of matching notes in pattern and text. */
        if (patternnote->dur < textnote->dur)
             common_duration += patternnote->dur;
        else common_duration += textnote->dur;
#endif

        /* Update q pointer: move to next position in the text
         * (from the current position pointed by q). */
        if (textpos < s->size - 1) {
            /* The current pointer is not at the end of source:
             * move to the next note */
            min->key2++;
            textnote = &text[min->key2];

            /* Update the vector in the priority queue. */
            min->key1 = (((int32_t) textnote->strt -
                          (int32_t) patternnote->strt) << 8) +
                          (int32_t) textnote->ptch -
                          (int32_t) patternnote->ptch;
            pq32_update(&pq, min);
        } else {
            /* Current pointer is at the end of the text;
             * remove the difference vector from the priority queue. */
            min->key1 = INT_MAX;
            pq32_update(&pq, min);
        }
    }

    pq32_free(&pq);
    return 1;
}


/**
 * Counts the number of matching notes for a given pattern and data
 * position.
 *
 * @param s          a song
 * @param songpos    position in the song
 * @param p          search pattern
 * @param patternpos position in the pattern that aligns with songpos
 * @param ms         structure where the match information is stored to
 *
 * @return match information item
 */
match *alignment_check_p2(const song *s, uint32_t songpos,
                          const song *p, uint32_t patternpos, matchset *ms) {

    vector *pnotes = p->notes;
    vector *snotes = s->notes;
    match  *m;

    int32_t  i, j, count;
    int32_t  p0, v0;
    int32_t  mstart, mend;
    float    msimilarity;
    int8_t   mtransposition;

    if ((songpos >= s->size) || (patternpos >= p->size)) return NULL;

    /* Scan the end */
    count = 1;
    i     = (int32_t) patternpos + 1;
    j     = (int32_t) songpos;

    p0    = ((int32_t) pnotes[patternpos].strt << 8) +
             (int32_t) pnotes[patternpos].ptch;
    v0    = ((int32_t) snotes[songpos].strt << 8) +
             (int32_t) snotes[songpos].ptch;

    for (; i < (int32_t) p->size; i++) {
        int32_t pi = ((int32_t) pnotes[i].strt << 8) +
                      (int32_t) pnotes[i].ptch - p0;
        int32_t vi = INT_MIN;

        /* Skip over notes that are not in the pattern. */
        do {
            j++;
            if (j >= (int32_t) s->size) break;
            vi = ((int32_t) snotes[j].strt << 8) +
                  (int32_t) snotes[j].ptch - v0;
        } while (vi < pi);

        /* Increase counter if there is a matching note */
        if (vi == pi) count++;
        else j--;
    }

    /* Scan the beginning */
    i = (int32_t) patternpos - 1;
    j = (int32_t) songpos;
    for (; i >= 0; i--) {
        int32_t pi = p0 - ((int32_t) pnotes[i].strt << 8) -
                           (int32_t) pnotes[i].ptch;
        int32_t vi = INT_MIN;
        /* Skip over notes that are not in the pattern. */
        do {
            j--;
            if (j < 0) break;
            vi = v0 - ((int32_t) snotes[j].strt << 8) -
                       (int32_t) snotes[j].ptch;
        } while (vi < pi);

        /* Increase counter if there is a matching note */
        if (vi == pi) count++;
        else j++;
    }

    mstart = snotes[songpos].strt - pnotes[patternpos].strt;
    mend   = mstart + pnotes[p->size-1].strt + pnotes[p->size-1].dur;
    mtransposition = (int8_t) (snotes[songpos].ptch - pnotes[patternpos].ptch);
    msimilarity = ((float) count) / ((float) p->size);
    m = insert_match(ms, s->id, mstart, mend, mtransposition, msimilarity);

    /* Find out matching note positions if they are requested */
    if ((m != NULL) && (m->num_notes > 0) && (m->notes != NULL)) {
        m->notes[0] = j;
        p0 = ((int32_t) pnotes[0].strt << 8) +
              (int32_t) pnotes[0].ptch;
        v0 = ((int32_t) snotes[j].strt << 8) +
              (int32_t) snotes[j].ptch;

        /*if (m->num_notes > p->size) m->num_notes = p->size;*/
        for (i = 1; i < (int32_t) m->num_notes; i++) {
            int32_t vi;
            int32_t pi = ((int32_t) pnotes[i].strt << 8) +
                          (int32_t) pnotes[i].ptch - p0;

            /* Skip over notes that are not in the pattern. */
            do {
                j++;
                if (j >= (int32_t) s->size) return m;
                vi = ((int32_t) snotes[j].strt << 8) +
                      (int32_t) snotes[j].ptch - v0;
            } while (vi < pi);

            /* Is there a matching note? If not, exit. */
            if (vi != pi) m->notes[i] = -1;
            else m->notes[i] = j;
        }
    }
    return m;
}


/**
 * Scanning phase of geometric algorithm P2' when only a single point is
 * specified and P1 cannot be used as a first step.
 *
 * @param s          song to scan
 * @param p          search pattern
 * @param errors     allowed number of errors (missing notes) in a match
 * @param ms         pointer to a structure where the results will be stored
 * @param num_points number of fixed pattern points
 * @param points     array of pattern note positions, specifying the group
 *                   of fixed points
 *
 * @return 1 when successful, 0 otherwise
 */
static int32_t scan_song_p2_1_point(const song *s, const song *p,
                                    const uint32_t errors, matchset *ms,
                                    uint32_t num_points, const uint32_t *points) {
    vector   *pattern = p->notes;
    vector   *text    = s->notes;
    uint8_t  *point_found;
    uint32_t *q;
    pq32      pq;

    int32_t  num_loops, i;
    int32_t  pattern_end;
    int32_t  previous_key;
    uint32_t c, maxcount;
    uint32_t min_pattern_size;

#if 0
    /* Keep all matched notes for the best match here. This is not
     * actually used, but serves as an example of what information
     * can be extracted. */
    uint32_t matchednotes[pattern_size];
#endif
#ifdef P2_CALCULATE_COMMON_DURATION
    float    common_duration = 0;
    uint32_t pattern_duration = 0;
#endif

    if ((p->size == 0) || (s->size == 0)) return 0;
    if (errors >= p->size) {
        min_pattern_size = 0;
    } else {
        min_pattern_size = p->size - errors;
    }

    point_found = (uint8_t *)  calloc(p->size,  sizeof(uint8_t));
    if (point_found == NULL) {
        fputs("Error in scan_song_p2_1_point(): "
              "failed to allocate temporary array 'point_found'", stderr);
        return 0;
    }

    q = (uint32_t *) malloc(p->size * sizeof(uint32_t));
    if (q == NULL) {
        fputs("Error in scan_song_p2_1_point(): "
              "failed to allocate temporary array 'q'", stderr);
        free(point_found);
        return 0;
    }
 
    /* Initialize the priority queue */
    if (pq32_init(&pq, p->size)) {
        fputs("Error in scan_song_p2_1_point(): "
              "failed to initialize priority queue", stderr);
        free(point_found);
        free(q);
        return 0;
    }

    pattern_end = p->notes[p->size-1].strt;

    /* Initialize q array: all point to the first note of the source */
    for (i = 0; i < (int32_t) p->size; i++) {
        pq32node *node;
        q[i] = 0;

        /* Add translation vectors to the priority queue */
        node       = pq32_getnode(&pq, i);
        node->key1 = (((int32_t) text[0].strt -
                       (int32_t) pattern[i].strt + pattern_end) << 8) +
                       (int32_t) text[0].ptch - (int32_t) pattern[i].ptch;
        /* printf("key: %d\n", node->key1); */
        pq32_update(&pq, node);

#ifdef P2_CALCULATE_COMMON_DURATION
        pattern_duration += pattern[i].dur;
#endif
    }

    c            = 0;
    maxcount     = 1;
    previous_key = INT_MIN;
    
    /* Loop as long as we can take items away from the priority queue.
     * p->size items are added before,
     * p->size * s->size - p->size items are added in the loop. */
    num_loops = (int32_t) (s->size * p->size);

    /* Get the smallest translation vector.
     * Equal difference vectors come out of priority queue in min_key order. */
    for (i = 0; i < num_loops; i++) {
        pq32node *min         = pq32_getmin(&pq);
        uint32_t  patternpos  = min->index;
        uint32_t  textpos     = q[patternpos];
        vector   *textnote    = &text[textpos];
        vector   *patternnote = &pattern[patternpos];

        if (previous_key == min->key1) {
            /* Another matching note */

#if 0
            /* Store matched note index */
            matchednotes[c] = textpos;
#endif
            c++;
            if (c > maxcount) maxcount = c;
            point_found[patternpos] = 1;
        } else {
            int32_t j;
            /* end of a matching section */
            if ((c == maxcount) && (c >= min_pattern_size)) {
                j = 0;
                for (; j < (int32_t) num_points; j++) {
                    uint32_t pos = points[j];
                    if (!point_found[pos]) {
                        j = -1;
                        break;
                    }
                }
                if (j >= 0) { 
                    int32_t start    = (previous_key >> 8) +
                                       pattern[0].strt - pattern_end;
                    int32_t end      = (previous_key >> 8) +
                                       pattern[p->size - 1].strt +
                                       pattern[p->size - 1].dur - pattern_end;
                    int8_t transposition = (int8_t)
                            ((previous_key & 0xFF) - NOTE_PITCHES);
#ifdef P2_CALCULATE_COMMON_DURATION                   
                    float similarity = common_duration / pattern_duration;
#else
#  ifdef P2_NORMALIZE_SIMILARITY
                    float similarity = ((float) c + 1.0F) /
                                       ((float) MIN2(p->size, s->size));
#  else
                    float similarity = ((float) c + 1.0F) /
                                       ((float) p->size);
#  endif
#endif
                    insert_match(ms, s->id, start, end,
                                 transposition, similarity);
                }
            }
            previous_key    = min->key1;
            c               = 0;
#ifdef P2_CALCULATE_COMMON_DURATION                   
            common_duration = 0.0F;
#endif
            for (j = 0; j < (int32_t) num_points; ++j) {
                point_found[points[j]] = 0;
            }
#if 0
            matchednotes[0] = textpos;
#endif
        }

#ifdef P2_CALCULATE_COMMON_DURATION
        /* Count common duration of matching notes in pattern and text. */
        if (patternnote->dur < textnote->dur)
             common_duration += patternnote->dur;
        else common_duration += textnote->dur;
#endif

        /* Update q pointer: move to next position in the text
         * (from the current position pointed by q). */
        if (textpos < s->size - 1) {
            /* The current pointer is not at the end of source:
             * move to the next note */
            q[patternpos]++;
            textnote = &text[q[patternpos]];

            /* Update the vector in the priority queue. */
            min->key1 = (((int32_t) textnote->strt -
                          (int32_t) patternnote->strt + pattern_end) << 8) +
                          (int32_t) textnote->ptch -
                          (int32_t) patternnote->ptch;
            pq32_update(&pq, min);
        } else {
            /* Current pointer is at the end of the text;
             * remove the difference vector from the priority queue. */
            min->key1 = INT_MAX;
            pq32_update(&pq, min);
        }
    }

    pq32_free(&pq);
    free(point_found);
    free(q);
    return 1;
}



/**
 * Geometric P2': P2 with fixed points that must be found in the matches.
 * The algorithm calls normal P2 if less than two points are given.
 * Otherwise it first runs P1 and evaluates matches with a P2-based checking
 * function.
 *
 * @param s          song to scan
 * @param p          search pattern
 * @param ms         pointer to a structure where the results will be stored
 * @param num_points number of fixed pattern points
 * @param points     an array of pattern note positions, specifying
 *                   the group of fixed points
 *
 * @return 1 when successful, 0 otherwise
 */
int scan_song_p2_points(const song *s, const song *p,
        uint32_t num_points, const uint32_t *points, matchset *ms) {
    vector   *pattern, *text;
    uint32_t *q;

    uint32_t  i, j, pattern_size, song_size;
    int32_t   v0, p0;

    if ((p->size < 2) || (s->size < 2)) return 0;
    if (num_points < 2)
        return scan_song_p2_1_point(s, p, p->size, ms, num_points, points);

    pattern_size = num_points;
    song_size    = s->size;
    pattern      = p->notes;
    text         = s->notes;

    /* q is an array that points to the last checked note for each
     * pattern position */
    q = (uint32_t *) calloc(pattern_size, sizeof(uint32_t));
    if (q == NULL) return 0;

    p0 = ((int32_t) pattern[points[0]].strt << 8) +
          (int32_t) pattern[points[0]].ptch - NOTE_PITCHES;

    /* Scan all notes */
    for (i = 0; i < song_size - pattern_size + 1; ++i) {
        v0 = ((int32_t) text[i].strt << 8) + (int32_t) text[i].ptch - p0;
        q[0] = i + 1;

        /* Start finding points of the transposed pattern starting from
         * this source position and do it until a match cannot be found. */
        for (j = 1; j < pattern_size; ++j) {
            int32_t v;
            int32_t pj = ((int32_t) pattern[points[j]].strt << 8) +
                          (int32_t) pattern[points[j]].ptch - NOTE_PITCHES;
            if (q[j] < q[j-1]) q[j] = q[j-1];
            if (q[j] == song_size) {
                free(q);
                return 0;
            }
            /* Move q over the notes that can't be part of a match for any
             * remaining pattern position. */
            do {
                v = ((int32_t) text[q[j]].strt << 8) +
                     (int32_t) text[q[j]].ptch - pj;
                q[j]++;
            } while ((v < v0) && (q[j] < song_size));
            /* After the loop we are at either a matching note or bigger,
             * i.e. there is no matching note. */
            if (v != v0) {
                q[j]--;
                break;
            }
        }

        /* Check matching positions with P2 */
        if (j == pattern_size) {
            alignment_check_p2(s, i, p, points[0], ms);
        }
    }
    free(q);
    return 1;
}

