/*
 * geometric_p3.h - Structures and external function declarations for
 *                  Geometric algorithm P3
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __GEOMETRIC_P3_H__
#define __GEOMETRIC_P3_H__

#include "config.h"

#include "search.h"
#include "song.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Song duration limit due to the priority queue key coding used in this
 * implementation */
#define P3_TIME_LIMIT (1 << 23)

#define P3_POSITION_PS_TS 0
#define P3_POSITION_PS_TE 1
#define P3_POSITION_PE_TE 2
#define P3_POSITION_PE_TS 3
#define P3_POSITION_ONSET_TOP 4
#define P3_POSITION_ONSET_START 5
#define P3_POSITION_ONSET_END 6

/**
 * A structure for items in the vertical translation table. It stores value,
 * slope and previous x for each vertical translation (y).
 */
typedef struct {
    int32_t slope;
    int32_t value;
    int32_t prev_x;
} VerticalTranslationTableItem;

typedef struct {
    double slope;
    double value;
    double prev_slope;
    int32_t prev_x;

    /*PAD_STRUCT(4)*/
} FVerticalTranslationTableItem;


/**
 * Startpoints (strt,ptch) and endpoints (strt+dur,ptch) are called
 * turning points. They are precalculated and stored in separate arrays of 
 * this struct. This adds to space requirements but makes the algorithm
 * simpler. (startpoints could be traversed from source data but endpoint
 * order must be stored somehow anyway.)
 */
typedef struct {
    int32_t  x;
    int32_t  y;
    int32_t  dur;
    uint32_t note;
} TurningPoint;


/**
 * A translation vector structure. Four of these are allocated for each
 * note in the pattern and the vectors are updated when scanning through
 * the song.
 */
typedef struct {
    /* Pointer to TP array; which TP is associated with this translation
     * vector. */
    TurningPoint *target;
    vector       *pattern;

    uint32_t target_index;
    uint32_t pattern_index;
    uint32_t target_size;
    uint32_t pattern_size;
    int32_t  x;
    int32_t  y;
    int32_t  pdur;
    int32_t  tdur;
    int32_t  position;

    /*PAD_STRUCT_64(4)*/
} TranslationVector;


/**
 * Structure for storing a song in P3 format.
 */
typedef struct {
    const song   *s;
    TurningPoint *note_starts;
    TurningPoint *note_ends;
    int32_t      *rest_starts;
    int32_t      *rest_ends;

    uint32_t      num_notes;
    uint32_t      num_rests;
} p3song;


/**
 * Structure for storing a song collection in P3 format.
 */
typedef struct {
    p3song               *p3_songs;
    const songcollection *song_collection;

    uint32_t size;
    /*PAD_STRUCT(4)*/
} p3songcollection;


/* External function declarations */


void alg_p3(const songcollection *sc, const song *pattern, int32_t alg,
            const searchparameters *parameters, matchset *ms);

int32_t scan_song_p3(const song *s, const song *p, matchset *ms);

int32_t scan_p3(const p3song *p3s, const song *pattern,
                const searchparameters *parameters, matchset *ms);

int32_t compare_turningpoints(const void *aa, const void *bb);


void *alloc_p3_song_collection(void);

int build_p3_song_collection(p3songcollection *p3_songcollection,
        const songcollection *sc, const dataparameters *dp);
int build_p3_song_collection_untyped(void *p3_songcollection,
        const songcollection *sc, const dataparameters *dp);

void clear_p3_song_collection(p3songcollection *p3_songcollection);
void clear_p3_song_collection_untyped(void *p3_songcollection);

void unalloc_p3_song_collection(void *p3_songcollection);


void init_p3_song(p3song *p3s);
void free_p3_song(p3song *p3s);
void song_to_p3(const song *s, p3song *p3s);


#ifdef __cplusplus
}
#endif

#endif

