/*
 * pq32.h - A priority queue implementation for the geometric P2 and P3
 *          algorithms.
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __PQ_32_H__
#define __PQ_32_H__

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * A priority queue node.
 */
typedef struct {
    uint32_t  index;
    int32_t   key1;
    int32_t   key2;
    int32_t   value;
} pq32node;


/**
 * Priority queue root.
 */
typedef struct {
    pq32node **tree;
    pq32node  *nodes;
    uint32_t   size;
    uint32_t   nodecount;
    uint32_t   levels;
    /*PAD_STRUCT_64(4)*/
} pq32;


int pq32_init(pq32 *pq, uint32_t size);

void pq32_free(pq32 *pq);

void pq32_test(uint32_t size,
               void (* pq32_update_key) (pq32 *pq, pq32node *n),
               int printtree);

/*
 * Function definitions.
 * These are are here beceuse most of the functions should be inlined for
 * maximum performance and currently using the "static inline" directive
 * seems to be the most portable way to do it.
 */

/**
 * Updates the priority queue after a change in node's key values.
 *
 * This function uses both key1 and key2 to order the nodes.
 * If two nodes are equal according to both of these keys, the node stored
 * at a smaller index in the node array comes first.
 *
 * @param pq the priority queue
 * @param n  changed node
 */
static inline void pq32_update_x2(pq32 *pq, pq32node *n) {
    pq32node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        uint32_t pair;
        /* If index is odd then its pair is the previous even entry, otherwise
           the pair is the next odd entry. */
        if (i & 1) {
            pq32node *n2;
            pair = i - 1;
            n2 = tree[pair];
            i >>= 1;
            if ((n->key1 < n2->key1) || ((n->key1 == n2->key1) &&
                                         (n->key2 < n2->key2))) {
                tree[i] = n;
            } else {
                tree[i] = n2;
                n = n2;
            }
        } else {
            pq32node *n2;
            pair = i + 1;
            n2 = tree[pair];
            i >>= 1;
            /* Sort the nodes so that one with the smaller index comes first
               if other keys are equal; hence the "<=" in this branch. */
            if ((n->key1 < n2->key1) || ((n->key1 == n2->key1) &&
                    (n->key2 <= n2->key2))) {
                tree[i] = n;
            } else {
                tree[i] = n2;
                n = n2;
            }
        }
    }
}


/**
 * Updates the priority queue after a change in node's key value.
 * This version only checks the primary key when updating. Nodes with
 * equal keys are not returned in any particular order. See
 * pq32_update_ascending and pq32_update_descending for
 * versions that use the node index as secondary ordering criteria.
 *
 * Note: This function is called often in the inner loop of the algorithms,
 * so optimizing it further can result in a significant speedup.
 *
 * @param pq the priority queue
 * @param n  changed node
 */
static inline void pq32_update(pq32 *pq, pq32node *n) {
    pq32node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        pq32node *n2;
        /* If index is odd then its pair is the previous even entry,
         * otherwise the pair is the next odd entry. */
        n2 = tree[i ^ 1];
        if (n->key1 >= n2->key1) n = n2;
        i >>= 1;
        tree[i] = n;
    }
}


/**
 * Updates the priority queue after a change in node's key value.
 * The only difference to pq32_update is that nodes with lesser
 * index are returned first if the keys are equal.
 *
 * Note: This function is called often in the inner loop of the algorithms,
 * so optimizing it further can result in a significant speedup.
 *
 * @param pq the priority queue
 * @param n  changed node
 */
static inline void pq32_update_ascending(pq32 *pq, pq32node *n) {
    pq32node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        pq32node *n2;
        n2 = tree[i ^ 1];
        if (n->key1 > n2->key1 - (int32_t) (i & 1)) n = n2;
        i >>= 1;
        tree[i] = n;
    }
}

/**
 * Updates the priority queue after a change in node's key value.
 * The only difference to pq32_update is that nodes with greater
 * index are returned first if the keys are equal.
 *
 * Note: This function is called often in the inner loop of the algorithms,
 * so optimizing it further can result in a significant speedup.
 *
 * @param pq the priority queue
 * @param n  changed node
 */
static inline void pq32_update_descending(pq32 *pq, pq32node *n) {
    pq32node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        pq32node *n2;
        i = i ^ 1;
        n2 = tree[i];
        if (n->key1 > n2->key1 - (int32_t) (i & 1)) n = n2;
        i >>= 1;
        tree[i] = n;
    }
}


/**
 * Updates the priority queue after a change in node's key value.
 * Unrolled version. Seems to be slightly faster on current CPUs
 * (Intel Core 2 and later).
 *
 * @param pq the priority queue
 * @param n  changed node
 */
static inline void pq32_update_opt(pq32 *pq, pq32node *n) {
    pq32node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 7) {
        pq32node *n2;
        /* If index is odd then its pair is the previous even entry, otherwise
           the pair is the next odd entry. */
        n2 = tree[i ^ 1];
        if (n->key1 >= n2->key1) n = n2;
        i >>= 1;
        tree[i] = n;

        n2 = tree[i ^ 1];
        if (n->key1 >= n2->key1) n = n2;
        i >>= 1;
        tree[i] = n;

        n2 = tree[i ^ 1];
        if (n->key1 >= n2->key1) n = n2;
        i >>= 1;
        tree[i] = n;
    }
    while (i > 1) {
        pq32node *n2;
        /* If index is odd then its pair is the previous even entry, otherwise
           the pair is the next odd entry. */
        n2 = tree[i ^ 1];
        if (n->key1 >= n2->key1) n = n2;
        i >>= 1;
        tree[i] = n;
    }
}

#define PQ32_UPDATE_DECLARE_VARIABLES(levels)  \
    pq32node **tree = pq->tree;                \
    pq32node *n2;                              \
    uint32_t i = pq->nodecount + n->index;     \

#define PQ32_UPDATE_LEVEL                 \
        n2 = tree[i ^ 1];                 \
        if (n->key1 >= n2->key1) n = n2;  \
        i >>= 1;                          \
        tree[i] = n

static inline void pq32_update_1(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(1);
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_2(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(2);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_3(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(3);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_4(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(4);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_5(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(5);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_6(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(6);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_7(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(7);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static inline void pq32_update_8(pq32 *pq, pq32node *n) {
    PQ32_UPDATE_DECLARE_VARIABLES(8);
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
    PQ32_UPDATE_LEVEL;
}

static void (* const PQ32_UPDATE_FUNCTIONS[])(
        pq32 *pq, pq32node *n) = {
    NULL,
    pq32_update_1,
    pq32_update_2,
    pq32_update_3,
    pq32_update_4,
    pq32_update_5,
    pq32_update_6,
    pq32_update_7,
    pq32_update_8,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt,
    pq32_update_opt
};

#define pq32_select_optimal_update(pq) PQ32_UPDATE_FUNCTIONS[(pq)->levels];


/**
 * Updates the priority queue after a change in node's key value.
 * This is an unoptimized version for reference. Use one of the other
 * functions instead.
 *
 * @param pq the priority queue
 * @param n changed node
 */
static inline void pq32_update_unopt(pq32 *pq, pq32node *n) {
    pq32node **tree = pq->tree;

    uint32_t i = pq->nodecount + n->index;

    while (i > 1) {
        uint32_t pair;
        /* If index is odd then its pair is the previous even entry, otherwise
           the pair is the next odd entry. */
        if (i & 1) {
            pq32node *n2;
            pair = i - 1;
            n2 = tree[pair];
            i >>= 1;
            if (n->key1 < n2->key1) {
                tree[i] = n;
            } else {
                tree[i] = n2;
                n = n2;
            }
        } else {
            pq32node *n2;
            pair = i + 1;
            n2 = tree[pair];
            i >>= 1;
            /* Sort the nodes so that one with the smaller index comes first
               if other keys are equal; hence the "<=" in this branch. */
            if (n->key1 <= n2->key1) {
                tree[i] = n;
            } else {
                tree[i] = n2;
                n = n2;
            }
        }
    }
}


/**
 * A macro that returns the requested node from a priority queue.
 * The caller should make sure that the index does not exceed queue size.
 *
 * @param pq the priority queue
 * @param i index of the node to retrieve
 *
 * @raturns a node in the queue
 */
#define pq32_getnode(pq, i) (&(pq)->nodes[i])


/**
 * A macro that returns the first (minimum) node of the given priority
 * queue.
 *
 * @param pq the priority queue
 *
 * @raturns the first node
 */
#define pq32_getmin(pq) ((pq)->tree[1])



#ifdef __cplusplus
}
#endif


#endif

