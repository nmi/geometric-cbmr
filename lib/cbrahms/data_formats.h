/*
 * data_formats.h - Constants for data formats
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __DATA_FORMATS_H__
#define __DATA_FORMATS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/* Index and data format types */

#define NUM_DATA_FORMATS 4

#define DATA_NONE 0
#define DATA_P2 1
#define DATA_P3 2
#define DATA_VINDEX 3
#define DATA_MSM 4


/**
 * Structure for data format parameters
 */
typedef struct {
    uint32_t c_window;
    int avindex_vector_max_width;
    int avindex_vector_max_height;
} dataparameters;


#ifdef __cplusplus
}
#endif

#endif

