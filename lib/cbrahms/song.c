/*
 * song.c - Functions for handling patterns and song collections
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

/* For getline */
#ifndef _GNU_SOURCE
    #define _GNU_SOURCE 1
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#include "clt.h"

#include "config.h"
#include "algorithms.h"
#include "data.h"
#include "song.h"
#include "util.h"


/**
 * Frees memory used by a song.
 *
 * @param s the song to free
 */
void free_song(song *s) {
    if (s->notes != NULL) {
        free(s->notes);
        s->notes = NULL;
    }
    if (s->title != NULL) {
        free(s->title);
        s->title = NULL;
    }
}

/**
 * Initializes a song.
 *
 * @param s        song to initialize
 * @param id       song id in the song collection
 * @param title    song title string
 * @param maxnotes maximum number of notes in the song
 */
void init_song(song *s, uint32_t id, const char *title, uint32_t maxnotes) {
    s->id = id;
    if (title != NULL) {
        s->title = (char *) malloc((strlen(title) + 1) * sizeof(char));
        strcpy(s->title, title);
    } else s->title = NULL;
    s->notes = (vector *) malloc(maxnotes * sizeof(vector));
    s->size  = 0;
    s->start = 0;
    s->end   = 0;
    s->total_note_duration = 0;
    memset(&s->instruments,  0, sizeof(instrumentdata));
    memset(&s->control,      0, sizeof(mididata));
    memset(&s->tempo,        0, sizeof(mididata));
}


/**
 * Frees memory used by a song collection.
 *
 * @param sc the song collection to free
 */
void free_song_collection(songcollection *sc) {
    uint32_t i;
    free_song_collection_data(sc);
    if (sc->songs != NULL) {
        for (i = 0; i < sc->size; i++) {
            free_song(&sc->songs[i]);
        }
        free(sc->songs);
    }
}



/**
 * Allocates memory for a song collection.
 *
 * @param sc the song collection to initialize
 * @param maxsongs maximum number of songs
 *
 * @return 1 if successful, 0 otherwise
 */
int init_song_collection(songcollection *sc, uint32_t maxsongs) {
    uint32_t i;
    if (maxsongs > 0) {
        sc->songs = (song *) calloc(maxsongs, sizeof(song));
    } else {
        sc->songs = NULL;
    }
    sc->size          = 0;
    sc->num_notes     = 0;
    sc->max_song_size = 0;
    for (i = 0; i <= NUM_DATA_FORMATS; i++) sc->data[i] = NULL;
    if (sc->songs == NULL) return 0;
    else return 1;
}


/**
 * Reads songs from a text file. After this, consider using function
 * preprocess_songs on the song collection to sort and quantize the data.
 *
 * Songfile format:
 *   line 1: <number of songs in the file><\n>
 *   n (>1): <name of a song><\n>
 *      n+1: <number of notes in the song><\n>
 *      n+2: note in format: <MIDI pitch (0..127)> <start time> <end time><\n>
 *      n+3: note
 *      ...
 *
 * Example: The beginning of a file with 10 songs and with the first song
 *          having 150 notes
 * 10
 * testsong-1
 * 150
 * 53 0 625
 * 65 625 1874
 * 69 625 1562
 * ...
 *
 *
 * @param songfile name of the file to read
 * @param songcollection the collection to store data to
 *
 * @return the number of songs read
 */
uint32_t read_songfile(const char *songfile, songcollection *sc) {
    char    *buffer, *line, *endptr;
    size_t   filesize;
    uint32_t i, size, numofsongs;

    buffer = (char *) read_file(songfile, 2, 1, &filesize);
    if (buffer == NULL) return 0;
    if (filesize > 0) filesize--;

    line = buffer;
    numofsongs = (uint32_t) MAX2(0, strtol(line, &endptr, 10));
    line = endptr + 1;

    if (!init_song_collection(sc, numofsongs)) {
        fputs("ERROR in read_songfile(): failed to allocate memory\n", stderr);
        free(buffer);
        return 0;
    }

    for (i = 0; i < numofsongs; i++) {
        song *s = &sc->songs[sc->size];

        /* Read the song title */
        if (line >= buffer + filesize) break;
        endptr = strpbrk(line, "\n");
        size = (uint32_t) (endptr - line);
        s->title = (char *) malloc((size + 1) * sizeof(char));
        if (s->title == NULL) {
            fputs("ERROR in read_songfile(): failed to allocate memory\n",
                stderr);
            free(buffer);
            return 0;
        }
        strncpy(s->title, line, size);
        s->title[size] = '\0';
        s->id = i;
        line = endptr + 1;

        /* Read the number of notes */
        if (line >= buffer + filesize) break;
        s->size = (uint32_t) MAX2(0, strtol(line, &endptr, 10));
        line = endptr + 1;

        s->start = 0;
        s->end = 0;
        if (s->size <= 0) {
            s->notes = NULL;
        } else {
            uint32_t j;
            s->notes = (vector *) malloc(sizeof(vector) * s->size);
            if (s->notes == NULL) {
                fputs("ERROR in read_songfile(): failed to allocate memory",
                        stderr);
                free(buffer);
                return 0;
            }
            /* Read the notes */
            for (j = 0; j < s->size; j++) {
                char pitch;
                int start, end;

                if (line >= buffer + filesize) break;
                pitch = (char) strtol(line, &endptr, 10);
                line = endptr;
                if (line >= buffer + filesize) break;
                start = (int) strtol(line, &endptr, 10);
                line = endptr;
                if (line >= buffer + filesize) break;
                end = (int) strtol(line, &endptr, 10);
                line = endptr + 1;
                s->notes[j].ptch = pitch;
                s->notes[j].strt = start;
                s->notes[j].dur  = end;
                if (end > s->end) s->end = end;
            }
            s->size = j;
        }
        sc->size++;
    }
    free(buffer);
    return sc->size;
}


/**
 * Checks the given song collection for problematic expressions, such as many
 * notes starting at the same time (or overlapping) at the same pitch. This
 * function also sorts notes if they are not in lexicographic order and
 * optionally quantizes note start times and durations.
 *
 * @param sc          song collection to process
 * @param q           amount of quantization to apply in milliseconds.
 *                    0 or 1 means no quantization, 125 means that
 *                    everything will be quantized to the precision of 1/8
 *                    seconds. Quantization is useful for raw MIDI input
 *                    when using algorithms P1 and P2.
 * @param trim_margin trim the silence in the beginning and end of the songs
 *                    to this duration. Disabled if negative.
 * @param trim_step   size of steps for moving the song when trimming.
 * @param shuffle     if nonzero, the song collection will be shuffled
 *                    (songs reordered randomly)
 * @param notes       song collection size in notes. If this is larger
 *                    than zero, the collection will be cropped to
 *                    the size given.
 */
void preprocess_songs(songcollection *sc, int q, int trim_margin,
                      int trim_step, int shuffle, uint32_t notes) {
    uint32_t i, n;
    if (notes > 0) {
        n = notes;
    } else {
        n = 0;
    }
    if (shuffle) {
        for (i = 0; i < sc->size; i++) {
            uint32_t j = (uint32_t) MAX2(0, lrintf(randf() * ((float)
                                                   (sc->size - 1))));
            song s = sc->songs[i];
            sc->songs[i] = sc->songs[j];
            sc->songs[j] = s;
        }
    }
    for (i = 0; i < sc->size; i++) {
        if (sc->songs[i].size > 0) {
            lexicographic_sort(&sc->songs[i]);
            quantize(&sc->songs[i], q);
            remove_overlap(&sc->songs[i]);
            if (trim_margin >= 0) {
                trim_song(&sc->songs[i], trim_margin, trim_step);
            }
        }
        if (notes > 0) {
            if (sc->songs[i].size >= n) {
                uint32_t s;
                sc->songs[i].size = n;
                i++;
                s = i;
                for (; i < sc->size; i++) {
                    free_song(&sc->songs[i]);
                }
                sc->size = s;
                return;
            } else {
                n -= sc->songs[i].size;
            }
        }
    }
    if ((notes > 0) && (notes != UINT_MAX) && (n > 0))
        fputs("Warning in preprocess_songs(): song collection has less "
              "notes than requested\n", stderr);
}


/**
 * Sorts the notes of a song lexicographically if they are not already in
 * ascending order.
 *
 * @param s a song whose notes may need sorting
 */
void lexicographic_sort(song *s) {
    vector  *n = s->notes;
    uint32_t i;
    int32_t  laststart;
    int8_t   lastpitch;

    if (s->size < 2) return;

    laststart = n[0].strt;
    lastpitch = n[0].ptch;

    /* Check if the notes are already ordered */
    for (i = 1; i < s->size; i++) {
        if ((n[i].strt < laststart) || ((n[i].strt == laststart)
                && (n[i].ptch < lastpitch))) {
            /* Notes are not in order, sort them all */
            qsort(n, s->size, sizeof(vector), compare_notes);
            return;
        }
        laststart = n[i].strt;
        lastpitch = n[i].ptch;
    }
}


/**
 * Compares two notes lexicographically. This is used to preprocess data for
 * the geometric algorithms.
 * 
 * @param aa a note
 * @param bb another note
 * @return -1 if aa comes before bb, 0 if they are the same and 1 if aa comes
 *         after bb
 */
int compare_notes(const void *aa, const void *bb) {
    const vector *a, *b;
    a = (const vector *) aa;
    b = (const vector *) bb;
    if (a->strt < b->strt)
        return -1;
    else if (a->strt > b->strt)
        return 1;
    else {
        if (a->ptch < b->ptch)
            return -1;
        else if (a->ptch > b->ptch)
            return 1;
        else
            return 0;
    }
}


/**
 * Removes notes that start at the same time as another note and shortens
 * overlapping notes: if two notes play at the same pitch level, the first
 * one is shortened to end before the second starts.
 *
 * @param s a song. The note array of this song will be reallocated.
 */
void remove_overlap(song *s) {
    vector  *n = s->notes;
    vector  *newnotes;
    uint32_t i;
    int32_t  laststart, lastpitch;
    int32_t  lastend[128];
    uint32_t lastnote[128];
    uint32_t notes_to_remove = 0;

    if (s->size < 2) return;

    /* Initialize */
    laststart = n[0].strt;
    lastpitch = n[0].ptch;
    for (i = 0; i < 128; i++) {
        lastend[i]  = -1;
        lastnote[i] = 0;
    }
    lastend[lastpitch] = laststart + n[0].dur;

    /* Shorten notes that overlap with later notes and mark completely
       overlapping notes (notes that are played at the same time and
       pitch as the note before them) for removal. */
    for (i = 1; i < s->size; i++) {
        int32_t curstart = n[i].strt;
        int32_t curend   = curstart + n[i].dur;
        int8_t  curpitch = n[i].ptch;

        if ((laststart == curstart) && (lastpitch == curpitch)) {
            notes_to_remove++;

            /* Grow the previous note's duration to cover the removed note */
            if (lastend[curpitch] < n[i].strt + n[i].dur) {
                lastend[curpitch] = n[i].strt + n[i].dur;
                n[lastnote[curpitch]].dur = n[i].dur;
            }
            /* Mark the note for removal */
            n[i].strt = INT_MAX;
        } else {
            /* Shorten overlapping notes */
            if (lastend[curpitch] > curstart) {
                s->notes[lastnote[curpitch]].dur -= lastend[curpitch] -
                    curstart;
            }
            lastnote[curpitch] = i;
            lastend[curpitch]  = curend;
            laststart = curstart;
            lastpitch = curpitch;
            if (n[i].strt == INT_MAX) n[i].strt = INT_MAX - 1;
        }
    }
    if (notes_to_remove > 0) {
        uint32_t counter = 0;
        newnotes = (vector *) malloc(sizeof(vector) *
                (s->size - notes_to_remove));
        for (i = 0; i < s->size; i++) {
            if (n[i].strt != INT_MAX) {
                memcpy(&newnotes[counter], &n[i], sizeof(vector));
                counter++;
            }
        }
        free(n);
        s->notes = newnotes;
        s->size  = counter;
    }
}


void p3_optimize_song_collection(songcollection *sc, int mindur) {
    uint32_t i;
    for (i = 0; i < sc->size; i++) {
        p3_optimize_song(&sc->songs[i], mindur);
    } 
}

/**
 * Makes all notes last at least mindur milliseconds. Combines notes that play
 * at the same pitch and are separated by a gap of less than mindur
 * milliseconds.
 *
 * @param s a song. The note array of this song will be reallocated.
 */
void p3_optimize_song(song *s, int mindur) {
    vector  *n = s->notes;
    vector  *newnotes;
    vector  *lastnote[128];
    int32_t  lastend[128];
    uint32_t notes_to_remove = 0;
    uint32_t i;

    if (s->size < 2) return;

    /* Initialize */
    for (i = 0; i < 128; i++) {
        lastend[i] = -mindur;
        lastnote[i] = NULL;
    }

    for (i = 0; i < s->size; i++) {
        int32_t curstart = n[i].strt;
        int32_t curend   = curstart + n[i].dur;
        int8_t  curpitch = n[i].ptch;

        if (lastnote[curpitch] != NULL) {
            if (lastend[curpitch] + mindur > curstart) {
                lastnote[curpitch]->dur = curend - lastnote[curpitch]->strt;
                n[i].strt = INT_MAX;
                notes_to_remove++;
                lastend[curpitch] = curend;
            } else {
                if (lastnote[curpitch]->dur < mindur) {
                    lastnote[curpitch]->dur = mindur;
                }
                lastnote[curpitch] = &n[i];
                lastend[curpitch] = curend;
            }
        } else {
            lastnote[curpitch] = &n[i];
            lastend[curpitch] = curend;
        }
    }
    for (i = 0; i < 128; i++) {
        if ((lastnote[i] != NULL) && (lastnote[i]->dur < mindur)) {
            lastnote[i]->dur = mindur;
            s->end = MAX2(s->end, lastnote[i]->strt +
                    lastnote[i]->dur);
        }
    }
    fprintf(stderr, "Song %d: notes before: %d, notes after: %d\n",
            s->id, s->size, s->size - notes_to_remove);
    if (notes_to_remove > 0) {
        uint32_t counter = 0;
        newnotes = (vector *) malloc(sizeof(vector) *
                                     (s->size - notes_to_remove));
        for (i = 0; i < s->size; i++) {
            if (n[i].strt != INT_MAX) {
                memcpy(&newnotes[counter], &n[i], sizeof(vector));
                counter++;
            }
        }
        free(n);
        s->notes = newnotes;
        s->size  = counter;
    }
}


void update_song_statistics(song *s) {
    vector  *snotes = s->notes;
    uint32_t i;
    int32_t  dur = 0;
    for (i = 0; i < s->size; i++) {
        dur += snotes[i].dur;
    }
    s->total_note_duration = dur;
}


void print_time_interval_histogram(song *s) {
    vector  *snotes = s->notes;
    int     *ifreq;
    uint32_t i;
    uint32_t max_delta = 10000;
    int32_t  last;

    if (s->size < 2) return;

    ifreq = (int *) calloc(max_delta, sizeof(int));
    if (ifreq == NULL) return;

    last = snotes[1].strt;
    for (i = 0; i < s->size; i++) {
        uint32_t d = (uint32_t) MAX2(0, snotes[i].strt - last);
        if ((d > 0) && (d < max_delta)) ifreq[d]++;
        last = snotes[i].strt;
    }
    for (i = 0; i < max_delta; i++) {
        if (ifreq[i] > 0) printf("%5d : %5d\n", i, ifreq[i]);
    }
    free(ifreq);
}

/**
 * Quantizes the note start times and durations to the closest multiple
 * of the given time interval.
 *
 * @param s a song to quantize
 * @param q time interval in milliseconds
 */
void quantize(song *s, int q) {
    uint32_t i;
    int32_t  half;

    if (q <= 1) return;
    half = q >> 1;

    for (i = 0; i < s->size; i++) {
        /* Quantize note start times */
        int r = s->notes[i].strt % q;
        if (r > half) s->notes[i].strt += q - r;
        else s->notes[i].strt -= r;

        /* Quantize durations */
        r = s->notes[i].dur % q;
        if (r > half) s->notes[i].dur += q - r;
        else s->notes[i].dur -= r;

        /* Don't allow zero durations */
        if (s->notes[i].dur == 0) s->notes[i].dur = q;
    }
}

/**
 * Extracts rests (silent time intervals) from a song.
 */
void extract_song_rests(const song *s, int start, int end, song *rests) {
    vector  *r = (vector *) malloc((s->size + 1) * sizeof(vector));
    uint32_t i = 0;
    uint32_t num_rests = 0;

    if (r == NULL) {
        fputs("Error in extract_song_rests: Unable to allocate memory\n",
              stderr);
    }
    for (; i < s->size; i++) {
        for (; i < s->size; i++) {
            if (s->notes[i].strt <= start)
                start = MAX2(start, s->notes[i].strt + s->notes[i].dur);
            else break;
        }
        if (start >= end) break;
        if (i >= s->size) {
            r[num_rests].dur = end - start;
        } else {
            r[num_rests].dur = s->notes[i].strt - start;
        }
        r[num_rests].strt       = start;
        r[num_rests].ptch       = 0;
        r[num_rests].velocity   = 0;
        r[num_rests].instrument = USHRT_MAX;
        /*fprintf(stderr, "rest: (%d, %d)\n", r[num_rests].strt,
                r[num_rests].strt + r[num_rests].dur);*/
        num_rests++;

        if (i < s->size) {
            start = s->notes[i].strt + s->notes[i].dur;
        }
    }
    rests->notes = (vector *) malloc(num_rests * sizeof(vector));
    memcpy(rests->notes, r, num_rests * sizeof(vector));
    rests->size = num_rests;
    free(r);
}

/**
 * Trims silence from the beginning and the end of a song, leaving optional
 * margins.
 */
void trim_song(song *s, int margin, int step) {
    uint32_t i;
    int32_t  shift, end;

    if (s->size < 1) return;
    shift = margin - s->notes[0].strt;
    shift -= shift % step;

    end = 0;
    for (i = 0; i < s->size; i++) {
        s->notes[i].strt += shift;
        end = MAX2(end, s->notes[i].strt + s->notes[i].dur);
    }
    s->start = shift;
    s->end = end + margin;
}

void remove_octave_information(song *s) {
    vector  *notes = s->notes;
    uint32_t i;
    for (i = 0; i < s->size; i++) {
        notes[i].ptch = notes[i].ptch % 12;
    }
}

void sc_remove_octave_information(const songcollection *sc) {
    song    *songs = sc->songs;
    uint32_t i;
    for (i = 0; i < sc->size; i++) {
        remove_octave_information(&songs[i]);
    }
}


/**
 * Joins all songs in a song collection into one long song. Songs are
 * only joined until the overall duration reaches MAX_SONG_LENGTH.
 *
 * @param sc        songcollection whose songs will be joined
 * @param gap       length of the gap between consecutive songs,
 *                  in milliseconds
 * @param num_songs number of new songs to create
 * @param song_size maximum number of notes in the song
 * @param shuffle   set to 1 to shuffle the song collection before joining
 *
 * @return a new song collection with num_songs new songs
 */
songcollection *join_songs(songcollection *sc, int32_t gap,
        uint32_t num_songs, uint32_t song_size, int shuffle) {
    songcollection *nsc = (songcollection *) malloc(sizeof(songcollection));
    song           *ns;
    uint32_t i, numnotes, snum;
    int32_t  end;

    if (nsc == NULL) return NULL;

    init_song_collection(nsc, num_songs);

    /* Count notes */
    numnotes = 0;
    for (i = 0; i < sc->size; i++) {
        numnotes += sc->songs[i].size;
    }
    if (numnotes < song_size) song_size = numnotes;

    for (snum = 0; snum < num_songs; snum++) {
        if (shuffle) preprocess_songs(sc, 0, 0, 0, shuffle, 0);
        ns = &nsc->songs[snum];
        init_song(ns, snum, "", numnotes);
        nsc->size = snum+1;

        numnotes = 0;
        end = 0;
        for (i = 0; i < sc->size; i++) {
            song    *s = &sc->songs[i];
            uint32_t j = numnotes;
            memcpy(&ns->notes[numnotes], s->notes,
                    MIN2(s->size, song_size-numnotes) * sizeof(vector));
            numnotes += MIN2(s->size, song_size-numnotes);
            for (; j < numnotes; j++) {
                ns->notes[j].strt += end;
                if (ns->notes[j].strt + ns->notes[j].dur >= MAX_SONG_LENGTH) {
                    CLT_WARNING("Maximum song length (%d time units) "
                                "reached. Reduce temporal accuracy to work "
                                "around this problem.", MAX_SONG_LENGTH);
                    ns->size = j;
                    ns->end  = ns->notes[j-1].strt + ns->notes[j-1].dur;
                    goto next_song;
                }
            }
            end = ns->notes[numnotes-1].strt + ns->notes[numnotes-1].dur + gap;
            if (numnotes >= song_size) break;
        }
        ns->size = numnotes;
        ns->end = end;
next_song:
        fprintf(stderr,"Song duration: %d\n", ns->end);
    }
    return nsc;
}


void update_song_collection_statistics(songcollection *sc) {
    uint32_t count, i;
    count = 0;
    sc->max_song_size = 0;
    for (i = 0; i < sc->size; i++) {
        uint32_t s = sc->songs[i].size;
        count += s;
        if (s > sc->max_song_size) sc->max_song_size = s;
        update_song_statistics(&sc->songs[i]);
    }
    sc->num_notes = count;
}


/**
 * Generates a pattern from a random song in the given song collection.
 *
 * @param pattern      pointer to a song structure that will hold
 *                     the generated data
 * @param sc           song collection
 * @param m            structure for storing the pattern position in
 *                     the collection
 * @param size         size of the generated pattern
 * @param maxskip      maximum number of notes that the function is
 *                     allowed to skip when selecting each consecutive
 *                     note from a random song in the collection
 * @param maxtranspose maximum allowed transposition for the whole pattern
 * @param errors       percentage of generated errors in the pattern
 *                     (notes that do not appear in the original song)
 *
 * @return 1 if successful, 0 otherwise
 */
int generate_pattern(song *pattern, const songcollection *sc, match *m,
        uint32_t size, uint32_t maxskip, int8_t maxtranspose,
        float errors) {
    song    *s = NULL;
    char    *error_positions;
    uint32_t sindex = 0;
    uint32_t i, errorcount;
    int32_t  p, lastpos, start;
    int8_t   minpitch, maxpitch;

    if (errors > 1.0F) errors = 1.0F;
    if (errors < 0.0F) errors = 0.0F;

    /* Select a random song */
    for (i = 0; i < 100; i++) {
        sindex = (uint32_t) lrintf((float) sc->size * randf());
        s = &sc->songs[sindex];
        if (s->size > size * 2) {
            break;
        } else s = NULL;
    }
    if (s == NULL) {
        fputs("Error in generate_pattern(): unable to find a song that is "
              "long enough\n", stderr);
        fprintf(stderr, "  Requested pattern size was %u\n", size);
        pattern->notes = NULL;
        pattern->size = 0;
        return 0;
    }

    /* Select a random start position */
    i = s->size / maxskip;
    if (i < size) maxskip = s->size / size;
    lastpos = (int32_t) s->size - (int32_t) size * ((int32_t) maxskip + 1) - 1;
    p = 0;
    if (lastpos > 0) {
        p = (int32_t) lrintf((float) lastpos * randf());
    } else {
        maxskip = 0;
        if (size + 2 > s->size) {
            size = s->size - 2;
        }
        lastpos = (int32_t) (s->size - size - 1);
        p = (int32_t) lrintf((float) lastpos * randf());
    }

    /* Allocate memory for the pattern */
    pattern->notes = (vector *) calloc(size, sizeof(vector));
    if (pattern->notes == NULL) {
        fputs("Error in generate_pattern(): failed to allocate memory\n",
                stderr);
        return 0;
    }
    pattern->title = (char *) malloc(strlen(s->title) + 1);
    if (pattern->title == NULL) {
        fputs("Error in generate_pattern(): failed to allocate memory\n",
                stderr);
        free(pattern->notes);
        return 0;
    }
    strcpy(pattern->title, s->title);

    pattern->start = 0;
    pattern->end   = 0;
    pattern->size  = size;
    start    = s->notes[p].strt;
    minpitch = NOTE_PITCHES - 1;
    maxpitch = 0;

    /* Fill in match information */
    m->song      = sindex;
    m->start     = start;
    m->num_notes = size;
    m->notes     = (int32_t *) malloc(size * sizeof(int));
    if (m->notes == NULL) {
        fputs("Error in generate_pattern(): failed to allocate memory\n",
                stderr);
        free(pattern->notes);
        free(pattern->title);
        return 0;
    }

    m->similarity = 1.0F - errors;


    for (i = 0; i < size; i++) {
        char pitch = s->notes[p].ptch;
        if (pitch < minpitch) minpitch = pitch;
        if (pitch > maxpitch) maxpitch = pitch;
        pattern->notes[i].ptch = pitch;
        pattern->notes[i].strt = s->notes[p].strt - start;
        pattern->notes[i].dur  = s->notes[p].dur;

        m->notes[i] = (int32_t) p;

        if (maxskip > 0) {
            p += 1 + (int32_t) lrintf((float) maxskip * randf());
        } else {
            p++;
        }
        if ((uint32_t) p >= s->size) {
            fputs("Error in generate_pattern(): unable to retrieve complete pattern: song eneded.\n", stderr);
            free(pattern->notes);
            pattern->notes = NULL;
            pattern->size = 0;
            return 0;
        }
    }
    m->end = start + pattern->notes[size-1].strt +
                     pattern->notes[size-1].dur;
    pattern->end = m->end - m->start;

    /* Transpose the notes if needed and possible */
    if (maxtranspose != 0) {
        if (minpitch - maxtranspose < 0)
            maxtranspose = minpitch;
        if ((int) maxpitch + (int) maxtranspose >= NOTE_PITCHES) {
            maxtranspose = (int8_t) (NOTE_PITCHES - maxpitch - 1);
        }
        maxtranspose = (int8_t) (maxtranspose - lrintf(
                (float) maxtranspose * 2.0F * randf()));
        for (i = 0; i < size; i++) {
            pattern->notes[i].ptch = (int8_t) (pattern->notes[i].ptch +
                                               maxtranspose);
        }
        m->transposition = (int8_t) -maxtranspose;
    }

    /* Change some notes to simulate errors */
    errorcount = (uint32_t) MAX2(0, lrintf((float) size * errors));
    error_positions = (char *) calloc(size, sizeof(char));

    for (i = 0; i < errorcount; i++) {
        uint32_t j;
        p = (int32_t) lrintf(((float) size - (float) i) * randf());
        for (j = 0; j < size; j++) {
            if (error_positions[j] == 0) {
                if (p <= 0) {
                    int32_t next, prev;
                    /* Modify note at position j */
                    error_positions[j] = 1;

                    /* Change start time */
                    if (j > 0) prev = pattern->notes[j-1].strt;
                    else prev = 0;
                    if (j + 1 < size) next = pattern->notes[j+1].strt;
                    else next = pattern->notes[j].strt + 4000;
                    next = prev + (int32_t) lrintf((float)
                        (next - prev) * randf());
                    pattern->notes[j].strt = next;

                    /* Change duration */
                    pattern->notes[j].dur = (int32_t) lrintf((float)
                            pattern->notes[j].dur * 2.0F *
                            randf());

                    /* Change pitch slightly if there is room */
                    if (next != prev) {
                        int pitch = pattern->notes[j].ptch;
                        if ((pitch > 1) && (pitch < 126)) {
                            pattern->notes[j].ptch = (int8_t) (
                                    pattern->notes[j].ptch + 2 -
                                    lrintf(5.0F * randf()));
                        }
                    }

                    /* Mark the changed note to the match information */
                    m->notes[j] = -1;

                    /* Break the loop */
                    j = size;
                }
                p--;
            }
        }
    }
    if (errorcount > 0) {
        /*fprintf(stderr, "sorting...");*/
        /* Make sure the pattern is ordered. It should be. */
        lexicographic_sort(pattern);
    }
    update_song_statistics(pattern);
    free(error_positions);
    return 1;
}



/**
 * Generates a collection of random patterns from the given song collection.
 *
 * @param patterncount number of patterns to generate
 * @param pc           pointer to a structure that will hold the generated
 *                     data
 * @param sc           song collection
 * @param ms           structure for storing pattern positions in
 *                     the song collection
 * @param minsize      minimum size of a pattern
 * @param maxsize      maximum size of a pattern
 * @param maxskip      maximum number of notes that the function is
 *                     allowed to skip when selecting each consecutive
 *                     note from a random song in the collection
 * @param maxtranspose maximum allowed transposition for a pattern
 * @param errors       percentage of generated errors in each pattern
 *                     (notes that do not appear in the original song)
 *
 * @return number of patterns generated, 0 if there was an error
 */
uint32_t generate_pattern_collection(uint32_t patterncount,
        songcollection *pc, const songcollection *sc, matchset *ms,
        uint32_t minsize, uint32_t maxsize, uint32_t maxskip,
        int8_t maxtranspose, float errors) {
    uint32_t i;
    if (!init_song_collection(pc, patterncount)) {
        fputs("ERROR in generate_patterncollection(): failed to "
              "initialize pattern collection", stderr);
        return 0;
    }

    if (!init_match_set(ms, patterncount, 0, 0, 0)) {
        free_song_collection(pc);
        return 0;
    }
    if (maxsize < minsize) maxsize = minsize;

    for (i = 0; i < patterncount; i++) {
        uint32_t l;
        if (maxsize - minsize > 0) {
            l = minsize + (uint32_t) lrintf((float) (maxsize - minsize) *
                                           randf());
        } else {
            l = minsize;
        }
        if (!generate_pattern(&pc->songs[i], sc, &ms->matches[i], l, maxskip,
                maxtranspose, errors)) {
            uint32_t j;
            for (j = 0; j < i; j++) {
                free_song(&pc->songs[j]);
            }
            free_match_set(ms);
            return 0;
        }
        pc->songs[i].id = i;
        ms->num_matches++;
    }
    pc->size = patterncount;
    return patterncount;
}


/**
 * Inserts pattern to the given song.
 *
 * @param s       song
 * @param songpos position within the song
 * @param p       pattern to insert
 * @param errors  ratio of notes that are changed in the inserted pattern
 *                [0.0-1.0]
 * @param noise   ratio of added notes in the inserted pattern [>= 0.0]
 */
void insert_pattern_to_song(song *s, uint32_t songpos, const song *p,
        float errors, float noise) {
    vector  *snotes;
    float    error_level = errors;
    float    noise_level = noise;
    uint32_t nsize      = (uint32_t) lrintf(noise * (float) p->size);
    uint32_t num_errors = (uint32_t) lrintf(errors * (float) p->size);
    uint32_t ssize = s->size + p->size + nsize;
    uint32_t i, spos;
    int32_t  start;

    snotes = (vector *) malloc(ssize * sizeof(vector));
    if (snotes == NULL) {
        fputs("Error in insert_pattern_to_song(): "
              "failed to allocate note buffer\n", stderr);
        return;
    }
    lexicographic_sort(s);
    songpos = MIN2(songpos, s->size);
    spos    = songpos;

    /* Copy the first notes */
    memcpy(snotes, s->notes, spos * sizeof(vector));

    if (spos > 0) start = s->notes[spos-1].strt + s->notes[spos-1].dur;
    else start = 0;
    for (i = 0; i < p->size; i++, spos++) {
        int j;
        int noisestart, noiseend;

        if (p->size - i <= num_errors) error_level = 1.1F;

        memcpy(&snotes[spos], &p->notes[i], sizeof(vector));
        snotes[spos].strt += start;
        noisestart = snotes[spos].strt;

        /* Change some of the notes according to the error rate */
        if ((num_errors > 0) && (randf() < error_level)) {
            /*int pitch;*/
            snotes[spos].strt = MAX2(0, snotes[spos].strt + 10000 -
                    (int) (randf() * 20000.0F));
            /* Change the note pitch by 32 or less */
            /*pitch = snotes[spos].ptch;
            pitch = MIN2(pitch, 127 - pitch);
            pitch = MIN2(32, pitch);
            snotes[spos].ptch = snotes[spos].ptch + pitch -
                    (int) (randf() * (float) (pitch * 2));*/
            snotes[spos].ptch = (char) (randf() * 127.0F);
            num_errors--;
        }

        /* Add extra notes (noise) */
        if (i < (p->size - 1)) {
            noiseend = noisestart + p->notes[i+1].strt - p->notes[i].strt;
        } else {
            noiseend = noisestart + 2000;
            noise_level = (float) nsize + 0.1F;
        }
        if ((nsize > 0) && (noise_level >= 1.0F)) {
            for (j=0; (((float) j)<noise_level) && (nsize > 0); ++j) {
                vector *note = &snotes[++spos];
                note->strt = noisestart + (int32_t) lrintf(
                        randf() * (float) (noiseend - noisestart));
                note->dur        = (int32_t) lrintf(randf() * 2000.0F);
                note->ptch       = (int8_t)  lrintf(randf() * 127.0F);
                note->velocity   = (uint8_t) lrintf(randf() * 127.0F);
                note->instrument = 1;
                --nsize;
            }
            noise_level = 0.0F;
        }
        noise_level += noise;
    }
    if (songpos < s->size) {
        start += p->notes[p->size - 1].strt + p->notes[p->size - 1].dur;
        memcpy(&snotes[spos], &s->notes[songpos], (s->size - songpos) *
               sizeof(vector));
        for (i = spos; i < ssize; i++) {
            snotes[i].strt += start;
        }
    }
    free(s->notes);
    s->notes = snotes;
    s->size  = ssize;
    lexicographic_sort(s);
    remove_overlap(s);
    update_song_statistics(s);
}


/**
 * Inserts patterns to the given song collection.
 *
 * @param sc      song collection
 * @param pc      patterns to insert
 * @param pattern_instances
 *                number of times that each pattern is inserted
 * @param errors  ratio of notes that are changed in the inserted patterns
 *                [0.0-1.0]
 * @param noise   ratio of added notes in inserted patterns [>= 0.0]
 */
void insert_patterns(songcollection *sc, const songcollection *pc,
        uint32_t pattern_instances, float errors, float noise) {
    uint32_t i;
    for (i = 0; i < pc->size; i++) {
        uint32_t j;
        for (j = 0; j < pattern_instances; j++) {
            song *s = &sc->songs[lrintf(randf() * (float) (sc->size - 1))];
            uint32_t songpos = (uint32_t) lrintf(randf() * (float) s->size);
            /* Spread error rate so that inserted patterns can be ordered by
               their similarity to the original pattern */
            float e = errors - randf() * (MIN2(errors, 0.25F));
            insert_pattern_to_song(s, songpos, &pc->songs[i], e, noise);
        }
    }
}

/**
 * Frees memory reserved for algorithm-specific song data.
 *
 * @param sc a song collection
 */
void free_song_collection_data(songcollection *sc) {
    int i;
    for (i=1; i<=NUM_DATA_FORMATS; ++i) {
        if (sc->data[i] != NULL) {
            free_data_format(i, sc->data[i]);
            sc->data[i] = NULL;
        }
    }
}


/**
 * Updates alternative data formats and indices within a song collection.
 *
 * @param sc   song collection
 * @param algorithm_list
 *             a -1-terminated list of algorithm IDs to define
 *             the required data formats. If NULL, data in all
 *             formats is updated.
 */
void update_song_collection_data(songcollection *sc,
        const int *algorithm_list, const dataparameters *dp) {
    const int *alg = algorithm_list;
    free_song_collection_data(sc);
    if (alg == NULL) {
        int i;
        for (i = 1; i <= NUM_DATA_FORMATS; i++) {
            sc->data[i] = init_data_format((int) i); 
            if (sc->data[i] == NULL) {
                fprintf(stderr, "Error in update_song_collection_data(): "
                        "initializing data format %d failed\n", i);
                return;
            }
            if (!convert_song_collection(i, sc->data[i], sc, dp)) {
                fprintf(stderr, "Error in update_song_collection_data(): "
                        "converting to format %d failed\n", i);
            }
        }
    } else {
        while (*alg > 0) {
            int format = get_algorithm_data_format(*alg);
            if ((format > 0) && (format <= NUM_DATA_FORMATS) &&
                    (sc->data[format] == NULL)) {
                sc->data[format] = init_data_format(format); 
                if (sc->data[format] == NULL) {
                    fprintf(stderr, "Error in update_song_collection_data(): "
                            "initializing data format %d failed\n", format);
                    return;
                }
                if (!convert_song_collection(format, sc->data[format], sc, dp)) {
                    fprintf(stderr, "Error in update_song_collection_data(): "
                            "converting to format %d failed\n", format);
                }
            }
            alg++;
        }
    }
}


/**
 * Reads MIREX '07 multiple fundamental frequency task output files.
 */
int read_mirex07_fundamental_frequency_file(const char *path, song *s,
        double linear_drift) {
    FILE    *f = fopen(path, "r");
    char    *linebuf = NULL;
    char    *notemap;
    vector  *playing_notes[128];
    size_t   linebuf_size;
    uint32_t notemap_width = 600 * 100;
    uint32_t i, lines, num_notes;


    if (f == NULL) {
        fprintf(stderr, "Error in read_mirex07_fundamental_frequency_file(): "
                "unable to read file '%s'\n", path);
        return 0;
    }

    notemap = (char *) calloc(128 * notemap_width, sizeof(char));
    if (notemap == NULL) {
        fclose(f);
        fprintf(stderr, "Error in read_mirex07_fundamental_frequency_file(): "
                "failed to allocate memory");
        return 0;
    }

    lines = 0;
    while(getline(&linebuf, &linebuf_size, f) >= 0) {
        double   time;
        int      j, ret, pos;

        /* Parse the line */
        pos = 0;
        ret = sscanf(linebuf, " %lf %n", &time, &j);
        if (ret > 0) {
            int32_t notemap_pos = (int32_t) (lrint(time * 100.0) * 128);
            lines = MAX2(lines, (uint32_t) lrint(time * 100.0));
            if (lines >= notemap_width) {
                uint32_t oldwidth = notemap_width;
                notemap_width *= 2;
                if ((notemap = (char *) realloc(notemap, 128 *
                        notemap_width * sizeof(char))) == NULL) {
                    fprintf(stderr, "Warning in "
                            "read_mirex07_fundamental_frequency_file: "
                            "failed to increase\nnote buffer size. "
                            "Data is clipped at %d seconds.",
                            notemap_width / 100);
                    break;
                }
                memset(notemap + 128 * oldwidth, 0, oldwidth * sizeof(char));
            }
            /* printf("Time: %d, ", (int) lrint(time * 100.0)); */
            pos += j;
            while (1) {
                double freq;
                ret = sscanf(linebuf+pos, " %lf %n", &freq, &j);
                pos += j;
                if (ret > 0) {
                    int32_t note = (int32_t) lrint(69.0 + 12.0 *
                                                   log2(freq/440.0));
                    notemap[notemap_pos + note] = 1;
                    /* printf("(%f, %d), ", freq, note); */
                } else break;
            }
        }
        /*printf("\n");*/
    }
    free(linebuf);
    fclose(f);

    /* Count notes */
    num_notes = 0; 
    for (i = 0; i < 128; i++) {
        if (notemap[i]) num_notes++;
        playing_notes[i] = NULL;
    }
    for (i = 1; i < lines; i++) {
        int j;
        int pos = (int) i * 128;
        for (j = 0; j < 128; j++) {
            if (notemap[pos + j] && (notemap[pos + j - 128] == 0)) {
                num_notes++;
            }
        }
    }

    /* Extract notes */
    s->notes = (vector *) malloc(num_notes * sizeof(vector));
    s->size = num_notes;
    num_notes = 0;
    for (i = 0; i < lines; i++) {
        int j;
        int pos = (int) i * 128;
        for (j = 0; j < 128; j++) {
            if (notemap[pos + j]) {
                if (playing_notes[j] == NULL) {
                    vector *n     = &s->notes[num_notes];
                    n->strt       = (int32_t) lrint(linear_drift *
                                                    (double) (i * 10));
                    n->ptch       = (int8_t) j;
                    n->velocity   = 127;
                    n->instrument = 0;
                    playing_notes[j] = n;
                    num_notes++;
                }
            } else {
                if (playing_notes[j] != NULL) {
                    vector *n = playing_notes[j];
                    n->dur = (int32_t) lrint(linear_drift *
                                             (double) (i * 10)) - n->strt;
                    playing_notes[j] = NULL;
                }
            }
        }
    }
    for (i = 0; i < 128; i++) {
        if (playing_notes[i] != NULL) {
            playing_notes[i]->dur = (int32_t)
                    lrint(linear_drift * (double) (lines * 10)) -
                    playing_notes[i]->strt;
        }
    }

    s->end = (int32_t) lrint(linear_drift * (double) (lines * 10));

    free(notemap);
    return 1;
}

