/*
 * midifile.c - Functions for reading and writing Standard MIDI Files.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <dirent.h>
#include <sys/stat.h>

#include "config.h"
#include "midifile.h"
#include "pq32.h"
#include "song.h"
#include "util.h"


#define INITIAL_MIDI_PATH_LENGTH 1024

/**
 * Recursive function that scans midi files from a tree of directories.
 *
 * @param path the directory to scan
 * @param sc song collection where the read songs will be stored. Use NULL to
 *        only count the number of files.
 * @param count number of potential MIDI files found will be stored here
 * @param skip_percussion set to 1 to skip percussion channel in the MIDI files
 *        that will be read
 */
/*static void scan_midi_files(const char *path, songcollection *sc, int *count,
        int skip_percussion) {
    int n;
    struct stat statbuf;

    if (stat(path, &statbuf) == 0) {
        if (S_ISDIR(statbuf.st_mode)) {
            struct dirent **files = NULL;
            char *p;
            int pathlen = strlen(path);

            p = (char *) malloc((pathlen + 2) * sizeof(char));
            if (p == NULL) return;
            strcpy(p, path);
            if ((pathlen > 0) && (path[pathlen-1] != '/')) {
                p[pathlen] = '/';
                p[pathlen+1] = 0;
                pathlen++;
            }

            n = scandir(p, &files, 0, alphasort);
            if (n >= 0) {
                int i;
                for (i = 0; i < n; i++) {
                    char *fn;
                    if ((strcmp(files[i]->d_name, "..") == 0) ||
                            (strcmp(files[i]->d_name, ".") == 0)) {
                        free(files[i]);
                        continue;
                    }
                    fn = (char *) malloc((pathlen +
                            strlen(files[i]->d_name) + 1) * sizeof(char));
                    if (fn == NULL) {
                        for (; i < n; i++) free(files[i]);
                        break;
                    }
                    strcpy(fn, p);
                    strcpy(fn + pathlen, files[i]->d_name);
                    scan_midi_files(fn, sc, count, skip_percussion);
                    free(fn);
                    free(files[i]);
                }
            } else fprintf(stderr, "Error: couldn't open directory: %s\n", p);

            free(p);
            free(files);
        } else if (S_ISREG(statbuf.st_mode)) {
            if ((sc != NULL) && (*count < sc->size)) {
                if (read_midi_file(path, &sc->songs[*count], NULL,
                        skip_percussion)) {
                    sc->songs[*count].id = *count;
                    *count += 1;
                }
            } else *count += 1;
        }
    }
}
*/


/**
 * Reads a MIDI file or a collection of MIDI files based on a list file.
 *
 * @param path            MIDI file or a text file that lists the MIDI
 *                        files to read
 * @param sc              song collection where the data is stored
 * @param skip_percussion set to 1 to skip percussion channel in the input
 *                        files
 *
 * @return number of MIDI files that were successfully read
 */
size_t read_midi_collection(const char *path, songcollection *sc,
        int skip_percussion) {
    FILE   *listfile = fopen(path, "r");
    char   *namebuffer;
    size_t  namebuffer_size = INITIAL_MIDI_PATH_LENGTH * sizeof(char);
    size_t  count = 0;
    ssize_t linelen;
    song    s;

    /* Try to read it as a MIDI file. */
    if (read_midi_file(path, &s, NULL, skip_percussion)) {
        sc->songs = (song *) malloc(sizeof(song));
        if (sc->songs == NULL) {
            fputs("Error allocating memory in read_midi_collection\n", stderr);
            return 0;
        }
        s.id = 0;
        sc->size = 1;
        sc->songs[0] = s;
        return 1;
    }
        

    listfile = fopen(path, "r");
    if (listfile == NULL) {
        fprintf(stderr, "Error in read_midi_collection while opening collection list: %s\n", path);
        return 0;
    }

    namebuffer = (char *) malloc(namebuffer_size);
    if (namebuffer == NULL) {
        fputs("Error allocating memory in read_midi_collection\n", stderr);
        fclose(listfile);
        return 0;
    }

    while ((linelen = getline(&namebuffer, &namebuffer_size, listfile)) > 0)
        count++;

    /* Seek back to the beginning */
    if (fseek(listfile, 0L, SEEK_SET)) {
        fclose(listfile);
        listfile = fopen(path, "r");
        if (listfile == NULL) {
            fprintf(stderr, "Error in read_midi_collection while opening collection list: %s\n", path);
            return 0;
        }
    }

    sc->songs = (song *) calloc(count, sizeof(song));
    if (sc->songs == NULL) {
        fputs("Error allocating memory in read_midi_collection\n", stderr);
        fclose(listfile);
        free(namebuffer);
        return 0;
    }

    count = 0;
    while ((linelen = getline(&namebuffer, &namebuffer_size, listfile)) > 0) {
        /* Remove trailing newline from the path */
        uint32_t i = 0;
        while ((i < namebuffer_size) && (namebuffer[i] != '\0')) {
            if (namebuffer[i] == '\n') {
                namebuffer[i] = '\0';
                break;
            }
            i++;
        }
        if (read_midi_file(namebuffer, &sc->songs[count], NULL,
                skip_percussion)) {
            sc->songs[count].id = (uint32_t) count;
            count++;
        }
    }
    fprintf(stderr, "%zu MIDI files read\n", count);
    sc->size = (uint32_t) count;
    free(namebuffer);
    fclose(listfile);

    /* scan_midi_files(path, NULL, &count, skip_percussion);
    sc->size = count;
    if (count > 0) {
        sc->songs = (song *) calloc(count, sizeof(song));
        count = 0;
        scan_midi_files(path, sc, &count, skip_percussion);
        sc->size = count;
    } */
    return count;
}


/**
 * Reads an unsigned short value from MIDI data.
 *
 * @param buffer MIDI data buffer
 * @param i position in the buffer. The pointer will be moved forward.
 *
 * @returns the read value
 */
static inline uint16_t midi_parse_short(const uint8_t *buffer, uint32_t *i) {
    uint32_t value = buffer[(*i)++];
    value <<= 8;
    value += buffer[(*i)++];
    return (uint16_t) value;
}

/**
 * Writes an unsigned short value to a MIDI data buffer.
 *
 * @param value the value to write
 * @param buffer MIDI data buffer
 * @param i position in the buffer. The pointer will be moved forward.
 */
static inline void midi_write_short(uint16_t value, uint8_t *buffer, uint32_t *i) {
    buffer[(*i)++] = (uint8_t) (value >> 8);
    buffer[(*i)++] = (uint8_t) (value & 0xFF);
}


/**
 * Reads an unsigned int value from MIDI data.
 *
 * @param buffer MIDI data buffer
 * @param i position in the buffer. The pointer will be moved forward.
 *
 * @returns the read value
 */
static inline uint32_t midi_parse_int(const uint8_t *buffer, uint32_t *i) {
    uint32_t value = buffer[(*i)++];
    value <<= 8;
    value += buffer[(*i)++];
    value <<= 8;
    value += buffer[(*i)++];
    value <<= 8;
    value += buffer[(*i)++];
    return value;
}

/**
 * Writes an unsigned int value to a MIDI data buffer.
 *
 * @param value  the value to write
 * @param buffer MIDI data buffer
 * @param i      position in the buffer. The pointer will be moved forward.
 */
static inline void midi_write_int(uint32_t value, uint8_t *buffer, uint32_t *i) {
    buffer[(*i)++] = (uint8_t)  (value >> 24);
    buffer[(*i)++] = (uint8_t) ((value >> 16) & 0xFF);
    buffer[(*i)++] = (uint8_t) ((value >> 8)  & 0xFF);
    buffer[(*i)++] = (uint8_t)  (value & 0xFF);
}


/**
 * Reads a variable-length value from MIDI data.
 *
 * @param buffer MIDI data buffer
 * @param i      position in the buffer. The pointer will be moved forward.
 * @paran ret    parsed value
 *
 * @returns 0 if successful, 1 if parsing fails
 */
static inline int midi_parse_varlen(const uint8_t *buffer, uint32_t *i,
                                    uint32_t *ret) {
    uint32_t max_i = *i + 4;
    uint32_t value;
    uint8_t  c;

    if ((value = buffer[(*i)++]) & 0x80) {
        value &= 0x7F;
        do {
            value = (value << 7) + ((c = buffer[(*i)++]) & 0x7F);
        } while ((c & 0x80) && ((*i) < max_i));
        if (c & 0x80) {
            fputs("Error in midi_parse_varlen(): "
                  "Invalid variable-length quantity\n", stderr);
            *ret = 0;
            return 1;
        }
    }
    *ret = value;
    return 0;
}


/**
 * Writes a variable-length value to a MIDI data buffer.
 *
 * @param value the value to write
 * @param buffer MIDI data buffer
 * @param i position in the buffer. The pointer will be moved forward.
 */
static inline void midi_write_varlen(uint32_t value, uint8_t *buffer,
        uint32_t *i) {
    uint32_t tmp;
    tmp = value & 0x7F;
    while ((value >>= 7)) {
        tmp <<= 8;
        tmp |= ((value & 0x7F) | 0x80);
    }
    while (1) {
        buffer[(*i)++] = (uint8_t) (tmp & 0xFF);
        if (tmp & 0x80) tmp >>= 8;
        else break;
    }
}

/**
 * Parses an event from MIDI data.
 *
 * @param buffer MIDI data buffer
 * @param buffersize size of the buffer
 * @param i current position in the buffer
 * @param event struct where the event data will be stored
 *
 * @return -2 or -1 if the buffer ends unexpectedly, 1 if successful,
 *         0 for end of track
 */
static int parse_event(const uint8_t *buffer, uint32_t buffersize,
        uint32_t *i, track_event *event, uint8_t *running_status) {
    uint32_t b = 1;
    uint32_t length;
    uint32_t delta;

    # if 0
    while ((*i < buffersize) && b) {
        if (b = midi_parse_varlen(buffer, i, &delta)) {
            /* Skip odd data */
            char c;
            while ((*i < buffersize) && ((c = buffer[*i]) & 0x80)) {
                *i += 1;
            }
            while ((*i < buffersize) && (!((c = buffer[*i]) & 0x80))) {
                *i += 1;
            }
        }
    }
    #endif
    if (*i >= buffersize) return -2;
    if (midi_parse_varlen(buffer, i, &delta)) {
        return -2;
    }

    event->tick += (double) delta;
    if (*i >= buffersize) return -1;

    b = buffer[(*i)++];
    /* Is there a status byte? Otherwise use running status (previous status) */
    if (b & STATUS_BIT) {
        if ((b >= 0x80) && (b < 0xF0)) *running_status = (uint8_t) b;
        /*else if (b >= 0xF0) *running_status = 0;*/
    } else if (running_status == 0) {
        while((*i < buffersize) && ((b = buffer[(*i)++]) < 0x80));
    } else {
        (*i)--;
        b = *running_status;
    }
    event->status = (uint8_t) b;

    switch (b & STATUS_MASK) {
        case EVENT_NOTE_OFF:
        case EVENT_NOTE_ON:
        case EVENT_AFTERTOUCH:
        case EVENT_CONTROLLER:
        case EVENT_PITCH_WHEEL:
            length = 2;
            break;
        case EVENT_PROGRAM_CHANGE:
        case EVENT_CHANNEL_PRESSURE:
            length = 1;
            break;
        case EVENT_SYSEX:
            /* Meta event */
            if (b == EVENT_META) {
                if (*i >= buffersize) return -1;
                b = buffer[(*i)++];
                event->metatype = (uint8_t) b;
                if (*i >= buffersize) return -1;
                if (midi_parse_varlen(buffer, i, &length)) return -2;
            /* SYSEX event or escaped data */
            } else if ((b == EVENT_SYSEX) || (b == EVENT_ESCAPE)) {
                if (*i >= buffersize) return -1;
                if (midi_parse_varlen(buffer, i, &length)) return -2;
                if ((*i + length) > buffersize) length = buffersize - *i;
            /* Unescaped System Common or Realtime message */
            } else {
                fputs("Warning in parse_event(): ignoring unescaped "
                      "MIDI system message\n", stderr);
                length = 0;
            }
            break;
        default:
            length = 1;
    }
    event->data = buffer + *i;
    event->length = length;
    /* Return -1 if the message was not complete */
    if ((*i + length) > buffersize) return -1;
    *i += length;
    if (event->metatype == EVENT_END_OF_TRACK) return 0;
    else return 1;
}


/**
 * Writes a MIDI event to a MIDI data buffer.
 *
 * @param event a MIDI track event
 * @param tick time value
 * @param buffer MIDI data buffer
 * @param i position in the buffer. The pointer will be moved forward.
 */
static inline void midi_write_event(track_event *event, uint32_t tick,
        uint8_t *buffer, uint32_t *i) {
    uint32_t j;
    midi_write_varlen(tick, buffer, i);
    buffer[(*i)++] = event->status;
    switch (event->status & STATUS_MASK) {
        case EVENT_SYSEX:
            /* Meta event */
            if (event->status == EVENT_META) buffer[(*i)++] = event->metatype;

            midi_write_varlen(event->length, buffer, i);
            for (j = 0; j < event->length; j++) {
                buffer[(*i)++] = event->data[j];
            }
            break;
        default:
            if (event->length >= 1) buffer[(*i)++] = event->data[0];
            if (event->length >= 2) buffer[(*i)++] = event->data[1];
            break;
    }
}


/**
 * Parses a set of MIDI tracks.
 *
 * @param tracks track data buffers
 * @param track_lengths lengths of track buffers
 * @param num_tracks number of tracks to parse
 * @param s song where the read music data will be stored
 * @param division_type PPQN or SMPTE
 * @param division ppqn division or SMPTE framerate
 * @param skip_percussion set to 1 to skip reading percussion channel events
 */
static void parse_tracks(const uint8_t **tracks,
        const uint32_t *track_lengths, const uint32_t num_tracks,
        song * const s, midisong * const midi_s, const int division_type,
        const uint32_t division, const int skip_percussion) {
    uint8_t     *running_status;
    uint32_t    *track_pointers;
    track_event *track_events;
    vector      *playing_notes[MIDI_CHANNELS][NOTE_PITCHES];
    uint8_t      patch[MIDI_CHANNELS];
    pq32         pq;

    double   ms_per_tick;
    double   tempo_change_time = 0.0;
    double   tempo_change_tick = 0.0;
    double   curtime = 0.0;
    /*double *track_last_tick;*/
    uint32_t i;

    if (num_tracks == 0) {
        fputs("Warning in parse_tracks: "
              "no tracks given\n", stderr);
        return;
    }

    if (pq32_init(&pq, num_tracks)) {
        fputs("Error in parse_tracks: "
              "failed to allocate a priority queue\n", stderr);
        return;
    }

    track_pointers = (uint32_t *) calloc(num_tracks, sizeof(uint32_t));
    if (track_pointers == NULL) {
        fputs("Error in parse_tracks: failed to allocate "
              "temporary buffer 'track_pointers'\n", stderr);
        pq32_free(&pq);
        return;
    }
    track_events = (track_event *) calloc(num_tracks, sizeof(track_event));
    if (track_events == NULL) {
        fputs("Error in parse_tracks: failed to allocate "
              "temporary buffer 'track_events'\n", stderr);
        free(track_pointers);
        pq32_free(&pq);
        return;
    }

    running_status = (uint8_t *) calloc(num_tracks, sizeof(uint8_t));
    if (running_status == NULL) {
        fputs("Error in parse_tracks: failed to allocate "
              "temporary buffer 'running_status'\n", stderr);
        free(track_events);
        free(track_pointers);
        pq32_free(&pq);
        return;
    }

    /*track_last_tick = (double *) calloc(num_tracks, sizeof(double));*/

    memset(playing_notes, 0, MIDI_CHANNELS * NOTE_PITCHES * sizeof(vector *));
    memset(patch, 0, MIDI_CHANNELS * sizeof(uint8_t));

    /* Initialize time base */
    if (division_type == SMPTE) ms_per_tick = 1000.0 / ((double) division);
    else ms_per_tick = MIDI_DEFAULT_TEMPO / ((double) division);

    /* Insert first events from each track to the queue */
    for (i = 0; i < num_tracks; i++) {
        pq32node *node = pq32_getnode(&pq, i);
        track_event *e = &track_events[i];
        int r = parse_event(tracks[i], track_lengths[i],
                            &track_pointers[i], e, &running_status[i]);
        /* FIXME: Check for a time overflow */
        if (r > 0) {
            node->key1  = (int) e->tick;
            node->value = (int) i;
        } else {
            node->key1 = INT_MAX;
        }
        pq32_update(&pq, node);
    }

    if ((s != NULL) && (s->size > 0)) {
        tempo_change_time = MIDI_TYPE3_GAP;
    }

    while (1) {
        pq32node    *node;
        track_event *e;
        uint32_t     t;
        int          r;

        node = pq32_getmin(&pq);
        if (node->key1 == INT_MAX) break;

        t = (uint32_t) node->value; 
        e = &track_events[t];

        curtime = tempo_change_time + (e->tick - tempo_change_tick) *
                  ms_per_tick;

        if (s != NULL) {
            switch (e->status & STATUS_MASK) {
                case EVENT_NOTE_OFF: {
                    uint8_t channel = e->status & CHANNEL_MASK;
                    int8_t  pitch;
                    vector *n;
                    if (skip_percussion && (channel == MIDI_PERCUSSION_CHANNEL))
                        break;
                    pitch = (int8_t) (e->data[0] & 0x7F);
                    /* velocity = e->data[1]; */
                    n = playing_notes[channel][pitch];
                    if (n != NULL) {
                        n->dur = ((int) lrint(curtime)) - n->strt;
                        playing_notes[channel][pitch] = NULL;
                    }
                    }
                    break;
                case EVENT_NOTE_ON: {
                    uint8_t channel = e->status & CHANNEL_MASK;
                    uint8_t velocity;
                    int8_t  pitch;
                    vector *n;
                    if (skip_percussion && (channel == MIDI_PERCUSSION_CHANNEL))
                        break;
                    pitch    = (int8_t)  e->data[0] & 0x7F;
                    velocity = (uint8_t) e->data[1] & 0x7F;
                    n = playing_notes[channel][pitch];
                    if (n != NULL) {
                        n->dur = ((int) lrint(curtime)) - n->strt;
                    }
                    if (velocity == 0) {
                        playing_notes[channel][pitch] = NULL;
                        break;
                    }
                    n = &s->notes[s->size++];
                    n->strt = (int) lrint(curtime);
                    n->dur = 0;
                    n->ptch = pitch;
                    n->velocity = velocity;
                    n->instrument = (uint16_t) ((channel << 8) +
                                                patch[channel]);
                    /*n->instrument = current_instrument[t][channel];*/
                    playing_notes[channel][pitch] = n;

                    /*printf("Note (start:%d, dur:%d, pitch:%d, velocity:%d, channel:%d, track:%d, instrument: %d)\n",
                            n->strt, n->dur, n->ptch, n->velocity, channel, t,
                            n->instrument);*/
                    }
                    break;
                case EVENT_CONTROLLER:
                    if ((e->status == CONTROLLER_ALL_NOTES_OFF) ||
                            (e->status == CONTROLLER_ALL_SOUND_OFF)) {
                        int c;
                        for (c=0; c<MIDI_CHANNELS; ++c) {
                            int p;
                            for (p=0; p<NOTE_PITCHES; ++p) {
                                vector *n = playing_notes[c][p];
                                if (n != NULL) {
                                    n->dur = ((int) curtime) - n->strt;
                                }
                                playing_notes[c][p] = NULL;
                            }
                        }
                    }
                    break;
                case EVENT_PROGRAM_CHANGE: {
                    int channel = e->status & CHANNEL_MASK;
                    patch[channel] = e->data[0];
                    }
                    break;
#if 0
                case EVENT_AFTERTOUCH:
                    break;
                case EVENT_PITCH_WHEEL:
                    break;
                case EVENT_CHANNEL_PRESSURE:
                    break;
                case EVENT_SYSEX:
                    break;
#endif
                default:
                    break;
            }
        }
        if (e->status == EVENT_META) {
            /* Meta event */
            if (e->metatype == EVENT_TEMPO && (division_type != SMPTE)) {
                int tempo = (((int) e->data[0]) << 16) +
                        (((int) e->data[1]) << 8) + ((int) e->data[2]);
                tempo_change_time = curtime;
                tempo_change_tick = e->tick;
                ms_per_tick = ((double) tempo) / (1000.0 * ((double) division));
            }
        }

        if (midi_s != NULL) {
            track_event *te = &midi_s->track_data[t][midi_s->track_size[t]++];
            memcpy(te, e, sizeof(track_event));
            te->tick = curtime;
        }

        /*track_last_tick = e->tick;*/

        /* Parse next event */
        r = parse_event(tracks[t], track_lengths[t], &track_pointers[t], e,
                &running_status[t]);
        /* FIXME: Check for a time overflow */
        if (r > 0) node->key1 = (int) e->tick;
        else {
            node->key1 = INT_MAX;
            if ((r == 0) && (midi_s != NULL)) {
                track_event *te =
                        &midi_s->track_data[t][midi_s->track_size[t]++];
                memcpy(te, e, sizeof(track_event));
                te->tick = curtime;    
            }
        }

        pq32_update(&pq, node);
    }

    if (s != NULL) {
        /* Stop all remaining notes */
        for (i=0; i<MIDI_CHANNELS; ++i) {
            int j;
            for (j=0; j<NOTE_PITCHES; ++j) {
                vector *n = playing_notes[i][j];
                if (n != NULL) {
                    n->dur = ((int) curtime) - n->strt;
                }
            }
        }
        s->end = (int) curtime;
    }

    pq32_free(&pq);
    free(track_events);
    free(track_pointers);
    free(running_status);
    /*free(track_last_tick);*/
}


/**
 * Reads a song from a standard MIDI file.
 *
 * @param file            path to the MIDI file
 * @param s               pointer to a song data structure for storing
 *                        the song in a geometric format. Use NULL to not
 *                        store song data.
 * @param midi_s          pointer to a midi data structure for storing all
 *                        the events. Use NULL to not store MIDI data.
 * @param skip_percussion set to 1 to skip percussion notes
 *                        (MIDI channel 10), 0 to parse all notes.
 *
 * @return 1 if successful, 0 otherwise
 */
int read_midi_file(const char *path, song *s, midisong *midi_s,
                   int skip_percussion) {
    const uint8_t **tracks = NULL;
    uint32_t       *track_lengths = NULL;
    uint8_t        *buffer;

    size_t   filesize;
    uint32_t header_num_tracks = 0;
    uint32_t num_tracks = 0;
    uint32_t division = MIDI_DEFAULT_PPQN_DIVISION;
    int      division_type = PPQN; 
    int      format = -1;
    uint32_t i, last_pos, num_events;

    /*fprintf(stderr, "Opening file: '%s'\n", path);*/
    buffer = read_file(path, SMF_MIN_SIZE, 10, &filesize);
    if (buffer == NULL) {
        fprintf(stderr, "Warning in read_midi_file(): failed to open file "
                        "'%s' \n", path);
        return 0;
    }


    for (i = 0; i < filesize - SMF_MIN_SIZE; i++) {
        if ((buffer[i]   == 'M') && (buffer[i+1] == 'T') &&
            (buffer[i+2] == 'h') && (buffer[i+3] == 'd')) {

            i += 8;
            format = midi_parse_short(buffer, &i);
            header_num_tracks = midi_parse_short(buffer, &i);
            header_num_tracks++;

            division = midi_parse_short(buffer, &i);
            if (division & 0x8000) {
                /* SMPTE division */
                uint32_t rate = ((division & 0xFF00) >> 8);
                division_type = SMPTE;
                division      = rate * (division & 0xFF);
            }

            /*printf("MIDI header detected (format:%d, smpte:%d, division:%d, tracks:%d)\n",
                    format + 1, division_type, division, header_num_tracks);*/
            break;
        }
    }
    if (i > filesize - 8) {
        free(buffer);
        return 0;
    }
    if (format > 3) {
        fprintf(stderr, "Warning in read_midi_file(): unknown MIDI file "
                        "type %d, parsing anyway...\n", format + 1);
        fprintf(stderr, "    File: %s\n", path);
    } else if (format < 0) {
        /* No valid MIDI header found. Search for track data. */
        format = 1;
        i = 0;
    }

    if (header_num_tracks > 0) {
        tracks = (const uint8_t **) malloc(header_num_tracks *
                sizeof(const uint8_t *));
        if (tracks == NULL) {
            fputs("Error in read_midi_file(): failed to allocate "
                  "temporary buffer 'tracks'\n", stderr);
            free(buffer);
            return 0;
        }
        track_lengths = (uint32_t *) malloc(header_num_tracks * sizeof(int));
        if (track_lengths == NULL) {
            fputs("Error in read_midi_file(): failed to allocate "
                  "temporary buffer 'track_lengths'\n", stderr);
            free(tracks);
            free(buffer);
            return 0;
        }
 
    }

    last_pos = i;
    for (; i < filesize - 8; i++) {
        if ((buffer[i]   == 'M') && (buffer[i+1] == 'T') &&
            (buffer[i+2] == 'r') && (buffer[i+3] == 'k')) {

            uint32_t len;
            i += 4;
            len = midi_parse_int(buffer, &i);

            if (i + len > (uint32_t) filesize) len = (uint32_t) filesize - i;
            /* If there are more tracks than the header reported,
             * allocate more space. */
            if (num_tracks >= header_num_tracks) {
                const uint8_t **new_tracks;
                uint32_t       *new_track_lengths;
                uint32_t j;
                new_tracks = (const uint8_t **) malloc(
                        2 * num_tracks * sizeof(const uint8_t *));
                if (new_tracks == NULL) {
                    fputs("Warning in read_midi_file(): failed to reallocate "
                            "temporary buffer 'tracks'\n", stderr);
                    continue;
                }
                new_track_lengths = (uint32_t *) malloc(
                        2 * num_tracks * sizeof(int));
                if (new_track_lengths == NULL) {
                    fputs("Warning in read_midi_file(): failed to reallocate "
                            "temporary buffer 'track_lengths'\n", stderr);
                    free(new_tracks);
                    continue;
                }
 
                for (j = 0; j < num_tracks; j++) {
                    new_tracks[j]        = tracks[j];
                    new_track_lengths[j] = track_lengths[j];
                }
                free(tracks);
                free(track_lengths);
                tracks            = new_tracks;
                track_lengths     = new_track_lengths;
                header_num_tracks = 2 * num_tracks;
            }
            tracks[num_tracks] = buffer + i;
            track_lengths[num_tracks] = len;
            num_tracks++;

            i += len - 1;
            last_pos = i + 1;
        } else {
            /* Unknown chunk, search for the next track chunk ID */
            for (i = last_pos; i < filesize - 8; i++) {
                if ((buffer[i]   == 'M') && (buffer[i+1] == 'T') &&
                    (buffer[i+2] == 'r') && (buffer[i+3] == 'k')) {
                    i--;
                    break;
                }
            }
        }
    }

    if (num_tracks == 0) {
        free(buffer);
        free(tracks);
        free(track_lengths);
        return 0;
    }

    /* Estimate maximum number of MIDI events in the file */
    num_events = (uint32_t) filesize >> 1;

    if (s != NULL) {
        s->notes = (vector *) malloc(num_events * sizeof(vector));
        if (s->notes == NULL) {

            fputs("Error in read_midi_file(): failed to allocate "
                  "buffer 's->notes'\n", stderr);
            goto EXIT;
        }
        s->size = 0;
        s->start = 0;
        s->end = 0;
    }

    if (format == 2) {
        /* Concatenate tracks of Type 3 MIDI files */
        if (midi_s != NULL) {
            midi_s->track_size = (uint32_t *) malloc(sizeof(uint32_t));
            if (midi_s->track_size == NULL) {
                fputs("Error in read_midi_file(): failed to allocate "
                      "buffer 'track_size'\n", stderr);
                goto EXIT;
            }
            midi_s->track_data = (track_event **) malloc(sizeof(
                        track_event *));
            if (midi_s->track_data == NULL) {
                fputs("Error in read_midi_file(): failed to allocate "
                      "buffer 'track_data'\n", stderr);
                free(midi_s->track_size);
                goto EXIT;
            }
            midi_s->track_size[0] = 0;
            midi_s->track_data[0] = (track_event *) malloc(num_events *
                    sizeof(track_event));
            if (midi_s->track_data[0] == NULL) {
                fputs("Error in read_midi_file(): failed to allocate "
                      "buffer 'track_data[0]'\n", stderr);
                free(midi_s->track_size);
                free(midi_s->track_data);
                goto EXIT;
            }
            midi_s->num_tracks  = 1;
            midi_s->buffer      = buffer;
            midi_s->buffer_size = (uint32_t) filesize;
        }

        for (i = 0; i < num_tracks; i++) {
            parse_tracks(&tracks[i], &track_lengths[i], 1, s, midi_s,
                    division_type, division, skip_percussion);
        }
    } else {
        /* Parse normal Type 1 or Type 2 files directly */
        if (midi_s != NULL) {
            midi_s->track_size = (uint32_t *) malloc(num_tracks *
                                                     sizeof(uint32_t));
            if (midi_s->track_size == NULL) {
                fputs("Error in read_midi_file(): failed to allocate "
                      "buffer 'track_size'\n", stderr);
                goto EXIT;
            }
            midi_s->track_data = (track_event **) malloc(num_tracks *
                    sizeof(track_event *));
            if (midi_s->track_data == NULL) {
                fputs("Error in read_midi_file(): failed to allocate "
                      "buffer 'track_data'\n", stderr);
                free(midi_s->track_size);
                goto EXIT;
            }
            for (i = 0; i < num_tracks; i++) {
                midi_s->track_size[i] = 0;
                midi_s->track_data[i] = (track_event *) malloc(
                        (track_lengths[i] >> 1) * sizeof(track_event));
                if (midi_s->track_data[i] == NULL) {
                    uint32_t j;

                    fprintf(stderr, "Error in read_midi_file(): failed to "
                                    "allocate buffer 'track_data[%u]'\n", i);
                    for (j = 0; j < i; j++) {
                        free(midi_s->track_data[j]);
                    }
                    free(midi_s->track_size);
                    free(midi_s->track_data);
                    goto EXIT;
                }
            }
            midi_s->num_tracks = num_tracks;
            midi_s->buffer = buffer;
            midi_s->buffer_size = (uint32_t) filesize;
        }

        parse_tracks(tracks, track_lengths, num_tracks, s, midi_s,
                division_type, division, skip_percussion);
    }

    if (MIDI_READ_TRIM_BUFFERS) {
        if (s != NULL) {
            vector *notes = (vector *) malloc(s->size * sizeof(vector));
            if (notes != NULL) {
                memcpy(notes, s->notes, s->size * sizeof(vector));
                free(s->notes);
                s->notes = notes;
            }
        }
        if (midi_s != NULL) {
            for (i = 0; i < num_tracks; i++) {
                uint32_t tsize = midi_s->track_size[i];
                track_event *events = (track_event *) malloc(
                        tsize * sizeof(track_event));
                if (events != NULL) {
                    memcpy(events, midi_s->track_data[i], tsize *
                            sizeof(track_event));
                    free(midi_s->track_data[i]);
                    midi_s->track_data[i] = events;
                }
            }
        }
    }

    /* Name the song */
    i = (uint32_t) strlen(path);
    if (s != NULL) {
        s->title = (char *) malloc((i+1) * sizeof(char));
        if (s->title != NULL) {
            memcpy(s->title, path, (i+1) * sizeof(char));
        }
    }
    if (midi_s != NULL) {
        midi_s->name = (char *) malloc((i+1) * sizeof(char));
        if (midi_s->name != NULL) {
            memcpy(midi_s->name, path, (i+1) * sizeof(char));
        }
    }

EXIT:
    if ((midi_s == NULL) || (midi_s->buffer != buffer)) {
        free(buffer);
    }
    free(tracks);
    free(track_lengths);
    return 1;
}

/**
 * Writes a MIDI song to a standard MIDI file.
 *
 * @param file output file path
 * @param midi_s pointer to a midi data structure
 *
 * @return 1 if successful, 0 otherwise
 */
int write_midi_file(const char *path, const midisong *midi_s,
        int force_leading_silence) {
    FILE    *f;
    uint8_t *buffer;
    double   ticks_per_ms;
    double   tempo      = MIDI_DEFAULT_TEMPO;
    uint32_t num_tracks = midi_s->num_tracks;
    uint32_t division   = MIDI_DEFAULT_PPQN_DIVISION;
    uint32_t bufferlen  = MIDI_HEADER_SIZE;
    int      ret = 1;
    uint32_t i, pos;

    /* Estimate track lengths in bytes to reserve a memory buffer */
    for (i = 0; i < num_tracks; i++) {
        uint32_t j;
        for (j = 0; j<midi_s->track_size[i]; j++) {
            bufferlen += 10 + midi_s->track_data[i][j].length;
        }
        bufferlen += MIDI_TRACK_HEADER_SIZE;
    }

    buffer = (uint8_t *) malloc(bufferlen * sizeof(uint8_t));
    if (buffer == NULL) {
        fputs("Error in write_midi_file(): failed to allocate "
                "buffer 'track_data'\n", stderr);
        free(midi_s->track_size);
        goto EXIT;
    }


    /* MIDI header */
    memcpy(buffer, "MThd", 4);
    pos = 4;
    midi_write_int(6, buffer, &pos);
    midi_write_short(MIDI_DEFAULT_FORMAT, buffer, &pos);
    midi_write_short((uint16_t) num_tracks, buffer, &pos);
    midi_write_short((uint16_t) division, buffer, &pos);

    ticks_per_ms = (double) division / tempo;

    /* Tracks */
    for (i = 0; i < num_tracks; i++) {
        double   lasttick = 0.0;
        uint32_t j = 0, track_length_pos;
        int      eot = 0;
        memcpy(&buffer[pos], "MTrk", 4);
        pos += 4;
        track_length_pos = pos;
        pos += 4;
        if ((force_leading_silence) && (i == num_tracks - 1)) {
            track_event note;
            uint8_t notedata[2];
            for (; j<midi_s->track_size[i]; ++j) {
                track_event *te = &midi_s->track_data[i][j];
                double tick;
                uint32_t tdelta;
                if (te->tick > 0.0) break;
                /* Skip tempo and end-of-track events */
                if (te->status == EVENT_META) {
                    if (te->metatype == EVENT_TEMPO) continue;
                    else if (te->metatype == EVENT_END_OF_TRACK) {
                        if (j < midi_s->track_size[i] - 1) continue;
                        else eot = 1;
                    }
                }
                tick = te->tick * ticks_per_ms;
                tdelta = (uint32_t) (tick - lasttick);
                midi_write_event(te, tdelta, buffer, &pos);
                lasttick += tdelta;
            }
            notedata[0] = 0;
            notedata[1] = 1;
            /* Note on */
            note.tick = 0.0;
            note.status = EVENT_NOTE_ON;
            note.metatype = 0;
            note.length = 2;
            note.data = notedata;
            midi_write_event(&note, 0, buffer, &pos);
            /* Note off */
            note.status = EVENT_NOTE_OFF;
            midi_write_event(&note, 0, buffer, &pos);
        }
        for (; j < midi_s->track_size[i]; j++) {
            track_event *te = &midi_s->track_data[i][j];
            double       tick;
            uint32_t     tdelta;
            /* Skip tempo and end-of-track events */
            if (te->status == EVENT_META) {
                if (te->metatype == EVENT_TEMPO) continue;
                else if (te->metatype == EVENT_END_OF_TRACK) {
                    if (j < midi_s->track_size[i] - 1) continue;
                    else eot = 1;
                }
            }
            tick   = te->tick * ticks_per_ms;
            tdelta = (uint32_t) (tick - lasttick);
            midi_write_event(te, tdelta, buffer, &pos);
            lasttick += tdelta;
        }
        /* Add an end-of-track event if there wasn't one */
        if (!eot) {
            track_event te;
            te.tick     = 0.0;
            te.status   = EVENT_META;
            te.metatype = EVENT_END_OF_TRACK;
            te.length   = 0;
            te.data     = NULL;
            midi_write_event(&te, 0, buffer, &pos);
        }
        midi_write_int(pos - track_length_pos - 4, buffer, &track_length_pos);
    }


    f = fopen(path, "w");
    if (f == NULL) {
        fprintf(stderr, "ERROR in write_midi_file: failed to open file '%s'\n",
                path);
        ret = 0;
        goto EXIT;
    }
    fwrite(buffer, sizeof(uint8_t), pos, f);
    fclose(f);

EXIT:
    free(buffer);
    return ret;
}

/**
 * Frees MIDI song data.
 *
 * @param midi_s pointer to a MIDI song
 */
void free_midisong(midisong *midi_s) {
    uint32_t i;
    for (i = 0; i < midi_s->num_tracks; ++i) free(midi_s->track_data[i]);
    free(midi_s->track_data);
    free(midi_s->track_size);
    free(midi_s->buffer);
    free(midi_s->name);
    memset(midi_s, 0, sizeof(midisong));
}

/**
 * Converts a song to a midisong.
 *
 * @param s      song to convert
 * @param midi_s MIDI output
 *
 * @return 1 on success, 0 on failure 
 */
int song_to_midisong(const song *s, midisong *midi_s) {
    uint8_t     *buffer;
    track_event *events;
    pq32         playing_notes;
    uint32_t     i, e, b;
    uint32_t     num_events = s->size * 2;

    midi_s->name = NULL;
    /* Copy title */
/*    if (s->title != NULL) {
        i = (strlen(s->title) + 1) * sizeof(char);
        midi_s->name = (char *) malloc(i);
        memcpy(midi_s->name, s->title, i);
    }
*/
    /* Allocate memory */
    midi_s->track_data = (track_event **) malloc(midi_s->num_tracks *
                                                 sizeof(track_event *));
    if (midi_s->track_data == NULL) {
        fputs("Error in song_to_midisong(): failed to allocate "
              "temporary buffer 'track_data'\n", stderr);
        return 0;
    }
 
    midi_s->track_size = (uint32_t *) malloc(midi_s->num_tracks *
                                             sizeof(uint32_t));
    if (midi_s->track_size == NULL) {
        fputs("Error in song_to_midisong(): failed to allocate "
              "temporary buffer 'track_size'\n", stderr);
        free(midi_s->track_data);
        return 0;
    }
 
    midi_s->track_data[0] = (track_event *) malloc(num_events *
            sizeof(track_event));
    if (midi_s->track_data[0] == NULL) {
        fputs("Error in song_to_midisong(): failed to allocate "
              "temporary buffer 'track_size'\n", stderr);
        free(midi_s->track_data);
        free(midi_s->track_size);
        return 0;
    }
 
    midi_s->track_size[0] = num_events;
    midi_s->num_tracks = 1;

    midi_s->buffer_size = 2 * num_events * sizeof(char);
    buffer = (uint8_t *) malloc(midi_s->buffer_size);
    if (buffer == NULL) {
        fputs("Error in song_to_midisong(): failed to allocate "
              "temporary buffer for output data\n", stderr);
        free(midi_s->track_data[0]);
        free(midi_s->track_data);
        free(midi_s->track_size);
        return 0;
    }
    midi_s->buffer = buffer;

    if (pq32_init(&playing_notes, 128)) {
        fputs("Error in song_to_midisong(): failed to initialize priority "
              "queue for tracking simultaneously playing notes\n", stderr);
        free(buffer);
        free(midi_s->track_data[0]);
        free(midi_s->track_data);
        free(midi_s->track_size);
        return 0;
    }
 

    events = midi_s->track_data[0];
    for (i = 0, e = 0, b = 0; i < s->size; i++) {
        pq32node *node;
        vector   *n = &s->notes[i];
        uint32_t  end;
        events[e].tick = n->strt;
        events[e].status = EVENT_NOTE_ON;
        events[e].metatype = 0;
        events[e].length = 2;
        events[e].data = &buffer[b];
        buffer[b++] = (uint8_t) n->ptch;
        buffer[b++] = n->velocity;
        e++;

        node = pq32_getnode(&playing_notes, (uint32_t) n->ptch);
        node->key1  = MAX2(0, n->strt + n->dur);
        node->value = (int) i;
        pq32_update_7(&playing_notes, node);

        /* Stop any playing notes that end before the next onset. */
        if (i == s->size - 1) end = UINT_MAX - 1;
        else end = (uint32_t) MAX2(0, s->notes[i+1].strt);
        while(1) {
            node = pq32_getmin(&playing_notes);
            if ((uint32_t) MAX2(0, node->key1) <= end) {
                n = &s->notes[node->value];
                events[e].tick     = node->key1;
                events[e].status   = EVENT_NOTE_OFF;
                events[e].metatype = 0;
                events[e].length   = 2;
                events[e].data     = &buffer[b];
                buffer[b++] = (uint8_t) n->ptch;
                buffer[b++] = 0;
                e++;
                node->key1 = INT_MAX;
                pq32_update_7(&playing_notes, node);
            } else break;
        }
    }
    pq32_free(&playing_notes);
    return 1;
}

