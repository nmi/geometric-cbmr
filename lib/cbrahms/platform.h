/*
 * platform.h - Definitions and macros to improve portability to various
 *              operating systems and compilers.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */


#ifndef __PLATFORM_H__
#define __PLATFORM_H__


/* Define C99 keywords when using old-style compilers or modes */

#ifndef __cplusplus

#if __STDC_VERSION__ >= 199901L

  #include <stdint.h>
  #include <stdbool.h>
  #include <limits.h>

#else

  #ifdef __GNUC__
    /* Use __inline__ when compiling with -ansi */
    #define inline __inline__
  #else
    #define inline /* nothing */
  #endif

  typedef char                      int8_t;
  typedef unsigned char            uint8_t;
  typedef short                    int16_t;
  typedef unsigned short          uint16_t;
  typedef int                      int32_t;
  typedef unsigned int            uint32_t;
  typedef long long int            int64_t;
  typedef unsigned long long int  uint64_t;

  typedef char                        bool;

#endif

#endif /* !__cplusplus */


/* Structure padding macros to help with GCC's -Wpadded. These macros can
 * be used to manually turn warnings off in places where padding is accepted. */ 
#if defined (ENABLE_STRUCT_PADDING) && defined (__GNUC__)

  #define PAD_STRUCT_SUBSTITUTE_NAME(var) #var
  #define PAD_STRUCT_BY(var,n) char PAD_STRUCT_SUBSTITUTE_NAME(var)[n];
  #define PAD_STRUCT(n)        char __padding__[n];

  #if (__WORDSIZE == 64)
    #define PAD_STRUCT_32(n)   /* nothing */
    #define PAD_STRUCT_64(n)   PAD_STRUCT(n)
  #else
    #define PAD_STRUCT_32(n)   PAD_STRUCT(n)
    #define PAD_STRUCT_64(n)   /* nothing */
  #endif

#else

  #define PAD_STRUCT_BY(var,n) /* nothing */
  #define PAD_STRUCT(n)        /* nothing */
  #define PAD_STRUCT_32(n)     /* nothing */
  #define PAD_STRUCT_64(n)     /* nothing */

#endif


#endif

