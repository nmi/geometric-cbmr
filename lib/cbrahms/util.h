/*
 * util.h - External declarations for the helper functions
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdint.h>
#include <sys/time.h>
#include <getopt.h>

#ifdef __cplusplus
extern "C" {
#endif

#define VECTOR_ALIGNMENT 16
#define CACHE_ALIGNMENT 64

/* Align a memory variable. */
#define ALIGNED(v,n) v __attribute__((aligned(n)))
#define ALIGNED_16(v) ALIGNED(v,16)
#define ALIGNED_8(v) ALIGNED(v,8)
#define ALIGNED_4(v) ALIGNED(v,4)

/* CEIL_MOD_x(a) returns the closest ceiling modulo x integer to a given value.
   s = log2(x) */
#define CEIL_MOD(a,s) (((a) - 1 >> s) + 1 << s)
#define CEIL_MOD_64(a) CEIL_MOD(a,6)
#define CEIL_MOD_32(a) CEIL_MOD(a,5)
#define CEIL_MOD_16(a) CEIL_MOD(a,4)
#define CEIL_MOD_8(a) CEIL_MOD(a,3)
#define CEIL_MOD_4(a) CEIL_MOD(a,2)
#define CEIL_MOD_CACHE(a) CEIL_MOD(a,6)


/** Returns the larger of two numerical values. */
#define MAX2(a,b) ((a)>(b)?(a):(b))

/** Returns the smaller of two numerical values. */
#define MIN2(a,b) ((a)<(b)?(a):(b))


/** Swaps the values of two variables. */
#define INT_SWAP(a,b) { int t=(a); (a)=(b); (b)=t; }

#define DOUBLE_SWAP(a,b) { double t=(a); (a)=(b); (b)=t; }

#define VOIDPTR_SWAP(a,b) { void *t=(a); (a)=(b); (b)=t; }

#define ABS(a) ((a) > 0 ? (a) : -(a))

/** Arithmetic shift right (>>). */
#define ASHIFTR(a,shift) ((a)>=0?((a)>>shift):(-((-a)>>shift)))


/**
 * A bitfield structure for masking double variables into their components.
 */
typedef struct {
    uint32_t mantissa2:32;
    uint32_t mantissa1:20;
    uint32_t exponentbias1023:11;
    uint32_t signbit:1;
} doubleMask;


/**
 * Double variable mask.
 */
typedef union {
    double asDouble;
    doubleMask asMask;
} doubleAndMask;


/**
 * Calculates a base 2 logarithm of the given value.
 *
 * @param n the input value
 *
 * @return log_2(n)
 */
static inline uint32_t fastlog2(uint32_t n) {
    doubleAndMask fm;
    fm.asDouble = (double) n;
    return (fm.asMask.exponentbias1023 - (uint32_t) 1023);
}



/* External function declarations */

void init_bitcount(void);

uint32_t bitcount32(uint32_t n);


void *alloc_aligned(size_t alignment, size_t size);


double timediff(struct timeval *time1, struct timeval *time0);


float randf(void);

double randd(void);


int round64(double x);

int round32(float x);


int cmp_int(const void *a, const void *b);


int kth_smallest(int a[], int n, int k);

double kth_smallest_double(double a[], int n, int k);

#define median(a, n) kth_smallest(a, n, (((n)&1) ? ((n)/2) : (((n)/2)-1)))


int strcicmp(const char *s1, const char *s2);

uint8_t *read_file(const char *file, size_t minsize, size_t nullpadding,
                   size_t *size);


void write_pgm(const char *file, uint8_t *image, int width, int height);

uint8_t *read_pgm(const char *file, int *width, int *height);


char *make_short_options(const struct option *long_options);

#ifdef __cplusplus
}
#endif

#endif

