/*
 * pq32.c - A priority queue implementation for the geometric P2 and P3
 *          algorithms.
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "config.h"
#include "util.h"
#include "pq32.h"

#include "clt.h"


/**
 * Initializes a priority queue. The queue is stored in an array and each
 * node in the queue can be accessed directly by an index.
 *
 * @param  pq    pointer to a pq32 struct
 * @param  size  queue size
 *
 * @return 0 if successful, EINVAL if size <= 0, ENOMEM if malloc fails.
 */
int pq32_init(pq32 *pq, uint32_t size) {
    uint32_t i, j, n, leaves, level;

    if (size <= 0) return EINVAL;

    pq->levels    = fastlog2(size-1) + 1;
    leaves        = (uint32_t) (1 << pq->levels);
    pq->size      = size;
    pq->nodecount = leaves;

    pq->nodes     = (pq32node  *) malloc(leaves * sizeof(pq32node  )    );
    pq->tree      = (pq32node **) malloc(leaves * sizeof(pq32node *) * 2);

    if ((pq->nodes == NULL) || (pq->tree == NULL)) {
        CLT_ERROR("Malloc of %d bytes failed",
                  leaves * (sizeof(pq32node) + sizeof(pq32node *) * 2));
        return ENOMEM;
    }

    /* generate an empty tree */
    for (i = 0; i < leaves; i++) {
        pq->tree[leaves + i] = &pq->nodes[i];
        pq->nodes[i].index = i;
        pq->nodes[i].key1 = INT_MAX;
        pq->nodes[i].key2 = INT_MAX;
        pq->nodes[i].value = 0;
    }

    level = 2;
    j = leaves >> 1;
    n = leaves - level;

    for (i = leaves - 1; i > 0; i--) {
        pq->tree[i] = &pq->nodes[n];
        n -= level;
        if (i == j) {
            j     >>= 1;
            level <<= 1;
            n = leaves - level;
        }
    }

    return 0;
}

/**
 * Releases the memory buffers allocated for a priority eueue.
 *
 * @param pq the queue to free
 */
void pq32_free(pq32 *pq) {
    free(pq->nodes);
    free(pq->tree);
    memset(pq, 0, sizeof(pq32));
}



/* Test functions. */


/**
 * Prints a representation of the given priority queue to standard output.
 *
 * @param pq the priority queue
 */
static void pq32_printtree(pq32 *pq) {
    uint32_t level = 1;
    uint32_t i;
    uint32_t j = 2;
    printf("leaves: %d\n", pq->nodecount);
    for (i = 1; i < pq->nodecount * 2; i++) {
        if (i == j) {
            printf("\n");
            level <<= 1;
            j += level;
        }
        printf("%d:%d ", pq->tree[i]->index, pq->tree[i]->key1);
    }
}


/**
 * A small test program for the priority queue data structure.
 *
 * @param size      queue size
 * @param key       update function
 * @param printtree set to 1 to print the tree structure to stderr
 */
void pq32_test(uint32_t size,
               void (* update_key) (pq32 *pq, pq32node *n),
               int printtree) {
    pq32     pq;
    uint32_t i;
    uint32_t seed = (uint32_t) time(NULL);
    int      lastmin;
    srand(seed);

    pq32_init(&pq, size);

    if (printtree) {
        fputs("\n\n", stderr);
        pq32_printtree(&pq);
        fputs("\n",   stderr);
    }

    CLT_INFO("pq32_test: adding keys...");
    for (i = 0; i < size; i++) {
        pq32node *n;
        int32_t key = rand();
        key = (int) (((float) key / (float) RAND_MAX - 0.5F) *
                     ((float) size * 0.5F + 1.0F));
        /* int32_t key = i % 3333; */
        n = pq32_getnode(&pq, i);
        n->key1 = key;
        update_key(&pq, n);
        /* printf("%d ", key); */
    }

    if (printtree) {
        fputs("\n\n", stderr);
        pq32_printtree(&pq);
        fputs("\n",   stderr);
    }

    CLT_INFO("\nPQ: getting keys...\n");
    lastmin = INT_MIN;
    for (i = 0; i < size; i++) {
        pq32node *n = pq32_getmin(&pq);
/*
        printf("min: %d:%d\n", n->index, n->key1);
*/
        if (n->key1 < lastmin) {
            CLT_ERROR("Retrieved item is smaller than the previous (%d < %d)\n",
                      n->key1, lastmin);
        }
        lastmin = n->key1;
        n->key1 = INT_MAX;
        update_key(&pq, n);
    }
    pq32_free(&pq);
}



