/*
 * vindex_array.c - Vector-based index structure for polyphonic music
 *                  retrieval. This variant is stored and accessed as
 *                  a two-dimensional array.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <string.h>

#include "config.h"
#include "data.h"
#include "util.h"
#include "vindex_array.h"


/**
 * Adds a new record to the index structure. The memory buffer that
 * contains all the index records for the vector is set if it hasn't been
 * done before.
 *
 * @param vi      the index structure
 * @param x       the x component of the indexed vector
 * @param y       the y component of the indexed vector
 * @param songid  song id in the song collection
 * @param note    vector start position in the song
 *
 * @return Number of index records reserved from the records buffer.
 *         This is usually 0.
 *
 * @see populate_vectorindex()
 */
#if 0
static inline int vindex_add(vindex *vi, int x, int y,
        unsigned short songid, unsigned short note, vindexrec *records) {
    vindextable *t;
    int pos = vindex_table_pos(vi, x, y);
    if (pos < 0) return 0;

    t = &vi->tables[pos];
    t->records[t->size].song = songid;
    t->records[t->size].note = note;
    t->size++;
    return pos;
}
#endif

/**
 * Initializes a vector index structure.
 *
 * @return pointer to the index structure.
 */
vindex *vindex_alloc(void) {
    vindex *vi = (vindex *) calloc(1, sizeof(vindex));
    return vi;
}

void *vindex_alloc_untyped(void) {
    return vindex_alloc();
}

/**
 * Initializes and builds a vector index for a song collection.
 *
 * @param vi            pointer to a vector index structure
 * @param sc            the song collection for which the index is built
 * @param max_x         maximum width of an index vector
 * @param max_y         maximum height of an index vector
 * @param c_window      maximum number of consecutive notes the algorithm is
 *                      allowed to skip when selecting indexed vectors
 * @param indexing_time if not NULL, time spent on populating the index
 *                      is written here in seconds
 */
int vindex_build(vindex *vi, const songcollection *sc,
        const dataparameters *dp) {
    uint32_t *vector_counts;
    size_t    bp;
    uint32_t  i, total_vcount;
    uint32_t  indexed_vectors;

    vi->width  = dp->avindex_vector_max_width;
    vi->height = dp->avindex_vector_max_height << 1;
    vi->size   = (uint32_t) (vi->width * vi->height);
    vector_counts = (uint32_t *) calloc(vi->size, sizeof(int));
    if (vector_counts == NULL) return 0;

/*    for (y = 0; y < vi->height; y++) {
        for (x = 0; x < vi->width; x++) {
            vindextable *t = vi->tables[y * vi->width + x];
            t->size = 0;
            t->records = NULL;
        }
    }
*/
    vi->c_window = dp->c_window;
    vi->scollection = sc;

    /* Count all vectors so that we can allocate the right amount of memory */
    total_vcount = 0;
    for (i = 0; i < sc->size; i++) {
        uint32_t j;
        song *s = &sc->songs[i];
        for (j = 0; j < s->size - 1; j++) {
            uint32_t end, k;
            if (vi->c_window == UINT_MAX) {
                end = s->size - 1;
            } else {
                end = j + vi->c_window;
                if (end >= s->size) end = s->size - 1;
            }
            for (k = j + 1; k <= end; k++) {
                int x, pos;
                char y;
                x = s->notes[k].strt - s->notes[j].strt;
                y = (int8_t) (s->notes[k].ptch - s->notes[j].ptch);
                pos = vindex_table_pos(vi, x, y);
                if (pos >= 0) {
                    vector_counts[pos]++;
                    total_vcount++;
                } else if (x >= vi->width) break;
            }
        }
    }
    indexed_vectors = 0;
    for (i = 0; i < vi->size; i++) {
        if (vector_counts[i] > MAX_INDEX_BUCKET_SIZE) {
            vector_counts[i] = 0;
        } else {
            if (vector_counts[i] > 0) ++indexed_vectors;
        }
    }

    /* Calculate total memory usage */
    vi->memory_usage = (size_t) total_vcount * sizeof(vindexrec) +
            (size_t) indexed_vectors * sizeof(vindextable);


    /* Allocate a continuous memory buffer for index vectors and records */
    vi->buffer = (uint8_t *) malloc(vi->memory_usage);

    /* Allocate space for pointers to index vectors */
    vi->tables = (vindextable **) malloc(vi->size * sizeof(vindextable *));

    vi->memory_usage += vi->size * sizeof(vindextable *) + sizeof(vindex);

    /* Divide the buffer among the vectors */
    bp = 0;
    for (i = 0; i < vi->size; i++) {
        if (vector_counts[i] > 0) {
            vi->tables[i] = (vindextable *) (&vi->buffer[bp]);
            vi->tables[i]->size = 0;
            bp += (size_t) vector_counts[i] * sizeof(vindexrec)
                    + sizeof(vindextable);
        } else vi->tables[i] = NULL;
    }

    /* Scan and store individual records */
    for (i = 0; i < sc->size; i++) {
        uint32_t j;
        song *s = &sc->songs[i];
        for (j = 0; j < s->size - 1; j++) {
            uint32_t end, k;
            if (vi->c_window == UINT_MAX) end = s->size - 1;
            else {
                end = j + vi->c_window;
                if (end >= s->size) end = s->size - 1;
            }
            for (k = j + 1; k <= end; k++) {
                int32_t x =           s->notes[k].strt - s->notes[j].strt;
                int8_t  y = (int8_t) (s->notes[k].ptch - s->notes[j].ptch);
                vindextable *vit = vindex_table_by_vector(vi, x, y);
                if (vit != NULL) {
                    vit->records[vit->size].song = (uint16_t) i;
                    vit->records[vit->size].note = (uint16_t) j;
                    vit->size++;
                } else if (x >= vi->width) break;
            }
        }
    }

    free(vector_counts);
    return 1;
}

int vindex_build_untyped(void *vi, const songcollection *sc,
        const dataparameters *dp) {
    return vindex_build((vindex *) vi, sc, dp);
}

/**
 * Frees memory buffers of a vector index and re-initializes it.
 *
 * @param vi the index structure
 */
void vindex_clear(vindex *vi) {
    if (vi != NULL) {
        if (vi->buffer != NULL) free(vi->buffer);
        if (vi->tables != NULL) free(vi->tables);
        vi->memory_usage = 0;
        vi->size = 0;
    }
}

void vindex_clear_untyped(void *vi) {
    vindex_clear((vindex *) vi);
}

/**
 * Frees the given vector index data structure.
 *
 * @param vi the index structure
 */
void vindex_unalloc(vindex *vi) {
    if (vi != NULL) {
        vindex_clear(vi);
        free(vi);
    }
}

void vindex_unalloc_untyped(void *vi) {
    vindex_unalloc((vindex *) vi);
}

