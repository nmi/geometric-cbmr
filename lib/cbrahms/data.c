/*
 * data.c - Functions for managing data formats and indices
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "data.h"
#include "song.h"

#include "vindex_array.h"

#ifdef ENABLE_MSM
#include "search_msm.h"
#endif

#include "geometric_p2.h"
#include "geometric_p3.h"


static void *(* const INIT_DATA_FORMAT[])(void) = {
    alloc_p2_song_collection,
    alloc_p3_song_collection,
    vindex_alloc_untyped,
#ifdef ENABLE_MSM
    init_msm_song_collection,
#else
    NULL,
#endif
};

static int (* const CONVERT_SONG_COLLECTION[])(
        void *data, const songcollection *sc, const dataparameters *dp) = {
    build_p2_song_collection_untyped,
    build_p3_song_collection_untyped,
    vindex_build_untyped,
#ifdef ENABLE_MSM
    build_msm_song_collection,
#else
    NULL,
#endif
};

static void (* const CLEAR_DATA_FORMAT[])(void *data) = {
    clear_p2_song_collection_untyped,
    clear_p3_song_collection_untyped,
    vindex_clear_untyped,
#ifdef ENABLE_MSM
    clear_msm_song_collection,
#else
    NULL,
#endif
};


static void (* const FREE_DATA_FORMAT[])(void *data) = {
    unalloc_p2_song_collection,
    unalloc_p3_song_collection,
    vindex_unalloc_untyped,
#ifdef ENABLE_MSM
    free_msm_song_collection,
#else
    NULL,
#endif
};


void *init_data_format(int format) {
    if ((format > 0) && (format <= NUM_DATA_FORMATS)) {
        return INIT_DATA_FORMAT[format-1]();
    } else return NULL;
}

int convert_song_collection(int format, void *data, const songcollection *sc,
        const dataparameters *dp) {
    if ((format > 0) && (format <= NUM_DATA_FORMATS)) {
        return CONVERT_SONG_COLLECTION[format-1](data, sc, dp);
    } else return 0;
}

void clear_data_format(int format, void *data) {
    if ((format > 0) && (format <= NUM_DATA_FORMATS)) {
        CLEAR_DATA_FORMAT[format-1](data);
    }
}

void free_data_format(int format, void *data) {
    if ((format > 0) && (format <= NUM_DATA_FORMATS)) {
        FREE_DATA_FORMAT[format-1](data);
    }
}

