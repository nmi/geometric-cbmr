/*
 * geometric_p3.c - Geometric algorithm P3
 *
 * Copyright (c) 2003-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Authors: Mika Turkia
 *          Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#include "search.h"
#include "song.h"
#include "geometric_p3.h"
#include "pq32.h"
#include "util.h"



/**
 * Allocates and initializes a P3 song.
 *
 * @return an empty P3 song
 */
void init_p3_song(p3song *p3s) {
    p3s->s           = NULL;
    p3s->num_notes   = 0;
    p3s->num_rests   = 0;
    p3s->note_starts = NULL;
    p3s->note_ends   = NULL;
    p3s->rest_starts = NULL;
    p3s->rest_ends   = NULL;
}


/**
 * Frees a P3 song.
 *
 * @param p3s pointer to the song to be freed
 */
void free_p3_song(p3song *p3s) {
    free(p3s->note_starts);
    free(p3s->note_ends);
    free(p3s->rest_starts);
    free(p3s->rest_ends);
    p3s->note_starts = NULL;
    p3s->note_ends   = NULL;
    p3s->rest_starts = NULL;
    p3s->rest_ends   = NULL;
    p3s->num_notes   = 0;
    p3s->num_rests   = 0;
    p3s->s           = NULL;
}

/**
 * Converts a song to the P3 format.
 *
 * @param s the song to convert
 * @param p3s pointer to target data structure
 */
void song_to_p3(const song *s, p3song *p3s) {
    vector       *notes = s->notes;
    TurningPoint *note_starts, *note_ends;
    uint32_t i;

    p3s->s           = s;
    p3s->num_notes   = s->size;
    p3s->note_starts = (TurningPoint *) malloc(s->size * sizeof(TurningPoint));
    p3s->note_ends   = (TurningPoint *) malloc(s->size * sizeof(TurningPoint));
    p3s->rest_starts = (int *) malloc((s->size + 1) * sizeof(TurningPoint));
    p3s->rest_ends   = (int *) malloc((s->size + 1) * sizeof(TurningPoint));
    note_starts      = p3s->note_starts;
    note_ends        = p3s->note_ends;

    /* Copy notes */
    for (i = 0; i < s->size; i++) {
        vector *n = &notes[i];
        if (n->strt >= P3_TIME_LIMIT) {
            p3s->num_notes = i;
            break;
        }

        note_starts[i].x    = n->strt;
        note_starts[i].y    = n->ptch;
        note_starts[i].dur  = n->dur;
        note_starts[i].note = i;

        note_ends[i].x = n->strt + n->dur;
        if (note_ends[i].x >= P3_TIME_LIMIT) {
            note_ends[i].x  = P3_TIME_LIMIT - 1;
        }
        note_ends[i].y    = n->ptch;
        note_ends[i].dur  = n->dur;
        note_ends[i].note = i;
    }

    /* Sort note_ends */
    qsort(note_ends, i, sizeof(TurningPoint), compare_turningpoints);
}


/**
 * Initializes a P3 song collection struct.
 *
 * @return pointer to the data structure.
 */
void *alloc_p3_song_collection(void) {
    p3songcollection *p3sc = (p3songcollection *) calloc(1,
            sizeof(p3songcollection));
    return p3sc;
}


/**
 * Converts a song collection to the P3 format.
 *
 * @param p3sc target P3 song collection
 * @param sc   song collection to convert
 * @param dp   parameters
 */
int build_p3_song_collection(p3songcollection *p3sc,
        const songcollection *sc, const dataparameters *dp) {
    uint32_t i;

    /* Unused parameter dp */
    (void) dp;

    p3sc->song_collection = sc;
    p3sc->size            = sc->size;
    p3sc->p3_songs        = (p3song *) calloc(sc->size, sizeof(p3song));
    if (p3sc->p3_songs == NULL) {
        fputs("Error in build_p3_song_collection(): "
              "failed to allocate memory for 'p3_songs'", stderr);
        return 0;
    }
    for (i = 0; i < sc->size; i++) {
        song_to_p3(&sc->songs[i], &p3sc->p3_songs[i]);
    }
    return 1;
}

int build_p3_song_collection_untyped(void *p3sc,
        const songcollection *sc, const dataparameters *dp) {
    return build_p3_song_collection((p3songcollection *) p3sc, sc, dp);
}


/**
 * Clears and re-initializes the given P3 song collection.
 *
 * @param p3sc P3 song data
 */
void clear_p3_song_collection(p3songcollection *p3sc) {
    uint32_t i;
    for (i = 0; i < p3sc->size; i++) {
        free_p3_song(&p3sc->p3_songs[i]);
    }
    free(p3sc->p3_songs);
    p3sc->p3_songs        = NULL;
    p3sc->song_collection = NULL;
    p3sc->size            = 0;
}

void clear_p3_song_collection_untyped(void *p3sc) {
    clear_p3_song_collection((p3songcollection *) p3sc);
}


/**
 * Frees the given P3 song collection.
 *
 * @param p3sc P3 song data
 */
void unalloc_p3_song_collection(void *p3sc) {
    clear_p3_song_collection((p3songcollection *) p3sc);
    free(p3sc);
}


/**
 * Search a song collection with c_geometric_p3_scan().
 *
 * @param sc         song collection to scan
 * @param pattern    search pattern
 * @param alg        search algorithm ID (see algorithms.h).
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         match set for returning search results
 */
void alg_p3(const songcollection *sc, const song *pattern, int alg,
        const searchparameters *parameters, matchset *ms) {
    uint32_t i;
    p3songcollection *p3sc = (p3songcollection *) sc->data[DATA_P3];

    /* Unused parameter alg */
    (void) alg;

    if (p3sc == NULL) {
        fputs("Error in alg_p3: song collection does not contain P3 data.\n"
              "Use update_song_collection_data() to generate it.\n", stderr);
        return;
    }

    for (i = 0; i < p3sc->size; i++) {
        scan_p3(&p3sc->p3_songs[i], pattern, parameters, ms);
    }
}

/** 
 * Scanning phase of geometric algorithm P3. This algorithm is described in
 * Esko Ukkonen, Kjell Lemstrom and Veli Makinen: Sweepline the Music! In
 * Computer Science in Perspective (LNCS 2598), R. Klein, H.-W. Six, L. Wegner
 * (Eds.), pp. 330-342, 2003.
 *
 * The algorithm described in this article had a flaw that allowed the total
 * duration of a match to exceed the total duration of the pattern. This
 * problem has been solved by merging overlapping segments in the note data
 * during the calculation of turning points. However overlapping notes in the
 * pattern are currently not checked for. The problem can also be solved using
 * counters.
 *
 * Rough description of the algorithm: First a priority queue is created, which
 * will give translations in sorted order. Translation vectors for first source
 * note and all pattern notes are inserted to the queue. The priority queue is
 * then processed with the help of an array of vertical translations, which
 * stores value, slope and previous x translation for each vertical translation
 * y. For each vector extracted from the priority queue, we check the type of
 * the turning point associated with this vector and adjust the slope and value
 * accordingly. Then we move on in the source and add to the queue a new
 * translation vector for each new source note and the pattern note associated
 * with the previous translation vector. This loop is repeated until the end of
 * source. After the processing we have acquired the longest common time for
 * this pattern and source. This method returns only the best match for each
 * song. Consult the article for details.
 *
 * @param p3s song to scan
 * @param pattern pattern song
 * @param searchparameters search parameters
 * @param ms pointer to a structure where the results will be stored
 *
 * @return 1 when successful, 0 otherwise
 */
int scan_p3(const p3song *p3s, const song *pattern,
        const searchparameters *parameters, matchset *ms) {

    VerticalTranslationTableItem *verticaltranslationtable;
    VerticalTranslationTableItem *item = NULL;
    TranslationVector            *translation_vectors;

    TurningPoint *note_starts = p3s->note_starts;
    TurningPoint *note_ends   = p3s->note_ends;
    vector       *pnotes      = pattern->notes;
    const song   *s           = p3s->s;

    pq32node     *min;
    pq32          pq;

    uint32_t i, j, num_loops;
    int best      = 0;
    int beststart = 0;
    int bestend   = 0;
    int besty     = 0;
    float    bestsimilarity   = 0.0F;
    float    pattern_duration = 0.0F;
    float    difference       = 0.0F;
    uint32_t num_tpoints      = p3s->num_notes;
    uint32_t pattern_size     = pattern->size;

    /* Create a priority queue */
    
    if ((pattern_size == 0) || (num_tpoints == 0)) return 0;

    if (pq32_init(&pq, pattern_size * 4)) {
        fputs("Error in scan_p3: "
              "failed to initialize priority queue", stderr);
        return 0;
    }
    translation_vectors = (TranslationVector *)
            malloc(pattern_size * 4 * sizeof(TranslationVector));
    if (translation_vectors == NULL) {
        fputs("Error in scan_p3: failed to allocate temporary array "
              "'translation_vectors'", stderr);
        pq32_free(&pq);
        return 0;
    }

    /* Initialize a vertical translation array */
    verticaltranslationtable = (VerticalTranslationTableItem *) malloc(
            NOTE_PITCHES * 2 * sizeof(VerticalTranslationTableItem));
    if (verticaltranslationtable == NULL) {
        fputs("Error in scan_p3: failed to allocate temporary array "
              "'verticaltranslationtable'", stderr);
        free(translation_vectors);
        pq32_free(&pq);
        return 0;
    }

    for (i = 0; i < (NOTE_PITCHES * 2); i++) {
        verticaltranslationtable[i].value = 0;
        verticaltranslationtable[i].slope = 0;
        verticaltranslationtable[i].prev_x = 0;
    }

    /* Create an array whose items have two pointers each: one for note_starts
     * and one for note_ends. Each item points to turning point array item Also
     * populate the priority queue with initial items. */
    for (i = 0, j = 0; i < pattern_size; i++) {
        TranslationVector *v;
        pattern_duration += (float) pnotes[i].dur;


        /* Add translation vectors calculated from the note start points */

        min = pq32_getnode(&pq, j);
        v   = &translation_vectors[j];
        v->target_index  = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x -
                (int) (pnotes[i].strt + pnotes[i].dur);
        v->position = P3_POSITION_PE_TS;
        min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
        min->value = (int32_t) j;
        pq32_update(&pq, min);
        ++j;

        min = pq32_getnode(&pq, j);
        v   = &translation_vectors[j];
        v->target_index  = 0;
        v->pattern_index = i;
        v->y = (int) note_starts[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_starts[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TS;
        min->key1   = (v->x << 8) + v->y + NOTE_PITCHES;
        min->value  = (int32_t) j;
        pq32_update(&pq, min);
        ++j;


        /* Add translation vectors calculated from the note end points */

        min = pq32_getnode(&pq, j);
        v   = &translation_vectors[j];
        v->target_index  = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) (pnotes[i].strt +
                pnotes[i].dur);
        v->position = P3_POSITION_PE_TE;
        min->key1   = (v->x << 8) + v->y + NOTE_PITCHES;
        min->value  = (int32_t) j;
        pq32_update(&pq, min);
        ++j;

        min = pq32_getnode(&pq, j);
        v   = &translation_vectors[j];
        v->target_index  = 0;
        v->pattern_index = i;
        v->y = (int) note_ends[0].y - (int) pnotes[i].ptch;
        v->x = (int) note_ends[0].x - (int) pnotes[i].strt;
        v->position = P3_POSITION_PS_TE;
        min->key1   = (v->x << 8) + v->y + NOTE_PITCHES;
        min->value  = (int32_t) j;
        pq32_update(&pq, min);
        ++j;
    }

    best = 0;
    num_loops = (pattern_size * num_tpoints) << 2;

    for (i = 0; i < num_loops; i++) {
        int x, y;
        /* Get the minimum element */
        TranslationVector *v;
        min = pq32_getmin(&pq);
        v   = &translation_vectors[min->value];
        x   = v->x;
        y   = v->y;

        /* Update value */
        item = &verticaltranslationtable[NOTE_PITCHES + y];
        item->value += item->slope * (x - item->prev_x);
        item->prev_x = x;

        switch (v->position) {
            case P3_POSITION_PS_TE:
            case P3_POSITION_PE_TS:
                item->slope++;
                break;
            case P3_POSITION_PS_TS:
            case P3_POSITION_PE_TE:
                item->slope--;
                break;
        }

        /* Check for a match */
        if (item->value >= best) {
            beststart = x + pnotes[0].strt;
            bestend = x + pnotes[pattern_size - 1].strt +
                    pnotes[pattern_size - 1].dur;
            bestsimilarity = ((float) item->value) / pattern_duration;
            besty = y;
            best = item->value;
            /*printf("%d\n",  item->value);*/
        }

        /* Move the pointer and insert a new translation vector according to
         * the turning point type. */
        if (v->target_index < num_tpoints - 1) {
            const vector *patp = &pnotes[v->pattern_index];
            v->target_index++;

            if ((v->position == P3_POSITION_PS_TS) ||
                (v->position == P3_POSITION_PE_TS)) {

                const TurningPoint *startp = &note_starts[v->target_index];
                v->x = (int) startp->x - (int) patp->strt;
                v->y = (int) startp->y - (int) patp->ptch;
            } else {
                const TurningPoint *endp = &note_ends[v->target_index];
                v->x = (int) endp->x - (int) patp->strt;
                v->y = (int) endp->y - (int) patp->ptch;
            }
            if ((v->position == P3_POSITION_PE_TS) ||
                (v->position == P3_POSITION_PE_TE)) {
                v->x -= (int) patp->dur;
            }
            min->key1 = (v->x << 8) + v->y + NOTE_PITCHES;
            pq32_update(&pq, min);
        } else {
            /* 'Remove' a translation vector by making it very large.
             * It won't be extracted since there will be only as many loops as 
             * there are real vectors. */
            min->key1 = INT_MAX;
            pq32_update(&pq, min);
        }
    }

    if (s != NULL) {
        if (parameters->p3_calculate_difference) {
            for (i = 0; i < s->size; i++) {
                int start = s->notes[i].strt;
                int end   = start + s->notes[i].dur;
                if (start > bestend) break;
                if (end >= beststart) {
                    difference += (float)
                        (MIN2(bestend, end) - MAX2(beststart, start));
                }
            }
            difference = (float) best / difference;
        }
        insert_match(ms, s->id, beststart, bestend, (int8_t) besty,
                     bestsimilarity * difference);
    } else {
        fputs("Warning in scan_p3: original song data is not available. Skipping difference calculations.\n", stderr);
        insert_match(ms, 0, beststart, bestend, (int8_t) besty,
                bestsimilarity * difference);
    }

    /* Free the reserved memory. */
    pq32_free(&pq);
    free(translation_vectors);
    free(verticaltranslationtable);
    return 1;
}


/**
 * Compares two turning points lexicographically. This is used to preprocess
 * data for the P3 algorithm. See the article for algorithm's input
 * requirements.
 * 
 * @param aa a turning point
 * @param bb another turning point
 * @return -1 if aa comes before bb, 0 if they are the same and 1 if aa comes
 *         after bb
 */
int compare_turningpoints(const void *aa, const void *bb) {
    /* sort by x coordinate of the translation vector */
    const TurningPoint *a, *b;
    a = (const TurningPoint *) aa;
    b = (const TurningPoint *) bb;
    if (a->x < b->x)
        return -1;
    else if (a->x > b->x)
        return 1;
    else {
        if (a->y < b->y)
            return -1;
        else if (a->y > b->y)
            return 1;
        else
            return 0;
    }
}


