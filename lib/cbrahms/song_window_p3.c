/*
 * song_window_p3.c - Implements a sliding window within a P3 song
 *
 * Copyright (c) 2008-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <assert.h>

#include "song.h"
#include "geometric_p3.h"
#include "song_window_p3.h"
#include "util.h"


/**
 * Initializes a P3 song window.
 */
void init_p3s_window(p3s_window *w, const p3song *p3s) {
    w->x1 = 0;
    w->x2 = 0;
    w->s1 = 0;
    w->s2 = 0;
    w->e1 = 0;
    w->e2 = 0;
    memset(w->notes_on_1, 0, 4 * sizeof(unsigned int));
    memset(w->notes_on_2, 0, 4 * sizeof(unsigned int));
    w->p3s = p3s;
    w->songend = p3s->note_ends[p3s->num_notes-1].x;
    w->note_starts = (TurningPoint *) malloc(
            p3s->num_notes * sizeof(TurningPoint));
    w->note_ends   = (TurningPoint *) malloc(
            p3s->num_notes * sizeof(TurningPoint));
    w->window.note_starts = w->note_starts;
    w->window.note_ends   = w->note_ends;
    w->window.s           = p3s->s;
    w->window.num_notes   = 0;
    init_bitcount();
}


void free_p3s_window(p3s_window *w) {
    free(w->note_starts);
    free(w->note_ends);
    w->note_starts        = NULL;
    w->note_ends          = NULL;
    w->window.note_starts = NULL;
    w->window.note_ends   = NULL;
    w->window.s           = NULL;
    w->window.num_notes   = 0;
    w->p3s = NULL;
}


/**
 * Selects a known position that is closest to the new position.
 */
static void move_p3s_window_edge_init(const p3s_window *w,
        int32_t new_x, int32_t *x, uint32_t *s, uint32_t *e,
        uint32_t *notes_on) {

    int32_t delta = ABS(new_x - *x);

    if (new_x < ((w->x1 + w->x2) >> 1)) {
        if (new_x < (w->x1 >> 1)) {
            /* Return if the edge is already at the closest position */
            if (delta <= new_x) return;
            *x = 0;
            memset(notes_on, 0, 4 * sizeof(unsigned int));
            *s = 0;
            *e = 0;
        } else {
            if (delta <= ABS(new_x - w->x1)) return;
            *x = w->x1;
            memcpy(notes_on, w->notes_on_1, 4 * sizeof(unsigned int));
            *s = w->s1;
            *e = w->e1;
        }
    } else {
        if (new_x > ((w->x2 + w->songend) >> 1)) {
            if (delta <= ABS(new_x - w->songend)) return;
            *x = w->songend;
            memset(notes_on, 0, 4 * sizeof(unsigned int));
            *s = w->p3s->num_notes;
            *e = w->p3s->num_notes;
        } else {
            if (delta <= ABS(new_x - w->x2)) return;
            *x = w->x2;
            memcpy(notes_on, w->notes_on_2, 4 * sizeof(unsigned int));
            *s = w->s2;
            *e = w->e2;
        }
    }
}


/**
 * Keeps track of playing notes while moving a window edge.
 */
static void move_p3s_window_edge(int new_x, int old_x,
        uint32_t *spos, uint32_t *epos, uint32_t *notes_on,
        uint32_t num_turningpoints, TurningPoint *note_starts,
        TurningPoint *note_ends, int starting_edge) {

    int32_t next_spos = (int32_t) *spos;
    int32_t next_epos = (int32_t) *epos;

    if (new_x == old_x) {
        if (starting_edge) old_x = new_x - 1;
        else old_x = new_x + 1;
    }
    if (new_x > old_x) {
        /* Scan forward */
        while(1) {
            int32_t start = 1;
            int32_t cur_x;
            int32_t cur_y;
            int slot, bit;

            if (next_spos < (int32_t) num_turningpoints) {
                cur_x = note_starts[next_spos].x;
                if ((next_epos < (int32_t) num_turningpoints) &&
                        (cur_x >= note_ends[next_epos].x)) {
                    start = 0;
                    cur_x = note_ends[next_epos].x;
                    cur_y = note_ends[next_epos].y;
                } else {
                    cur_y = note_starts[next_spos].y;
                }
            } else if (next_epos < (int32_t) num_turningpoints) {
                start = 0;
                cur_x = note_ends[next_epos].x;
                cur_y = note_ends[next_epos].y;
            } else break;
            if (starting_edge) {
                if (cur_x > new_x) break;
            } else if (cur_x >= new_x) break;

            /* Calculate pitch vector bit position for the note */
            slot = cur_y >> 5;
            bit = cur_y - (slot << 5);

            if (start) {
                /* Set the corresponding bit in the vector */
                notes_on[slot] = notes_on[slot] | (uint32_t) (1 << bit);
                next_spos++;
            } else {
                /* Clear the corresponding bit in the vector */
                notes_on[slot] = notes_on[slot] &
                                 (0xFFFFFFFF - (uint32_t) (1 << bit));
                next_epos++;
            }
        }
    } else if (new_x < old_x) {
        /* Scan backward */
        next_epos--;
        next_spos--;
        while(1) {
            int32_t end = 1;
            int32_t cur_x;
            int32_t cur_y;
            int slot, bit;

            if (next_epos >= 0) {
                cur_x = note_ends[next_epos].x;
                if ((next_spos >= 0) && (cur_x <= note_starts[next_spos].x)) {
                    end = 0;
                    cur_x = note_starts[next_spos].x;
                    cur_y = note_starts[next_spos].y;
                } else {
                    cur_y = note_ends[next_epos].y;
                }
            } else if (next_spos >= 0) {
                end = 0;
                cur_x = note_starts[next_spos].x;
                cur_y = note_starts[next_spos].y;
            } else break;
            if (starting_edge) {
                if (cur_x <= new_x) break;
            } else if (cur_x < new_x) break;

            /* Calculate pitch vector bit position for the note */
            slot = cur_y >> 5;
            bit = cur_y - (slot << 5);

            if (end) {
                /* Set the corresponding bit in the vector */
                notes_on[slot] = notes_on[slot] | (uint32_t) (1 << bit);
                next_epos--;
            } else {
                /* Clear the corresponding bit in the vector */
                notes_on[slot] = notes_on[slot] &
                                 (0xFFFFFFFF - (uint32_t) (1 << bit));
                next_spos--;
            }
        }
        next_spos++;
        next_epos++;
    }
    *spos = (uint32_t) next_spos;
    *epos = (uint32_t) next_epos;
}


/**
 * Adds turningpoints for notes that are on at the window edge.
 */
static void move_p3s_window_mark_playing_notes(uint32_t *notes_on,
        int time, uint32_t num_turningpoints, TurningPoint *tp,
        uint32_t *pos, int start) {

    int i;
    uint32_t p = *pos;
    uint32_t num_points = bitcount32(notes_on[0]) +
                          bitcount32(notes_on[1]) +
                          bitcount32(notes_on[2]) +
                          bitcount32(notes_on[3]);
    if (!num_points) return;
    if (start) {
        if (p < num_points) {
            p = 0;
        } else {
            p -= num_points;
        }
        *pos = p;
    } else {
        if (p + num_points > num_turningpoints) {
            fprintf(stderr, "Too many points: %d (limit: %d)\n",
                    p + num_points, num_turningpoints); 
            return;
        }
        *pos = p + num_points;
    }
    for (i = 0; i < 4; i++) {
        int j;
        unsigned int on_32 = notes_on[i];
        if (!on_32) continue;
        for (j = 0; j < 4; j++) {
            int k;
            unsigned char on_8 = on_32 & 0xFF;
            on_32 >>= 8;
            if (!on_8) continue;
            for (k = 0; k < 8; k++) {
                unsigned char on_1 = on_8 & 0x1;
                on_8 >>= 1;
                if (on_1) {
                    tp[p].x = time;
                    tp[p].y = (i << 5) + (j << 3) + k;
                    p++;
                }
            }
        }
    }
}

/**
 * Moves a window within P3 song.
 */
void move_p3s_window(p3s_window *w, int x1, int x2) {
    const p3song *s     = w->p3s;
    TurningPoint *sp    = s->note_starts;
    TurningPoint *ep    = s->note_ends;
    uint32_t      ssize = s->num_notes;
    uint32_t      i, j;

    uint32_t notes_on_1[4], notes_on_2[4];
    uint32_t s1 = 0, e1 = 0;
    uint32_t s2, e2;
    int32_t  old_x1 = 0, old_x2;

    if (x1 > x2) INT_SWAP(x1, x2);

    /* Move the left edge */
    memset(notes_on_1, 0, 4 * sizeof(unsigned int));
    move_p3s_window_edge_init(w, x1, &old_x1, &s1, &e1, notes_on_1);
    move_p3s_window_edge(x1, old_x1, &s1, &e1, notes_on_1, ssize, sp, ep, 1);

    /* Move the right edge */
    old_x2 = x1;
    s2 = s1;
    e2 = e1;
    memcpy(notes_on_2, notes_on_1, 4 * sizeof(unsigned int));
    move_p3s_window_edge_init(w, x2, &old_x2, &s2, &e2, notes_on_2);
    move_p3s_window_edge(x2, old_x2, &s2, &e2, notes_on_2, ssize, sp, ep, 0);

    memcpy(w->notes_on_1, notes_on_1, 4 * sizeof(unsigned int));
    memcpy(w->notes_on_2, notes_on_2, 4 * sizeof(unsigned int));

    /* Copy turning points that are not yet in place */

    /* note_starts */
    if (s1 < w->s1) {
        i = s1;
        j = MIN2(w->s1, s2);
        memcpy(&w->note_starts[i], &sp[i], (j - i) * sizeof(TurningPoint));
    }
    if (s2 > w->s2) {
        i = MAX2(w->s2, s1);
        j = s2;
        memcpy(&w->note_starts[i], &sp[i], (j - i) * sizeof(TurningPoint));
    }

    /* note_ends */
    if (e1 < w->e1) {
        i = e1;
        j = MIN2(w->e1, e2);
        memcpy(&w->note_ends[i], &ep[i], (j - i) * sizeof(TurningPoint));
    }
    if (e2 > w->e2) {
        i = MAX2(w->e2, e1);
        j = e2;
        memcpy(&w->note_ends[i], &ep[i], (j - i) * sizeof(TurningPoint));
    }

    i = s1;
    j = e2;
    move_p3s_window_mark_playing_notes(notes_on_1, x1, ssize, w->note_starts,
            &i, 1);
    move_p3s_window_mark_playing_notes(notes_on_2, x2, ssize, w->note_ends,
            &j, 0);

    w->s1 = s1;
    w->e1 = e1;
    w->s2 = s2;
    w->e2 = e2;
    w->x1 = x1;
    w->x2 = x2;
    w->window.note_starts = &w->note_starts[i];
    w->window.note_ends = &w->note_ends[e1];
    i = s2 - i;
    assert(i == j - e1);
/*  
    if (i != j - e1) {
        fprintf(stderr, "Error: different number of note start and end points: %d and %d\n", i, j - e1);
    }
*/
    w->window.num_notes = i;
}


#ifdef ENABLE_UNIT_TESTS

void test_p3s_window(const song *s, int moves, int resets, int window_length) {

    int r;
    p3song p3s;
    p3s_window sw;
    song testw;
    p3song testw_p3s;
    int x1, x2;
    int song_duration;

    song_to_p3(s, &p3s);

    song_duration = p3s.note_ends[s->size-1].x;

    init_song(&testw, 0, NULL, s->size);

    init_p3s_window(&sw, &p3s);

    for (r=0; r<resets; ++r) {
        int m;
        x1 = (int) (randf() * 0.5F * (float) song_duration);
        x2 = x1 + (int) (randf() * (float) window_length);
        for (m=0; m<moves; ++m) {
            /* Scan in a slower way to produce a ground truth */
            int i;
            testw.size = 0;
            if (x1 > x2) INT_SWAP(x1, x2);
            for (i=0; i<s->size; ++i) {
                vector *n = &s->notes[i];
                int start = n->strt;
                int end = start + n->dur;
                if ((start < x2) && (end > x1)) {
                    vector *tn = &testw.notes[testw.size];
                    memcpy(tn, n, sizeof(vector));
                    tn->strt = MAX2(start, x1);
                    tn->dur = MIN2(end, x2) - MAX2(start, x1);
                    ++testw.size;
                }
            }
            lexicographic_sort(&testw);
            song_to_p3(&testw, &testw_p3s);

            /* Move the window */
            move_p3s_window(&sw, x1, x2);

            /* Compare to ground truth */
            for (i=0; i<testw_p3s.size; ++i) {
                TurningPoint *sp = &testw_p3s.note_starts[i];
                if (i >= sw.window.size) {
                    fprintf(stderr, "Error in test_p3s_window: too few notes within the window (%d instead of %d)\n", sw.window.size, testw_p3s.size);
                    break;
                }
                if (sp->x != sw.window.note_starts[i].x) {
                    fprintf(stderr, "Error in test_song_window: wrong note start time (%d instead of %d)\n", sw.window.note_starts[i].x, sp->x);
                    break;
                }
                if (sp->y != sw.window.note_starts[i].y) {
                    fprintf(stderr, "Error in test_song_window: wrong note pitch (%d instead of %d)\n", sw.window.note_starts[i].y, sp->y);
                    break;
                }
            }
            if (i < testw_p3s.size) {
                fprintf(stderr, "Window x1:%d, x2:%d\n", x1, x2);
                fputs("GT start: ", stderr);
                for (i=0; i<testw_p3s.size; ++i) {
                    TurningPoint *sp = &testw_p3s.note_starts[i];
                    fprintf(stderr, "(%d, %d) ", sp->x, sp->y);
                }
                fputs("\nWindow:   ", stderr);
                for (i=0; i<sw.window.size; ++i) {
                    TurningPoint *sp = &sw.window.note_starts[i];
                    fprintf(stderr, "(%d, %d) ", sp->x, sp->y);
                }
                fputs("\n\n", stderr);
            }
            for (i=0; i<testw_p3s.size; ++i) {
                TurningPoint *ep = &testw_p3s.note_ends[i];
                if (i >= sw.window.size) {
                    fprintf(stderr, "Error in test_p3s_window: too few notes within the window (%d instead of %d)\n", sw.window.size, testw_p3s.size);
                    break;
                }
                if (ep->x != sw.window.note_ends[i].x) {
                    fprintf(stderr, "Error in test_song_window: wrong note end time (%d instead of %d)\n", sw.window.note_ends[i].x, ep->x);
                    break;
                }
                if (ep->y != sw.window.note_ends[i].y) {
                    fprintf(stderr, "Error in test_song_window: wrong note pitch (%d instead of %d)\n", sw.window.note_ends[i].y, ep->y);
                    break;
                }
            }
            if (i < testw_p3s.size) {
                fprintf(stderr, "Window x1:%d, x2:%d\n", x1, x2);
                fputs("GT end: ", stderr);
                for (i=0; i<testw_p3s.size; ++i) {
                    TurningPoint *ep = &testw_p3s.note_ends[i];
                    fprintf(stderr, "(%d, %d) ", ep->x, ep->y);
                }
                fputs("\nWindow: ", stderr);
                for (i=0; i<sw.window.size; ++i) {
                    TurningPoint *ep = &sw.window.note_ends[i];
                    fprintf(stderr, "(%d, %d) ", ep->x, ep->y);
                }
                fputs("\n\n", stderr);
            }
            free_p3_song(&testw_p3s);

            x1 += (int) (randf() * 2.0F * (float) window_length);
            x2 += (int) (randf() * 2.0F * (float) window_length);
        }
    }

    free_song(&testw);
    free_p3s_window(&sw);
    free_p3_song(&p3s);
}

#endif /* ENABLE_UNIT_TESTS */

