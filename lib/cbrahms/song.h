/*
 * song.h - Structures and external declarations for the song collection
 *          functions
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __SONG_H__
#define __SONG_H__

/*#include "config.h"*/
#include "data_formats.h"
#include "results.h"

#ifdef __cplusplus
extern "C" {
#endif


/** Number of discrete note pitches. Since the data mostly comes from MIDI
 * files, 128 is a natural choise. */
#define NOTE_PITCHES 128

/** Maximum song duration in milliseconds. See vector.strt below */
#define MAX_SONG_LENGTH 1 << 23


/**
 * A two-dimensional note vector for the geometric algorithms.
 */
typedef struct {
    /* Note start time in 1/1024 seconds. If this is not accurate enough or
     * does not give enough room, the data needs to be scaled appropriately.
     *
     * NOTE: Some of the algorithm implementations only support 23-bit time
     * values, which means 8192 seconds for the whole song (the last 8 bits
     * are used to store the pitch to the same 32-bit vector and the direction
     * sign needs one bit). */
    int32_t strt;

    /* Duration of the note in 1/1024 seconds */
    int32_t dur;

    /* Instrument/track id in the song structure */
    uint16_t instrument;

    /* MIDI pitch value */
    int8_t ptch;

    /* Note velocity or volume */
    uint8_t velocity;
} vector;


/**
 * Instrument and track metadata.
 */
typedef struct {
    char *track_name;
    char *module_name;
    char *program_name;

    /* MIDI track number */
    int16_t track;

    /* MIDI bank number */
    int16_t bank;

    /* MIDI channel and port number.
     * The actual channel number (0-15) is (channel % 16) and
     * the MIDI port is (channel / 16). */
    uint8_t channel;

    /* MIDI program (voice) number */
    int8_t program;

    /*PAD_STRUCT(2)*/
} instrument;


/**
 * A set of instruments.
 */
typedef struct {
    instrument *i;
    uint32_t size;

    /*PAD_STRUCT_64(4)*/
} instrumentdata;


/**
 * A generic MIDI event. This is used to describe events that are not
 * important to this system, but that are still handy to have around.
 */
typedef struct {
    /* Pointer to more event data */
    uint8_t *data;

    /* Event time */
    int32_t time;

    /* Note or instrument that is affected by this event. Negative
       values refer to instrument IDs, positive values refer to notes. */
    int32_t target;

    /* Event type */
    int32_t type;

    /* Event value */
    int32_t value;
} midievent;


/**
 * A set of MIDI events
 */
typedef struct {
    midievent *e;
    uint32_t size;

    /*PAD_STRUCT_64(4)*/
} mididata;


/**
 * A song record.
 */
typedef struct {
    /* Song title */
    char *title;

    /* ID number or position in a song collection array */
    uint32_t id;

    /* Length of song time unit in nanoseconds */
    int32_t tu_ns;

    /* Time unit numerator */
    int32_t tu_num;

    /* Time unit denominator */
    int32_t tu_denom;

    /* Start of playback in time units */
    int32_t start;

    /* End of playback in milliseconds */
    int32_t end;

    /* Total duration of played notes in milliseconds */
    int32_t total_note_duration;

    /* Number of notes in the song */
    uint32_t size;

    /* Notes */
    vector *notes;

    /* Instrument descriptions */
    instrumentdata instruments;

    /* MIDI control message track. */
    mididata control;

    /* Tempo and time signature track. */
    mididata tempo;
} song;


#if 0
/**
 * A container for song data in algorithm-specific formats.
 */
typedef struct {
    /* Data format, as defined in algorithms.h */
    int32_t format;

    /* Song collection data */
    void *data;
} songdata;
#endif

/**
 * A song collection.
 */
typedef struct {
    /* Songs */
    song *songs;

    /* Algorithm-specific data formats and indices.
     * This is an array of pointers indexed by data format IDs
     * (DATA_* in algorithms.h). Also see update_song_collection_data() */
    void *data[NUM_DATA_FORMATS+1];

    /* Number of songs in the collection */
    uint32_t size;

    /* Overall number of notes in the collection. */
    uint32_t num_notes;

    /* Number of notes in the largest song. */
    uint32_t max_song_size;

    /*PAD_STRUCT_64(4)*/
} songcollection;



/* External function declarations */


void init_song(song *s, uint32_t id, const char *title, uint32_t maxnotes);
void free_song(song *s);

int32_t  init_song_collection(songcollection *sc, uint32_t maxsongs);
void     free_song_collection(songcollection *sc);

uint32_t read_songfile(const char *songfile, songcollection *sc);

int32_t  generate_pattern(song *pattern, const songcollection *sc, match *m,
        uint32_t size, uint32_t maxskip, int8_t maxtranspose, float errors);

uint32_t generate_pattern_collection(uint32_t patterncount,
        songcollection *pc, const songcollection *sc, matchset *ms,
        uint32_t minsize, uint32_t maxsize, uint32_t maxskip,
        int8_t maxtranspose, float errors);

void preprocess_songs(songcollection *sc, int q, int trim_margin,
        int trim_step, int shuffle, uint32_t notes);

void lexicographic_sort(song *s);

int32_t compare_notes(const void *aa, const void *bb);

void remove_overlap(song *s);

void quantize(song *s, int32_t q);

void extract_song_rests(const song *s, int32_t start, int32_t end,
        song *rests);

void trim_song(song *s, int32_t margin, int32_t step);

void update_song_statistics(song *s);

void print_time_interval_histogram(song *s);

void remove_octave_information(song *s);

void p3_optimize_song_collection(songcollection *sc, int32_t mindur);

void p3_optimize_song(song *song, int32_t mindur);

void sc_remove_octave_information(const songcollection *sc);

songcollection *join_songs(songcollection *sc, int32_t gap,
        uint32_t num_songs, uint32_t song_size, int shuffle);

void update_song_collection_statistics(songcollection *sc);

void free_song_collection_data(songcollection *sc);

void update_song_collection_data(songcollection *sc,
        const int32_t *algorithms, const dataparameters *dp);

void insert_pattern_to_song(song *s, uint32_t songpos, const song *p,
        float errors, float noise);

void insert_patterns(songcollection *sc, const songcollection *pc,
        uint32_t pattern_instances, float errors, float noise);

int read_mirex07_fundamental_frequency_file(const char *path, song *s,
        double linear_drift);

#ifdef __cplusplus
}
#endif

#endif

