/*
 * song_window_p3.h - External function declarations for P3 song window
 *
 * Copyright (c) 2008-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __SONG_WINDOW_P3_H__
#define __SONG_WINDOW_P3_H__

#include <stdint.h>

#include "config.h"
#include "song.h"
#include "geometric_p3.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * P3 song window data structure.
 */
typedef struct {
    const p3song *p3s;
    TurningPoint *note_starts;
    TurningPoint *note_ends;
    p3song   window;
    int32_t  x1, x2;
    uint32_t s1, s2;
    uint32_t e1, e2;
    int32_t  songend;
    uint32_t notes_on_1[4];
    uint32_t notes_on_2[4];
} p3s_window;


void init_p3s_window(p3s_window *w, const p3song *p3s);

void free_p3s_window(p3s_window *w);

void move_p3s_window(p3s_window *w, int32_t x1, int32_t x2);

void test_p3s_window(const song *s, int moves, int resets,
                     int32_t window_length);

#ifdef __cplusplus
}
#endif

#endif

