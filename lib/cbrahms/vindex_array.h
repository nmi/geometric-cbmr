/*
 * vindex_array.h - Structures and external declarations for the index
 *                  functions of vindex_array
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __VINDEX_ARRAY_H__
#define __VINDEX_ARRAY_H__

#include "config.h"
#include "data.h"
#include "song.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Maximum number of vectors allowed to be stored into a single index bucket.
 */
#define MAX_INDEX_BUCKET_SIZE INT_MAX
/* #define MAX_INDEX_BUCKET_SIZE 1000 */



/**
 * Index record (a single position of a vector).
 */
typedef struct vindexrec {
    uint16_t song;
    uint16_t note;
} vindexrec;

/**
 * Indexed vector (contains multiple position records).
 */
typedef struct vindextable {
    uint32_t  size;
    /* Variable-length buffer for records. This is called "flexible array
       member" in C99. */
    vindexrec records[];
} vindextable;


/**
 * Vector index structure.
 */
typedef struct vindex {
    /* Number of index vectors. In this implementation this is
     * the same as width * height. */
    uint32_t size;
    /* Maximum vector width */
    int32_t width;
    /* Maximum vector height */
    int32_t height;
    /* Maximum number of consecutive notes the algorithm is allowed to skip 
     * when building an index for a song collection */
    uint32_t c_window;
    /* A continuous buffer of index records for all the vectors */
    uint8_t *buffer;
    /* Pointers to vindextables stored within the buffer */
    vindextable **tables;
    /* The indexed song collection */
    const songcollection *scollection;
    /* Index memory usage in bytes */
    size_t memory_usage;
} vindex;



/* Global inline functions */


/**
 * Returns the position of a table in the index structure.
 *
 * @param vindex index structure
 * @param x      vector's x component
 * @param y      vector's y component
 *
 * @return position of the table for a given vector
 */
static inline int32_t vindex_table_pos(const vindex *vi,
        int32_t x, int32_t y) {
    y += vi->height >> 1;
    if ((y < 0) || (x < 0) || (y >= vi->height) || (x >= vi->width))
        return -1;
    return (y * vi->width + x);
}

#define vindex_table_by_pos(vi, pos) \
    ((pos >= 0) ? (vi)->tables[pos] : NULL)

static inline vindextable *vindex_table_by_vector(const vindex *vi,
        int32_t x, int32_t y) {
    y += vi->height >> 1;
    if ((y < 0) || (x < 0) || (y >= vi->height) || (x >= vi->width))
        return NULL;
    return vi->tables[y * vi->width + x];
}

/* External function declarations. */


vindex *vindex_alloc(void);
void   *vindex_alloc_untyped(void);

int vindex_build(vindex *vi, const songcollection *sc,
	const dataparameters *dp);
int vindex_build_untyped(void *vi, const songcollection *sc,
	const dataparameters *dp);

void vindex_clear(vindex *vi);
void vindex_clear_untyped(void *vi);

void vindex_unalloc(vindex *vi);
void vindex_unalloc_untyped(void *vi);


#ifdef __cplusplus
}
#endif

#endif

