/*
 * filter_p2.c - Vector index search filters based on the P2 algorithm.
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>


#include "config.h"
#include "filter_p2.h"
#include "geometric_p1.h"
#include "geometric_p2.h"
#include "pq32.h"
#include "results.h"
#include "vindex.h"
#include "util.h"

#ifdef ENABLE_BLOSSOM4
#include "match.h"
#endif


#define MAX_EDGE_WEIGHT 1000000

/**
 * P2/F6-greedy, index filter that is based on the pigeonhole principle and
 * use of P1 index filters. Error tolerance of this filter is controlled
 * by p2_select_threshold in the search parameters struct: 0.0 means no
 * expected corruption, 0.5 means that half of the notes may be wrong in
 * potential matches. 0.5 is the highest error rate that this filter can
 * handle without loss in accuracy. Lower values make the algorithm run
 * faster, and it should still find most of the matches whose error rate
 * is higher than the specified tolerance level.
 *
 * @param sc         a song collection
 * @param pattern    search pattern
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         information about the found matches will be stored here
 */
void filter_p2_greedy_pigeonhole(const songcollection *sc,
        const song *pattern, int32_t alg,
        const searchparameters *parameters, matchset *ms) {

    vector    *pnotes   = pattern->notes;
    song      *songs    = sc->songs;
    vindex    *vi       = (vindex *) sc->data[DATA_VINDEX];
    fp2vector *chosenvectors;
    char      *selected;

    pq32     pq;
    uint32_t i, num_pattern_vectors, vcount = 0;

    /* Parameter alg is unused */
    (void) alg;

    if (vi == NULL) {
        if (vi == NULL) {
            fputs("Error in filter_p2_greedy_pigeonhole(): "
                  "song collection does not contain\n"
                  "vectorindex data. Use update_song_collection_data() "
                  "before calling this function\n", stderr);
        }
        return;
    }

    selected = (char *) calloc(pattern->size, sizeof(char));
    if (selected == NULL) {
        fputs("Error in filter_p2_window(): failed to allocate memory "
              "for temporary table 'selected'", stderr);
        return;
    }

    chosenvectors = (fp2vector *) malloc(pattern->size * sizeof(fp2vector));
    if (chosenvectors == NULL) {
        fputs("Error in filter_p2_window(): failed to allocate memory "
              "for temporary table 'chosenvectors'", stderr);
        free(selected);
        return;
    }

    num_pattern_vectors = (uint32_t) lrintf(
            parameters->p2_select_threshold * (float) pattern->size);
    if (num_pattern_vectors <= 0) num_pattern_vectors = 1;

    for (i = 0; i < pattern->size - 1; i++) {
        uint32_t j;
        uint32_t end = i + parameters->d_window + 1;
        chosenvectors[i].patternpos = i;
        chosenvectors[i].vit = NULL;
        if (selected[i]) continue;
        if (end > pattern->size) end = pattern->size;
        for (j = i + 1; j < end; j++) {
            vindextable *vit;
            int32_t      x, y;
            if (selected[j]) continue;
            x = pnotes[j].strt - pnotes[i].strt;
            y = pnotes[j].ptch - pnotes[i].ptch;
            vit = vindex_table_by_vector(vi, x, y);
            if (vit != NULL) {
                if ((chosenvectors[i].vit == NULL) ||
                        (vit->size < chosenvectors[i].vit->size)) {
                    chosenvectors[i].t1  = j;
                    chosenvectors[i].vit = vit;
                }
            }
        }
        if (chosenvectors[i].vit != NULL) {
            selected[chosenvectors[i].t1] = 1;
            ++vcount;
        }
    }
    free(selected);

    if (vcount == 0) {
        free(chosenvectors);
        return;
    }

    /* Sort the vectors by their frequencies */
    if (pq32_init(&pq, vcount)) {
        fputs("Error in filter_p2_greedy_pigeonhole: failed to initialize "
              "priority queue", stderr);
        free(chosenvectors);
        return;
    }
    vcount = 0;
    for (i = 0; i < pattern->size - 1; i++) {
        if (chosenvectors[i].vit != NULL) {
            pq32node *node = pq32_getnode(&pq, vcount);
            node->key1 = (int) chosenvectors[i].vit->size;
            node->key2 = (int) i;
            pq32_update(&pq, node);
            ++vcount;
        }
    }

    vcount = 0;
    while (vcount < num_pattern_vectors) {
        vindexrec *records;
        uint32_t   k;

        pq32node *min = pq32_getmin(&pq);

        if (min->key1 == INT32_MAX) break;
        i = (uint32_t) min->key2;

        records = chosenvectors[i].vit->records;
        for (k = 0; k < chosenvectors[i].vit->size; k++) {
            song *s = &songs[records[k].song];
            alignment_check_p2(s, records[k].note, pattern, i, ms); 
        }

        min->key1 = INT32_MAX;
        min->key2 = INT32_MAX;
        pq32_update(&pq, min);
        ++vcount;
    }
    /*fprintf(stderr, "Vectors: %d\n", vcount);*/
    pq32_free(&pq);
    free(chosenvectors);
}


#ifdef ENABLE_BLOSSOM4
/**
 * P2/F6, index filter that is based on the pigeonhole principle and
 * use of P1 index filters. This version uses a graph matching algorithm to
 * retrieve an optimal set of vectors.
 *
 * Error tolerance of this filter is controlled by p2_select_threshold in
 * the search parameters struct: 0.0 means no expected corruption,
 * 0.5 means that half of the notes may be wrong in the potential matches.
 * 0.5 is the highest error rate that this filter can handle without loss
 * in accuracy. Lower values make the algorithm run faster, and it should
 * still find most of the matches whose error rate is higher than
 * the specified tolerance level.
 *
 * @param sc         a song collection
 * @param pattern    search pattern
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         information about the found matches will be stored here
 */
void filter_p2_pigeonhole(const songcollection *sc, const song *pattern,
        int32_t alg, const searchparameters *parameters, matchset *ms) {
    int     *edges, *edgelengths, *graph_match;
    song    *songs  = sc->songs;
    vector  *pnotes = pattern->notes;
    vindex  *vi     = (vindex *) sc->data[DATA_VINDEX];
    pq32     pq;
#ifdef ENABLE_TIME_MEASUREMENTS
    struct timeval t1, t2;
#endif
    uint32_t i, num_pattern_vectors, vcount = 0;

    if (vi == NULL) {
        fputs("Error in filter_p2_pigeonhole(): song collection does not contain vectorindex data.\nUse update_song_collection_data() before calling this function.\n", stderr);
        return;
    }

    edges = (int *) malloc(sizeof(int) *
                           (1 + 2 * pattern->size * parameters->d_window));
    if (edges == NULL) {
        fputs("Error in filter_p2_pigeonhole: failed to allocate memory "
              "for temporary table 'edges'", stderr);
        return;
    }

    edgelengths = (int *) malloc(sizeof(int) *
                  (1 +     pattern->size * parameters->d_window));
    if (edgelengths == NULL) {
        fputs("Error in filter_p2_pigeonhole: failed to allocate memory "
              "for temporary table 'edgelengths'", stderr);
        free(edges);
        return;
    }

    graph_match = (int *) calloc(pattern->size * 2, sizeof(int));
    if (graph_match == NULL) {
        fputs("Error in filter_p2_pigeonhole: failed to allocate memory "
              "for temporary table 'graph_match'", stderr);
        free(edges);
        free(edgelengths);
        return;
    }


    if (pq32_init(&pq, (pattern->size >> 1) + 1)) {
        fputs("Error in filter_p2_pigeonhole: failed to initialize "
              "priority queue", stderr);
        free(edges);
        free(edgelengths);
        free(graph_match);
        return;
    }

    num_pattern_vectors = (uint32_t) lrintf(
            parameters->p2_select_threshold * (float) pattern->size);
    if (num_pattern_vectors <= 0) num_pattern_vectors = 1;

#ifdef ENABLE_TIME_MEASUREMENTS
    if (ms->measure_time_consumption) gettimeofday(&t1, NULL);
#endif

    for (i = 0; i < pattern->size; i++) {
        uint32_t j;
        /* Keep track of vertices so that those with no connecting
           edges can be located. */
        for (j = i + 1; j <= i + parameters->d_window; j++) {
            int32_t x, y;
            vindextable *vit;
            if (j >= pattern->size) break;
            x = pnotes[j].strt - pnotes[i].strt;
            y = pnotes[j].ptch - pnotes[i].ptch;
            vit = vindex_table_by_vector(vi, x, y);
            if (vit != NULL) {
                /* Mark these vertices connected */
                graph_match[i] = 1;
                graph_match[j] = 1;

                edgelengths[vcount >> 1] = (int) vit->size;
                edges[vcount] = (int) i;
                ++vcount;
                edges[vcount] = (int) j;
                ++vcount;
                /*printf("edge: %d->%d, %d\n", i, j, iv->size);*/
            }
        }
        if (graph_match[i] == 0) {
            fputs("Warning: Unconnected vertex -- adding a dummy edge\n",
                    stderr);
/*
            edgelengths[vcount >> 1] = MAX_EDGE_WEIGHT;
            edges[vcount] = (int32_t) i;
            ++vcount;
            edges[vcount] = (int32_t) pattern->size;
            ++vcount;
*/
        } else graph_match[i] = 0;
    }

#ifdef ENABLE_TIME_MEASUREMENTS
    if (ms->measure_time_consumption) {
        gettimeofday(&t2, NULL);
        ms->time.indexing += timediff(&t2, &t1);
    }
#endif

    vcount >>= 1;
    if (blossom4_match((int) pattern->size, (int) vcount, edges, edgelengths,
                       0, 0, 0, 0, graph_match)) {
#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t1, NULL);
            ms->time.other += timediff(&t1, &t2);
        }
#endif
        fputs("Error in filter_p2_pigeonhole: graph matching failed.\n"
              "Running a greedy search instead.\n", stderr);
        filter_p2_greedy_pigeonhole(sc, pattern, alg, parameters, ms);
#ifdef ENABLE_TIME_MEASUREMENTS
        if (ms->measure_time_consumption) {
            gettimeofday(&t2, NULL);
            ms->time.verifying += timediff(&t2, &t1);
        }
#endif
        goto EXIT;
    }
    free(edges);
    free(edgelengths);
    edges = NULL;
    edgelengths = NULL;

#ifdef ENABLE_TIME_MEASUREMENTS
    if (ms->measure_time_consumption) {
        gettimeofday(&t1, NULL);
        ms->time.other += timediff(&t1, &t2);
    }
#endif

    /* Sort the vectors by their frequencies */
    vcount = 0;
    for (i = 0; i < pattern->size; i += 2) {
        vindextable *vit;
        pq32node    *node;
        int          x, y;
        int          vit_pos;

        if ((graph_match[i] >= (int) pattern->size) ||
                (graph_match[i+1] >= (int) pattern->size)) {
            /* Dummy edge */
            continue;
        }

        if (graph_match[i] == graph_match[i+1]) break;

        x = pnotes[graph_match[i+1]].strt - pnotes[graph_match[i]].strt;
        y = pnotes[graph_match[i+1]].ptch - pnotes[graph_match[i]].ptch;
        vit_pos = vindex_table_pos(vi, x, y);
        vit = vindex_table_by_pos(vi, vit_pos);

        node = pq32_getnode(&pq, vcount);
        if (vit != NULL) {
            node->value = vit_pos;
            node->key1  = (int) vit->size;
        } else {
            node->value = -1;
            node->key1  = 0;
        }
        node->key2 = graph_match[i];
        pq32_update(&pq, node);
        vcount++;
    }

#ifdef ENABLE_TIME_MEASUREMENTS
    if (ms->measure_time_consumption) {
        gettimeofday(&t2, NULL);
        ms->time.indexing += timediff(&t2, &t1);
    }
#endif

    if (vcount > num_pattern_vectors) vcount = num_pattern_vectors;
    while (vcount > 0) {
        vindextable *vit;
        uint32_t     k;

        pq32node *min = pq32_getmin(&pq);

        if (min->key1 == INT_MAX) break;
        i = (uint32_t) min->key2;
        vit = vindex_table_by_pos(vi, min->value);
        if (vit != NULL) {
            for (k = 0; k < vit->size; k++) {
                song *s = &songs[vit->records[k].song];
                alignment_check_p2(s, vit->records[k].note, pattern, i, ms); 
            }
        }
        min->key1 = INT_MAX;
        min->key2 = INT_MAX;
        pq32_update(&pq, min);
        vcount--;
    }

#ifdef ENABLE_TIME_MEASUREMENTS
    if (ms->measure_time_consumption) {
        gettimeofday(&t1, NULL);
        ms->time.verifying += timediff(&t1, &t2);
    }
#endif

    /*fprintf(stderr, "Vectors: %d\n", vcount);*/
EXIT:
    pq32_free(&pq);
    free(edges);
    free(edgelengths);
    free(graph_match);
}

#endif


/**
 * Index filter P2/F4. Picks all valid vectors from the pattern and scans
 * them (or their starting points) in the same way that the actual P2
 * algorithm scans notes.
 *
 * @param sc         a song collection
 * @param pattern    search pattern
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         information about the found matches will be stored here
 */
void filter_p2_window(const songcollection *sc, const song *pattern,
        int alg, const searchparameters *parameters, matchset *ms) {
    song      *songs  = sc->songs;
    vector    *pnotes = pattern->notes;
    vindex    *vi     = (vindex *) sc->data[DATA_VINDEX];
    fp2vector *chosenvectors;
    pq32       pq;

    uint32_t i, j;
    uint32_t vcount = 0;
    int32_t  songid = INT_MIN;
    int32_t  previous_key = INT_MIN;
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
    uint32_t previous_spos = 0;
    uint32_t previous_ppos = 0;
#endif
    uint32_t count = 0;
    uint32_t maxcount = 0;
    /* int32_t matchstart = 0; */

    /* Parameter alg is unused */
    (void) alg;

    chosenvectors = (fp2vector *) malloc(pattern->size *
            parameters->p2_window * sizeof(fp2vector));
    if (chosenvectors == NULL) {
        fputs("Error in filter_p2_window(): failed to allocate memory "
              "for temporary table 'chosenvectors'", stderr);
        return;
    }

    if (vi == NULL) {
        fputs("Error in filter_p2_window(): song collection does not "
              "contain vectorindex data.\n"
              "Use update_song_collection_data() before calling this "
              "function.\n", stderr);
        free(chosenvectors);
        return;
    }

    /* Pick all the valid vectors from the pattern. */
    for (i = 0; i < pattern->size - 1; i++) {
        uint32_t end = i + parameters->p2_window + 1;
        if (end > pattern->size) end = pattern->size;
        for (j = i + 1; j < end; j++) {
            int32_t x = pnotes[j].strt - pnotes[i].strt;
            int32_t y = pnotes[j].ptch - pnotes[i].ptch;
            vindextable *vit = vindex_table_by_vector(vi, x, y);
            if (vit != NULL) {
                chosenvectors[vcount].vit = vit;
                chosenvectors[vcount].patternpos = i;
                chosenvectors[vcount].shift = (((int32_t) pnotes[i].strt -
                    (int32_t) pnotes[0].strt) << 8) +
                    (int32_t) pnotes[i].ptch -
                    (int32_t) pnotes[0].ptch + NOTE_PITCHES;
                chosenvectors[vcount].t1 = 0;
                ++vcount;
            }
        }
    }
    if (vcount == 0) {
        free(chosenvectors);
        return;
    }

    /* Initialize a priority queue. */
    if (pq32_init(&pq, vcount)) {
        fputs("Error in filter_p2_window: failed to initialize "
              "priority queue", stderr);
        free(chosenvectors);
        return;
    }

    /* Merge the location lists by inserting them to the priority queue. */
    for (i = 0; i < vcount; i++) {
        pq32node  *node = pq32_getnode(&pq, i);
        vindexrec *vir  = chosenvectors[i].vit->records;
        song      *s    = &songs[vir->song];
        vector    *textnote    = &s->notes[vir->note];
        vector    *patternnote = &pnotes[chosenvectors[i].patternpos];

        node->key1 = vir->song;
        node->key2 = ((int32_t) (textnote->strt - patternnote->strt) << 8) +
                      (int32_t) (textnote->ptch - patternnote->ptch) +
                      NOTE_PITCHES;
        node->value = vir->note;
        pq32_update(&pq, node);
        chosenvectors[i].t1 = 1;
    }

    /* Scan through the data and mark the location where the largest number
     * of vectors are in the same relative positions as in the pattern. */
    --vcount;
    while (1) {
        pq32node *min = pq32_getmin(&pq);
        i = min->index;
        if ((min->key1 == songid) && (min->key2 == previous_key)) {
            ++count;
            if (count > maxcount) maxcount = count;
        } else {
            /* Check if the previous match was a good one */
            if ((count == maxcount) && (count > 1)) {
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
                alignment_check_p2(&songs[songid], previous_spos,
                        pattern, previous_ppos, ms);
#else
                int32_t match_start = (previous_key >> 8) + pnotes[0].strt;
                int32_t match_end = (previous_key >> 8) +
                        pnotes[pattern->size - 1].strt +
                        pnotes[pattern->size - 1].dur;
                char transposition = (char) ((previous_key & 0xFF) -
                        NOTE_PITCHES);
                float similarity = ((float) maxcount) / ((float) vcount);
                insert_match(ms, (uint32_t) songid, match_start, match_end,
                             transposition, similarity);
#endif
            }
            if (min->key1 == INT_MAX) break;
            count = 0;
            if (songid != min->key1) {
                /* Next song, reset counter */
                songid = min->key1;
                maxcount = 0;
            }
            previous_key  = min->key2;
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
            previous_ppos = chosenvectors[i].patternpos;
            previous_spos = (uint32_t) min->value;
#endif
        }
        if (chosenvectors[i].t1 < chosenvectors[i].vit->size) {    
            vindexrec *vir = &chosenvectors[i].vit->records[chosenvectors[i].t1];
            song *s = &songs[vir->song];
            vector *textnote = &s->notes[vir->note];
            vector *patternnote = &pnotes[chosenvectors[i].patternpos];
            min->key1 = vir->song;
            min->key2 = ((int32_t) (textnote->strt - patternnote->strt) << 8) +
                         (int32_t) (textnote->ptch - patternnote->ptch) +
                         NOTE_PITCHES;
            min->value = vir->note;
            pq32_update(&pq, min);
            chosenvectors[i].t1++;
        } else {
            min->key1 = INT_MAX;
            min->key2 = INT_MAX;
            pq32_update(&pq, min);
        }
    }
    free(chosenvectors);
    pq32_free(&pq);
}


/**
 * Index filter P2/F5. Picks locally a group of least frequent vectors in
 * the pattern to approximate P2. Search parameter p2_select_threshold
 * controls the ratio of vectors to discard:
 * 1.0 keeps all vectors, which is the same as P2/F4,
 * 0.0 discards all. Value 0.5 is generally a good choise. Longest
 * vectors are discarded and the shortest will be kept.
 *
 * @param sc         a song collection
 * @param pattern    search pattern
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         information about the found matches will be stored here
 */
void filter_p2_select_local(const songcollection *sc, const song *pattern,
        int32_t alg, const searchparameters *parameters, matchset *ms) {
    song      *songs  = sc->songs;
    vector    *pnotes = pattern->notes;
    vindex    *vi     = (vindex *) sc->data[DATA_VINDEX];
    fp2vector *chosenvectors;
    uint32_t  *bucket_size;
    pq32       pq;
 
    int32_t  songid = INT_MIN;
    int32_t  previous_key = INT_MIN;
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
    uint32_t previous_spos = 0;
    uint32_t previous_ppos = 0;
#endif
    uint32_t count = 0;
    uint32_t maxcount = 0;
    uint32_t vcount = 0;
    uint32_t i, j;
    /* int32_t matchstart = 0; */

    /* Parameter alg is unused */
    (void) alg;

    if (vi == NULL) {
        fputs("Error in filter_p2_select_local(): song collection "
              "does not contain\n"
              "vectorindex data. Use update_song_collection_data() "
              "before calling this function\n", stderr);
        return;
    }

    chosenvectors = (fp2vector *) malloc(
            pattern->size * parameters->p2_window * sizeof(fp2vector));
    if (chosenvectors == NULL) {
        fputs("Error in filter_p2_select_local(): failed to allocate "
              "memory for temporary table 'chosenvectors'", stderr);
        return;
    }

    bucket_size = (uint32_t *) malloc(parameters->p2_window *
                                      sizeof(uint32_t));
    if (bucket_size == NULL) {
        fputs("Error in filter_p2_select_local(): failed to allocate "
              "memory for temporary table 'bucket_size'", stderr);
        free(chosenvectors);
        return;
    }

    /* Pick all the valid vectors from the pattern. */
    for (i = 0; i < pattern->size - 1; i++) {
        uint32_t c = 0;
        uint32_t end = i + parameters->p2_window + 1;
        if (end > pattern->size) end = pattern->size;

        /* Calculate vector occurrence median */
        memset(bucket_size, 0, parameters->p2_window * sizeof(int32_t));
        for (j = i + 1; j < end; j++) {
            int32_t x = pnotes[j].strt - pnotes[i].strt;
            int32_t y = pnotes[j].ptch - pnotes[i].ptch;
            vindextable *vit = vindex_table_by_vector(vi, x, y);
            if (vit != NULL) {
                bucket_size[c] = vit->size;
                c++;
            }
        }
        if (c == 0) continue;
        maxcount = (uint32_t) kth_smallest(
                (int *) bucket_size, (int) c,
                MIN2((int) (c - 1), (int) lrintf(((float) c) *
                        parameters->p2_select_threshold)));

        /* Select vectors */
        for (j = i + 1; j < end; j++) {
            int32_t x = pnotes[j].strt - pnotes[i].strt;
            int32_t y = pnotes[j].ptch - pnotes[i].ptch;
            vindextable *vit = vindex_table_by_vector(vi, x, y);
            if ((vit != NULL) && (vit->size <= maxcount)) {
                chosenvectors[vcount].vit = vit;
                chosenvectors[vcount].patternpos = i;
                chosenvectors[vcount].shift = (((int32_t) pnotes[i].strt -
                    (int32_t) pnotes[0].strt) << 8) +
                    (int32_t) pnotes[i].ptch -
                    (int32_t) pnotes[0].ptch + NOTE_PITCHES;
                chosenvectors[vcount].t1 = 0;
                vcount++;
            }
        }
    }
    free(bucket_size);
    if (vcount == 0) {
        free(chosenvectors);
        return;
    }

    /* Initialize a priority queue. */
    if (pq32_init(&pq, vcount)) {
        fputs("Error in filter_p2_select_local(): failed to initialize "
              "priority queue", stderr);
        free(chosenvectors);
        return;
    }

    /* Merge the location lists by inserting them to the priority queue. */
    for (i = 0; i < vcount; i++) {
        pq32node  *node = pq32_getnode(&pq, i);
        vindexrec *vir  = chosenvectors[i].vit->records;
        song      *s    = &songs[vir->song];
        vector    *textnote    = &s->notes[vir->note];
        vector    *patternnote = &pnotes[chosenvectors[i].patternpos];
        node->key1 = vir->song;
        node->key2 = ((int32_t) (textnote->strt - patternnote->strt) << 8) +
                (int32_t) (textnote->ptch - patternnote->ptch) + NOTE_PITCHES;
        node->value = vir->note;
        pq32_update(&pq, node);
        chosenvectors[i].t1 = 1;
    }

    /* Scan through the data and mark the location where the largest number
     * of vectors are in the same relative positions as in the pattern. */
    --vcount;
    maxcount = 0;
    while (1) {
        pq32node *min = pq32_getmin(&pq);
        i = min->index;
        if ((min->key1 == songid) && (min->key2 == previous_key)) {
            ++count;
            if (count > maxcount) maxcount = count;
        } else {
            /* Check if the previous match was a good one */
            if ((count == maxcount) && (count > 1)) {
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
                alignment_check_p2(&songs[songid], previous_spos,
                        pattern, previous_ppos, ms);
#else
                int32_t match_start = (previous_key >> 8) + pnotes[0].strt;
                int32_t match_end = (previous_key >> 8) +
                        pnotes[pattern->size - 1].strt +
                        pnotes[pattern->size - 1].dur;
                char transposition = (char) ((previous_key & 0xFF) -
                        NOTE_PITCHES);
                float similarity = ((float) maxcount) / ((float) vcount);

                insert_match(ms, (uint32_t) songid, match_start,
                        match_end, transposition, similarity);
#endif
    /*printf("%d, %d\n", songid + 1, maxcount);*/
            }
            if (min->key1 == INT_MAX) break;
            count = 0;
            if (songid != min->key1) {
                /* Next song, reset counter */
                songid = min->key1;
                maxcount = 0;
            }
            previous_key = min->key2;
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
            previous_ppos = chosenvectors[i].patternpos;
            previous_spos = (uint32_t) min->value;
#endif
        }
        if (chosenvectors[i].t1 < chosenvectors[i].vit->size) {    
            vindexrec *vir = &chosenvectors[i].vit->records[chosenvectors[i].t1];
            song *s = &songs[vir->song];
            vector *textnote = &s->notes[vir->note];
            vector *patternnote = &pnotes[chosenvectors[i].patternpos];
            min->key1 = vir->song;
            min->key2 = ((int32_t) (textnote->strt - patternnote->strt) << 8) +
                    (int32_t) (textnote->ptch - patternnote->ptch) + NOTE_PITCHES;
            min->value = vir->note;
            pq32_update(&pq, min);
            chosenvectors[i].t1++;
        } else {
            min->key1 = INT_MAX;
            min->key2 = INT_MAX;
            pq32_update(&pq, min);
        }
    }
    free(chosenvectors);
    pq32_free(&pq);
}


/**
 * Index filter P2/F5-global. Picks globally a group of least frequent
 * vectors in the pattern to approximate P2. Seach parameter
 * p2_select_threshold controls the ratio of vectors to discard:
 * 1.0 keeps all vectors, which is the same as P2/F4,
 * 0.0 discards all. Value 0.5 is generally a good choise.
 * Longest vectors are discarded and the shortest will be kept.
 *
 * @param sc         a song collection
 * @param pattern    search pattern
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         information about the found matches will be stored here
 */
void filter_p2_select_global(const songcollection *sc, const song *pattern,
        int32_t alg, const searchparameters *parameters, matchset *ms) {
    song      *songs  = sc->songs;
    vector    *pnotes = pattern->notes;
    vindex    *vi     = (vindex *) sc->data[DATA_VINDEX];
    fp2vector *chosenvectors;
    uint32_t  *bucket_size;
    pq32       pq;
 
    uint32_t i, j;
    uint32_t maxcount;
    uint32_t count;
    uint32_t vcount        = 0;
    int32_t  previous_key  = INT_MIN;
    int32_t  songid        = INT_MIN;
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
    uint32_t previous_spos = 0;
    uint32_t previous_ppos = 0;
#endif

    /* Parameter alg is unused */
    (void) alg;

    if (vi == NULL) {
        fputs("Error in filter_p2_select_global(): song collection "
              "does not contain\n"
              "vectorindex data. Use update_song_collection_data() "
              "before calling this function\n", stderr);
        return;
    }

    bucket_size = (uint32_t *) malloc(pattern->size *
            parameters->p2_window * sizeof(uint32_t));
    if (bucket_size == NULL) {
        fputs("Error in filter_p2_select_global(): failed to allocate "
              "memory for temporary table 'bucket_size'", stderr);
        return;
    }

    /* Calculate median bucket size of all possible index vectors. */
    for (i = 0; i < pattern->size - 1; i++) {
        uint32_t end = i + parameters->p2_window + 1;
        if (end > pattern->size) end = pattern->size;
        for (j = i + 1; j < end; j++) {
            int32_t x = pattern->notes[j].strt - pattern->notes[i].strt;
            int32_t y = pattern->notes[j].ptch - pattern->notes[i].ptch;
            vindextable *vit = vindex_table_by_vector(vi, x, y);
            if (vit != NULL) {
                bucket_size[vcount] = vit->size;
                vcount++;
            }
        }
    }

    count = (uint32_t) lrintf((float) vcount *
                              parameters->p2_select_threshold);
    if (vcount > count) {
        maxcount = (uint32_t) kth_smallest((int *) bucket_size,
                (int) vcount, MIN2((int) (vcount - 1), (int) count));
    } else maxcount = INT_MAX;

    free(bucket_size);

    vcount = count;
    if (vcount == 0) {
        return;
    }

    chosenvectors = (fp2vector *) malloc(vcount * sizeof(fp2vector));
    if (chosenvectors == NULL) {
        fputs("Error in filter_p2_select_global(): failed to allocate "
              "memory for temporary table 'chosenvectors'", stderr);
        return;
    }

    /* Pick vectors from the pattern. */
    count = 0;
    for (i = 0; i < pattern->size - 1; i++) {
        uint32_t end = i + parameters->p2_window + 1;
        if (end > pattern->size) end = pattern->size;
        for (j = i + 1; j < end; j++) {
            int32_t x = pnotes[j].strt - pnotes[i].strt;
            int32_t y = pnotes[j].ptch - pnotes[i].ptch;
            vindextable *vit = vindex_table_by_vector(vi, x, y);
            if ((vit != NULL) && (vit->size <= maxcount)) {
                chosenvectors[count].vit = vit;
                chosenvectors[count].patternpos = i;
                chosenvectors[count].shift =
                    (((int32_t) pnotes[i].strt -
                      (int32_t) pnotes[0].strt) << 8) +
                      (int32_t) pnotes[i].ptch -
                      (int32_t) pnotes[0].ptch + NOTE_PITCHES;
                chosenvectors[count].t1 = 0;
                count++;
                if (count >= vcount) {
                    j = end;
                    i = pattern->size;
                }
            }
        }
    }

    /* Initialize a priority queue. */
    if (pq32_init(&pq, vcount)) {
        fputs("Error in filter_p2_select_global(): failed to initialize "
              "priority queue", stderr);
        free(chosenvectors);
        return;
    }

    /* Merge the location lists by inserting them to the priority queue. */
    for (i = 0; i < vcount; i++) {
        pq32node  *node = pq32_getnode(&pq, i);
        vindexrec *vir  = chosenvectors[i].vit->records;
        song      *s    = &songs[vir->song];
        vector    *textnote    = &s->notes[vir->note];
        vector    *patternnote = &pnotes[chosenvectors[i].patternpos];
        node->key1 = vir->song;
        node->key2 = ((int32_t) (textnote->strt - patternnote->strt) << 8) +
                      (int32_t) (textnote->ptch - patternnote->ptch) +
                      NOTE_PITCHES;
        node->value = vir->note;
        pq32_update(&pq, node);
        chosenvectors[i].t1 = 1;
    }

    /* Scan through the data and mark the location where the largest number
     * of vectors are in the same relative positions as in the pattern. */
    count    = 0;
    maxcount = 0;
    --vcount;
    while (1) {
        pq32node *min = pq32_getmin(&pq);
        i = min->index;
        if ((min->key1 == songid) && (min->key2 == previous_key)) {
            count++;
            if (count > maxcount) maxcount = count;
        } else {
            /* Check if the previous match was a good one */
            if ((count == maxcount) && (count > 1)) {
#ifdef ORDER_F4_F5_RESULTS_WITH_P2
                alignment_check_p2(&songs[songid], previous_spos,
                        pattern, previous_ppos, ms);
#else
                int32_t match_start = (previous_key >> 8) + pnotes[0].strt;
                int32_t match_end   = (previous_key >> 8) +
                        pnotes[pattern->size - 1].strt +
                        pnotes[pattern->size - 1].dur;
                int8_t  transposition = (int8_t) ((previous_key & 0xFF) -
                                                  NOTE_PITCHES);
                float similarity = ((float) maxcount) / ((float) vcount);
                insert_match(ms, (uint32_t) songid, match_start, match_end,
                             transposition, similarity);
#endif
            }
            if (min->key1 == INT_MAX) break;

            count = 0;
            if (songid != min->key1) {
                /* Next song, reset counter */
                songid   = min->key1;
                maxcount = 0;
            }
            previous_key  = min->key2;

#ifdef ORDER_F4_F5_RESULTS_WITH_P2
            previous_ppos = chosenvectors[i].patternpos;
            previous_spos = (uint32_t) min->value;
#endif
        }
        if (chosenvectors[i].t1 < chosenvectors[i].vit->size) {    
            vindexrec *vir = &chosenvectors[i].vit->records[chosenvectors[i].t1];
            song      *s           = &songs[vir->song];
            vector    *textnote    = &s->notes[vir->note];
            vector    *patternnote = &pnotes[chosenvectors[i].patternpos];
            min->key1 = vir->song;
            min->key2 = ((int32_t) (textnote->strt - patternnote->strt) << 8) +
                         (int32_t) (textnote->ptch - patternnote->ptch) +
                         NOTE_PITCHES;
            min->value = vir->note;
            pq32_update(&pq, min);
            chosenvectors[i].t1++;
        } else {
            min->key1 = INT_MAX;
            min->key2 = INT_MAX;
            pq32_update(&pq, min);
        }
    }
    pq32_free(&pq);
    free(chosenvectors);
}


/**
 * Index filter P2'/F7. Picks all valid pattern vectors around
 * user-specified notes that must be found in the matches.
 * Otherwise works like the generic P2 filter.
 *
 * @param sc         a song collection
 * @param pattern    search pattern
 * @param alg        algorithm ID as defined in search.h.
 *                   Not used in this function.
 * @param parameters search parameters
 * @param ms         information about the found matches will be stored here
 */
void filter_p2_points(const songcollection *sc, const song *pattern,
        int32_t alg, const searchparameters *parameters, matchset *ms) {

    song      *songs  = sc->songs;
    vector    *pnotes = pattern->notes;
    uint32_t  *points = parameters->p2_points;
    vindex    *vi     = (vindex *) sc->data[DATA_VINDEX];
    fp2vector *chosenvectors;

    uint32_t   i, vcount = 0;
    uint32_t   num_points = parameters->p2_num_points;

    /* Parameter alg is unused */
    (void) alg;

   if (vi == NULL) {
        fputs("Error in filter_p2_points(): song collection "
              "does not contain\n"
              "vectorindex data. Use update_song_collection_data() "
              "before calling this function\n", stderr);
        return;
    }
    chosenvectors = (fp2vector *) malloc(
            2 * parameters->d_window * sizeof(fp2vector));
    if (chosenvectors == NULL) {
        fputs("Error in filter_p2_points(): failed to allocate "
              "memory for temporary table 'chosenvectors'", stderr);
        return;
    }
 
    if (num_points > 1) {
        vindextable *smallestvit;
        uint32_t     smallestcount = INT_MAX;
        int32_t      smallestpos = -1;
        uint32_t     ppos = 0;
        for (i = 0; i < num_points; i++) {
            uint32_t j;
            for (j = i + 1; j < num_points; j++) {
                vindextable *vit;
                int32_t x, y;
                uint32_t pj = points[j];
                uint32_t pi = points[i];
                if (pj >= pi + parameters->d_window) break;
                x = pnotes[pj].strt - pnotes[pi].strt;
                y = pnotes[pj].ptch - pnotes[pi].ptch;
                vit = vindex_table_by_vector(vi, x, y);
                if ((vit != NULL) && (vit->size < smallestcount)) {
                    smallestcount = vit->size;
                    smallestvit   = vit;
                    ppos = pi;
                }
            }
        }
        if (smallestpos >= 0) {
            chosenvectors[vcount].vit = smallestvit;
            chosenvectors[vcount].patternpos = ppos;
            chosenvectors[vcount].shift =
                    (((int32_t) pnotes[ppos].strt -
                      (int32_t) pnotes[0].strt) << 8) +
                      (int32_t) pnotes[ppos].ptch -
                      (int32_t) pnotes[0].ptch + NOTE_PITCHES;
            chosenvectors[vcount].t1 = 0;
            vcount++;       
        } else num_points = 1;
    }

    if (num_points <= 1) {
        uint32_t p, start, end;
        if (num_points == 0) {
            p = 0;
        } else {
            p = points[0];
        }
        if (p < parameters->d_window) {
            start = 0;
        } else {
            start = p - parameters->d_window;
        }
        end = p + parameters->d_window + 1;
        if (end > pattern->size) end = pattern->size;
        for (i = start; i < end; i++) {
            vindextable *vit;
            uint32_t pstart;
            int32_t  x, y;
            /*printf("%d %d %d %d\n", i, p, start, end);*/
            if (i < p) {
                pstart = i;
                x = pnotes[p].strt - pnotes[i].strt;
                y = pnotes[p].ptch - pnotes[i].ptch;
            } else if (i > p) {
                pstart = p;
                x = pnotes[i].strt - pnotes[p].strt;
                y = pnotes[i].ptch - pnotes[p].ptch;
            } else continue;
            vit = vindex_table_by_vector(vi, x, y);
            if (vit != NULL) {
                chosenvectors[vcount].vit = vit;
                chosenvectors[vcount].patternpos = pstart;
                chosenvectors[vcount].shift =
                    (((int32_t) pnotes[pstart].strt -
                      (int32_t) pnotes[0].strt) << 8) +
                      (int32_t) pnotes[pstart].ptch -
                      (int32_t) pnotes[0].ptch + NOTE_PITCHES;
                vcount++;
            }
        }
        for (i = 0; i < vcount; i++) {
            vindextable *vit = chosenvectors[i].vit;
            uint32_t j;
            /*printf("%d: %d\n", i, chosenvectors[i].patternpos);*/
            for (j = 0; j < vit->size; j++) {
                alignment_check_p2(&songs[vit->records[j].song],
                        vit->records[j].note, pattern,
                        chosenvectors[i].patternpos, ms);
            }
        }
    } else {
        song subpat;
        subpat.notes = (vector *) malloc(num_points * sizeof(vector));
        if (subpat.notes == NULL) {
            free(chosenvectors);
            return;
        }
        subpat.size = num_points;
        for (i = 0; i < num_points; i++) {
            subpat.notes[i] = pnotes[points[i]];
        }
 
        for (i = 0; i < vcount; i++) {
            vindextable *vit = chosenvectors[i].vit;
            uint32_t j;
            /*printf("%d: %d\n", i, chosenvectors[i].patternpos);*/
            for (j = 0; j < vit->size; j++) {
                song *s = &songs[vit->records[j].song];
                uint32_t spos = vit->records[j].note;
                uint32_t ppos = chosenvectors[i].patternpos;
                if (alignment_check_p1(s, spos, pattern, ppos, NULL))
                    alignment_check_p2(s, spos, pattern, ppos, ms);
            }
        }
        free(subpat.notes);
    }

    free(chosenvectors);
}

