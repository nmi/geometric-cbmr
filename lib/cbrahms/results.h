/*
 * results.h - Search result structures and external declarations
 *
 * Copyright (c) 2007-2011 University of Helsinki,
 * Department of Computer Science, C-BRAHMS project
 *
 * Author: Niko Mikkilä <niko.mikkila@gmail.com>
 *
 * This file is part of libcbrahms:
 * C-BRAHMS algorithms for Content-Based Music Retrieval.
 *
 * Libcbrahms is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Libcbrahms is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with libcbrahms; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Suite 500, Boston, MA 02111-1335 USA
 */

#ifndef __RESULTS_H__
#define __RESULTS_H__

#include <stdint.h>
/*#include "config.h"*/

#ifdef __cplusplus
extern "C" {
#endif


/**
 * Information about a single matching position in a song.
 */
typedef struct {
    /* Note positions that match the pattern notes. -1 means no match. */
    int32_t *notes;

    /* Number of notes in the pattern */
    uint32_t num_notes;

    /* Song id in the song collection */
    uint32_t song;

    /* Match start time */
    int32_t start;

    /* Match end time. This may be the end time of the last matching
     * note, so that the algorithm does not need to check the end times
     * of all the matching notes */
    int32_t end;

    /* Similarity score */
    float similarity;

    /* Transposition of the match compared to the pattern */
    int8_t transposition;

    /*PAD_STRUCT(3)*/
} match;


/**
 * Time counters.
 */
typedef struct {
   double indexing;
   double verifying;
   double other;
} searchtime;


/**
 * A set of matches.
 */
typedef struct {
    /* Search time */
    searchtime time;

    /* Items */
    match *matches;

    /* Number of items the set actually holds */
    uint32_t num_matches;

    /* Capacity of the match set */
    uint32_t capacity;

     /* Flag for search time measurement */
    int measure_search_time;

    /* Flag for setting how multiple matches in a song should be stored:
     * 1: the set can contain many matches from the same song, interleaved
     *    with other matches
     * 0: only the best match of each song is stored. */
    int multiple_matches_per_song;
} matchset;


/* External function declarations */


void free_match(match *m);

void free_match_set(matchset *ms);

int init_match_set(matchset *ms, uint32_t size, uint32_t pattern_size,
                   int measure_search_time, int multiple_matches_per_song);

void clear_match_set(matchset *ms);

match *insert_match(matchset *ms, uint32_t song, int32_t start, int32_t end,
                    int8_t transposition, float similarity);


#ifdef __cplusplus
}
#endif

#endif

