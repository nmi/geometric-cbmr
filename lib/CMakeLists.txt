
add_subdirectory(clt)

add_subdirectory(cbrahms)

if (USE_FG)
    add_subdirectory (fg)
endif (USE_FG)

if (USE_BLOSSOM4)
    add_subdirectory (concorde)
    add_subdirectory (blossom4)
endif (USE_BLOSSOM4)

if (USE_MSM)
    add_subdirectory (msm)
endif (USE_MSM)


