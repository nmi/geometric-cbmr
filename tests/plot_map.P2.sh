#!/bin/bash

export PLOT_TITLE_E="(a) MAP(err), noise=0"
export PLOT_TITLE_N="(c) MAP(noise), err=0.0"
NOTEST=1 ALG=P2 ./run_map.P2.sh
export PLOT_TITLE_E="(b) MAP(err), noise=20"
export PLOT_TITLE_N="(d) MAP(noise), err=0.5"
NOTEST=1 ALG=P2 ./run_map.P2.mixed.sh

PLOT_SPACE2=0.2 PLOT_LEGEND_FROM=1 PLOT_LEGEND="in-lb" ./combine_three_plots.sh map.errors.nonoise.P2.p map.errors.noise_20.P2.p map.noise.noerrors.P2.p map.combined.P2
