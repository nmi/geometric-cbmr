#!/bin/bash


if [ -z "$ALG" ]; then
	echo ""
	echo "Please specify algorithm with variable ALG. Possible values are:"
	echo "  P2, P2F4, P2F6a, P2F6gd, MSMe, MSMn"
	echo ""
	exit
fi


function run_map_errors() {
	if [ -z "$NOERRORS" ]; then
	run_map -N $NOISE -A "$ALGDESC" -o "$OUTFILE" \
		-a "$ALG" -e 0.0 --similarity-cutoff 1.0 --row-label 0.0 \
		-a "$ALG" -e 0.1 --similarity-cutoff 0.9 --row-label 0.1 \
		-a "$ALG" -e 0.2 --similarity-cutoff 0.8 --row-label 0.2 \
		-a "$ALG" -e 0.3 --similarity-cutoff 0.7 --row-label 0.3 \
		-a "$ALG" -e 0.4 --similarity-cutoff 0.6 --row-label 0.4 \
		-a "$ALG" -e 0.5 --similarity-cutoff 0.5 --row-label 0.5 \
		-a "$ALG" -e 0.6 --similarity-cutoff 0.4 --row-label 0.6 \
		-a "$ALG" -e 0.7 --similarity-cutoff 0.3 --row-label 0.7 \
		-a "$ALG" -e 0.8 --similarity-cutoff 0.2 --row-label 0.8 \
		-a "$ALG" -e 0.9 --similarity-cutoff 0.1 --row-label 0.9 \
		-a "$ALG" -e 0.95 --similarity-cutoff 0.05 --row-label 0.95
	fi
}

function run_map_noise() {
	if [ -z "$NONOISE" ]; then
	run_map -e $ERRORS --similarity-cutoff $CUTOFF -A "$ALGDESC" \
		-o "$OUTFILE" \
		-a "$ALG" -N 0.0 --row-label 0.0 \
		-a "$ALG" -N 10.0 --row-label 10.0 \
		-a "$ALG" -N 20.0 --row-label 20.0 \
		-a "$ALG" -N 30.0 --row-label 30.0 \
		-a "$ALG" -N 40.0 --row-label 40.0 \
		-a "$ALG" -N 45.0 --row-label 45.0 \
		-a "$ALG" -N 50.0 --row-label 50.0 \
		-a "$ALG" -N 55.0 --row-label 55.0 \
		-a "$ALG" -N 60.0 --row-label 60.0 \
		-a "$ALG" -N 70.0 --row-label 70.0 \
		-a "$ALG" -N 80.0 --row-label 80.0
	fi
}

function run_map() {
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--pattern-notes $PATTERN_SIZE \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--inserted-patterns $INSERTED_PATTERNS \
		--measurement-points 0 \
		--p2-threshold $P2_THRESHOLD \
		-g $NUM_PATTERNS "$@"
}

TEST_PROGRAM=../test_precision_recall

SONGS="$@"

if [ -z "$ERRORS" ]; then
	ERRORS=0.0
fi

if [ -z "$CUTOFF" ]; then
	CUTOFF=0.9
fi

if [ -z "$NOISE" ]; then
	NOISE=0.0
fi

OUT_N="map.noise.noerrors.P2"
OUT_E="map.errors.nonoise.P2"

SEED=123

NUM_PATTERNS=25
PATTERN_SIZE=50

PATTERN_GENERATOR_MAX_SKIP=50
PATTERN_GENERATOR_MAX_TRANSPOSITION=32
INSERTED_PATTERNS=25

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128


DATAFILES_N="$OUT_N.dat $OUT_N.F4.dat $OUT_N.F5.dat $OUT_N.F6a.dat $OUT_N.F6gd.dat $OUT_N.MSM.dat"

DATAFILES_E="$OUT_E.dat $OUT_E.F4.dat $OUT_E.F5.dat $OUT_E.F6a.dat $OUT_E.F6gd.dat $OUT_E.MSM.dat"


# Run the tests
if [ -z "$NOTEST" ]; then
	P2_THRESHOLD=0.5

	if [ "$ALG" == "P2" ]; then
		ALG="P2" ALGDESC="P2" OUTFILE="$OUT_E.dat" run_map_errors
		ALG="P2" ALGDESC="P2" OUTFILE="$OUT_N.dat" run_map_noise
	elif [ "$ALG" == "P2F4" ]; then
		ALG="P2F4" ALGDESC="P2/F4" OUTFILE="$OUT_E.F4.dat" run_map_errors
		ALG="P2F4" ALGDESC="P2/F4" OUTFILE="$OUT_N.F4.dat" run_map_noise
	elif [ "$ALG" == "P2F5" ]; then
		ALG="P2F5" ALGDESC="P2/F5" OUTFILE="$OUT_E.F5.dat" run_map_errors
		ALG="P2F5" ALGDESC="P2/F5" OUTFILE="$OUT_N.F5.dat" run_map_noise
	elif [ "$ALG" == "P2F6a" ]; then
		ALG="P2F6" ALGDESC="P2/F6 (k=m/2)" OUTFILE="$OUT_E.F6a.dat" \
			run_map_errors
		ALG="P2F6" ALGDESC="P2/F6 (k=m/2)" OUTFILE="$OUT_N.F6a.dat" \
			run_map_noise
	elif [ "$ALG" == "P2F6gd" ]; then
		P2_THRESHOLD=0.0625
		ALG="P2F6g" ALGDESC="P2/F6 greedy (k=m/16)" \
			OUTFILE="$OUT_E.F6gd.dat" run_map_errors
		ALG="P2F6g" ALGDESC="P2/F6 greedy (k=m/16)" \
			OUTFILE="$OUT_N.F6gd.dat" run_map_noise
	elif [ "$ALG" == "MSM" ]; then
		ALG="MSMFFT" ALGDESC="MSM (FFT)" OUTFILE="$OUT_E.MSM.dat" \
			run_map_errors
		ALG="MSMFFT" ALGDESC="MSM (FFT)" OUTFILE="$OUT_N.MSM.dat" \
			run_map_noise
	else
		echo ""
		echo "Invalid algorithm: ALG=$ALG"
		echo ""
		exit
	fi
fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	export PLOT_TITLE="$PLOT_TITLE_E"
	if [ -z "$PLOT_TITLE" ]; then
		export PLOT_TITLE="(a) MAP(err), noise=0"
	fi
	export PLOT_X_SCALE=lin
	export PLOT_Y_SCALE=lin
	export PLOT_X_LABEL="Maximum error rate of ground truth"
	export PLOT_Y_LABEL="Mean Average Precision"
	export PLOT_X_COLUMN="1"
	export PLOT_Y_COLUMN="2"
	export PLOT_Y_RANGE="0.0:1.1"
	export PLOT_Y_LABEL_POS="1.5,0"
	export PLOT_X_TICS='0.1'

	./plot.sh "$OUT_E" $DATAFILES_E

	export PLOT_TITLE="$PLOT_TITLE_N"
	if [ -z "$PLOT_TITLE" ]; then
		export PLOT_TITLE="(b) MAP(noise), err=0.0"
	fi
	export PLOT_X_LABEL="Noise notes / pattern note"
	export PLOT_X_TICS='10'
	export PLOT_X_RANGE="0:60"

	./plot.sh "$OUT_N" $DATAFILES_N
fi

