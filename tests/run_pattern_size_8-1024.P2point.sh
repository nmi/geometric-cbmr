#!/bin/bash

TEST_PROGRAM=../test_speed

SONGS="$@"

OUT="pattern_size_8-1024.P2point"

SEED=123

PATTERN_SIZES="8,16,32,64,128,256,512,1024"

PATTERN_GENERATOR_MAX_SKIP=10
PATTERN_GENERATOR_ERRORS=0.0
PATTERN_GENERATOR_MAX_TRANSPOSITION=32

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

NUM_RESULTS=10


# Algorithm-specific settings

DATAFILES="$OUT.1.dat $OUT.2.dat $OUT.4.dat $OUT.16.dat $OUT.F7.1.dat $OUT.F7.2.dat $OUT.F7.4.dat $OUT.F7.16.dat $OUT.F6.dat"


# Run the tests
if [ -z "$NOTEST" ]; then
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--pattern-notes $PATTERN_SIZES \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $PATTERN_GENERATOR_ERRORS \
		--num-results $NUM_RESULTS \
		-a P2p   -g 5  -R 1 -o $OUT.1.dat --p2-fixed-points 1 \
			-A "P2' (1 point)" \
		-a P2p   -g 50 -R 1 -o $OUT.2.dat --p2-fixed-points 2 \
			-A "P2' (2 points)" \
		-a P2p   -g 50 -R 1 -o $OUT.4.dat --p2-fixed-points 4 \
			-A "P2' (4 points)" \
		-a P2p   -g 50 -R 1 -o $OUT.16.dat --p2-fixed-points 16 \
			-A "P2' (16 points)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.1.dat --p2-fixed-points 1 \
			-A "P2'/F7 (1 point)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.2.dat --p2-fixed-points 2 \
			-A "P2'/F7 (2 points)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.4.dat --p2-fixed-points 4 \
			-A "P2'/F7 (4 points)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.16.dat --p2-fixed-points 16 \
			-A "P2'/F7 (16 points)" \
		-a P2F6  -g 50 -R 1 -o $OUT.F6.dat --p2-threshold 0.25 \
			-A "P2/F6 (k=m/4)"
fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	#export PLOT_TITLE="Effect of pattern size on search time"
	export PLOT_X_SCALE=log
	export PLOT_Y_SCALE=log
	export PLOT_X_LABEL="Pattern size (notes)"
	export PLOT_Y_LABEL="Time (ms)"
	export PLOT_X_COLUMN="2"
	export PLOT_Y_COLUMN="6"
	export PLOT_X_TICS="(8,16,32,64,128,256,512,1024)"

	export COLORS="1 1 1 1 3 3 3 3 9"
	export LINEWIDTHS="2 2 2 2 2 2 2 2 3"

	export PLOT_X_RANGE="8:1024"
	./plot.sh "$OUT" $DATAFILES

	#export PLOT_BOX_OFFSET=0.25
	export PLOT_BOX_WHISKER_COLUMNS="5:4:8:7"
	export PLOT_X_RANGE="5:1500"
	./plot.sh "$OUT.boxwhiskers" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="5:5:7:7"
	export PLOT_X_RANGE="5:1500"
	./plot.sh "$OUT.boxes" $DATAFILES

fi

