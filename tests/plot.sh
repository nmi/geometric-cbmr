#!/bin/bash
#
# Writes a gnuplot script for a set of data files
#

function print_usage {
	echo ""
	echo "A gnuplot script generator for tables of tab-separated data"
	echo ""
	echo "usage: plot <output.p> [table1.dat] [table2.dat] ..."
	echo ""
	echo "header line for each data file:"
	echo "  # Name of the measured entity"
	echo "Rest of the rows define data points in the format"
	echo "  <x coordinate> <column 1> [column 2] ..."
	echo ""
	echo "Plot title and axis labels are read from environment variables"
	echo "PLOT_TITLE, X_TITLE, Y_TITLE"
	echo ""
}

if [ -z "$GNUPLOT" ]; then
	GNUPLOT=./gnuplot
fi

if [ -z "$LINESTYLE" ]; then
	LINESTYLE="linespoints"
fi

if [ -z "$INDEX_LINESTYLE" ]; then
	INDEX_LINESTYLE="lines lt 2 lw 4"
fi

if [ -z "$PLOT_BOX_OFFSET" ]; then
	PLOT_BOX_OFFSET=0.25
fi

if [ -z "$PLOT_LEGEND" ]; then
	PLOT_LEGEND="below"
fi

if [ -z "$PLOT_LEGEND_WIDTH" ]; then
	PLOT_LEGEND_WIDTH=-3
fi

if [ -z "$PLOT_Y_LABEL_POS" ]; then
	PLOT_Y_LABEL_POS="3,0"
fi

if [ -z "$PLOT_WIDTH" ]; then
	PLOT_WIDTH="0.75"
fi

if [ -z "$PLOT_HEIGHT" ]; then
	PLOT_HEIGHT="1.0"
fi




OUTPUT="$1.p"
PLOT_OUTPUT="$1.eps"

TERMINAL="postscript eps enhanced solid \"Helvetica\" 18"

# PLOT_OUTPUT="$1.png"
# TERMINAL="png"

PLOT_SIZE="$PLOT_WIDTH,$PLOT_HEIGHT"


if [ -z "$COLORS" ]; then
	if [ "$GRAYSCALE" ]; then
		COLORS=(-1 1 2 3 4 5 6 7 8 9 10)
	else
		COLORS=(-1 1 3 4 8 9 2 5 6 7 10)
	fi
else
	COLORS=($COLORS)
fi
if [ -z "$LINEWIDTHS" ]; then
	if [ "$GRAYSCALE" ]; then
		LINEWIDTHS=(2 2 2 2 2 2 2 2 2 3 2)
	else
		LINEWIDTHS=(2 2 2 2 2 3 2 2 2 2 2)
	fi
else
	LINEWIDTHS=($LINEWIDTHS)
fi
if [ -z "$GRAYSCALE" ]; then
	TERMINAL="$TERMINAL color"
fi


if [ -z "$PLOT_BOXPOS" ]; then
	PLOT_BOXPOS=(-0.5 0.0 0.5 -0.75 -0.25 0.25 0.75 -0.5 0.0 -0.5 -0.75 -0.25)
else
	PLOT_BOXPOS=($PLOT_BOXPOS)
fi


# Trims leading and trailing white space from strings
function trim {
	TRIMMED=$(echo "$1" | sed -e 's/^ *//' -e 's/ *$//')
}


# Reads a label from the header line of a data file
function read_header {
	line1=$(head -n 1 "$1")

	IFS="#"
	read tmp HEADER <<< "$line1"
	trim $HEADER
	HEADER=$TRIMMED
}


if [ $# -lt 1 ]; then
	print_usage
	exit
fi



echo "# gnuplot script $OUTPUT" > "$OUTPUT"
echo "reset" >> "$OUTPUT"
echo -e "set term $TERMINAL" >> "$OUTPUT"

echo -e "set output \"$PLOT_OUTPUT\"" >> "$OUTPUT"
echo "set size $PLOT_SIZE" >> "$OUTPUT"
echo "set grid back lc rgb '#BBBBBB'" >> "$OUTPUT"

if [ "$PLOT_X_SCALE" == "log" ]; then
	echo "set logscale x" >> "$OUTPUT"
	PLOT_BOX_OFFSET="$PLOT_BOX_OFFSET*\$$PLOT_X_COLUMN"
fi

if [ "$PLOT_Y_SCALE" == "log" ]; then
	echo "set logscale y" >> "$OUTPUT"
fi

echo "set autoscale" >> "$OUTPUT"

if [ "$PLOT_X_RANGE" ]; then
	echo "set xrange [$PLOT_X_RANGE]" >> "$OUTPUT"
fi

if [ "$PLOT_Y_RANGE" ]; then
	echo "set yrange [$PLOT_Y_RANGE]" >> "$OUTPUT"
fi

if [ "$PLOT_MX_TICS" ]; then
	echo "set mxtics $PLOT_MX_TICKS" >> "$OUTPUT"
fi

if [ "$PLOT_X_TICS" ]; then
	echo "set xtics $PLOT_X_TICS" >> "$OUTPUT"
else
	echo "set xtics auto" >> "$OUTPUT"
fi

if [ "$PLOT_MY_TICS" ]; then
	echo "set mytics $PLOT_MY_TICKS" >> "$OUTPUT"
fi

if [ "$PLOT_Y_TICS" ]; then
	echo "set ytics $PLOT_Y_TICS" >> "$OUTPUT"
else
	echo "set ytics auto" >> "$OUTPUT"
fi

echo "set style fill empty" >> "$OUTPUT"

if [ "$PLOT_TITLE" ]; then
	echo -e "set title \"$PLOT_TITLE\"" >> "$OUTPUT"
fi

if [ "$PLOT_X_LABEL" ]; then
	echo -e "set xlabel \"$PLOT_X_LABEL\"" >> "$OUTPUT"
fi
if [ "$PLOT_Y_LABEL" ]; then
	echo -e "set ylabel \"$PLOT_Y_LABEL\" offset $PLOT_Y_LABEL_POS" >> "$OUTPUT"
fi

echo "set style line 1 lt -1 lw 1" >> "$OUTPUT"

if [ "$PLOT_LEGEND" == "below" ]; then

#echo "set key outside below center Left reverse width 1 height 0.5 box linestyle 1 samplen 2" >> "$OUTPUT"
#echo "set key horiz" >> "$OUTPUT"
echo "set key outside center bottom horizontal Left reverse enhanced width $PLOT_LEGEND_WIDTH box linestyle 1 samplen 2 spacing 1.1" >> "$OUTPUT"

elif [ "$PLOT_LEGEND" == "right" ]; then

echo "set key outside right vertical Left reverse enhanced width $PLOT_LEGEND_WIDTH box linestyle 1 samplen 2 spacing 1.1" >> "$OUTPUT"

elif [ "$PLOT_LEGEND" == "in_lt" ]; then

echo "set key inside left top vertical Left reverse enhanced width $PLOT_LEGEND_WIDTH box linestyle 1 samplen 2 spacing 1.1" >> "$OUTPUT"

elif [ "$PLOT_LEGEND" == "in_rb" ]; then

echo "set key inside right bottom vertical Left reverse enhanced width $PLOT_LEGEND_WIDTH box linestyle 1 samplen 2 spacing 1.1" >> "$OUTPUT"


elif [ "$PLOT_LEGEND" == "off" ]; then

echo "set key off" >> "$OUTPUT"

fi

echo -e "\n$PLOT_EXTRA_SETTINGS\n" >> "$OUTPUT"

if [ -z "$PLOT_COMMAND" ]; then
	echo -n "plot " >> "$OUTPUT"
	firstline=1
	color=0
	boxpos=0
	for file in "$@"; do
		if [ "$file" == "$1" ]; then
			continue
		fi
		read_header "$file"
		i=2

		if [ -z "$firstline" ]; then
			echo -e ", \\" >> "$OUTPUT"
			echo -n "     " >> "$OUTPUT"
		else
			firstline=""
		fi

		if [ "$HEADER" == "Index construction" ]; then
			echo -n -e "\"$file\" u $INDEX_PLOT_X_COLUMN:$INDEX_PLOT_Y_COLUMN t \"$HEADER\" w $INDEX_LINESTYLE" >> "$OUTPUT"
		else
			if [ "$PLOT_BOX_WHISKER_COLUMNS" ]; then
				echo -e "\"$file\" u (\$$PLOT_X_COLUMN+${PLOT_BOXPOS[$boxpos]}*$PLOT_BOX_OFFSET):$PLOT_BOX_WHISKER_COLUMNS w candlesticks notitle lt ${COLORS[$color]}, \\" >> "$OUTPUT"
				echo -n "     " >> "$OUTPUT"
			fi
			echo -n -e "\"$file\" u $PLOT_X_COLUMN:$PLOT_Y_COLUMN t \"$HEADER\" w $LINESTYLE lt ${COLORS[$color]} lw ${LINEWIDTHS[$color]}" >> "$OUTPUT"
		fi
		color=$(($color + 1))
		boxpos=$(($boxpos + 1))
	done
else
	echo -e "$PLOT_COMMAND" >> "$OUTPUT"
fi

$GNUPLOT $OUTPUT

# Enable transparent patterns
sed -e "s/TransparentPatterns false def/TransparentPatterns true def/g" "$PLOT_OUTPUT" > "$PLOT_OUTPUT.transparent"
mv -f "$PLOT_OUTPUT.transparent" "$PLOT_OUTPUT"

