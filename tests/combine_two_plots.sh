#!/bin/bash


function write_key {

if [ "$PLOT_LEGEND" == "below" ]; then
	echo "set key at screen $PLOT_WIDTH, screen $PLOT_LEGEND_POS top center horizontal Left reverse enhanced width -2 box linestyle -1 samplen 2 spacing 1.1" >> "$OUT"
elif [ "$PLOT_LEGEND" == "right" ]; then
	echo "set key at screen ($PLOT_WIDTH*2), screen ($PLOT_HEIGHT*0.5) center vertical left Left reverse enhanced width -3 box linestyle -1 samplen 2 spacing 1.3" >> "$OUT"
fi

}


echo ""
echo "Combining scripts $1 and $2 to $3, plotting to $3.eps"
echo ""

OUT="$3.p"

if [ -z "$GNUPLOT" ]; then
	GNUPLOT="./gnuplot"
fi	

if [ -z "$PLOT_LEGEND" ]; then
	PLOT_LEGEND="below"
fi

if [ -z "$PLOT_WIDTH" ]; then
	PLOT_WIDTH=0.85
fi	

if [ -z "$PLOT_HEIGHT" ]; then
	PLOT_HEIGHT=1.0
fi

if [ -z "$PLOT_LEGEND_SPACE" ]; then
	if [ "$PLOT_LEGEND" == "below" ]; then
		PLOT_LEGEND_SPACE=0.2
	else
		PLOT_LEGEND_SPACE=0.0
	fi
fi

if [ -z "$PLOT_LEGEND_POS" ]; then
	PLOT_LEGEND_POS=0.17
fi

if [ -z "$PLOT_LEGEND_FROM" ]; then
	PLOT_LEGEND_FROM=2
fi

if [ -z "$PLOT_LEGEND_WIDTH" ]; then
	if [ "$PLOT_LEGEND" == "below" ]; then
		PLOT_LEGEND_WIDTH=0.0
	else
		PLOT_LEGEND_WIDTH=0.35
	fi
fi


echo "# gnuplot script that combines plots" > "$OUT"
echo "# $1 and $2" >> "$OUT"
echo "" >> "$OUT"

grep "set term" $1 >> "$OUT"
echo -e "set output \"$3.eps\"" >> "$OUT"
echo "" >> "$OUT"

echo "set size ($PLOT_WIDTH*2+$PLOT_LEGEND_WIDTH),($PLOT_HEIGHT+$PLOT_LEGEND_SPACE)" >> "$OUT"
echo "set multiplot" >> "$OUT"
echo "set origin 0.0,$PLOT_LEGEND_SPACE" >> "$OUT"
echo "" >> "$OUT"

if [ $PLOT_LEGEND_FROM == 1 ]; then
	write_key
else
	echo "unset key" >> "$OUT"
fi
echo "set size $PLOT_WIDTH,$PLOT_HEIGHT" >> "$OUT"
sed -e "s/\(^.*set size .*$\)//;s/\(^.*set key .*$\)//;s/\(^.*reset.*$\)//;s/\(^.*set output.*$\)//;s/\(^.*set term.*$\)//;s/\(^.*set multiplot.*$\)//" $1 >> "$OUT"

echo "" >> "$OUT"
echo "" >> "$OUT"
if [ $PLOT_LEGEND_FROM == 2 ]; then
	write_key
else
	echo "unset key" >> "$OUT"
fi
echo "" >> "$OUT"

echo "set origin $PLOT_WIDTH,$PLOT_LEGEND_SPACE" >> "$OUT"

echo "set size $PLOT_WIDTH,$PLOT_HEIGHT" >> "$OUT"
sed -e "s/\(^.*set size .*$\)//;s/\(^.*set key .*$\)//;s/\(^.*reset.*$\)//;s/\(^.*set output.*$\)//;s/\(^.*set term.*$\)//;s/\(^.*set multiplot.*$\)//" $2 >> "$OUT"
#sed -e "s/\(^.*set key .*$\)//;s/\(^.*reset.*$\)//;s/\(^.*set output.*$\)//;s/\(^.*set term.*$\)//;s/\(^.*set multiplot.*$\)//" $2 >> "$OUT"

echo "" >> "$OUT"

echo "unset multiplot" >> "$OUT"

$GNUPLOT "$OUT"

# Enable transparent patterns
sed -e "s/TransparentPatterns false def/TransparentPatterns true def/g" "$3.eps" > "$3.eps.transparent"
mv -f "$3.eps.transparent" "$3.eps"

