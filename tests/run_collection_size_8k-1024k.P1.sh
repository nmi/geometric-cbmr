#!/bin/bash

TEST_PROGRAM=../test_speed

SONGS="$@"

OUT="collection_size_8k-1024k.P1"

SEED=123

PATTERN_SIZE=64
COLLECTION_SIZES="1024000,512000,256000,128000,64000,32000,16000,8000"

PATTERN_GENERATOR_MAX_SKIP=10
PATTERN_GENERATOR_ERRORS=0.0
PATTERN_GENERATOR_MAX_TRANSPOSITION=32

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

NUM_RESULTS=10


# Algorithm-specific settings

DATAFILES="$OUT.dat $OUT.F0.dat $OUT.F1.dat $OUT.F2.dat $OUT.F3.dat $OUT.FG6.dat $OUT.indexing.dat"


# Run the tests
if [ -z "$NOTEST" ]; then
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--collection-notes $COLLECTION_SIZES \
		--pattern-notes $PATTERN_SIZE \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $PATTERN_GENERATOR_ERRORS \
		--num-results $NUM_RESULTS \
		--shuffle-songs \
		--output-indexing-time "$OUT.indexing.dat" \
		-a P1   -g 20   -R 1   -o "$OUT.dat" \
			-A "P1" \
		-a P1F0 -g 500  -R 10   -o "$OUT.F0.dat" \
			-A "P1/F0" \
		-a P1F1 -g 500  -R 100 -o "$OUT.F1.dat" \
			-A "P1/F1" \
		-a P1F2 -g 500  -R 100 -o "$OUT.F2.dat" \
			-A "P1/F2" \
		-a P1F3 -g 500  -R 100 -o "$OUT.F3.dat"  --p1-samples 50 \
			-A "P1/F3 (50 samples)" \
		-a FG6  -g 10  -R 1   -o "$OUT.FG6.dat" \
			-A "FG6"
#		-a FG7  -g 20   -R 1   -o $OUT.FG7.dat"


fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	#export PLOT_TITLE="Effect of pattern size on search time"
	export PLOT_X_SCALE=log
	export PLOT_Y_SCALE=log
	export PLOT_X_LABEL="Database size (thousands of notes)"
	export PLOT_Y_LABEL="Time (ms)"
	export PLOT_X_COLUMN="1"
	export PLOT_Y_COLUMN="6"
	export INDEX_PLOT_X_COLUMN="1"
	export INDEX_PLOT_Y_COLUMN="4"
	export PLOT_X_TICS='("8" 8000, "16" 16000, "32" 32000, "64" 64000, "128" 128000, "256" 256000, "512" 512000, "1024" 1024000)'

	export PLOT_BOXPOS="-0.25 1.0 0.0 -0.5 0.5 0.25 0.0"
	#export PLOT_BOX_OFFSET=0.25

	export PLOT_X_RANGE="8000:1024000"
	./plot.sh "$OUT" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="5:4:8:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.boxwhiskers" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="5:5:7:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.boxes" $DATAFILES
fi

