#!/bin/bash

TEST_PROGRAM=../test_speed

SONGS="$@"

if [ -z "$NUM_PATTERNS" ]; then
	NUM_PATTERNS=8
fi

OUT="repeated_subpattern.P2"

SEED=123

COLLECTION_SIZES="32768,16384,8192,4096,2048,1024,512,256,128"

PATTERN_GENERATOR_MAX_SKIP=0
PATTERN_GENERATOR_ERRORS=0.0
PATTERN_GENERATOR_MAX_TRANSPOSITION=32

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

NUM_RESULTS=10


DATAFILES="$OUT.dat $OUT.F4.dat $OUT.F5.dat $OUT.F6a.dat $OUT.F6gd.dat $OUT.MSM.dat"


# Run the tests
if [ -z "$NOTEST" ]; then
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--collection-notes $COLLECTION_SIZES \
		--pattern-notes 0 \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $PATTERN_GENERATOR_ERRORS \
		--num-results $NUM_RESULTS \
		--shuffle-songs \
		--join-songs $NUM_PATTERNS \
		-a P2     -g 1 -R 1  -o "$OUT.dat" \
			-A "P2" \
		-a P2F4   -g 1 -R 1  -o "$OUT.F4.dat" \
			-A "P2/F4" \
		-a P2F5   -g 1 -R 1 -o "$OUT.F5.dat" \
			-A "P2/F5" \
		-a P2F6   -g 1 -R 1 -o "$OUT.F6a.dat" --p2-threshold 0.5 \
			-A "P2/F6 (k=m/2)" \
		-a P2F6g  -g 1 -R 1 -o "$OUT.F6gd.dat" --p2-threshold 0.0625 \
			-A "P2/F6 greedy (k=m/16)" \
		-a MSMFFT -g 1 -R 1 -o "$OUT.MSM.dat" \
			-A "MSM (FFT)"

fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	#export PLOT_TITLE="Effect of pattern size on search time"
	export PLOT_WIDTH="1.0"
	export PLOT_X_SCALE="log"
	export PLOT_Y_SCALE="log"
	export PLOT_X_LABEL="Song length (notes)"
	export PLOT_Y_LABEL="Time (ms)"
	export PLOT_X_COLUMN="2"
	export PLOT_Y_COLUMN="(\$6/$NUM_PATTERNS)"
	export PLOT_X_RANGE="128:32768"
	export PLOT_X_TICS="(128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768)"

	export PLOT_BOXPOS="0.0 1.0 0.5 0.0 -0.5 -0.5"

	export PLOT_LEGEND_WIDTH=-3

	./plot.sh "$OUT" $DATAFILES
	export PLOT_BOX_WHISKER_COLUMNS="(\$5/$NUM_PATTERNS):(\$4/$NUM_PATTERNS):(\$8/$NUM_PATTERNS):(\$7/$NUM_PATTERNS)"
	./plot.sh "$OUT.boxwhiskers" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="(\$5/$NUM_PATTERNS):(\$5/$NUM_PATTERNS):(\$7/$NUM_PATTERNS):(\$7/$NUM_PATTERNS)"
	./plot.sh "$OUT.boxes" $DATAFILES
fi

