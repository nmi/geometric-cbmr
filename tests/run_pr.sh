#!/bin/bash

export PLOT_TITLE="(a) err=0.25, noise=16"
ERRORS=0.25 CUTOFF=0.75 ./run_precision_recall.P2.sh $@
export PLOT_TITLE="(b) err=0.5, noise=16"
ERRORS=0.5 CUTOFF=0.5 ./run_precision_recall.P2.sh $@
export PLOT_TITLE="(c) err=0.75, noise=16"
ERRORS=0.75 CUTOFF=0.25 ./run_precision_recall.P2.sh $@

