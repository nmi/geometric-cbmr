#!/bin/bash

NOTEST=1 ALG=P2 ./run_pr.sh

PLOT_LEGEND_FROM=1 PLOT_LEGEND="in-lb" ./combine_three_plots.sh precision_recall.gts_0.75.noise_16.P2.p precision_recall.gts_0.5.noise_16.P2.p precision_recall.gts_0.25.noise_16.P2.p precision_recall.combined.P2
