#!/bin/bash

if [ -z "$ALG" ]; then
	echo ""
	echo "Please specify algorithm with variable ALG. Possible values are:"
	echo "  P2, P2F4, P2F6a, P2F6gd, MSM"
	echo ""
	exit
fi


function run_test() {
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--pattern-notes $PATTERN_SIZE \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $ERRORS \
		--similarity-cutoff $CUTOFF \
		--measurement-points $MEASUREMENT_POINTS \
		--inserted-patterns $INSERTED_PATTERNS \
		--inserted-noise $NOISE \
		--p2-threshold $P2_THRESHOLD \
		-g $NUM_PATTERNS -a $ALG -A "$ALGDESC" -o "$OUTFILE" "$@"
}

TEST_PROGRAM=../test_precision_recall

SONGS="$@"

if [ -z "$ERRORS" ]; then
	ERRORS=0.0
fi

if [ -z "$CUTOFF" ]; then
	CUTOFF=0.5
fi

if [ -z "$NOISE" ]; then
	NOISE=16
fi

OUT="precision_recall.gts_$CUTOFF.noise_$NOISE.P2"

SEED=123

NUM_PATTERNS=25
PATTERN_SIZE=100

PATTERN_GENERATOR_MAX_SKIP=50
PATTERN_GENERATOR_MAX_TRANSPOSITION=32
INSERTED_PATTERNS=25

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

MEASUREMENT_POINTS=11


DATAFILES="$OUT.dat $OUT.F4.dat $OUT.F5.dat $OUT.F6a.dat $OUT.F6gd.dat $OUT.MSM.dat"



# Run the tests
if [ -z "$NOTEST" ]; then
	P2_THRESHOLD=0.5
	if [ "$ALG" == "P2" ]; then
		ALG="P2" ALGDESC="P2" OUTFILE="$OUT.dat" run_test
	elif [ "$ALG" == "P2F4" ]; then
		ALG="P2F4" ALGDESC="P2/F4" OUTFILE="$OUT.F4.dat" run_test
	elif [ "$ALG" == "P2F5" ]; then
		ALG="P2F5" ALGDESC="P2/F5" OUTFILE="$OUT.F5.dat" run_test
	elif [ "$ALG" == "P2F6a" ]; then
		ALG="P2F6" ALGDESC="P2/F6 (k=m/2)" OUTFILE="$OUT.F6a.dat" \
			run_test
	elif [ "$ALG" == "P2F6gd" ]; then
		P2_THRESHOLD=0.0625
		ALG="P2F6g" ALGDESC="P2/F6 greedy (k=m/16)" \
			OUTFILE="$OUT.F6gd.dat" run_test
	elif [ "$ALG" == "MSM" ]; then
		ALG="MSMFFT" ALGDESC="MSM (FFT)" OUTFILE="$OUT.MSM.dat" \
			run_test
	else
		echo ""
		echo "Invalid algorithm: ALG=$ALG"
		echo ""
		exit
	fi
fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	#export PLOT_TITLE="Precision - Recall"
	export PLOT_X_SCALE=lin
	export PLOT_Y_SCALE=lin
	export PLOT_X_LABEL="Recall"
	export PLOT_Y_LABEL="Precision"
	export PLOT_X_COLUMN="1"
	export PLOT_Y_COLUMN="2"
	export PLOT_Y_RANGE="0.0:1.1"
	export PLOT_Y_LABEL_POS="1.5,0"

	./plot.sh "$OUT" $DATAFILES
fi

