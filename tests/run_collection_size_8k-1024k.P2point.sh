#!/bin/bash

TEST_PROGRAM=../test_speed

SONGS="$@"

OUT="collection_size_8k-1024k.P2point"

SEED=123

PATTERN_SIZE=64
COLLECTION_SIZES="1024000,512000,256000,128000,64000,32000,16000,8000"

PATTERN_GENERATOR_MAX_SKIP=10
PATTERN_GENERATOR_ERRORS=0.0
PATTERN_GENERATOR_MAX_TRANSPOSITION=32

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

NUM_RESULTS=10


DATAFILES="$OUT.1.dat $OUT.2.dat $OUT.4.dat $OUT.16.dat $OUT.F7.1.dat $OUT.F7.2.dat $OUT.F7.4.dat $OUT.F7.16.dat $OUT.F6.dat $OUT.indexing.dat"


# Run the tests
if [ -z "$NOTEST" ]; then
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--collection-notes $COLLECTION_SIZES \
		--pattern-notes $PATTERN_SIZE \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $PATTERN_GENERATOR_ERRORS \
		--num-results $NUM_RESULTS \
		--shuffle-songs \
		--output-indexing-time "$OUT.indexing.dat" \
		-a P2p   -g 3  -R 1 -o $OUT.1.dat --p2-fixed-points 1 \
			-A "P2' (1 point)" \
		-a P2p   -g 50 -R 1 -o $OUT.2.dat --p2-fixed-points 2 \
			-A "P2' (2 points)" \
		-a P2p   -g 50 -R 1 -o $OUT.4.dat --p2-fixed-points 4 \
			-A "P2' (4 points)" \
		-a P2p   -g 50 -R 1 -o $OUT.16.dat --p2-fixed-points 16 \
			-A "P2' (16 points)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.1.dat --p2-fixed-points 1 \
			-A "P2'/F7 (1 point)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.2.dat --p2-fixed-points 2 \
			-A "P2'/F7 (2 points)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.4.dat --p2-fixed-points 4 \
			-A "P2'/F7 (4 points)" \
		-a P2pF7 -g 50 -R 1 -o $OUT.F7.16.dat --p2-fixed-points 16 \
			-A "P2'/F7 (16 points)" \
		-a P2F6  -g 50 -R 1 -o $OUT.F6.dat --p2-threshold 0.25 \
			-A "P2/F6 (k=m/4)"

fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	#export PLOT_TITLE="Effect of pattern size on search time"
	export PLOT_X_SCALE=log
	export PLOT_Y_SCALE=log
	export PLOT_X_LABEL="Database size (thousands of notes)"
	export PLOT_Y_LABEL="Time (ms)"
	export PLOT_X_COLUMN="1"
	export PLOT_Y_COLUMN="6"
	export INDEX_PLOT_X_COLUMN="1"
	export INDEX_PLOT_Y_COLUMN="4"
	export PLOT_X_TICS='("8" 8000, "16" 16000, "32" 32000, "64" 64000, "128" 128000, "256" 256000, "512" 512000, "1024" 1024000)'

	export COLORS="1 1 1 1 3 3 3 3 9"
	export LINEWIDTHS="2 2 2 2 2 2 2 2 3"

	export PLOT_X_RANGE="8000:1024000"
	./plot.sh "$OUT" $DATAFILES

	#export PLOT_BOX_OFFSET=0.25
	export PLOT_BOX_WHISKER_COLUMNS="5:4:8:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.boxwhiskers" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="5:5:7:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.boxes" $DATAFILES
fi

