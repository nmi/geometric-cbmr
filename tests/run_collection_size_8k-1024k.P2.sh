#!/bin/bash

TEST_PROGRAM=../test_speed

SONGS="$@"

OUT="collection_size_8k-1024k.P2"

SEED=123

PATTERN_SIZE=64
COLLECTION_SIZES="1024000,512000,256000,128000,64000,32000,16000,8000"

PATTERN_GENERATOR_MAX_SKIP=10
PATTERN_GENERATOR_ERRORS=0.0
PATTERN_GENERATOR_MAX_TRANSPOSITION=32

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

NUM_RESULTS=10


DATAFILES="$OUT.dat $OUT.F4.dat $OUT.F5.dat $OUT.F6a.dat $OUT.F6gd.dat $OUT.MSM.dat $OUT.indexing.dat"

DATAFILES_F6="$OUT.F6a.dat $OUT.F6b.dat $OUT.F6c.dat $OUT.F6d.dat $OUT.F6ga.dat $OUT.F6gd.dat"


# Run the tests
if [ -z "$NOTEST" ]; then
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--collection-notes $COLLECTION_SIZES \
		--pattern-notes $PATTERN_SIZE \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $PATTERN_GENERATOR_ERRORS \
		--num-results $NUM_RESULTS \
		--shuffle-songs \
		--output-indexing-time "$OUT.indexing.dat" \
		-a P2     -g 10  -R 1  -o "$OUT.dat" \
			-A "P2" \
		-a P2F4   -g 100 -R 1  -o "$OUT.F4.dat" \
			-A "P2/F4" \
		-a P2F5   -g 200 -R 1 -o "$OUT.F5.dat" \
			-A "P2/F5" \
		-a P2F6   -g 200 -R 1 -o "$OUT.F6a.dat" --p2-threshold 0.5 \
			-A "P2/F6 (k=m/2)" \
		-a P2F6   -g 200 -R 1 -o "$OUT.F6b.dat" --p2-threshold 0.25 \
			-A "P2/F6 (k=m/4)" \
		-a P2F6  -g 200 -R 1 -o "$OUT.F6c.dat" --p2-threshold 0.125 \
			-A "P2/F6 (k=m/8)" \
		-a P2F6  -g 200 -R 1 -o "$OUT.F6d.dat" --p2-threshold 0.0625 \
			-A "P2/F6 (k=m/16)" \
		-a P2F6g  -g 200 -R 1 -o "$OUT.F6ga.dat" --p2-threshold 0.5 \
			-A "P2/F6 greedy (k=m/2)" \
		-a P2F6g  -g 200 -R 1 -o "$OUT.F6gd.dat" --p2-threshold 0.0625 \
			-A "P2/F6 greedy (k=m/16)" \
		-a MSMFFT -g 5   -R 1 -o "$OUT.MSM.dat" \
			-A "MSM (FFT)"

fi


# Plot the data

if [ -z "$NOPLOT" ]; then
	#export PLOT_TITLE="Effect of pattern size on search time"
	export PLOT_X_SCALE=log
	export PLOT_Y_SCALE=log
	export PLOT_X_LABEL="Database size (thousands of notes)"
	export PLOT_Y_LABEL="Time (ms)"
	export PLOT_X_COLUMN="1"
	export PLOT_Y_COLUMN="6"
	export INDEX_PLOT_X_COLUMN="1"
	export INDEX_PLOT_Y_COLUMN="4"
	export PLOT_X_TICS='("8" 8000, "16" 16000, "32" 32000, "64" 64000, "128" 128000, "256" 256000, "512" 512000, "1024" 1024000)'

	export PLOT_BOXPOS="0.0 1.0 0.5 0.0 -0.5 -0.5"

	export PLOT_X_RANGE="8000:1024000"
	./plot.sh "$OUT" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="5:4:8:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.boxwhiskers" $DATAFILES

	export PLOT_BOX_WHISKER_COLUMNS="5:5:7:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.boxes" $DATAFILES


	export PLOT_BOXPOS="-0.25 -0.75 0.5 0.0 0.25 -0.5"

	export PLOT_BOX_WHISKER_COLUMNS=""
	export PLOT_X_RANGE="8000:1024000"
	./plot.sh "$OUT.F6" $DATAFILES_F6

	export PLOT_BOX_WHISKER_COLUMNS="5:4:8:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.F6.boxwhiskers" $DATAFILES_F6

	export PLOT_BOX_WHISKER_COLUMNS="5:5:7:7"
	export PLOT_X_RANGE="5000:1500000"
	./plot.sh "$OUT.F6.boxes" $DATAFILES_F6
fi

