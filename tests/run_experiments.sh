#!/bin/bash

echo ""
echo "######################################################################"
echo "# Experiment 1/10:   P1 pattern size                                 #"
echo "######################################################################"
echo ""
time PLOT_TITLE="(a) Effect of pattern size on P1 algorithms" \
	PLOT_Y_RANGE="0.001:10000" \
	./run_pattern_size_8-1024.P1.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 2/10:   P2 pattern size                                 #"
echo "######################################################################"
echo ""
time PLOT_TITLE="(a) Effect of pattern size on P2 algorithms" \
	PLOT_Y_RANGE="0.01:1000000" \
	./run_pattern_size_8-1024.P2.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 3/10:   P2' pattern size                                #"
echo "######################################################################"
echo ""
time PLOT_TITLE="(a) Effect of pattern size on P2' algorithms" \
	./run_pattern_size_8-1024.P2point.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 4/10:   P1 database size                                #"
echo "######################################################################"
echo ""
time PLOT_TITLE="(b) Effect of database size on P1 algorithms" \
	PLOT_Y_RANGE="0.001:10000" \
	./run_collection_size_8k-1024k.P1.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 5/10:   P2 database size                                #"
echo "######################################################################"
echo ""
time PLOT_TITLE="(b) Effect of database size on P2 algorithms" \
	PLOT_Y_RANGE="0.01:1000000" \
	./run_collection_size_8k-1024k.P2.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 6/10:   P2' database size                               #"
echo "######################################################################"
echo ""
time PLOT_TITLE="(a) Effect of database size on P2' algorithms" \
	./run_collection_size_8k-1024k.P2point.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 7/10:   Repeated subpatterns                            #"
echo "######################################################################"
echo ""
time PLOT_LEGEND=in_lt ./run_repeated_subpatterns.sh $@

echo ""
echo "######################################################################"
echo "# Experiment 8/10:   Filter time allocation                          #"
echo "######################################################################"
echo ""
time ./run_time_allocation-pattern_size.sh $@
time ./run_time_allocation-collection_size.sh $@


# Combine plots

PLOT_LEGEND="right" \
./combine_two_plots.sh pattern_size_8-1024.P1.p collection_size_8k-1024k.P1.p execution_time.P1

PLOT_LEGEND_WIDTH=0.40 PLOT_LEGEND="right" \
./combine_two_plots.sh pattern_size_8-1024.P2.p collection_size_8k-1024k.P2.p execution_time.P2

./combine_two_plots.sh pattern_size_8-1024.P2.F6.p collection_size_8k-1024k.P2.F6.p execution_time.P2.F6

PLOT_LEGEND_SPACE=0.25 PLOT_LEGEND_POS=0.22 \
./combine_two_plots.sh pattern_size_8-1024.P2point.p collection_size_8k-1024k.P2point.p execution_time.P2point


PLOT_LEGEND="right" \
./combine_two_plots.sh pattern_size_8-1024.P1.boxes.p collection_size_8k-1024k.P1.boxes.p execution_time.P1.boxes

PLOT_LEGEND_WIDTH=0.40 PLOT_LEGEND="right" \
./combine_two_plots.sh pattern_size_8-1024.P2.boxes.p collection_size_8k-1024k.P2.boxes.p execution_time.P2.boxes

./combine_two_plots.sh pattern_size_8-1024.P2.F6.boxes.p collection_size_8k-1024k.P2.F6.boxes.p execution_time.P2.F6.boxes

PLOT_LEGEND_SPACE=0.25 PLOT_LEGEND_POS=0.22 \
./combine_two_plots.sh pattern_size_8-1024.P2point.boxes.p collection_size_8k-1024k.P2point.boxes.p execution_time.P2point.boxes


PLOT_LEGEND="right" \
./combine_two_plots.sh pattern_size_8-1024.P1.boxwhiskers.p collection_size_8k-1024k.P1.boxwhiskers.p execution_time.P1.boxwhiskers

PLOT_LEGEND_WIDTH=0.40 PLOT_LEGEND="right" \
./combine_two_plots.sh pattern_size_8-1024.P2.boxwhiskers.p collection_size_8k-1024k.P2.boxwhiskers.p execution_time.P2.boxwhiskers

./combine_two_plots.sh pattern_size_8-1024.P2.F6.boxwhiskers.p collection_size_8k-1024k.P2.F6.boxwhiskers.p execution_time.P2.F6.boxwhiskers

PLOT_LEGEND_SPACE=0.25 PLOT_LEGEND_POS=0.22 \
./combine_two_plots.sh pattern_size_8-1024.P2point.boxwhiskers.p collection_size_8k-1024k.P2point.boxwhiskers.p execution_time.P2point.boxwhiskers


./combine_two_plots.sh time_allocation-pattern_size_8-1024.P1.F0.p time_allocation-collection_size_8k-1024k.P1.F0.p time_allocation.P1.F0

./combine_two_plots.sh time_allocation-pattern_size_8-1024.P1.F2.p time_allocation-collection_size_8k-1024k.P1.F2.p time_allocation.P1.F2

./combine_two_plots.sh time_allocation-pattern_size_8-1024.P2.F6.p time_allocation-collection_size_8k-1024k.P2.F6.p time_allocation.P2.F6


