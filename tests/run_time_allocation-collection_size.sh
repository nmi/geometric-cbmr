#!/bin/bash

TEST_PROGRAM=../test_speed

SONGS="$@"

OUT="time_allocation-collection_size_8k-1024k"

SEED=123

PATTERN_SIZE=64
COLLECTION_SIZES="1024000,512000,256000,128000,64000,32000,16000,8000"

PATTERN_GENERATOR_MAX_SKIP=10
PATTERN_GENERATOR_ERRORS=0.0
PATTERN_GENERATOR_MAX_TRANSPOSITION=32

C_WINDOW=50
D_WINDOW=5

MAX_VECTOR_WIDTH=100000
MAX_VECTOR_HEIGHT=128

NUM_RESULTS=10


DATAFILES="$OUT.P1.F0 $OUT.P1.F2 $OUT.P2.F6"


# Run the tests
if [ -z "$NOTEST" ]; then
	time $TEST_PROGRAM $SONGS -v --seed $SEED --c-window $C_WINDOW \
		--d-window $D_WINDOW \
		--vector-width $MAX_VECTOR_WIDTH \
		--vector-height $MAX_VECTOR_HEIGHT \
		--collection-notes $COLLECTION_SIZES \
		--pattern-notes $PATTERN_SIZE \
		--pattern-max-skip $PATTERN_GENERATOR_MAX_SKIP \
		--max-transposition $PATTERN_GENERATOR_MAX_TRANSPOSITION \
		--pattern-errors $PATTERN_GENERATOR_ERRORS \
		--num-results $NUM_RESULTS \
		--shuffle-songs \
		--time-allocation \
		-a P1F0 -g 50  -R 1   -o "$OUT.P1.F0.dat" \
			-A "P1/F0" \
		-a P1F2 -g 500  -R 3   -o "$OUT.P1.F2.dat" \
			-A "P1/F2" \
		-a P2F6 -g 50  -R 1   -o "$OUT.P2.F6.dat" \
			-A "P2/F6"
fi


# Plot the data

if [ -z "$NOPLOT" ]; then
for file in $DATAFILES; do
	if [ `expr "$file" : '.*\(.P1.F0\)'` ]; then
		ALG="P1/F0"
		O1="Merging"
	elif [ `expr "$file" : '.*\(.P1.F2\)'` ]; then
		ALG="P1/F2"
		O1="Merging"
	elif [ `expr "$file" : '.*\(.P2.F6\)'` ]; then
		ALG="P2/F6"
		O1="Graph matching"
	else
		ALG="Filter"
		O1="Other"
	fi
	export PLOT_TITLE="(b) $ALG time allocation at different database sizes"
	export PLOT_X_SCALE=log
	export PLOT_Y_SCALE=lin
	export PLOT_X_LABEL="Database size (thousands of notes)"
	export PLOT_Y_LABEL="% of total search time"
	export PLOT_Y_LABEL_POS="2,0"
	export PLOT_X_RANGE="8000:1024000"
	export PLOT_Y_RANGE="0:100"
	export PLOT_X_TICS='("8" 8000, "16" 16000, "32" 32000, "64" 64000, "128" 128000, "256" 256000, "512" 512000, "1024" 1024000)'

	export COLORS="1 8 9 4"

	if [ `expr "$file" : '.*\(.P1.\)'` ]; then
		O1="Merging"
	elif [ `expr "$file" : '.*\(.P2.F6\)'` ]; then
		O1="Graph matching"
	else
		O1="Other"
	fi

	export PLOT_EXTRA_SETTINGS="set style fill transparent solid 0.5 border\nset style data lines\nset grid front lc rgb '#333333'"

	export PLOT_COMMAND="set multiplot\nplot '$file.dat' u 1:(100.0*\$9/\$6):(0.0) w filledcurves fill transparent solid 0.3 lc rgb 'yellow' title 'Index lookups', \
		'' u 1:(100.0*(\$10+\$9)/\$6):(100.0*\$9/\$6) w filledcurves fill solid 0.3 lc rgb 'red' title '$O1', \
		'' u 1:(100.0*(\$11+\$10+\$9)/\$6):(100.0*(\$10+\$9)/\$6) w filledcurves fill solid 0.3 lc rgb 'violet' title 'Verifying matches', \
		'' u 1:(100.0):(100.0*(\$11+\$10+\$9)/\$6) w filledcurves fill solid 0.3 lc rgb 'blue' title 'Other (overhead)', \
		'' u 1:(100.0*\$9/\$6) lt -1 lw 2 notitle, \
		'' u 1:(100.0*(\$10+\$9)/\$6) lt -1 lw 2 notitle, \
		'' u 1:(100.0*(\$11+\$10+\$9)/\$6) lt -1 lw 2 notitle

	set style fill pattern border

	plot '$file.dat' u 1:(100.0*\$9/\$6):(0.0) w filledcurves fill pattern 0 lc rgb 'yellow' title 'Index lookups', \
		'' u 1:(100.0*(\$10+\$9)/\$6):(100.0*\$9/\$6) w filledcurves fill pattern 4 lc rgb 'red' title '$O1', \
		'' u 1:(100.0*(\$11+\$10+\$9)/\$6):(100.0*(\$10+\$9)/\$6) w filledcurves fill pattern 1 lc rgb 'violet' title 'Verifying matches', \
		'' u 1:(100.0):(100.0*(\$11+\$10+\$9)/\$6) w filledcurves fill pattern 2 lc rgb 'blue' title 'Other (overhead)', \
		'' u 1:(100.0*\$9/\$6) lt -1 lw 2 notitle, \
		'' u 1:(100.0*(\$10+\$9)/\$6) lt -1 lw 2 notitle, \
		'' u 1:(100.0*(\$11+\$10+\$9)/\$6) lt -1 lw 2 notitle"

	./plot.sh "$file"
	sed -e "s/TransparentPatterns false def/TransparentPatterns true def/g" "$file.eps" > "$file.transparent.eps"
	mv -f "$file.transparent.eps" "$file.eps"
done
fi

